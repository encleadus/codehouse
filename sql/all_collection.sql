--essexstats queries
	--archive method for essexstats
		--this is for spelling mistakes
Update durs_centroid set job_reference = upper(job_reference);

update durs_centroid set job_reference = replace(job_reference, ' ', '_');

update durs_area set job_reference = replace(job_reference, ' ', '_');


update durs_centroid set job_reference = replace(job_reference, '/', '_');

update durs_area set job_reference = replace(job_reference, '/', '_');

update durs_centroid set job_reference = replace(job_reference, '-', '_');

update durs_area set job_reference = replace(job_reference, '-', '_');

update durs_centroid set job_reference = replace(job_reference, ',', '_');

update durs_area set job_reference = replace(job_reference, ',', '_');

--status change
Update durs_centroid set cat_durs_status = 'ARCHIVING' WHERE t_complition_date < NOW() - INTERVAL '180 days' AND cat_durs_status != 'ARCHIVED';
-- geometria eltolasa
update durs_area
set the_geom = st_translate(durs_area.the_geom,400000,400000)
from durs_centroid bb
WHERE bb.job_reference = upper(durs_area.job_reference) and bb.cat_durs_status = 'ARCHIVING';
--status change
Update durs_centroid set cat_durs_status = 'ARCHIVED' WHERE cat_durs_status = 'ARCHIVING' ;

------------------------------------------------------------------------------------------------------------------------------------------------------
	--Check for duplicates / in durs area
select * from durs_area ou
where (select count(*) from durs_area inr
where inr.job_reference = ou.job_reference) > 1
------------------------------------------------------------------------------------------------------------------------------------------------------

--refresh prepared materialized views, ez vegezte a route calculaciot, auto qct, es a finance calculaciot
REFRESH MATERIALIZED VIEW mateyupi;
REFRESH MATERIALIZED VIEW mateyupi2;
REFRESH MATERIALIZED VIEW mateyupi3;
REFRESH MATERIALIZED VIEW mateyupi4;
REFRESH MATERIALIZED VIEW mateyupi5;
REFRESH MATERIALIZED VIEW mateyupi6;
REFRESH MATERIALIZED VIEW mateinvoice2;
create or replace view yupi as select * from mateyupi;
create or replace view yupi2 as select * from mateyupi2;
create or replace view yupi3 as select * from mateyupi3;
create or replace view yupi4 as select * from mateyupi4;
create or replace view yupi5 as select * from mateyupi5;
create or replace view yupi6 as select * from mateyupi6;
create or replace view invoice2 as select * from mateinvoice2;
------------------------------------------------------------------------------------------------------------------------------------------------------

-- egyszeri munkanak a price lekerdezse (durs_length tartalmazza a berajzolt utat)
SELECT ff.fid, ff.job_reference, ((sum(g.length_m))/250)*57.50 as sum_price FROM durs_area ff
JOIN durs_length g ON st_contains(st_buffer(ff.the_geom,5), g.the_geom)
where ff.job_reference='1580308' GROUP BY ff.fid, ff.job_reference; 
------------------------------------------------------------------------------------------------------------------------------------------------------

--Yippi Qurey csomag
-- 5 meteres buffer a durs area kore
create or replace view yupi as select st_buffer(the_geom,5) as geom, job_reference, fid from durs_area;

--ezzel lehet letenni a upathra torespontokat coordinatakkal, noticehoz (az intervallum allithato)
Create or replace view yippi as
WITH line AS 
    (SELECT
        id,
        (ST_Dump(the_geom)).geom AS geom
    FROM durs_upath),
linemeasure AS
    (SELECT
        ST_AddMeasure(line.geom, 0, ST_Length(line.geom)) AS linem,
        generate_series(0, ST_Length(line.geom)::int, 100) AS intervall
    FROM line),
geometries AS (
    SELECT
        intervall,
        (ST_Dump(ST_GeometryN(ST_LocateAlong(linem, intervall), 1))).geom AS geom 
    FROM linemeasure)

SELECT
    intervall,
    ST_SetSRID(ST_MakePoint(ST_X(geom), ST_Y(geom)), 27700) AS geom
FROM geometries;


Create or replace view yippi2 as SELECT  row_number() over(), intervall, st_y(geom) as northing, st_x(geom) as easting, geom from yippi;
--ezzel lehet letenni az endpoint coordinatakat.
create or replace view yippi3 as select id, st_y(st_endpoint(the_geom)) as northing, st_x(st_endpoint(the_geom)) as easting, st_endpoint(the_geom) from durs_upath;
--egyedi munkara vonatkozo lekerdezes durs upathoz
Create or replace view yippi4 as select yippi2.* from yippi2, yupi where yupi.job_reference = 'LHAR00200_2' AND st_contains(yupi.geom, yippi2.geom);
Create or replace view yippi5 as select yippi3.* from yippi3, yupi where yupi.job_reference = 'LHAR00200_2' AND st_contains(yupi.geom, yippi3.st_endpoint);
------------------------------------------------------------------------------------------------------------------------------------------------------

--yupi query csomag szamlazashoz - el lettek mentve materialized viewkba, mert nagyon lassuak voltak, minden yupi cserelendo mateyupira
--CREATE MATERIALIZED VIEW blabla AS

--5m buffer
create or replace view yupi as select st_buffer(the_geom,5) as geom, job_reference, fid from durs_area;

create or replace view yupi2 as SELECT ff.fid, ff.job_reference, sum(g.length_m) as sum_length FROM yupi ff
JOIN durs_length g ON st_contains(ff.geom, g.the_geom)
GROUP BY ff.fid, ff.job_reference;

create or replace view yupi3 as SELECT st_union(the_geom) as u_geom from durs_length;

create or replace view yupi4 as SELECT ff.fid, ff.job_reference FROM yupi ff
JOIN yupi3 g ON st_disjoint(ff.geom, g.u_geom)
GROUP BY ff.fid, ff.job_reference;

create or replace view yupi5 as SELECT st_union(yupi.geom) AS u_geom FROM yupi;


create or replace view yupi6 as SELECT ff.*
FROM durs_length ff, yupi5
WHERE ST_Crosses(ff.the_geom, yupi5.u_geom);


CREATE or replace VIEW invoice2 AS 
 SELECT cc.cat_id,
    cc.job_reference,
    cc.priority_of_request,
    cc.budget,
    cc.road_name,
    cc.t_requesting_officer,
    cc.t_office,
    cc.auto_durs_renewal,
    cc.cat_durs_status,
    aa.sum_length
   FROM yupi2 aa
     JOIN durs_centroid cc ON (upper(aa.job_reference)= cc.job_reference)
  GROUP BY cc.cat_id, aa.job_reference, aa.sum_length

------------------------------------------------------------------------------------------------------------------------------------------------------
--difference test - no idea
 SELECT ff.fid,
    ff.job_reference,
    st_difference(ff.geom, g.the_geom) AS new_geom
   FROM (yupi ff
     JOIN durs_area g ON ((upper((g.job_reference)::text) = (ff.job_reference)::text)))
  GROUP BY ff.fid, ff.job_reference, g.the_geom, ff.geom;
------------------------------------------------------------------------------------------------------------------------------------------------------
--Triggers
--generate file download link on update check the status COMPLETED

CREATE OR REPLACE FUNCTION "public"."generate_filelink_for_stats"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
IF NEW.cat_durs_status = 'COMPLETED' THEN
  NEW.durs_report :='<a href="http://pas-128durs.co.uk:8081/share/page/search?t='||NEW.job_reference||'" target="_blank">DOWNLOAD</a>';
ELSE
NEW.durs_report :='N/A';
END IF;
RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."generate_filelink_for_stats"() OWNER TO "postgres";
------------------------------------------------------------------------------------------------------------------------------------------------------
--insert point coordinates on new RETURN
CREATE OR REPLACE FUNCTION "public"."insert_coordinates"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
  NEW.t_northing := ST_Y(new.the_geom);
  NEW.t_easting := ST_X(new.the_geom);
  RETURN NEW;
END; $BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."insert_coordinates"() OWNER TO "postgres";

------------------------------------------------------------------------------------------------------------------------------------------------------
--on insert set status to new
CREATE OR REPLACE FUNCTION "public"."insert_new_status"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
    NEW.cat_durs_status := 'NEW';
  RETURN NEW;
END; $BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."insert_new_status"() OWNER TO "postgres";
------------------------------------------------------------------------------------------------------------------------------------------------------
--on insert set TIMESTAMP
CREATE OR REPLACE FUNCTION "public"."insert_tiemestamp_and_ex_date"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
    NEW.t_date_of_request := current_timestamp;
  RETURN NEW;
END; $BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."insert_tiemestamp_and_ex_date"() OWNER TO "postgres";

------------------------------------------------------------------------------------------------------------------------------------------------------
--on insert/update calculate "length"(object)

CREATE OR REPLACE FUNCTION "public"."length_calc"()
  RETURNS "pg_catalog"."trigger" AS $BODY$BEGIN
  NEW.length_m := ST_LENGTH(ST_Transform(NEW.the_geom,27700));
  RETURN NEW;
END;$BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."length_calc"() OWNER TO "postgres";

------------------------------------------------------------------------------------------------------------------------------------------------------
--on insert/update check priority validation es meg meg ki is szamolja a leadasi datumokat
CREATE OR REPLACE FUNCTION "public"."priority_validation_date_calc"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
  
	IF NEW.priority_of_request = 'HIGH' THEN
		NEW.t_priority_valid :='VALID';
		NEW.t_isodow_calc:= count(*) AS count_days_no_weekend FROM generate_series(NEW.t_date_of_request::date, NEW.t_date_of_request::date+1, '1 day') d(the_day) WHERE  extract('ISODOW' FROM the_day) > 5;
		NEW.t_raw_dd := NEW.t_date_of_request::timestamp::date + 2 + NEW.t_isodow_calc;
	ELSEIF NEW.priority_of_request = 'NORMAL' THEN
		NEW.t_priority_valid :='VALID';
		NEW.t_isodow_calc:= count(*) AS count_days_no_weekend FROM generate_series(NEW.t_date_of_request::date, NEW.t_date_of_request::date+9, '1 day') d(the_day) WHERE extract('ISODOW' FROM the_day) > 5;
		NEW.t_raw_dd := NEW.t_date_of_request::timestamp::date + 10 + NEW.t_isodow_calc;
	ELSEIF NEW.priority_of_request = 'LOW' THEN
		NEW.t_priority_valid :='VALID';
		NEW.t_isodow_calc:= count(*) AS count_days_no_weekend FROM generate_series(NEW.t_date_of_request::date, NEW.t_date_of_request::date+19, '1 day') d(the_day) WHERE  extract('ISODOW' FROM the_day) > 5;
		NEW.t_raw_dd := NEW.t_date_of_request::timestamp::date + 20 + NEW.t_isodow_calc;
	ELSEIF NEW.priority_of_request = 'RENEWAL' THEN
		NEW.t_priority_valid :='VALID';
		NEW.t_isodow_calc:= count(*) AS count_days_no_weekend FROM generate_series(NEW.t_date_of_request::date, NEW.t_date_of_request::date+24, '1 day') d(the_day) WHERE  extract('ISODOW' FROM the_day) > 5;
		NEW.t_raw_dd := NEW.t_date_of_request::timestamp::date + 25 + NEW.t_isodow_calc;  
	ELSEIF NEW.priority_of_request = 'STANDARD' THEN
		NEW.t_priority_valid :='VALID';
		NEW.t_isodow_calc:= count(*) AS count_days_no_weekend FROM generate_series(NEW.t_date_of_request::date, NEW.t_date_of_request::date+19, '1 day') d(the_day) WHERE  extract('ISODOW' FROM the_day) > 5;
		NEW.t_raw_dd := NEW.t_date_of_request::timestamp::date + 20 + NEW.t_isodow_calc;
	ELSE
		NEW.t_priority_valid :='INVALID';
		NEW.t_isodow_calc:= count(*) AS count_days_no_weekend FROM generate_series(NEW.t_date_of_request::date, NEW.t_date_of_request::date+0, '1 day') d(the_day) WHERE  extract('ISODOW' FROM the_day) > 5;
		NEW.t_raw_dd := NEW.t_date_of_request::timestamp::date - 10000 + NEW.t_isodow_calc;
END IF;

	if extract('ISODOW' FROM NEW.t_raw_dd) = 6 then
		NEW.t_delivery_date:= NEW.t_raw_dd+2;
		NEW.t_qc_date:= NEW.t_delivery_date-1;
	elseif extract('ISODOW' FROM NEW.t_raw_dd) = 7 then
		NEW.t_delivery_date:= NEW.t_raw_dd+1;
		NEW.t_qc_date:= NEW.t_delivery_date-2;
	elseif extract('ISODOW' FROM NEW.t_raw_dd) = 1 then
	NEW.t_delivery_date:= NEW.t_raw_dd;	
	NEW.t_qc_date:= NEW.t_delivery_date - 3;	
	ELSE
		NEW.t_delivery_date:= NEW.t_raw_dd;
		NEW.t_qc_date:= NEW.t_delivery_date-1;
end if;

if extract('ISODOW' FROM NEW.t_qc_date) = 6 then
		NEW.t_qc_date:= NEW.t_qc_date-1;
	elseif extract('ISODOW' FROM NEW.t_qc_date) = 7 then
		NEW.t_qc_date:= NEW.t_qc_date-2;
	ELSE
		NEW.t_qc_date:= NEW.t_qc_date;
end if;

IF (NEW.t_delivery_date::timestamp::date - current_timestamp::timestamp::date)>0 THEN
	NEW.t_days_left :=  NEW.t_delivery_date::timestamp::date - current_timestamp::timestamp::date;
ELSE
	NEW.t_days_left := 0;
END IF;

if NEW.cat_durs_status= 'COMPLETED' THEN
	NEW.t_missed_deadlines := NEW.t_delivery_date - NEW.t_complition_date;
End if;

RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."priority_validation_date_calc"() OWNER TO "postgres";

------------------------------------------------------------------------------------------------------------------------------------------------------
--on insert/update nagybetusiti a jobreferenciat
CREATE OR REPLACE FUNCTION "public"."upper_area_jobref"()
  RETURNS "pg_catalog"."trigger" AS $BODY$BEGIN
		NEW.job_reference := upper(new.job_reference);
  RETURN NEW;
END;$BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."upper_area_jobref"() OWNER TO "postgres";

------------------------------------------------------------------------------------------------------------------------------------------------------
--on insert/update validalja a usereket es megnezi hogy autorizaltak e priorityt kerni
CREATE OR REPLACE FUNCTION "public"."user_validation_and_prior_auth"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
IF NEW.client_email_address = 'ANDREW.DELLAR@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ANDREW DELLAR';
	NEW.t_officer_username:= 'adellar';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'TKOSA@CAT-SURVEYS.COM' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'TAMAS KOSA';
	NEW.t_officer_username:= 'tkosa';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CODHAM HALL';
	NEW.t_client_team:= 'JANOS.DOBSI@CAT-SURVEYS.COM';
ELSEIF NEW.client_email_address = 'ZNOVAK@CAT-SURVEYS.COM' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ZULEJKA NOVAK';
	NEW.t_officer_username:= 'znovak';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CODHAM HALL';
	NEW.t_client_team:= 'ZNOVAK@CAT-SURVEYS.COM';
ELSEIF NEW.client_email_address = 'ADJELEY.DSANE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ADJELEY DSANE';
	NEW.t_officer_username:= 'adsane';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'ALFIE.HUNTER@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ALFIE HUNTER';
	NEW.t_officer_username:= 'ahunter';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'GRAEME.EDWARDS@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'ANTHONY.SCOFIELD@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ANTHONY SCOFIELD';
	NEW.t_officer_username:= 'ascofield';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'ANDREW.SCULLION@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ANDREW SCULLION';
	NEW.t_officer_username:= 'ascullion';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'ADRIAN.SUMMONS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ADRIAN SUMMONS';
	NEW.t_officer_username:= 'asummons';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'ANDREW.VALE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ANDREW VALE';
	NEW.t_officer_username:= 'avale';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'KEN.KINTREA@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'BRAD.ELLIS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'BRAD ELLIS';
	NEW.t_officer_username:= 'bellis';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'BASIL.OSBORN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'BASIL OSBORN';
	NEW.t_officer_username:= 'bosborn';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.BOWGEN@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'CATHERINE.BOTTOMS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'CATHERINE BOTTOMS';
	NEW.t_officer_username:= 'cbottoms';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'CHRIS.FOX@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'CHRIS FOX';
	NEW.t_officer_username:= 'cfox';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'ANDREW.VALE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'CLINT.NICHOLLS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'CLINT NICHOLLS';
	NEW.t_officer_username:= 'cnicholls';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'CONSTANTINOS.PIPPIAS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'CONSTANTINOS PIPPIAS';
	NEW.t_officer_username:= 'cpippias';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'ANDREW.VALE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'CHRISTIAN.SADLER-SMITH@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'CHRISTIAN SADLER-SMITH';
	NEW.t_officer_username:= 'csadlersmith';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'CLIVE.WOODRUFF@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'CLIVE.WOODRUFF@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'CLIVE WOODRUFF';
	NEW.t_officer_username:= 'cwoodruff';
	NEW.t_officer_role:= 'MANAGER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'CLIVE.WOODRUFF@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'DAVID.BOWGEN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'DAVID BOWGEN';
	NEW.t_officer_username:= 'dbowgen';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'KEN.KINTREA@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'DAVE.CHAPMAN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'DAVE CHAPMAN';
	NEW.t_officer_username:= 'dchapman';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.BOWGEN@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'DEONE.COSTLEY@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'DEONE COSTLEY';
	NEW.t_officer_username:= 'dcostley';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'FADI.JEREIS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'FADI JEREIS';
	NEW.t_officer_username:= 'fjereis';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'FRANCIS.MOLISSO@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'FRANCIS MOLISSO';
	NEW.t_officer_username:= 'fmolisso';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'GARETH.CRUMP@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'GARETH CRUMP';
	NEW.t_officer_username:= 'gcrump';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'GRAEME.EDWARDS@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'GRAEME.EDWARDS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'GRAEME EDWARDS';
	NEW.t_officer_username:= 'gedwards';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'KEN.KINTREA@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'GARY.WITHAM@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'GARY WITHAM';
	NEW.t_officer_username:= 'gwitham';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'CLIVE.WOODRUFF@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'IAN.DUNCAN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'IAN DUNCAN';
	NEW.t_officer_username:= 'iduncan';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'KEN.KINTREA@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JAMES.DAWSON@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JAMES DAWSON';
	NEW.t_officer_username:= 'jdawson';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'IAN.DUNCAN@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JORDAN.HOPPER@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JORDAN HOPPER';
	NEW.t_officer_username:= 'jhopper';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JACK.TAMCKEN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JACK TAMCKEN';
	NEW.t_officer_username:= 'jtamcken';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'GRAEME.EDWARDS@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JAMIE.TWINN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JAMIE TWINN';
	NEW.t_officer_username:= 'jtwinn';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JOHN.WOODHOUSE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JOHN WOODHOUSE';
	NEW.t_officer_username:= 'jwoodhouse';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'ANDREW.VALE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'KELLY.BURNS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'KELLY BURNS';
	NEW.t_officer_username:= 'kburns';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'KEN.KINTREA@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'KEN KINTREA';
	NEW.t_officer_username:= 'kkintrea';
	NEW.t_officer_role:= 'MANAGER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'KEN.KINTREA@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'LEAH.WHITWELL@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'LEAH WHITWELL';
	NEW.t_officer_username:= 'lwhitwell';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'MATTHEW.AGER@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'MATTHEW AGER';
	NEW.t_officer_username:= 'mager';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'ANDREW.VALE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'MOIRA.MASON@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'MOIRA MASON';
	NEW.t_officer_username:= 'mmason';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'PAUL.EASTALL@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'PAUL EASTALL';
	NEW.t_officer_username:= 'peastall';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'GRAEME.EDWARDS@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'PAUL.HAILSTONE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'PAUL HAILSTONE';
	NEW.t_officer_username:= 'phailstone';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'GRAEME.EDWARDS@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'PETER.MILLWARD@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'PETER MILLWARD';
	NEW.t_officer_username:= 'pmillward';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'RICHARD.HOLLIS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'RICHARD HOLLIS';
	NEW.t_officer_username:= 'rhollis';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'CLIVE.WOODRUFF@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'SHAUN.MORGAN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'SHAUN MORGAN';
	NEW.t_officer_username:= 'smorgan';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHILDERDITCH';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'STEVEN.RODZIANKO@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'STEVEN RODZIANKO';
	NEW.t_officer_username:= 'srodzianko';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'GRAEME.EDWARDS@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'SIMON.WALKER@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'SIMON WALKER';
	NEW.t_officer_username:= 'swalker';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'TERESA.MILBOURN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'TERESA MILBOURN';
	NEW.t_officer_username:= 'tmilbourn';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'DAVID GOLLOP';
	NEW.t_officer_username:= 'dgollop';
	NEW.t_officer_role:= 'MANAGER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'MIKE.SHEARCROFT@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'MIKE SHEARCROFT';
	NEW.t_officer_username:= 'mshearcroft';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'PHIL.HOPE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'PHIL HOPE';
	NEW.t_officer_username:= 'phope';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JAMES.LEGGETT@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JAMES LEGGETT';
	NEW.t_officer_username:= 'jleggett';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'LEWIS.PEARMAIN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'LEWIS PEARMAIN';
	NEW.t_officer_username:= 'lpearmain';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'LIAM.NUGENT@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'LIAM NUGENT';
	NEW.t_officer_username:= 'lnugent';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'MATTHEW.WHITLEY@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'MATTHEW WHITLEY';
	NEW.t_officer_username:= 'mwhitley';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'NIGEL.FINCH@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'NIGEL FINCH';
	NEW.t_officer_username:= 'nfinch';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JENNY.JAMES@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JENNY JAMES';
	NEW.t_officer_username:= 'jjames';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'REMOVED';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'IAIN.JONES@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'IAIN JONES';
	NEW.t_officer_username:= 'ijones';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'KAREN.JENKINS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'KAREN JENKINS';
	NEW.t_officer_username:= 'kjenkins';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'PAUL.CANSDALE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'PAUL CANSDALE';
	NEW.t_officer_username:= 'pcansdale';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'KWABENA.ADU-GYAMFI@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'KWABENA ADU-GYAMFI';
	NEW.t_officer_username:= 'kadugyamfi';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'MARC.PARSONSON@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'MARC PARSONSON';
	NEW.t_officer_username:= 'mparsonson';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JASON.MCCLOUD@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JASON MCCLOUD';
	NEW.t_officer_username:= 'jmccloud';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'PAUL.NORRIS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'PAUL NORRIS';
	NEW.t_officer_username:= 'pnorris';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'SPRINGFIELD';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JAN.WILLIS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JAN WILLIS';
	NEW.t_officer_username:= 'jwillis';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JOE.HAZELTON2@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JOE HAZELTON';
	NEW.t_officer_username:= 'jhazelton';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COLCHESTER';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JANOS.DOBSI@CAT-SURVEYS.COM' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JANOS DOBSI';
	NEW.t_officer_username:= 'JANOS.DOBSI';
	NEW.t_officer_role:= 'MANAGER';
	NEW.t_office:= 'BRENTWOOD';
	NEW.t_client_team:= 'TEAM0';
ELSEIF NEW.client_email_address = 'KEVIN.AYEN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'KEVIN AYEN';
	NEW.t_officer_username:= 'kayen';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'COUNTY HALL';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'GRAHAME.WICKENDEN' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'PETER MILES';
	NEW.t_officer_username:= 'pmiles';
	NEW.t_officer_role:= 'MANAGER';
	NEW.t_office:= 'CHELMSFORD';
	NEW.t_client_team:= 'GRAHAME.WICKENDEN';
ELSEIF NEW.client_email_address = 'GORDON.WYPER@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'GORDON WYPER';
	NEW.t_officer_username:= 'gwyper';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHELMSFORD';
	NEW.t_client_team:= 'GRAHAME.WICKENDEN';
ELSEIF NEW.client_email_address = 'KOULA.VALSAMIS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'KOULA VALSAMIS';
	NEW.t_officer_username:= 'kvalsamis';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHELMSFORD';
	NEW.t_client_team:= 'GRAHAME.WICKENDEN';
ELSEIF NEW.client_email_address = 'DEBENDRA.DAHAL@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'DEBENDRA DAHAL';
	NEW.t_officer_username:= 'ddahal';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHELMSFORD';
	NEW.t_client_team:= 'GRAHAME.WICKENDEN';
ELSEIF NEW.client_email_address = 'SAADAT.CHAUDHURY@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'SAADAT CHAUDHURY';
	NEW.t_officer_username:= 'schaudhury';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHELMSFORD';
	NEW.t_client_team:= 'GRAHAME.WICKENDEN';
ELSEIF NEW.client_email_address = 'NAGINA.BAWAR@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'NAGINA BAWAR';
	NEW.t_officer_username:= 'nbawar';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHELMSFORD';
	NEW.t_client_team:= 'GRAHAME.WICKENDEN';
ELSEIF NEW.client_email_address = 'NIV.THARUMARAJAH@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'NIV THARUMARAJAH';
	NEW.t_officer_username:= 'ntharumarajah';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'CHELMSFORD';
	NEW.t_client_team:= 'GRAHAME.WICKENDEN';
ELSEIF NEW.client_email_address = 'ANDREW.COLE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ANDREW COLE';
	NEW.t_officer_username:= 'acole';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'REMOVED';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'TREVOR.TUCK@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'TREVOR TUCK';
	NEW.t_officer_username:= 'ttuck';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'REMOVED';
	NEW.t_client_team:= 'KEN.KINTREA@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'GEOFF.YORK@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'GEOFF YORK';
	NEW.t_officer_username:= 'gyork';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'ANDREW.VALE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'ATIH.RAJA@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ATIH RAJA';
	NEW.t_officer_username:= 'araja';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'GRAHAME.WICKENDEN';
ELSEIF NEW.client_email_address = 'RICHARD.AVEYARD@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'RICHARD AVEYARD';
	NEW.t_officer_username:= 'raveyard';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'STUART LIVINGSTONE';
	NEW.t_officer_username:= 'SLIVINGSTONE';
	NEW.t_officer_role:= 'MANAGER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'AARON.SMITH@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'AARON SMITH';
	NEW.t_officer_username:= 'ASMITH';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.BOWGEN@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'WILL.JUNGELING@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'WILL JUNGELING';
	NEW.t_officer_username:= 'WJUNGELING';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.BOWGEN@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'DAVID.CHAPMAN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'DAVID CHAPMAN';
	NEW.t_officer_username:= 'DCHAPMAN';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'KEN.KINTREA@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'MELISSA.JACKSON@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'MELISSA JACKSON';
	NEW.t_officer_username:= 'MJACKSON';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'CHRISTINA.EDWARDS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'CHRISTINA EDWARDS';
	NEW.t_officer_username:= 'CEDWARDS';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'ANDREW.VALE@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'VASILEIOS.PAPADIMAS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'VASILEIOS PAPADIMAS';
	NEW.t_officer_username:= 'VPAPADIMAS';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'CLIVE.WOODRUFF@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'MAYOORUN.P@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'MAYOORUN PARASURAMAN';
	NEW.t_officer_username:= 'MPARASURAMAN';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'CLIVE.WOODRUFF@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'JOSHUA.DAVIS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JOSHUA DAVIS';
	NEW.t_officer_username:= 'JDAVIS';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'JAMES.O''REGAN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JAMES OREGAN';
	NEW.t_officer_username:= 'JOREGAN';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'JOHN.GOODYEAR@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JOHN GOODYEAR';
	NEW.t_officer_username:= 'JGOODYEAR';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'TIFFANY.SMITH@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'TIFFANY SMITH';
	NEW.t_officer_username:= 'TSMITH';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'ADRIAN.DAPLYN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ADRIAN DAPLYN';
	NEW.t_officer_username:= 'ADAPLYN';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'CLIVE.WOODRUFF@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'HOLLY.TREND@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'HOLLY TREND';
	NEW.t_officer_username:= 'HTREND';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'RUSSELL.BURCH@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'RUSSELL BURCH';
	NEW.t_officer_username:= 'RBURCH';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'GRAEME.EDWARDS@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'DANIEL.GONZALEZ@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'DANIEL GONZALEZ';
	NEW.t_officer_username:= 'DGONZALEZ';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG'; 
ELSEIF NEW.client_email_address = 'ALBERTO.FERNANDEZ@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ALBERTO FERNANDEZ';
	NEW.t_officer_username:= 'AFERNANDEZ';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'DAN.TURNER@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'DAN TURNER';
	NEW.t_officer_username:= 'DTURNER';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.FORKIN@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'PETER.ROSE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'PETER ROSE';
	NEW.t_officer_username:= 'PROSE';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.FORKIN@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'SEAN.PURCELL@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'SEAN PURCELL';
	NEW.t_officer_username:= 'SPURCELL';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.FORKIN@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'GRAHAME.WICKENDEN@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'GRAHAME WICKENDEN';
	NEW.t_officer_username:= 'GWICKENDEN';
	NEW.t_officer_role:= 'MANAGER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'GRAHAME.WICKENDEN@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'DINAH.MALONE@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'DINAH MALONE';
	NEW.t_officer_username:= 'DMALONE';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'IAN.DUNCAN@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'HOSEAH.GIKUNGU@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'HOSEAH GIKUNGU';
	NEW.t_officer_username:= 'HGIKUNGU';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'GRAEME.EDWARDS@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'MARIGOULA.ZIKOU@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'MARIGOULA ZIKOU';
	NEW.t_officer_username:= 'MZIKOU';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'HARRISON.COOPER@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'HARRISON COOPER';
	NEW.t_officer_username:= 'HCOOPER';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'JENNIFER.GUDKA@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'JENNIFER GUDKA';
	NEW.t_officer_username:= 'JGUDKA';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'CLAIRE.WILLIAMS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'CLAIRE WILLIAMS';
	NEW.t_officer_username:= 'CWILLIAMS';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'STUART.LIVINGSTONE@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'ANTHONY.HICKS@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ANTHONY HICKS';
	NEW.t_officer_username:= 'ahicks';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSEIF NEW.client_email_address = 'ANDREW.BILBY@ESSEXHIGHWAYS.ORG' THEN
  NEW.t_client_email_valid :='VALID';
	NEW.t_requesting_officer:= 'ANDREW BILBY';
	NEW.t_officer_username:= 'abilby';
	NEW.t_officer_role:= 'USER';
	NEW.t_office:= 'N/A';
	NEW.t_client_team:= 'DAVID.GOLLOP@ESSEXHIGHWAYS.ORG';
ELSE

  NEW.t_client_email_valid :='INVALID';
	NEW.t_requesting_officer:= 'INVALID';
	NEW.t_officer_username:= 'INVALID';
	NEW.t_officer_role:= 'INVALID';
	NEW.t_office:= 'INVALID';
	NEW.t_client_team:= 'INVALID';
END IF;
IF ((NEW.t_officer_role = 'USER') AND (NEW.priority_of_request = 'HIGH'))THEN
  NEW.t_priority_auth :='NOT AUTHORIZED';
ELSEIF NEW.t_officer_role = 'INVALID' THEN
NEW.t_priority_auth :='NOT AUTHORIZED';
ELSEIF NEW.t_priority_valid = 'INVALID' THEN
NEW.t_priority_auth :='NOT AUTHORIZED';
ELSE
  NEW.t_priority_auth :='AUTHORIZED';
END IF;
IF ((NEW.t_officer_role = 'USER') AND (NEW.auto_durs_renewal = 'YES'))THEN
  NEW.t_renewal_auth :='NOT AUTHORIZED';
ELSEIF NEW.t_officer_role = 'INVALID' THEN
NEW.t_renewal_auth :='NOT AUTHORIZED';
ELSE
  NEW.t_renewal_auth :='AUTHORIZED';
END IF;
IF length(NEW.requisition_number) < 3 THEN
	NEW.t_requisition_valid := 'INVALID';
ELSEIF
	NEW.requisition_number is null THEN
	NEW.t_requisition_valid := 'INVALID';
ELSE
	NEW.t_requisition_valid := 'VALID';
END IF;
IF length(NEW.qs_name) < 3 THEN
	NEW.t_qs_name_valid := 'INVALID';
ELSEIF
	NEW.qs_name is null THEN
	NEW.t_qs_name_valid := 'INVALID';
ELSE
	NEW.t_qs_name_valid := 'VALID';
END IF;
RETURN NEW;
END; $BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."user_validation_and_prior_auth"() OWNER TO "postgres";

------------------------------------------------------------------------------------------------------------------------------------------------------
--on insert/update manager approvalt highlightolja
CREATE OR REPLACE FUNCTION "public"."x_manager_approval"()
  RETURNS "pg_catalog"."trigger" AS $BODY$BEGIN

if NEW.notes = 'map' then
NEW.t_priority_auth :='AUTHORIZED';
elseif NEW.notes = 'mar' then
NEW.t_renewal_auth :='AUTHORIZED';
end if;
RETURN NEW;
END;
$BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."x_manager_approval"() OWNER TO "postgres";

------------------------------------------------------------------------------------------------------------------------------------------------------
--on insert/update expiry datet szamolja
CREATE OR REPLACE FUNCTION "public"."z_ex_date"()
  RETURNS "pg_catalog"."trigger" AS $BODY$
BEGIN
		NEW.t_rjecc_expiry_date := (new.t_delivery_date::timestamp::date)+ 90;
  RETURN NEW;
END;$BODY$
  LANGUAGE 'plpgsql' VOLATILE COST 100
;

ALTER FUNCTION "public"."z_ex_date"() OWNER TO "postgres";
