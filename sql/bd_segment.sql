create or replace view bd_segment as SELECT
      ST_PointN(geom, generate_series(1, ST_NPoints(geom)-1)) as sp,
      ST_PointN(geom, generate_series(2, ST_NPoints(geom)  )) as ep
    FROM
       -- extract the individual linestrings
 (SELECT (ST_Dump(ST_Boundary(the_geom))).geom
       FROM bd
       ) AS linestrings;

create or replace view bd_segment_geom as SELECT sp, ep, st_makeline(sp, ep) from bd_segment;

create or replace view bd_segment_id as SELECT bd.gid, row_number() over (order by bd.gid), degrees(st_azimuth(ff.sp, ff.ep)) as az_deg, ST_LENGTH(ff.st_makeline) , ff.st_makeline FROM bd_segment_geom ff
JOIN bd ON st_touches(ff.st_makeline, bd.the_geom)
GROUP BY bd.gid, ff.sp, ff.ep, ff.st_makeline;
