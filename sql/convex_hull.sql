create or replace view southend_pd_pts as select southend_bd.gid, southend_bd.buildheight as height, (st_dumppoints(southend_bd.the_geom)).geom as geom from southend_bd;

create or replace view southend_pd_pts1 as select row_number() over () as row_number, southend_pd_pts.gid, southend_pd_pts.height,  southend_pd_pts.geom  from southend_pd_pts;

create or replace view southend_pd_pts2 as select row_number() over () as row_number, southend_pd_pts.gid, southend_pd_pts.height, st_setsrid(st_makepoint((st_x(southend_pd_pts.geom) + ((southend_pd_pts.height * sin(173.67)) / tan(15.03))), (st_y(southend_pd_pts.geom) + ((southend_pd_pts.height * cos(173.67)) / tan(15.03)))), 27700) as geom  from southend_pd_pts;

create or replace view southend_pts_uninon as select * from southend_pd_pts1 union select * from southend_pd_pts2;

create or replace view hulla as SELECT d.gid, d.height,
	ST_ConvexHull(ST_Collect(d.geom)) As the_geom
	FROM southend_pts_uninon As d
	GROUP BY d.gid, d.height;
