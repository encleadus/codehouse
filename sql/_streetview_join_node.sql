﻿-- Function: _streetview_join_node(text, integer[], integer[], integer, boolean, integer, double precision, double precision, double precision, double precision, double precision)

-- DROP FUNCTION _streetview_join_node(text, integer[], integer[], integer, boolean, integer, double precision, double precision, double precision, double precision, double precision);

CREATE OR REPLACE FUNCTION _streetview_join_node(IN network text, IN path_arr integer[], IN phids integer[], IN ipid integer, IN pos boolean, IN srid integer, IN min_radius double precision, IN max_radius double precision, IN weight_az double precision, IN weight_dist double precision, IN weight_pid double precision, OUT onetwork text, OUT opath_arr integer[], OUT ophids integer[], OUT ot boolean)
  RETURNS record AS
$BODY$
DECLARE
	phid1 int; -- Photon node what we looking for
	phid2 int; -- Previous node in path
	phid3 int; -- One before previous node in path, if exists
	phid4 int; -- Two before previous node in path, if exists
BEGIN
	-- Set default status for returning value
	ot := true;
	-- If there is an existing line in current path, so we can handle azimuth check
	IF array_upper(path_arr, 1) >= 2 THEN
		-- Whether connecting line search is at the begining of the path, or at the end
		IF pos THEN -- begining
			phid2 := path_arr[1]; -- Note: reversed order!
			phid3 := path_arr[2];
			IF array_upper(path_arr, 1) >= 3 THEN
				phid4 := path_arr[3];
			ELSE
				phid4 := 0;
			END IF;
		ELSE -- end
			phid2 := path_arr[array_upper(path_arr, 1)];
			phid3 := path_arr[array_upper(path_arr, 1)-1];
			IF array_upper(path_arr, 1) >= 3 THEN
				phid4 := path_arr[array_upper(path_arr, 1)-2];
			ELSE
				phid4 := 0;
			END IF;
		END IF;
		phid1 := 
			p1.phid
			FROM
			photos AS p1,
			photos AS p2,
			photos AS p3
			WHERE
			p1.phid != phid2 AND
			p1.phid != phid3 AND
			p1.phid != phid4 AND
			p2.phid = phid2 AND
			p3.phid = phid3 AND
			p1.pid = ipid AND
			ST_DWithin(ST_Transform(p1.coord, srid), ST_Transform(p2.coord, srid), max_radius) AND
			NOT ST_DWithin(ST_Transform(p1.coord, srid), ST_Transform(p2.coord, srid), min_radius) AND
			NOT network LIKE '%' || phid2::varchar || ',' || p1.phid::varchar || '%' AND NOT network LIKE '%' || p1.phid::varchar || ',' || phid2::varchar || '%' AND
			network !~ (phid2::varchar || ',[0-9]+,' || p1.phid::varchar) AND network !~ (p1.phid::varchar || ',[0-9]+,' || phid2::varchar)
			ORDER BY
			weight_az/DEGREES(ABS(ST_Azimuth(p1.coord, p2.coord)-ST_Azimuth(p2.coord, p3.coord))) + 
			weight_dist/max_radius/(ST_Transform(p1.coord, srid) <-> ST_Transform(p2.coord, srid)) + 
			weight_pid/ABS(phid2-p1.phid) DESC
			LIMIT 1;
			--RAISE WARNING '%', phid3::varchar || ',[0-9]+,' || phid1::varchar;
		-- IF connected node is detected
		IF phid1 IS NOT NULL THEN
			-- Whether connecting line search is at the begining of the path, or at the end
			IF pos THEN -- begining
				-- Add node at the begining of current path array
				path_arr := phid1 || path_arr;
				-- Add node at the begining of current network string
				network := replace(network, array_to_string(path_arr[2:array_upper(path_arr, 1)], ','), array_to_string(path_arr, ','));
			ELSE -- end
				-- Add node at the end of current path array
				path_arr := path_arr || phid1;
				-- Add node at the end of current network string
				network := network || ',' || phid1::varchar;
			END IF;
			-- Remove node from unprocessed nodes
			phids := ARRAY(SELECT t FROM (SELECT unnest(phids) AS t) AS tt WHERE t != phid1);
			-- Update status
			ot := false;
		END IF;
	-- A current path has only one node, no azimuth, and no path string exists (old path may exists, be carefull!)
	-- This means a path has be just started, old one should be saved!!!
	ELSIF array_upper(path_arr, 1) = 1 THEN
		-- Previous node
		phid2 := path_arr[1];
		phid1 := 
			p1.phid
			FROM
			photos AS p1,
			photos AS p2
			WHERE
			p1.phid != phid2 AND
			p2.phid = phid2 AND
			p1.pid = ipid AND
			ST_DWithin(ST_Transform(p1.coord, srid), ST_Transform(p2.coord, srid), max_radius) AND
			NOT ST_DWithin(ST_Transform(p1.coord, srid), ST_Transform(p2.coord, srid), min_radius) AND
			NOT network LIKE '%' || phid2::varchar || ',' || p1.phid::varchar || '%' AND NOT network LIKE '%' || p1.phid::varchar || ',' || phid2::varchar || '%' AND
			network !~ (phid2::varchar || ',[0-9]+,' || p1.phid::varchar) AND network !~ (p1.phid::varchar || ',[0-9]+,' || phid2::varchar)
			ORDER BY
			weight_dist/max_radius/(ST_Transform(p1.coord, srid) <-> ST_Transform(p2.coord, srid)) + 
			weight_pid/ABS(phid2-p1.phid) DESC
			LIMIT 1;
		-- IF connected node is detected
		IF phid1 IS NOT NULL THEN
			-- Add detected node to network string
			network := network || ',' || phid1::varchar;
			-- Add detected node to path array
			path_arr := path_arr || phid1;
			-- Remove node from unprocessed nodes
			phids := ARRAY(SELECT t FROM (SELECT unnest(phids) AS t) AS tt WHERE t != phid1);
			-- Update status
			ot := false;
		END IF;
	END IF;
	-- Prepare return values
	onetwork := network;
	opath_arr := path_arr;
	ophids := phids;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _streetview_join_node(text, integer[], integer[], integer, boolean, integer, double precision, double precision, double precision, double precision, double precision)
  OWNER TO catsurvey;
