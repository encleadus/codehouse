
SELECT 
    cat_projects_polygon.reference_number, osbg1kmgrid.plan_no
FROM osbg1kmgrid, cat_projects_polygon
WHERE ST_Intersects(osbg1kmgrid.geom, st_transform(cat_projects_polygon.the_geom,27700)) 
GROUP BY cat_projects_polygon.reference_number, osbg1kmgrid.plan_no;

