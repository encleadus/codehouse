﻿-- Function: _streetview_first_node(text, integer[])

-- DROP FUNCTION _streetview_first_node(text, integer[]);

CREATE OR REPLACE FUNCTION _streetview_first_node(IN network text, IN phids integer[], OUT onetwork text, OUT opath_arr integer[], OUT ophids integer[])
  RETURNS record AS
$BODY$
BEGIN
	-- Check if there is an unprocessed photo node
	IF array_upper(phids, 1) >= 1 THEN
		-- Start a new path array
		opath_arr := ARRAY[phids[1]];
		-- Add point to network string
		IF length(network) > 0 THEN -- Concat only if it already has a node from previous line
			network := network || ',0,' || phids[1]::varchar;
		ELSE -- Else just add the value
			network := phids[1]::varchar;
		END IF;
		-- Check wether multiple photo nodes is to be preocess
		IF array_upper(phids, 1) >= 2 THEN
			-- Remove first node from unprocessed list
			phids := phids[2:array_upper(phids, 1)];
		ELSE
			-- Make unprocessed list NULL
			phids := '{}'::int[];
		END IF;
	END IF;
	-- Prepare return values
	onetwork := network;
	ophids := phids;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _streetview_first_node(text, integer[])
  OWNER TO catsurvey;
