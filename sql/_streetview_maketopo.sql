﻿-- Function: _streetview_maketopo(integer, integer, double precision, double precision, double precision, double precision, double precision, double precision)

-- DROP FUNCTION _streetview_maketopo(integer, integer, double precision, double precision, double precision, double precision, double precision, double precision);

CREATE OR REPLACE FUNCTION _streetview_maketopo(IN ipid integer, IN srid integer, IN min_radius double precision, IN max_radius double precision, IN weight_az double precision, IN weight_dist double precision, IN weight_pid double precision, IN link_radius double precision, OUT onetwork text, OUT olinks text)
  RETURNS record AS
$BODY$
DECLARE
	phids int[]; -- Unprocessed photo ids
	path_arr int[] := '{}'::int[]; -- Current path ids
	network text;
	t bool; -- Variable for status check
BEGIN
	-- Collect photo list for processing
	phids := ARRAY(SELECT phid FROM photos WHERE pid = ipid ORDER BY phid);
	-- Detect first photo (this is just for optimization)
	SELECT * FROM _streetview_first_node(network, phids) INTO network, path_arr, phids;

	-- Loop till all photo node will be processed
	WHILE array_upper(phids, 1)>0 LOOP
		-- Detect connecting line at the end
		SELECT * FROM _streetview_join_node(network, path_arr, phids, ipid, FALSE, srid, min_radius, max_radius, weight_az, weight_dist, weight_pid) INTO network, path_arr, phids, t;
		-- If no connecting line was detected, start a new path
		IF t THEN
			-- Before starting a new lin, old one's first connecting line should be detected
			SELECT * FROM _streetview_join_node(network, path_arr, phids, ipid, TRUE, srid, min_radius, max_radius, weight_az, weight_dist, weight_pid) INTO network, path_arr, phids, t;
			-- Start te new path
			SELECT * FROM _streetview_first_node(network, phids) INTO network, path_arr, phids;
		END IF;
	END LOOP;

	-- Find last connecting line if it connects to an other line
	SELECT * FROM _streetview_join_node(network, path_arr, phids, ipid, FALSE, srid, min_radius, max_radius, weight_az, weight_dist, weight_pid) INTO network, path_arr, phids, t;
	-- Find first connecting line if it connects to an other line
	SELECT * FROM _streetview_join_node(network, path_arr, phids, ipid, TRUE, srid, min_radius, max_radius, weight_az, weight_dist, weight_pid) INTO network, path_arr, phids, t;
	-- Find additional links
	olinks := string_agg(p1.phid::varchar || '-' || p2.phid::varchar, ',') FROM photos AS p1, photos AS p2 WHERE p1.pid = ipid AND p2.pid = ipid AND p1.phid<p2.phid AND NOT network LIKE ('%' || p1.phid || ',' || p2.phid || '%') AND NOT network LIKE ('%' || p2.phid || ',' || p1.phid || '%') AND ST_DWithin(ST_Transform(p1.coord, 27700), ST_Transform(p2.coord, 27700), link_radius);
	-- Prepare return value
	onetwork := network;
END;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;
ALTER FUNCTION _streetview_maketopo(integer, integer, double precision, double precision, double precision, double precision, double precision, double precision)
  OWNER TO catsurvey;
