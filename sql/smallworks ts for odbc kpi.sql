SELECT
"public".redmine_timesheet."Project",
"public".redmine_timesheet."Task",
"public".redmine_timesheet."Date",
"public".redmine_timesheet."User",
"public".redmine_timesheet."Activity",
"public".redmine_timesheet."Comment",
"public".redmine_timesheet."Spent time",
"public".redmine_timesheet."Hours Type",
"public".redmine_timesheet."Car Reg. Number (Site)",
"public".redmine_timesheet."Service Code",
"public".redmine_timesheet."Tracker",
"public".redmine_timesheet."Created",
"public".redmine_timesheet."Task ID ts"
FROM
"public".redmine_timesheet
WHERE
"public".redmine_timesheet."Project" = '.Small Works' OR
"public".redmine_timesheet."Service Code" = 'DURS'
ORDER BY
"public".redmine_timesheet."Date" ASC
