CREATE TABLE "public"."NewTable" (
"job_reference" varchar(30) COLLATE "default" NOT NULL,
"cat_durs_status" varchar(255) COLLATE "default",
"date_of_request" date NOT NULL,
"your_email_address" varchar(100) COLLATE "default" NOT NULL,
"your_name" varchar(100) COLLATE "default" NOT NULL,
"priority_of_request" varchar(20) COLLATE "default" NOT NULL,
"sse_po_number" varchar(20) COLLATE "default" NOT NULL,
"sse_job_title" varchar(100) COLLATE "default" NOT NULL,
"delivery_date" date NOT NULL,
"expiry_date" date NOT NULL,
"renewal" varchar(20) COLLATE "default" NOT NULL,
"how_many_times" int2,
"first_renewal_delivery_date" date NOT NULL,
"second_renewal_delivery_date" date NOT NULL,
"number_of_units" int2,
"price" int4,
"confirmation_map" varchar(1000) COLLATE "default" NOT NULL,
"area_geom" text COLLATE "default" NOT NULL,
"handoverlink" text COLLATE "default" NOT NULL,
"kml_link" text COLLATE "default" NOT NULL,
"client_company_shortcode" varchar(20) COLLATE "default" NOT NULL,

"road_name" varchar(80) COLLATE "default",
"town" varchar(40) COLLATE "default",
"post_code" varchar(10) COLLATE "default",
"easting" numeric,
"northing" numeric,
"lat" numeric,
"lon" numeric,
"cat_user" varchar(255) COLLATE "default",
"cat_user_email" varchar(255) COLLATE "default",

"fid" int4 DEFAULT nextval('zulu'::regclass) NOT NULL,

"com1" varchar(150) COLLATE "default",
"cav1" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor1" date,
"cem1" varchar(150) COLLATE "default",
"com2" varchar(150) COLLATE "default",
"cav2" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor2" date,
"cem2" varchar(150) COLLATE "default",
"com3" varchar(150) COLLATE "default",
"cav3" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor3" date,
"cem3" varchar(150) COLLATE "default",
"com4" varchar(150) COLLATE "default",
"cav4" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor4" date,
"cem4" varchar(150) COLLATE "default",
"com5" varchar(150) COLLATE "default",
"cav5" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor5" date,
"cem5" varchar(150) COLLATE "default",
"com6" varchar(150) COLLATE "default",
"cav6" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor6" date,
"cem6" varchar(150) COLLATE "default",
"com7" varchar(150) COLLATE "default",
"cav7" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor7" date,
"cem7" varchar(150) COLLATE "default",
"com8" varchar(150) COLLATE "default",
"cav8" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor8" date,
"cem8" varchar(150) COLLATE "default",
"com9" varchar(150) COLLATE "default",
"cav9" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor9" date,
"cem9" varchar(150) COLLATE "default",
"com10" varchar(150) COLLATE "default",
"cav10" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor10" date,
"cem10" varchar(150) COLLATE "default",
"com11" varchar(150) COLLATE "default",
"cav11" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor11" date,
"cem11" varchar(150) COLLATE "default",
"com12" varchar(150) COLLATE "default",
"cav12" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor12" date,
"cem12" varchar(150) COLLATE "default",
"com13" varchar(150) COLLATE "default",
"cav13" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor13" date,
"cem13" varchar(150) COLLATE "default",
"com14" varchar(150) COLLATE "default",
"cav14" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor14" date,
"cem14" varchar(150) COLLATE "default",
"com15" varchar(150) COLLATE "default",
"cav15" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor15" date,
"cem15" varchar(150) COLLATE "default",
"com16" varchar(150) COLLATE "default",
"cav16" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor16" date,
"cem16" varchar(150) COLLATE "default",
"com17" varchar(150) COLLATE "default",
"cav17" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor17" date,
"cem17" varchar(150) COLLATE "default",
"com18" varchar(150) COLLATE "default",
"cav18" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor18" date,
"cem18" varchar(150) COLLATE "default",
"com19" varchar(150) COLLATE "default",
"cav19" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor19" date,
"cem19" varchar(150) COLLATE "default",
"com20" varchar(150) COLLATE "default",
"cav20" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor20" date,
"cem20" varchar(150) COLLATE "default",
"com21" varchar(150) COLLATE "default",
"cav21" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor21" date,
"cem21" varchar(150) COLLATE "default",
"com22" varchar(150) COLLATE "default",
"cav22" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor22" date,
"cem22" varchar(150) COLLATE "default",
"com23" varchar(150) COLLATE "default",
"cav23" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor23" date,
"cem23" varchar(150) COLLATE "default",
"com24" varchar(150) COLLATE "default",
"cav24" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor24" date,
"cem24" varchar(150) COLLATE "default",
"com25" varchar(150) COLLATE "default",
"cav25" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor25" date,
"cem25" varchar(150) COLLATE "default",
"com26" varchar(150) COLLATE "default",
"cav26" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor26" date,
"cem26" varchar(150) COLLATE "default",
"com27" varchar(150) COLLATE "default",
"cav27" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor27" date,
"cem27" varchar(150) COLLATE "default",
"com28" varchar(150) COLLATE "default",
"cav28" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor28" date,
"cem28" varchar(150) COLLATE "default",
"com29" varchar(150) COLLATE "default",
"cav29" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor29" date,
"cem29" varchar(150) COLLATE "default",
"com30" varchar(150) COLLATE "default",
"cav30" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor30" date,
"cem30" varchar(150) COLLATE "default",
"com31" varchar(150) COLLATE "default",
"cav31" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor31" date,
"cem31" varchar(150) COLLATE "default",
"com32" varchar(150) COLLATE "default",
"cav32" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor32" date,
"cem32" varchar(150) COLLATE "default",
"com33" varchar(150) COLLATE "default",
"cav33" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor33" date,
"cem33" varchar(150) COLLATE "default",
"com34" varchar(150) COLLATE "default",
"cav34" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor34" date,
"cem34" varchar(150) COLLATE "default",
"com35" varchar(150) COLLATE "default",
"cav35" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor35" date,
"cem35" varchar(150) COLLATE "default",
"com36" varchar(150) COLLATE "default",
"cav36" varchar(50) COLLATE "default" DEFAULT 'request to be sent'::character varying,
"cdor36" date,
"cem36" varchar(150) COLLATE "default",


PRIMARY KEY ("fid"),

FOREIGN KEY ("cav36") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com36") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav35") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com35") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav34") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com34") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav33") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com33") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav32") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com32") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav31") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com31") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav30") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com30") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav29") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com29") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav28") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com28") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav27") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com27") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav26") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com26") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav25") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com25") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav24") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com24") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav23") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com23") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav22") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com22") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav21") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com21") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav20") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com20") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav19") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com19") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav18") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com18") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav17") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com17") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav16") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com16") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav15") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com15") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav14") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com14") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav13") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com13") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav12") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com12") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav11") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com11") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav10") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com10") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav9") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com9") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav8") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com8") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav7") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com7") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav6") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com6") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav5") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com5") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav4") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com4") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav36") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com36") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav35") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com35") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav34") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com34") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav33") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com33") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav32") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com32") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav31") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com31") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav30") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com30") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav29") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com29") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav28") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com28") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav27") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com27") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav26") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com26") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav25") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com25") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav24") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com24") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav23") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com23") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav22") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com22") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav21") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com21") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav20") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com20") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav19") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com19") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav18") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com18") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav17") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com17") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav16") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com16") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav15") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com15") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav14") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com14") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav13") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com13") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav12") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com12") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav11") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com11") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav10") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com10") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav9") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com9") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav8") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com8") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav7") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com7") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav6") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com6") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav5") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com5") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav4") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com4") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav3") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com3") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav2") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com2") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cav1") REFERENCES "public"."cat_drawings_availability" ("drawings_available") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("com1") REFERENCES "public"."cat_ucompany_reg" ("utility_company") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cat_durs_status") REFERENCES "public"."cat_statuses_key" ("status") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cat_user_email") REFERENCES "public"."cat_user_key" ("email") ON DELETE RESTRICT ON UPDATE CASCADE,
FOREIGN KEY ("cat_user") REFERENCES "public"."cat_user_key" ("name") ON DELETE RESTRICT ON UPDATE CASCADE

)
WITH (OIDS=FALSE)
;

ALTER TABLE "public"."NewTable" OWNER TO "postgres";

CREATE TRIGGER "insert_new_status_trigger" BEFORE INSERT ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "insert_new_status"();

CREATE TRIGGER "durs_user_mail" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cat_user_email"();


CREATE TRIGGER "cem1" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem1"();

CREATE TRIGGER "cem10" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem10"();

CREATE TRIGGER "cem11" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem11"();

CREATE TRIGGER "cem12" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem12"();

CREATE TRIGGER "cem13" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem13"();

CREATE TRIGGER "cem14" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem14"();

CREATE TRIGGER "cem15" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem15"();

CREATE TRIGGER "cem16" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem16"();

CREATE TRIGGER "cem17" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem17"();

CREATE TRIGGER "cem18" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem18"();

CREATE TRIGGER "cem19" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem19"();

CREATE TRIGGER "cem2" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem2"();

CREATE TRIGGER "cem20" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem20"();

CREATE TRIGGER "cem21" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem21"();

CREATE TRIGGER "cem22" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem22"();

CREATE TRIGGER "cem23" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem23"();

CREATE TRIGGER "cem24" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem24"();

CREATE TRIGGER "cem25" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem25"();

CREATE TRIGGER "cem26" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem26"();

CREATE TRIGGER "cem27" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem27"();

CREATE TRIGGER "cem28" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem28"();

CREATE TRIGGER "cem29" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem29"();

CREATE TRIGGER "cem3" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem3"();

CREATE TRIGGER "cem30" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem30"();

CREATE TRIGGER "cem31" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem31"();

CREATE TRIGGER "cem32" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem32"();

CREATE TRIGGER "cem33" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem33"();

CREATE TRIGGER "cem34" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem34"();

CREATE TRIGGER "cem35" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem35"();

CREATE TRIGGER "cem36" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem36"();

CREATE TRIGGER "cem4" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem4"();

CREATE TRIGGER "cem5" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem5"();

CREATE TRIGGER "cem6" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem6"();

CREATE TRIGGER "cem7" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem7"();

CREATE TRIGGER "cem8" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem8"();

CREATE TRIGGER "cem9" BEFORE INSERT OR UPDATE ON "public"."NewTable"
FOR EACH ROW
EXECUTE PROCEDURE "cem9"();