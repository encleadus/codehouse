Update durs_centroid set cat_durs_status = 'ARCHIVING' WHERE t_complition_date < NOW() - INTERVAL '180 days' AND cat_durs_status != 'ARCHIVED';

update durs_area
set the_geom = st_translate(durs_area.the_geom,400000,400000)
from durs_centroid bb
WHERE bb.job_reference = durs_area.job_reference and bb.cat_durs_status = 'ARCHIVING';

Update durs_centroid set cat_durs_status = 'ARCHIVED' WHERE cat_durs_status = 'ARCHIVING' ;