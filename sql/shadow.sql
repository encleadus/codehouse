create or replace view southend_pd_pts as select southend_bd.gid, southend_bd.height, (st_dumppoints(southend_bd.the_geom)).geom as geom from southend_bd;

create or replace view southend_pd_pts1 as select row_number() over () as row_number, southend_pd_pts.gid, southend_pd_pts.height,  southend_pd_pts.geom  from southend_pd_pts;

create or replace view southend_pd_pts2 as select row_number() over () as row_number, southend_pd_pts.gid, southend_pd_pts.height, st_setsrid(st_makepoint((st_x(southend_pd_pts.geom) + (southend_pd_pts.height * sin(190) / tan(10))), (st_y(southend_pd_pts.geom) + (southend_pd_pts.height * cos(190) / tan(10)))), 27700) as geom  from southend_pd_pts;


create or replace view southend_pd_shadow as select southend_pd_pts2.gid, southend_pd_pts2.height, st_makeline(southend_pd_pts2.geom order by row_number) as newgeom from southend_pd_pts2 group by southend_pd_pts2.gid, southend_pd_pts2.height;

create or replace view southend_pd_shadow_poly as select g.path[1] as gid, g.geom::geometry(polygon, 27700) as the_geom from (select (st_dump(st_polygonize(newgeom))).* from southend_pd_shadow) as g



