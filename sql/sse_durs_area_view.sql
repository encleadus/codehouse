--select ST_GeomFromGeoJSON(area_geom) from sse_durs_area_raw

--CREATE or replace VIEW SSE_DURS_AREA_VIEW AS SELECT ROW_NUMBER() OVER (ORDER BY area_geom ASC) AS ROW_NUMBER, st_setsrid(ST_GeomFromGeoJSON(left(area_geom, LENGTH(area_geom)-3)),4326) from sse_durs_area_raw


CREATE or replace VIEW SSE_DURS_AREA_VIEW AS WITH data AS (SELECT sse_durs_area_raw.area_geom::json AS fc from sse_durs_area_raw)

SELECT
  row_number() OVER () AS gid,
  st_setsrid(ST_GeomFromGeoJSON(feat->>'geometry'),4326) AS geom
  --feat->'properties' AS properties
FROM (
  SELECT json_array_elements(fc->'features') AS feat
  FROM data
) AS f;