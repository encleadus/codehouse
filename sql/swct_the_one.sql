CREATE Or Replace VIEW the_one_swct_catapon_information AS 
SELECT * from swct_catapon_information where project_reference = 'Checkendon_V42';

CREATE Or Replace VIEW the_one_swct_catopon_master AS 
SELECT * from swct_catopon_master where project_reference_label = 'Checkendon_V42';

CREATE Or Replace VIEW the_one_swct_catopon_master_address_list AS 
SELECT * from swct_catopon_master_address_list where project_reference_label_for_addresses = 'Checkendon_V42';

CREATE Or Replace VIEW the_one_swct_catopon_master_address_list_network AS 
SELECT * from swct_catopon_master_address_list_network where project_reference_label_for_addresses = 'Checkendon_V42';

CREATE Or Replace VIEW the_one_swct_catopon_master_network AS 
SELECT * from swct_catopon_master_network where project_reference_label = 'Checkendon_V42';