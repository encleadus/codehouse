Sub HighlightDupes()

Dim i As Long, dic As Variant, v As Variant

Application.ScreenUpdating = False
Set dic = CreateObject("Scripting.Dictionary")

i = 1
For Each v In Selection.Value2
    If dic.exists(v) Then dic(v) = "" Else dic.Add v, i
    i = i + 1
Next v

Selection.Font.Color = 255
For Each v In dic
    If dic(v) <> "" Then Selection(dic(v)).Font.Color = 0
Next v

End Sub