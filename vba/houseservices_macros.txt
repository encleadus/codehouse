
Sub b_saveas_and_Delete_raw_data_and_formulas()

    Worksheets("house_service_form").Activate
    ActiveWorkbook.SaveAs (ThisWorkbook.Path & "\" & Worksheets("house_service_form").Range("k14").Value & "\" & Worksheets("house_service_form").Range("e12").Value & "_" & Worksheets("house_service_form").Range("k14").Value & "_" & Worksheets("house_service_form").Range("e14").Value & ".xlsm")

    
    Dim ws As Worksheet

    For Each ws In ThisWorkbook.Worksheets
        ws.Cells.Copy
        ws.Cells.PasteSpecial xlPasteValues
    Next

    Application.CutCopyMode = False
    
        Application.DisplayAlerts = False
        Sheets("house_service_general").Delete
        Sheets("house_service_measurements").Delete
        Application.DisplayAlerts = True
    
    Range("A6:B6").Select
    Selection.ClearContents
    With Selection.Interior
        .Pattern = xlNone
        .TintAndShade = 0
        .PatternTintAndShade = 0
    End With
    Range("A6:B6").Select
    Selection.Borders(xlDiagonalDown).LineStyle = xlNone
    Selection.Borders(xlDiagonalUp).LineStyle = xlNone
    Selection.Borders(xlEdgeLeft).LineStyle = xlNone
    Selection.Borders(xlEdgeTop).LineStyle = xlNone
    Selection.Borders(xlEdgeBottom).LineStyle = xlNone
    Selection.Borders(xlEdgeRight).LineStyle = xlNone
    Selection.Borders(xlInsideVertical).LineStyle = xlNone
    Selection.Borders(xlInsideHorizontal).LineStyle = xlNone
       
        ActiveWorkbook.Save
        
End Sub



Sub a_add_pics()
Worksheets("house_service_form").Activate
Range("B42").Select
Set pic = ActiveSheet.Shapes.AddPicture(filename:=ThisWorkbook.Path & "\" & Worksheets("house_service_form").Range("k14").Value & "\" & Worksheets("house_service_form").Range("b6").Value & "_SKETCH.jpg", LinkToFile:=msoFalse, SaveWithDocument:=msoTrue, Left:=Range("B42").Left, Top:=Range("B42").Top, Width:=Range("H42").Left - Range("B42").Left, Height:=Range("H68").Top - Range("B42").Top)
 On Error Resume Next
 
 
Worksheets("house_service_form").Activate
Range("H42").Select
Set pic = ActiveSheet.Shapes.AddPicture(filename:=ThisWorkbook.Path & "\" & Worksheets("house_service_form").Range("k14").Value & "\" & Worksheets("house_service_form").Range("b6").Value & "_FRONT.jpg", LinkToFile:=msoFalse, SaveWithDocument:=msoTrue, Left:=Range("H42").Left, Top:=Range("H42").Top, Width:=Range("N42").Left - Range("H42").Left, Height:=Range("N68").Top - Range("H42").Top)
 On Error Resume Next

End Sub

'ez nem kell - you dont need this below
'Sub create_service_report_structure()

'create project folder - check before create
'If Dir$(ThisWorkbook.Path & "\" & Worksheets("house_service_form").Range("e12").Value, vbDirectory) = vbNullString Then
'     MkDir ThisWorkbook.Path & "\" & Worksheets("house_service_form").Range("e12").Value
'End If

'create roadname folder - check before create
'If Dir$(ThisWorkbook.Path & "\" & Worksheets("house_service_form").Range("e12").Value & "\" & Worksheets("house_service_form").Range("k14").Value, vbDirectory) = vbNullString Then
'     MkDir ThisWorkbook.Path & "\" & Worksheets("house_service_form").Range("e12").Value & "\" & Worksheets("house_service_form").Range("k14").Value
'End If
'End Sub

Sub c_export2pdf()
ActiveWorkbook.Save

Dim str As String
str = ThisWorkbook.Path


Dim NameOfWorkbook
NameOfWorkbook = Left(ThisWorkbook.Name, (InStrRev(ThisWorkbook.Name, ".", -1, vbTextCompare) - 1))

    ActiveSheet.ExportAsFixedFormat Type:=xlTypePDF, filename:= _
        str & "\" & NameOfWorkbook & ".pdf", Quality:=xlQualityStandard, IncludeDocProperties:= _
        True, IgnorePrintAreas:=True, OpenAfterPublish:=False
End Sub



