Sub ForwardEmail(Item As Outlook.MailItem)
    With Item.Forward
        .Subject = ("ITS - ") & Item.Subject
        .Recipients.Add "encleadus@gmail.com"
        ' You need to overwrite the Body or HTMLBody to get rid of the auto signature
        .HTMLBody = Item.HTMLBody ' <-- Or use .Body for Plain Text
        '.Display ' <-- For Debug
        .Send ' <-- Put break here to Debug
    End With
    With Item.Forward
        .Subject = ("UKPN DRAWINGS - ") & Item.Subject
        .Recipients.Add "encleadus@gmail.com"
        ' You need to overwrite the Body or HTMLBody to get rid of the auto signature
        .HTMLBody = Item.HTMLBody ' <-- Or use .Body for Plain Text
        '.Display ' <-- For Debug
        .Send ' <-- Put break here to Debug
    End With
End Sub
