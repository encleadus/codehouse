'start blinking ;) 
Sub StartBlink()
 
BlinkRange ThisWorkbook.Worksheets(1).Range("A5:A25"), 0, True
 
End Sub
 
'stop blinking ;)
Sub StopBlink()
 
BlinkRange ThisWorkbook.Worksheets(1).Range("A5:A25"), 0
 
End Sub
 
'input:
' - the range of cells 
' - value to be passed (where condotion)
' - value to start or stop schedule (blinking)
Sub BlinkRange(oRng As Range, cellValue As Variant, Optional bBlink As Boolean = False)
Dim c As Range, dRunWhen As Date
 
'for each cell in range of cells
For Each c In oRng.Cells
    'if cell value is equal to ...
    If c.Value = cellValue Then
        'change color of font red/white
        c.Font.ColorIndex = IIf(c.Font.ColorIndex = 3, 2, 3)
    Else
        'change to default color of font 
        c.Font.ColorIndex = xlColorIndexAutomatic
    End If
Next c
 
'period: 1 sec.
dRunWhen = Now + TimeSerial(0, 0, 1)
Application.OnTime EarliestTime:=dRunWhen, Procedure:="'" & ThisWorkbook.Name & "'!StartBlink", Schedule:=bBlink
 
End Sub

Sub Flash()
NextTime = Now + TimeValue("00:00:01")
With Range("j3:j1000").Font
If .ColorIndex = 2 Then .ColorIndex = 3 Else .ColorIndex = 2
End With
Application.OnTime NextTime, "Flash"
End Sub

Sub StopIt()
Application.OnTime NextTime, "Flash"
Application.OnTime NextTime, "Flash", schedule:=False
Range("j3:j1000").Font.ColorIndex = xlAutomatic
End Sub

Private Sub SpinButton1_Change()
StopIt
If Range("j3:j1000").Value < 6 Then Flash
End Sub

Private Sub Workbook_Open()
If Range("j3:j1000").Value < 6 Then Flash
End Sub

Private Sub Workbook_BeforeClose(Cancel As Boolean)
StopIt
End Sub
