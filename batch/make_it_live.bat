REM upload shapefiles to the database server

cd /D "P:\Project_live"
@echo off
setlocal enabledelayedexpansion
for %%a in (*_*) do (
  set file=%%a
  ren "!file!" "!file: =_!"
)

cd /D "P:\Project_live"
@echo off
setlocal enabledelayedexpansion
for %%a in (*_*) do (
  set file=%%a
  ren "!file!" "!file:(=_!"
)

cd /D "P:\Project_live"
@echo off
setlocal enabledelayedexpansion
for %%a in (*_*) do (
  set file=%%a
  ren "!file!" "!file:)=_!"
)

for /f "eol=: delims=" %%F in ('dir /b /a-d * ^| find /i "LINES_Points"') do del "%%F"
for /f "eol=: delims=" %%F in ('dir /b /a-d * ^| find /i "Default"') do del "%%F"
cd /D "C:\Program Files\PostgreSQL\9.2\bin"

for %%f in (P:\Project_live\CSGEO_LINES*.shp) do shp2pgsql -s 27700 -a %%f public.csgeo_lines > P:\Project_live\%%~nf.sql
for %%f in (P:\Project_live\CSGEO_POINTS*.shp) do shp2pgsql -s 27700 -a %%f public.csgeo_points > P:\Project_live\%%~nf.sql
for %%f in (P:\Project_live\CSU_LINES*.shp) do shp2pgsql -s 27700 -a %%f public.csu_lines > P:\Project_live\%%~nf.sql
for %%f in (P:\Project_live\*.sql) do psql -h localhost -p 5433 -d catsurveys -U postgres -w -f %%f > nul

cd /D "P:\Project_live\backup" 
setlocal enableextensions
set name=%DATE:/=_%
mkdir shp_backup_%name%
move /-y "P:\Project_live\*.dbf" "P:\Project_live\backup\shp_backup_%name%"
move /-y "P:\Project_live\*.shp" "P:\Project_live\backup\shp_backup_%name%"
move /-y "P:\Project_live\*.shx" "P:\Project_live\backup\shp_backup_%name%"
move /-y "P:\Project_live\*.sql" "P:\Project_live\backup\shp_backup_%name%"
