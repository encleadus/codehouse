REM PDF TIMESHEET FORM TO DATABASE MADE BY TAMAS KOSA 27112012
REM create a text list from the weekly timesheets

cd /D "P:\timesheets\weekly_save" 
dir /s/b >P:\timesheets\process_files\filelist.txt

REM PDF form capture timesheet fields

cd /D "C:\Program Files (x86)\A-PDF Form Data Extractor"
PFDECMD -Rmonday -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\monday.csv" -TCSV -PA
PFDECMD -Rtuesday -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\tuesday.csv" -TCSV -PA
PFDECMD -Rwednesday -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\wednesday.csv" -TCSV -PA
PFDECMD -Rthursday -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\thursday.csv" -TCSV -PA
PFDECMD -Rfriday -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\friday.csv" -TCSV -PA
PFDECMD -Rsaturday -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\saturday.csv" -TCSV -PA
PFDECMD -Rsunday -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\sunday.csv" -TCSV -PA

REM PDF form capture project fields

PFDECMD -Rm1 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\m1.csv" -TCSV -PA
PFDECMD -Rm2 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\m2.csv" -TCSV -PA
PFDECMD -Rm3 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\m3.csv" -TCSV -PA
PFDECMD -Rm4 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\m4.csv" -TCSV -PA
PFDECMD -Rm5 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\m5.csv" -TCSV -PA
PFDECMD -Rm6 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\m6.csv" -TCSV -PA
PFDECMD -Rm7 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\m7.csv" -TCSV -PA
PFDECMD -Rm8 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\m8.csv" -TCSV -PA
PFDECMD -Rtu1 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\tu1.csv" -TCSV -PA
PFDECMD -Rtu2 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\tu2.csv" -TCSV -PA
PFDECMD -Rtu3 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\tu3.csv" -TCSV -PA
PFDECMD -Rtu4 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\tu4.csv" -TCSV -PA
PFDECMD -Rtu5 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\tu5.csv" -TCSV -PA
PFDECMD -Rtu6 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\tu6.csv" -TCSV -PA
PFDECMD -Rtu7 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\tu7.csv" -TCSV -PA
PFDECMD -Rtu8 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\tu8.csv" -TCSV -PA
PFDECMD -Rw1 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\w1.csv" -TCSV -PA
PFDECMD -Rw2 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\w2.csv" -TCSV -PA
PFDECMD -Rw3 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\w3.csv" -TCSV -PA
PFDECMD -Rw4 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\w4.csv" -TCSV -PA
PFDECMD -Rw5 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\w5.csv" -TCSV -PA
PFDECMD -Rw6 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\w6.csv" -TCSV -PA
PFDECMD -Rw7 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\w7.csv" -TCSV -PA
PFDECMD -Rw8 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\w8.csv" -TCSV -PA
PFDECMD -Rth1 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\th1.csv" -TCSV -PA
PFDECMD -Rth2 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\th2.csv" -TCSV -PA
PFDECMD -Rth3 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\th3.csv" -TCSV -PA
PFDECMD -Rth4 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\th4.csv" -TCSV -PA
PFDECMD -Rth5 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\th5.csv" -TCSV -PA
PFDECMD -Rth6 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\th6.csv" -TCSV -PA
PFDECMD -Rth7 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\th7.csv" -TCSV -PA
PFDECMD -Rth8 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\th8.csv" -TCSV -PA
PFDECMD -Rf1 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\f1.csv" -TCSV -PA
PFDECMD -Rf2 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\f2.csv" -TCSV -PA
PFDECMD -Rf3 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\f3.csv" -TCSV -PA
PFDECMD -Rf4 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\f4.csv" -TCSV -PA
PFDECMD -Rf5 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\f5.csv" -TCSV -PA
PFDECMD -Rf6 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\f6.csv" -TCSV -PA
PFDECMD -Rf7 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\f7.csv" -TCSV -PA
PFDECMD -Rf8 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\f8.csv" -TCSV -PA
PFDECMD -Rsa1 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\sa1.csv" -TCSV -PA
PFDECMD -Rsa2 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\sa2.csv" -TCSV -PA
PFDECMD -Rsa3 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\sa3.csv" -TCSV -PA
PFDECMD -Rsa4 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\sa4.csv" -TCSV -PA
PFDECMD -Rsa5 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\sa5.csv" -TCSV -PA
PFDECMD -Rsa6 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\sa6.csv" -TCSV -PA
PFDECMD -Rsa7 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\sa7.csv" -TCSV -PA
PFDECMD -Rsa8 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\sa8.csv" -TCSV -PA
PFDECMD -Rsu1 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\su1.csv" -TCSV -PA
PFDECMD -Rsu2 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\su2.csv" -TCSV -PA
PFDECMD -Rsu3 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\su3.csv" -TCSV -PA
PFDECMD -Rsu4 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\su4.csv" -TCSV -PA
PFDECMD -Rsu5 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\su5.csv" -TCSV -PA
PFDECMD -Rsu6 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\su6.csv" -TCSV -PA
PFDECMD -Rsu7 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\su7.csv" -TCSV -PA
PFDECMD -Rsu8 -F"P:\timesheets\process_files\filelist.txt" -O"P:\timesheets\process_files\su8.csv" -TCSV -PA


REM import csv files into the database

cd /D "C:\Program Files (x86)\PremiumSoft\Navicat for PostgreSQL"
navicat.exe /import catsurveys catsurveys public timesheet
navicat.exe /import catsurveys catsurveys public projectsheet

REM backup PDF forms and text files

cd /D "P:\timesheets\backups" 
setlocal enableextensions
set name=%DATE:/=_%
mkdir timesheet_backup_%name%
move /-y "P:\timesheets\weekly_save\*.pdf" "P:\timesheets\backups\timesheet_backup_%name%"
move /-y "P:\timesheets\process_files\*.txt" "P:\timesheets\backups\timesheet_backup_%name%"
move /-y "P:\timesheets\process_files\*.csv" "P:\timesheets\backups\timesheet_backup_%name%"

pause



