del merged.txt

for %%I in (*.GPS) do (
    REM echo %%~fI >> merged.tmp 
    
    rem echo [ entry1 ] >> merged.tmp
    rem echo ------ >> merged.tmp
    type %%I >> merged.tmp
    echo DEL,0,0,N,0,E,0,0,0,0,M,0,M,0,0 >> merged.tmp
    REM echo. >> merged.tmp
    REM echo. >> merged.tmp
)

ren merged.tmp merged.txt

rem $ gawk '{if(match($0, /^\[ (.+?) \]/, k)){name=k[1]}} {print >name".txt" }' entry.txt
rem perl -ne 'open(F, ">", ($1).".txt") if /\[ (entry\d+) \]/; print F;' file