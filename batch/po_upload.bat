REM PDF PO FORM TO DATABASE MADE BY TAMAS KOSA 26042013
REM create a text list from po forms

cd /D "G:\csg_purchase_order\approved"
dir /s/b >G:\csg_purchase_order\process_files\filelist.txt

REM PDF form capture PO fields

cd /D "C:\Program Files (x86)\A-PDF Form Data Extractor"
PFDECMD -Rpo_rule -F"G:\csg_purchase_order\process_files\filelist.txt" -O"G:\csg_purchase_order\process_files\po_all.csv" -TCSV -PA

REM import csv files into the database

cd /D "C:\Program Files (x86)\PremiumSoft\Navicat for PostgreSQL"
navicat.exe /import catsurveys catsurveys public po_upload

REM backup PDF forms and text files

cd /D "G:\csg_purchase_order\backups"
setlocal enableextensions
set name=%DATE:/=_%
mkdir po_backup_%name%
move /-y "G:\csg_purchase_order\approved\*.pdf" "G:\csg_purchase_order\backups\po_backup_%name%"
move /-y "G:\csg_purchase_order\process_files\*.txt" "G:\csg_purchase_order\backups\po_backup_%name%"
move /-y "G:\csg_purchase_order\process_files\*.csv" "G:\csg_purchase_order\backups\po_backup_%name%"

pause