REM upload shapefiles to the database server

start rename_dbf.bru
ping 1.1.1.1 -n 1 -w 3000 > nul
wscript "sendkey.vbs"
ping 1.1.1.1 -n 1 -w 3000 > nul
taskkill /im "Bulk Rename Utility.exe"
ping 1.1.1.1 -n 1 -w 3000 > nul
start rename_shp.bru
ping 1.1.1.1 -n 1 -w 3000 > nul
wscript "sendkey.vbs"
ping 1.1.1.1 -n 1 -w 3000 > nul
taskkill /im "Bulk Rename Utility.exe"
ping 1.1.1.1 -n 1 -w 3000 > nul
start rename_shx.bru
ping 1.1.1.1 -n 1 -w 3000 > nul
wscript "sendkey.vbs"
ping 1.1.1.1 -n 1 -w 3000 > nul
taskkill /im "Bulk Rename Utility.exe"
ping 1.1.1.1 -n 1 -w 3000 > nul

cd /D "C:\Program Files\PostgreSQL\9.2\bin"

for %%f in (P:\Project_live\CSGEO_LINES*.shp) do shp2pgsql -s 27700 -a %%f public.csgeo_lines > P:\Project_live\%%~nf.sql
for %%f in (P:\Project_live\CSGEO_POINTS*.shp) do shp2pgsql -s 27700 -a %%f public.csgeo_points > P:\Project_live\%%~nf.sql
for %%f in (P:\Project_live\CSU_LINES*.shp) do shp2pgsql -s 27700 -a %%f public.csu_lines > P:\Project_live\%%~nf.sql
for %%f in (P:\Project_live\*.sql) do psql -h localhost -p 5433 -d catsurveys -U postgres -w -f %%f > nul

cd /D "P:\Project_live\backup" 
setlocal enableextensions
set name=%DATE:/=_%
mkdir shp_backup_%name%
move /-y "P:\Project_live\*.dbf" "P:\Project_live\backup\shp_backup_%name%"
move /-y "P:\Project_live\*.shp" "P:\Project_live\backup\shp_backup_%name%"
move /-y "P:\Project_live\*.shx" "P:\Project_live\backup\shp_backup_%name%"
move /-y "P:\Project_live\*.sql" "P:\Project_live\backup\shp_backup_%name%"