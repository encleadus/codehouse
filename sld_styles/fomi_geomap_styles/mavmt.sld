<?xml version="1.0" encoding="ISO-8859-1"?>
<StyledLayerDescriptor version="1.0.0" xmlns="http://www.opengis.net/sld" xmlns:ogc="http://www.opengis.net/ogc"
  xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:gml="http://www.opengis.net/gml" xmlns:wrs="http://www.opengis.net/cat/wrs"
  xsi:schemaLocation="http://www.opengis.net/sld http://schemas.opengis.net/sld/1.0.0/StyledLayerDescriptor.xsd">
  <NamedLayer>
    <Name>megjelenites</Name>
    <UserStyle>
      <Name>felhasznalo</Name>
      <Title>adott_felhasznalora_keres</Title>
      <Abstract>lasd_lent</Abstract>
      <FeatureTypeStyle>
        <Rule>
          <Title>1</Title>
          <ogc:Filter>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*01</ogc:Literal>
            </ogc:PropertyIsLike>
          </ogc:Filter>
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#00FF00</CssParameter>
                </Fill>
                                <Stroke>
                  <CssParameter name="stroke">
                    <ogc:Literal>#000000</ogc:Literal>
                  </CssParameter>
                  <CssParameter name="stroke-width">
                    <ogc:Literal>0.7</ogc:Literal>
                  </CssParameter>
                </Stroke>
              </Mark>
              <Size>6</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Title>2</Title>
          <ogc:Filter>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*02</ogc:Literal>
            </ogc:PropertyIsLike>
          </ogc:Filter>
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#FFFF00</CssParameter>
                </Fill>
                                <Stroke>
                  <CssParameter name="stroke">
                    <ogc:Literal>#000000</ogc:Literal>
                  </CssParameter>
                  <CssParameter name="stroke-width">
                    <ogc:Literal>0.7</ogc:Literal>
                  </CssParameter>
                </Stroke>
              </Mark>
              <Size>6</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Title>3</Title>
          <ogc:Filter>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*03</ogc:Literal>
            </ogc:PropertyIsLike>
          </ogc:Filter>
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#FFcc00</CssParameter>
                </Fill>
                                <Stroke>
                  <CssParameter name="stroke">
                    <ogc:Literal>#000000</ogc:Literal>
                  </CssParameter>
                  <CssParameter name="stroke-width">
                    <ogc:Literal>0.7</ogc:Literal>
                  </CssParameter>
                </Stroke>
              </Mark>
              <Size>6</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Title>4</Title>
          <ogc:Filter>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*04</ogc:Literal>
            </ogc:PropertyIsLike>
          </ogc:Filter>
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#00FFFF</CssParameter>
                </Fill>
                                <Stroke>
                  <CssParameter name="stroke">
                    <ogc:Literal>#000000</ogc:Literal>
                  </CssParameter>
                  <CssParameter name="stroke-width">
                    <ogc:Literal>0.7</ogc:Literal>
                  </CssParameter>
                </Stroke>
              </Mark>
              <Size>6</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Title>5</Title>
          <ogc:Filter>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*05</ogc:Literal>
            </ogc:PropertyIsLike>
          </ogc:Filter>
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#FFccFF</CssParameter>
                </Fill>
                                <Stroke>
                  <CssParameter name="stroke">
                    <ogc:Literal>#000000</ogc:Literal>
                  </CssParameter>
                  <CssParameter name="stroke-width">
                    <ogc:Literal>0.7</ogc:Literal>
                  </CssParameter>
                </Stroke>
              </Mark>
              <Size>6</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>
        <Rule>
          <Title>6</Title>
          <ogc:Filter>
          <ogc:And>
	 <ogc:Not>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*01</ogc:Literal>
            </ogc:PropertyIsLike>
            </ogc:Not>
          <ogc:Not>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*02</ogc:Literal>
            </ogc:PropertyIsLike>
            </ogc:Not>
            <ogc:Not>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*03</ogc:Literal>
            </ogc:PropertyIsLike>
            </ogc:Not>
            <ogc:Not>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*04</ogc:Literal>
            </ogc:PropertyIsLike>
            </ogc:Not>
            <ogc:Not>
            <ogc:PropertyIsLike wildCard="*" singleChar="_" escape="%">
             <ogc:PropertyName>user_name</ogc:PropertyName>
             <ogc:Literal>*05</ogc:Literal>
            </ogc:PropertyIsLike>
            </ogc:Not>
            </ogc:And>
          </ogc:Filter>
          <PointSymbolizer>
            <Graphic>
              <Mark>
                <WellKnownName>circle</WellKnownName>
                <Fill>
                  <CssParameter name="fill">#000000</CssParameter>
                </Fill>
                                <Stroke>
                  <CssParameter name="stroke">
                    <ogc:Literal>#000000</ogc:Literal>
                  </CssParameter>
                  <CssParameter name="stroke-width">
                    <ogc:Literal>0.7</ogc:Literal>
                  </CssParameter>
                </Stroke>
              </Mark>
              <Size>6</Size>
            </Graphic>
          </PointSymbolizer>
        </Rule>      
     </FeatureTypeStyle>
    </UserStyle>
    </NamedLayer>
</StyledLayerDescriptor>