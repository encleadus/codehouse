(defun c:utcanev()
(setq tabldefn 
    '(("tablename" . "utcanev")  
       ("tabledesc" . " ") 
       ("columns" 
            ; Define a field 
            (("colname" . "utcanev") 
             ("coldesc" . " ")  
             ("coltype" . "character") 
            ("defaultval" . " ")) 
 )))
(command "_layer" "_make" "kozt_tomb" "") 
;(command "_osnap" "_none")
(command "_cmdecho" 0)
(command "raszter" "ki")
(ade_oddefinetab tabldefn ) 
 (setq sset (ssget "x" '((8 . "B48_UTCAN�V"))))
 (setq j 0)
 (repeat (sslength sset)
 (print j)
 (setq ssn1 (ssname sset j))
; (setq p0 (getpoint "Jelolj ki egy belso pontot"))
 (setq lst1 (entget ssn1))
 (setq nev (getval 1 lst1))
 (setq p0 (getval 10 lst1))
 (command "_zoom" "_c" p0 100)
 (setq x0 (nth 0 p0) y0 (nth 1 p0))
 (setq p1 (list 0 y0))
 (setq p2 (list (/ x0 2.0) (+ y0 1.0)))
; (command "line" p0 p1 p2 p0 "")
 (setq sset1 (ssget "_cp" (list p0 p1 p2 p0) '(( 0 . "lwpolyline")(-4 . "<or")(8 . "POLYGON")(8 . "KOZT_TOMB")(-4 . "or>"))))
 (if sset1
 (progn
 (setq i 0)
  (repeat (sslength sset1)
    (setq ssn (ssname sset1 i))
    (setq lst (entget ssn))
    (setq tipus (getval 0 lst))
    (if (= tipus "POLYLINE") (setq vtx (vertex_list ssn)))
    (if (= tipus "LWPOLYLINE") (setq vtx (vertex_list31 ssn)))
    (setq bent1 (ecov vtx p0))
    (if (= (- bent1 (* (/ bent1 2) 2)) 1) 
      (progn
        (command "_chprop" ssn "" "_la" "kozt_tomb" "_c" 3 "")
        (command "_chprop" ssn1 "" "_la" "kozt_tomb" "_c" 3 "")
        (setq utcanev  (ade_odgetfield ssn "utcanev" "utcanev" 0))
        (if (and utcanev (/= utcanev nev))
        (progn
         (command "_circle" p0 1.0)
        )
        (progn
       (setq new (ade_odnewrecord "utcanev"))
       (ade_odattachrecord ssn new)
       (ade_odsetfield ssn "utcanev" "utcanev" 0 nev) 
        ))

      )
    )
   (setq i (1+ i))
  );
 )


 (progn
 (setq p1 (list x0 0))
 (setq p2 (list (+ x0  1.0)(/ y0 2.0) ))
; (command "line" p0 p1 p2 p0 "")
 (setq sset1 (ssget "_cp" (list p0 p1 p2 p0) '(( 0 . "lwpolyline")(-4 . "<or")(8 . "POLYGON")(8 . "KOZT_TOMB")(-4 . "or>"))))
 (if sset1
 (progn
 (setq i 0)
  (repeat (sslength sset1)
    (setq ssn (ssname sset1 i))
    (setq lst (entget ssn))
    (setq tipus (getval 0 lst))
    (if (= tipus "POLYLINE") (setq vtx (vertex_list ssn)))
    (if (= tipus "LWPOLYLINE") (setq vtx (vertex_list31 ssn)))

    (setq bent1 (ecov vtx p0))
    (if (= (- bent1 (* (/ bent1 2) 2)) 1) 
      (progn
        (command "_chprop" ssn "" "_la" "kozt_tomb" "_c" 3 "")
        (command "_chprop" ssn1 "" "_la" "kozt_tomb" "_c" 3 "")
        (setq utcanev  (ade_odgetfield ssn "utcanev" "utcanev" 0))
        (if (and utcanev (/= utcanev nev))
        (progn
          (command "_circle" p0 1.0)
        )
        (progn
         (setq new (ade_odnewrecord "utcanev"))
         (ade_odattachrecord ssn new)
         (ade_odsetfield ssn "utcanev" "utcanev" 0 nev) 
        ))
      )
    )
   (setq i (1+ i))
  );
 )
 )
 )
 )

  (setq j (1+ j))
 )
)
(defun ecov ( vtx p0)
  (setq ii 0)
  (setq ptest (list 0 (nth 1 p0)))
  (setq metnum 0)
  (repeat (1- (length vtx))
   (setq p1 (nth ii vtx))
   (setq p2 (nth (1+ ii) vtx))
   (setq pm (inters p1 p2 p0 ptest))
   (if pm 
    (progn
     (setq metnum (1+ metnum))
     (if (< (distance pm p0) 0.001)  (setq metnum -100))
    );progn
   );if
 
   (setq ii (1+ ii))
  );repeat
   metnum
);defun
(defun vertex_list31 ( en / ed vertex ret )
;
; input:	entity name
; output:	list of vertices if the entity was a polyline
;		nil otherwise
;
 (setq ed (entget en) vertex nil)

 (SETQ AKTENT (GETVAL 0 ED)) 
 (setq zarte (getval 70 ed))
; (PRINT AKTENT)
(if (= AKTENT "LWPOLYLINE")
   (progn
   (setq ii 0)
   (repeat (length ed)
   (setq akt (nth ii ed))
   (if (= (car akt) 10)
    (progn
     (setq xy (cdr akt))
     (setq vertex (cons xy vertex) )
    )
   )
   (setq ii (1+ ii))
  );repeat
 );progn
 );if
; (PRINT 'KESZPOLY)
 (if (= AKTENT "LINE")
 (PROGN
 ;(PRINT 'EZLINE)
 (setq vertex (cons (getval 10 ed) vertex))
 ;(PRINT 'KEZDOP)
 (setq vertex (cons (getval 11 ed) vertex))
 ;(PRINT VERTEX)
 ;(PRINT 'VEGP)
 ) ; PROGN
 ); line
; (PRINT 'LINEVEG)
  (if (or (= zarte 1) (= zarte 129))
    (progn
 ;    (print "\nZart poligon")
     (setq vertex (cons (last vertex) vertex))
    ) 
   )
 (if vertex (setq ret (reverse vertex)) )
)


(prin1)


