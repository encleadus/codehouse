(defun c:j()
 (command "_color" 1)
 (command "_osnap" "_none")
 (command "_layer" "_make" "dummy" "_set" "dummy" "")
; Maxim�lis m�r�si vonalhossz kikeres�se a keres�si sug�r meghat�roz�s�hoz
 (setq pifel (/ pi 2.0))
 (setq sset (ssget "x" '((-4 . "<or") ( 8 . "GAZ-Gfnk-MERES-G")
                                      ( 8 . "GAZ-Gfk-MERES-G")
                                      ( 8 . "GAZ-Gf-MERES-G")
                                      ( 8 . "GAZ-Gfn-MERES-G")
                         (-4 . "or>") (0 . "text"))))
 (setq i 0 maxtav 0.0)
 (repeat (sslength sset)
  (setq ssn (ssname sset i))
  (setq lst (entget ssn))
  (setq szam (getval 1 lst))
  (if (= (substr szam 1 1) "(") 
   (progn
     (setq numszam (atof (substr szam 2 (- (strlen szam) 2))))
     (if (> numszam maxtav ) (setq maxtav numszam))
    )
  )
 
  (setq i (1+ i))
 )
 (print (strcat "Maxim�lis m�r�si vonal hossz: " (rtos maxtav 2 2)))
 (setq lst (entsel "V�lassz nyilat"))
; A v�gm�ret kikeres�se
 (setq ssn (nth 0 lst))
 (setq lst (entget ssn))
 (setq szog (getval 50 lst))
 (setq p0 (getval 10 lst))
 (setq p1 (polar p0 (- szog pifel) 5))
 (setq p2 (polar p0 (+ szog pifel) 5))
 (setq p3 (polar p1  szog maxtav))
 (setq p4 (polar p2  szog maxtav))
 (command "_zoom" "_c" p0 (* maxtav 1.5))
; (command "_line" p1 p2 p4 p3 p1 "")
 (setq sset1 (ssget "_cp" (list p1 p2 p4 p3) '((-4 . "<or") ( 8 . "GAZ-Gfnk-MERES-G")
                                      ( 8 . "GAZ-Gfk-MERES-G")
                                      ( 8 . "GAZ-Gf-MERES-G")
                                      ( 8 . "GAZ-Gfn-MERES-G")
                         (-4 . "or>") (0 . "text"))))
 
 (setq i 0 vegmin 9999)
 (repeat (sslength sset1)
  (setq ssn (ssname sset1 i))
  (setq lst (entget ssn))
  (setq txt (getval 1 lst))
  (if (= (substr txt 1 1) "(")
   (progn
    (setq txtszog (getval 50 lst))
    (print txt)
    (print txtszog)
    (print szog)
    (if (or (< (abs (- txtszog szog)) 0.035) (< (abs (- txtszog szog pi)) 0.035) (< (abs (+ (- txtszog szog) pi)) 0.01745))
     (progn
      (print txt)
      (setq numveg (atof (substr txt 2 (- (strlen txt) 2))))
      (setq ptxt (getval 10 lst))
      (setq vegakt (distance ptxt p0))
      (if (< vegakt vegmin)
      (progn
      (setq vegmin vegakt)
      (setq ptxtx (nth 0 ptxt) ptxty (nth 1 ptxt))
      (setq ptxt1 (list (- ptxtx 5.0) (- ptxty 5.0)))
      (setq ptxt2 (list (+ ptxtx 5.0) (- ptxty 5.0)))
      (setq ptxt3 (list (+ ptxtx 5.0) (+ ptxty 5.0)))
      (setq ptxt4 (list (- ptxtx 5.0) (+ ptxty 5.0)))
; A v�gm�ret lesz�r�sai pontj�hoz legk�zelebb es� "dummy" r�tegen lev� vonal kikeres�se
      (command "_zoom" "_c" ptxt 15)
      (setq sset2 (ssget "_cp" (list ptxt1 ptxt2 ptxt3 ptxt4 ptxt1) '( ( 8 . "dummy")(0 . "line"))))

      (setq j 0 mintor 9999 ptor nil)
      (repeat (sslength sset2)
       (setq ssn2 (ssname sset2 j))
       (setq lst2 (entget ssn2))
       (setq p10 (getval 10 lst2))
       (setq p11 (getval 11 lst2))
       (if (< (distance ptxt p10) mintor) 
         (progn
          (setq mintor (distance ptxt p10))
          (setq ptor p11 ptxtjo ptxt numvegjo numveg ssnjo ssn lstjo lst)  
         )
       )
       (if (< (distance ptxt p11) mintor) 
         (progn
          (setq mintor (distance ptxt p11))
          (setq ptor p10 ptxtjo ptxt numvegjo numveg ssnjo ssn lstjo lst)  
         )
       )
       (setq j (1+ j))
      );repeat j
      ))
     ) 
    ) ;(if (< (abs (- txtszog szog)) 0.035)
   )  
  )  ;(if (= (substr txt 1 1) "(")

  (setq i (1+ i))
 )
; Megvan a m�r�si vonal v�gpontja, a m�r�si vonalhoz tartoz� m�retek , vet�t�vonalak, jelkulcsok gy�jt�se
  (if (and ptor ptxt)
   (progn
    (setq jotav (getreal "Add meg a helyes v�gm�retet"))
    (setq lst lstjo)
    (setq ssn ssnjo)
; felirt sz�veg numveg  , a v�gm�ret jav�t�sa  
       (setq lst (subst (cons 1 (strcat "(" (rtos jotav 2 2) ")")) (assoc 1 lst) lst))
                  (entmod lst)
                  (entupd ssn)
    (setq arany (/ jotav numvegjo ))
 (setq i 0 vegmin 9999)
 ; A m�retmeg�r�sok jav�t�sa
 (repeat (sslength sset1)
  (setq ssn (ssname sset1 i))
  (setq lst (entget ssn))
  (setq txt (getval 1 lst))
  (if (/= (substr txt 1 1) "(")
   (progn
    (setq txtszog (getval 50 lst))
    (setq ptxt (getval 10 lst))
    (if (and (< (abs  txtszog)  numveg) (or (< (abs (- txtszog szog)) 0.035) (< (abs (- txtszog szog pi)) 0.035) (< (abs (+ (- txtszog szog) pi)) 0.01745)))
     (progn
      (print txt)
      (setq elotag nil utotag nil)
      (if (= (substr txt 1 1) "-") 
       (progn
        (setq elotag "-" ertek (substr txt 2))
       )
      )
      (if (= (substr txt (strlen txt) 1) "-") 
       (progn
         (setq utotag "-" ertek (substr txt 1 (1- (len txt))))
       )
      )
      (setq ujertek (* (atof ertek) arany))
      (setq cujertek (rtos ujertek 2 2))
      (if elotag (setq ujertek (strcat "-" cujertek)))
      (if utotag (setq ujertek (strcat cujertek "-")))
      (setq lst (subst (cons 1 ujertek) (assoc 1 lst) lst))
                  (entmod lst)
                  (entupd ssn)
      (setq kozakt (distance ptxt p0))
     );progn
    ); sz�g �s t�vols�g ellen�rz�s
   );progn
  )
  (setq i (1+ i))
  )
; A "mer�leges" nev� blokkok �s a vet�t�vonalak �thelyez�se
 (setq p1 (polar p0 (- szog pifel) 5))
 (setq p2 (polar p0 (+ szog pifel) 5))
 (setq p3 (polar p1  szog (+ numvegjo 5)))
 (setq p4 (polar p2  szog (+ numvegjo 5)))
 (command "_zoom" "_c" p0 (* (+ numvegjo 5) 1.5))
  (setq sset3 (ssget "_cp" (list p1 p2 p4 p3 p1) '((-4 . "<or") ( 8 . "GAZ-Gfnk-MERES-G")
                                      ( 8 . "GAZ-Gfk-MERES-G")
                                      ( 8 . "GAZ-Gf-MERES-G")
                                      ( 8 . "GAZ-Gfn-MERES-G")
                         (-4 . "or>") (-4 . "<or")
                                       (-4 . "<and")(0 . "insert")(2 . "mer�leges")(-4 . "and>")
                                       (-4 . "<or")(0 . "line")(0 . "text")(-4 . "or>")
                                      (-4 . "or>")     )))

   (setq i 0 virany (angle p0 ptxt) )
   (if (< arany 1.0) (setq virany (angle ptxt p0)))
   (repeat (sslength sset3)
    (setq ssn3 (ssname sset3 i))
    (setq lst3 (entget ssn3))
    (setq tipus (getval 0 lst3))
    (setq p3 (getval 10 lst3))
    (setq akttav (distance p3 p0))
    (if (= tipus "LINE")
     (progn
      (setq p4 (getval 11 lst3))
      (setq pell (getval 11 lst3))
      (if (< (distance p4 p0) akttav) (setq akttav (distance p4 p0) pell p3))
     )
    )

    (if (< akttav jotav)
     (progn
       
      (setq dt (abs (- akttav (* arany akttav))))
      (setq puj (polar p3 virany dt))
      (command "_move" ssn3 "" p3 puj)
 ; A g�zvezet�k nyomvonal�nak jav�t�sa
         (if (= tipus "LINE")
          (progn
            (command "_zoom" "_c" pell 10)
             (setq pellx (nth 0 pell) pelly (nth 1 pell))
             (setq pell1 (list (- pellx 0.5) (- pelly 0.5)))
             (setq pell2 (list (+ pellx 0.5) (- pelly 0.5)))
             (setq pell3 (list (+ pellx 0.5) (+ pelly 0.5)))
             (setq pell4 (list (- pellx 0.5) (+ pelly 0.5)))
  (setq sset4 (ssget "_cp" (list pell1 pell2 pell3 pell4 pell1) '((-4 . "<or") 
                                      ( 8 . "GAZ-Gfnk-GERINC")
                                      ( 8 . "GAZ-Gfk-GERINC")
                                      ( 8 . "GAZ-Gf-GERINC")
                                      ( 8 . "GAZ-Gfn-GERINC")
                         (-4 . "or>")(0 . "line")))) 
           (setq j 0)
           (repeat (sslength sset4)
             (setq ssn4 (ssname sset4 j))
             (setq lst4 (entget ssn4))
             (setq p410 (getval 10 lst4))
             (setq p411 (getval 11 lst4))
             (if (< (distance pell p410) 0.001)
              (progn
                 (setq puj (polar pell virany dt))
                 (setq lst4 (subst (cons 10 puj) (assoc 10 lst4) lst4))
                  (entmod lst4)
                  (entupd ssn4)
              )
             )
             (if (< (distance pell p411) 0.001)
              (progn
                 (setq puj (polar pell virany dt))
                 (setq lst4 (subst (cons 11 puj) (assoc 11 lst4) lst4))
                  (entmod lst4)
                  (entupd ssn4)
              )
             )
             (setq j (1+ j))
           )
          );progn
         )
     )
    )
    (setq i (1+ i))
   )

   )
 
  ) ; if (and ptor ptxt)

)
