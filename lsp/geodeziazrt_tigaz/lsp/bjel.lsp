(defun c:bj()
 (command "cmdecho" "0")
 (setq puffer 0.1)
 (command "_view" "_save" "temp")
 (setq ent1 (car (entsel "\n B�kj a Text-re!")))
 (setq tlist (entget ent1))
 (setq txt (cdr (assoc 1 tlist)))
 (setq ntxt (strcat "~" txt))
 (setq tlist (subst (cons 1 ntxt)(assoc 1 tlist) tlist))
 (entmod tlist)
 (entupd ent1)
 (setvar "CMDECHO" 1)
)

(defun c:bjk()
 (command "cmdecho" "0")
 (setq puffer 0.1)
 (setq lay (getstring "\nAdd meg a f�lia nev�t vagy [Kiv�laszt�s] "))
 (if (or (= lay "") (= lay "k") (= lay "K"))
		(progn
			(setq lay "")
			(while (= lay "")
				(setq ent1 (car (entsel "\nB�kj az elemre!")))
				(setq enttip (cdr (assoc 0 (entget ent1))))
				(if (= enttip "TEXT") (setq lay (cdr (assoc 8 (entget ent1))))  (princ "\nNincs text kiv�lasztva!") )
			)
		)
	)

 (command "_view" "_save" "temp")
 (command "_zoom" "_e")
 (setq sset0 (ssget "x" '((-4 . "<and")(62 . 1)(0 . "circle")(-4 . "and>"))))
 (if sset0
 (progn
   (setq hlof (sslength sset0))
   (setq i 0)
   (while (< i hlof) 
    (setq ent1 (ssname sset0 i))
    (setq i (+ i 1))
    (setq tlist (entget ent1))
    (setq pc (cdr (assoc 10 tlist)))
    (setq sp11 (polar pc (/ pi 4) puffer))
    (setq sp13 (polar pc (* 5 (/ pi 4)) puffer))

    (setq sset1 (ssget "_C" sp11 sp13 (list (cons 0 "TEXT") (cons 8 lay))))
    (setq j 0)
    (if sset1
     
      (repeat (sslength sset1)
       (setq ent2 (ssname sset1 j))
       (setq tlist2 (entget ent2))
       (setq txt (cdr (assoc 1 tlist2)))
       (if (= (substr txt 1 1) "(")
        (progn
         (setq ntxt (strcat "~" txt))
         (setq tlist2 (subst (cons 1 ntxt)(assoc 1 tlist2) tlist2))
         (entmod tlist2)
         (entupd ent2)
        ))
       (setq j (+ j 1))
      )
     
    )
   )
 ))
  (command "_view" "_restore" "temp")	
 (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
 (command "cmdecho" "1")
)






