;F� program
(defun c:BlInt (/ kv n i vnev)
    ;(SETVAR "CMDECHO" 0)
    (setq omode (getvar "osmode"))
    (command "_osnap" "_none")
    (command "_layer" "�" "_1_3" "s" "1" "_1_3" "")
    (command "_layer" "�" "_4" "")
    (setq kv (ssget "x" '((0 . "insert"))))
    (setq n (sslength kv))
    (setq i 0)
    (repeat n
	(setq vnev (ssname kv i))
	(print (- n i))
	(elj1 vnev)
	(setq i (+ i 1))
    ) ;_ repeat
    (command "_setvar" "osmode" omode)
    (print "Kesz")
    (print)
) ;_ defun


(defun elj1 (blID     /	       lista	bl_X	 bl_Y	  bl_Z	   bl_P	    P1	     P2
	     P3	      P4       P	kv	 n	  vnev	   i	    kord_list
	     t_list   tav      t_list_i	tav_list poz	  klist_elem	    tav_list_elem
	     elem     sat      t_elem
	    )
;;;Ide jon a bezumolas merteke
    (setq delta 20)
    (setq lista (entget blID))
    (setq bl_X (nth 0 (cdr (assoc 10 lista))))
    (setq bl_Y (nth 1 (cdr (assoc 10 lista))))
    (setq bl_Z (nth 2 (cdr (assoc 10 lista))))
    (setq bl_P (list bl_X bl_Y bl_Z))
    (setq P1 (list (- bl_x delta) (- bl_y delta)))
    (setq P2 (list (+ bl_x delta) (- bl_y delta)))
    (setq P3 (list (+ bl_x delta) (+ bl_Y delta)))
    (setq P4 (list (- bl_x delta) (+ bl_y delta)))
    (setq P (list p1 p2 p3 p4))
    (command "_zoom" "_W" P1 P3)
    (setq kv (ssget "_CP" P '((0 . "polyline"))))
    (if	(/= kv nil)
	(progn
	    (setq n (sslength kv))
	    (setq i 0)
	    (repeat n
		(setq vnev (ssname kv i))
		(setq kord_list (vertex_list vnev))
		(setq t_list (append kord_list t_list))
		(setq i (+ i 1))
	    ) ;_ repeat

;;; Kiszamolom a tavolsagokat
	    (setq tav_list (tr2 bl_P t_list))
;;;	    (print t_list)
	    ;(print tav_list)
	    (setq elem (hany tav_list))
	    ;(print elem)
	    (if	(/= elem 0)
		(progn
		    (setq tav_list_elem (mini tav_list elem))
		    ;(print tav_list_elem)
		    (setq poz (kere tav_list tav_list_elem))
		    ;(print poz)
		    (setq klist_elem (tr1 t_list poz))
		    ;(print klist_elem)
		    (setq t_elem (tr2 bl_P klist_elem))
		    ;(print t_elem)
		    (setq sat (satl klist_elem tav_list_elem))
		    ;(print sat)
		    (kiir bl_p sat elem t_elem)
		) ;_ progn
	    ) ;_ if
	) ;_ progn
    ) ;_ if
) ;_ defun

(defun vertex_list (en / ed vertex ret)
;;; input:	entity name
;;; output:	list of vertices if the entity was a polyline
;;;		nil otherwise
    (setq ed	 (entget en) vertex nil)
    (SETQ AKTENT (GETVAL 0 ED))
    (setq zarte (getval 70 ed))
    (if	(= AKTENT "POLYLINE")
	(progn
	    (while (/= (getval 0 (entget (entnext en))) "SEQEND")
		(setq en (entnext en)
		      ed (entget en)
		) ;_ setq
		(setq vertex (cons (getval 10 ed) vertex))
	    ) ;_ while
	    (if	(or (= zarte 1) (= zarte 129))
		(progn
		    (setq vertex (cons (last vertex) vertex))
		) ;_ progn
	    ) ;_ if
	) ;_ progn
    ) ;_ if
    (if	(= AKTENT "LINE")
	(PROGN
	    (setq vertex (cons (getval 10 ed) vertex))
	    (setq vertex (cons (getval 11 ed) vertex))
	) ;_ PROGN
    ) ;_ if
    (if	vertex
	(setq ret (reverse vertex))
    ) ;_ if
) ;_ defun

;;; Lista i. -edik elem�t kiveszi a list�b�l ( 0. -t�l indul)
(defun ki (AL kiv / Bl n kiv i ret)
    (setq n (length al))
    (setq i 0)
    (repeat n
	(if (/= i kiv)
	    (progn
		(setq BL (cons (nth i AL) BL))
	    ) ;_ progn
	) ;_ if
	(setq i (+ i 1))
    ) ;_ repeat
    (setq ret (reverse bl))
) ;_ defun

;;; Minimum keres�s
(defun mini (mlista elemsz / minimum n i minimum_i ret)
    (repeat elemsz
        (setq n (length mlista))
	(setq i 0)
	(setq minimum (nth i mlista))
	(setq minimum_i i)
	(repeat	(- n 1)
	    (setq i (+ i 1))
	    (if	(<= (nth i mlista) minimum)
		(progn
		    (setq minimum (nth i mlista))
		    (setq minimum_i i)
		) ;_ progn
	    ) ;_ if
	) ;_ repeat
	(setq mlista (ki mlista minimum_i ))
	(setq ret  (cons minimum ret))
    ) ;_ repeat
    (setq ret  (reverse ret))
    ;(print ret)
) ;_ defun

;;;2d tavols�got szamol p1(x y z) es p2(x y z) kozt
(defun tav2d (P1 P2 / P1_X P1_Y P2_X P2_Y dx dy dxdx dydy ret)
    (setq P1_X (nth 0 P1))
    (setq P1_Y (nth 1 P1))
    (setq P2_X (nth 0 P2))
    (setq P2_Y (nth 1 P2))
    (setq dx (- P1_X P2_X))
    (setq dy (- P1_Y P2_Y))
    (setq dxdx (* dx dx))
    (setq dydy (* dy dy))
    (setq ret (sqrt (+ dxdx dydy)))
) ;_ defun

;;;Kikeres miben mit
;;;Ret-> lista a lista sorszamokkal
(defun kere (mb mi / i j a b ret)
    (setq i 0)
    (setq j 0)
    (repeat (length mi)
	(repeat	(length mb)
	    (if	(= (nth i mb) (nth j mi))
	        (setq ret (cons i ret))
	    ) ;_ if
	    (setq i (+ i 1))
	) ;_ repeat
	(setq i 0)
	(setq j (+ j 1))
    ) ;_ repeat
    (setq ret ret)
) ;_ defun

(defun tr1 (lista poz / i ret)
    (setq i 0)
    (repeat (length poz)
	(setq ret (cons (nth (nth i poz) lista) ret))
	(setq i (+ i 1))
    ) ;_ repeat
    (setq ret  ret)
) ;_ defun

;;;tavolsagokat szamol
(defun tr2 (B_P klist /  l_p  tav i ret)
    (setq i 0)
    (repeat (length klist)
	(setq l_p (nth i klist))
	(setq i (+ i 1))
	(setq tav (tav2d l_p b_p))
        (setq ret (cons tav ret))
    ) ;_ repeat
    (setq ret (reverse ret))
) ;_ defun

(defun satl (Klist Tlist / tav i ret1 z1 z2 z3 z4 t1 t2 t3 t4 p1 p2 p3 p4 sumap n ret)
    (setq i 0)
    (repeat (length Tlist)
	(setq tav (+ 0.01 (nth i Tlist)))
	(setq ret1 (cons tav ret1))
	(setq i (+ i 1))
    ) ;_ repeat
    (setq Tlist (reverse ret1))
    (setq n (length Tlist))
    (print n)
    (cond
	((= n 1)
	    (progn
              (setq z1 (caddr (car Klist)))
		 (setq ret z1)
	    ) ;_ progn
	)
	((= n 2)
	    (progn
		 (setq z1 (caddr (car Klist)))
		 (setq z2 (caddr (cadr Klist)))
		 (setq t1 (car Tlist))
		 (setq t2 (cadr Tlist))
		 (setq p1 (/ 1 (* t1 t1)))
		 (setq p2 (/ 1 (* t2 t2)))
		 (setq sumap (+ p1 p2))
		 (setq ret (/ (+ (* p1 z1) (* p2 z2)) sumap))
	    ) ;_ progn
	)
	((= n 3)
	    (progn
		    (setq z1 (caddr (car Klist)))
		    (setq z2 (caddr (cadr Klist)))
		    (setq z3 (caddr (caddr Klist)))
		    (setq t1 (car Tlist))
		    (setq t2 (cadr Tlist))
		    (setq t3 (caddr Tlist))
		    (setq p1 (/ 1 (* t1 t1)))
		    (setq p2 (/ 1 (* t2 t2)))
		    (setq p3 (/ 1 (* t3 t3)))
		    (setq sumap (+ p1 p2 p3))
		    (setq ret (/ (+ (* p1 z1) (* p2 z2) (* p3 z3)) sumap))
	    ) ;_ progn
	)
	((= n 4)
	    (progn
		 (setq z1 (caddr (car Klist)))
		 (setq z2 (caddr (cadr Klist)))
		 (setq z3 (caddr (caddr Klist)))
		 (setq z4 (caddr (cadddr Klist)))
		 (setq t1 (car Tlist))
		 (setq t2 (cadr Tlist))
		 (setq t3 (caddr Tlist))
		 (setq t4 (cadddr Tlist))
		 (setq p1 (/ 1 (* t1 t1)))
		 (setq p2 (/ 1 (* t2 t2)))
		 (setq p3 (/ 1 (* t3 t3)))
		 (setq p4 (/ 1 (* t4 t4)))
		 (setq sumap (+ p1 p2 p3 p4))
		 (setq ret (/ (+ (* p1 z1) (* p2 z2) (* p3 z3) (* p4 z4)) sumap))
	    ) ;_ progn
	)
    )
    (setq ret ret)
) ;_ defun

(defun kiir (bl_p1 Satlag elemsz tavolsagok / atlagt t_Point i )
    (setq i 0)
    (setq atlagt 0)
    (setq t_Point (list (car bl_p1 ) (- (cadr bl_p1) 1.5)))
    (repeat (length tavolsagok)
	(setq atlagt (+ atlagt (nth i tavolsagok)))
	(setq i (+ i 1))
    ) ;_ repeat
    (setq atlagt (/ atlagt (length tavolsagok)))
    (if	(= elemsz 4)
	(progn
	    (setq szoveg (- (caddr bl_p1) Satlag))
	    (setq szoveg (rtos szoveg 2 1))
            (setq atlagt (rtos atlagt 2 1))
	    (command "_layer" "�" "_4" "")
	    (command "_text" bl_p1 1 0 szoveg "")
	    (command "_text" t_Point 1 0 atlagt "")
	) ;_ progn
	(progn
	    (setq szoveg (- (caddr bl_p1) Satlag))
	    (setq szoveg (rtos szoveg 2 1))
            (setq atlagt (rtos atlagt 2 1))
	    (command "_layer" "�" "_1_3" "")
	    (command "_text" bl_p1 1 0 szoveg "")
	    (command "_text" t_Point 1 0 atlagt "")
	)
    ) ;_ if
) ;_ defun



;Megmondja hany eleme lesz a listanak
(defun hany (hany_A /  i ret)
    (setq i 0)
    (setq ret 0)
    (repeat (length hany_A)
	(if (< (nth i hany_A) delta)
	    (progn
		(setq ret (+ 1 ret))
	    ) ;_ progn
	) ;_ if
	(setq i (+ i 1))
    ) ;_ repeat
    (if (> ret 4) (setq ret 4))
    (setq ret  ret)
) ;_ defun