(defun c:para ( ) 
  (alert
     " Parancsok:\n
    vcszar	- V�d�cs�/burokcs� lez�r�, amit nem tud lez�rni, oda k�rt tesz
    kk	- hibak�r keres�, v�gigl�pked az �llom�nyban tal�lhat� k�r�k�n, a m�r vizsg�ltat t�rli
    para	- ez az ablak
    
"
)
)

(defun c:vcszar ()
   (setq puffer 0.1)
   (setq pufferb 2)
   (setvar "CMDECHO" 0)
   (command "_osnap" "_non") 
   (command "_zoom" "_e")
   (setq lof (ssget "x"
		     '((-4 . "<AND")(0 . "LWPOLYLINE")(-4 . "<OR")(8 . "GAZ-Gfk-VEDOCSO")(8 . "GAZ-Gf-VEDOCSO")(8 . "GAZ-Gfn-VEDOCSO")(8 . "GAZ-Gfnk-VEDOCSO")(-4 . "OR>")(-4 . "AND>"))

	      )
  )
  (if lof (command "_explode" lof))
  (setq lof (ssget "x"
 		     '((-4 . "<AND")(0 . "LINE")(-4 . "<OR")(8 . "GAZ-Gfk-VEDOCSO")(8 . "GAZ-Gf-VEDOCSO")(8 . "GAZ-Gfn-VEDOCSO")(8 . "GAZ-Gfnk-VEDOCSO")(-4 . "OR>")(-4 . "AND>"))
	      )
  )
  (if lof
   (progn
     (command "_layer" "_m" "vcs_tmp" "_c" "1" "" "")
     ;(command "_layer" "_s" "GAZ-Gfk-VEDOCSO" "")
     (setq hlof (sslength lof))
     (setq j 0)
     (while (< j hlof)
        (setq entx (ssname lof j))
	(setq tlist (entget entx))
        (setq layer (cdr (assoc 8 tlist)))
	(command "_layer" "_s" layer "")
        (command "_change" entx "" "_p" "_la" "vcs_tmp" "") 
        (setq tlist (entget entx))
        (setq pstart (cdr (assoc 10 tlist)))
        (setq pend (cdr (assoc 11 tlist)))
        (setq midpt (list (/ (+ (car pstart) (car pend)) 2) (/ (+ (cadr pstart) (cadr pend)) 2)))
        (setq sp11 (polar pstart (/ pi 4) puffer))
        (setq sp13 (polar pstart (* 5 (/ pi 4)) puffer))
        (command "_zoom" "_c" pstart "30")
        (setq lef (ssget "_C" sp11 sp13 (list (cons 0 "LINE") (cons 8 layer))))    
        (if lef () 
         (progn     
		(setq sp12 (polar midpt (/ pi 4) pufferb))
	        (setq sp14 (polar midpt (* 5 (/ pi 4)) pufferb))
		;(setq lcf (ssget "_C" sp12 sp14 '((0 . "LINE") (8 . "GAZ-Gfk-VEDOCSO,GAZ-Gf-VEDOCSO"))))
                (setq lcf (ssget "_C" sp12 sp14 (list(cons 0 "LINE") (cons 8 layer))))
		(if lcf
                 (progn
                   (setq hlcf (sslength lcf))
                   (if (< hlcf 2) 
                      (progn
                         (setq enty (ssname lcf 0))
			 (setq tlisty (entget enty))
			 (setq pstarty (cdr (assoc 10 tlisty)))
		         (setq pendy (cdr (assoc 11 tlisty)))
                         (setq da (distance pstart pstarty))
			 (setq db (distance pstart pendy))
                         (if (< da db)
  				(progn
  					(command "_line" pstart pstarty "")
					(command "_line" pend pendy "")
				)
				(progn
					(command "_line" pstart pendy "")
					(command "_line" pend pstarty "")
				)
			)
		     )
			(command "circle" midpt"5")
		  )
		 )
		 (command "circle" midpt "5")
		 )
	        
	)
 	
     )
    (command "_change" entx "" "_p" "_la" layer "")
    (setq j (+ j 1))
    (princ (strcat "\n " (itoa j) "/" (itoa hlof)))
   )
 
)
)
(setvar "CMDECHO" 1)
)
(defun c:kk ()
  (setq zoomk 30)
 ;(command "_zoom" "e")
  (setq circle-list (ssget "x" '((0 . "circle"))))
  (if circle-list
    (progn
      (setq h-circle (sslength circle-list))
      (setq circlex (ssname circle-list 0))
      (setq pt1 (cdr (assoc 10 (entget circlex))))
      (command "_zoom" "c" pt1 zoomk)
      (print (strcat "meg: " (itoa h-circle) "db van!"))
      (getstring "\nTorlom!")
      (command "erase" (ssname circle-list 0) "")
      (ssdel circlex circle-list)
      (setq h-circle (sslength circle-list))
    )
    (alert "Nincs benne t�bb hibak�r!")
  )
) ;defun kk
(c:para)
