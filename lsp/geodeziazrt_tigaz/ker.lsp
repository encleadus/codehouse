(defun c:keres()
 (command "cmdecho" "0")
 (command "_osnap" "_non")
 (command "_view" "_save" "temp")
 (command "_color" 1)
 (command "_layer" "_make" "dummy" "_set" "dummy" "")
 (command "_dimzin" "0")

; Maxim�lis m�r�si vonalhossz kikeres�se a keres�si sug�r meghat�roz�s�hoz
 
 (setq sset (ssget "x" '((-4 . "<or") ( 8 . "GAZ-Gfnk-MERES-G")
                                      ( 8 . "GAZ-Gfk-MERES-G")
                                      ( 8 . "GAZ-Gf-MERES-G")
                                      ( 8 . "GAZ-Gfn-MERES-G")
                         (-4 . "or>") (0 . "text"))))
 (setq i 0)
 (repeat (sslength sset)
  (setq ssn (ssname sset i))
  (setq lst (entget ssn))
  (setq szam (getval 1 lst))
  (setq pstart (getval 10 lst))
  (if (= (substr szam (- (strlen szam) 1)) ")~")

   (progn
     (setq ntxt (strcat "~" (substr szam 1 (- (strlen szam) 1))))
         (setq lst (subst (cons 1 ntxt)(assoc 1 lst) lst))
         (entmod lst)
         (entupd ssn)
    )
  )
 
  (setq i (1+ i))
 )
 
 (command "_view" "_restore" "temp")	
 (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
 (command "cmdecho" "1")
)


(defun GETVAL (grp ele)                 ;"dxf value" of any ent...
  (cond ((= (type ele) 'ENAME)          ;ENAME
          (cdr (assoc grp (entget ele))))
        ((not ele) nil)                 ;empty value
        ((not (listp ele)) nil)         ;invalid ele
        ((= (type (car ele)) 'ENAME)    ;entsel-list
          (cdr (assoc grp (entget (car ele)))))
        (T (cdr (assoc grp ele)))))     ;entget-list

(defun c:kk ()
  (setq zoomk 30)
 ;(command "_zoom" "e")
  (setq circle-list (ssget "x" '((0 . "circle"))))
  (setq h-circle (sslength circle-list))
  (if (/= h-circle 0)
    (progn
      (setq circlex (ssname circle-list 0))
      (setq pt1 (cdr (assoc 10 (entget circlex))))
      (command "_zoom" "c" pt1 zoomk)
      (print (strcat "meg: " (itoa h-circle) "db van!"))
      (getstring "\nTorlom!")
      (command "erase" (ssname circle-list 0) "")
      (ssdel circlex circle-list)
      (setq h-circle (sslength circle-list))
    )
    (alert "Nincs benne tobb kor!")
  )
) ;defun kk
