
(DEFUN c:ivfelbont ( / ivtures_ q halmaz)
   ;---- ARC rajzelemek helyettes�t�se vonalszakaszokkal:
   (setq ivtures (IF (and ivtures (equal (type ivtures) 'REAL)) ivtures 0.05)
      	ivtures_ 0.0
   )
 	(WHILE (<= ivtures_ 0.0)
      (setq ivtures_ ivtures
           ivtures_ (getreal (strcat "\n�v - h�r maxim�lis t�vols�ga " (IF (> ivtures_ 0.0) (strcat "<" (rtos ivtures_ 2 3) ">") "") " : "))
			  ivtures_ (IF ivtures_ ivtures_ ivtures)
			  ivtures  (IF (> ivtures_ 0) ivtures_ ivtures)
      )
   )
   (WHILE (setq halmaz (ssget))
		(setq q -1)
      (WHILE (setq q (1+ q) elem (ssname halmaz q))
			(iv_felbont elem ivtures_)
      )
   )
   (princ)
)


(DEFUN iv_felbont (elem tures / ret szi vti vtl vva cen sug kir zir szo alf han kpnt vpnt p1 p2 iq ford w)
   (setq eleml (entget elem))
   (IF (and (equal (cdr (assoc 0 eleml)) "ARC") (equal (type tures) 'REAL) (> tures 0.0009))
      (progn
	      (setq ret (assoc  8 eleml)
               szi (assoc 62 eleml)
               szi (IF szi szi (cons 62 256))
               vti (assoc  6 eleml)
               vti (IF vti vti (cons 6 "BYLAYER"))
               vtl (assoc 48 eleml)
               vtl (IF vtl vtl (cons 48 1.0))
               vva (assoc 370 eleml)
            	cen (cdr (assoc 10 eleml))
	            sug (cdr (assoc 40 eleml))
	            kir (cdr (assoc 50 eleml))
	            zir (cdr (assoc 51 eleml))
	            szo (IF (< zir kir) (- (* pi 2) (- kir zir)) (- zir kir))
	            alf (- 1 (/ tures sug))
	            alf (* 2 (atan (/ (sqrt (- 1 (* alf alf))) alf)))
	            han (max (fix (+ (/ szo alf) 0.5)) (fix (* (/ szo (* pi 2)) 10)) 3)
	            alf (/ szo han)
              kpnt (polar cen kir sug)
              vpnt (polar cen zir sug)
	             p1 kpnt
	             iq 0
	      )
	      (entdel elem)
	      (IF (> (distance kpnt (polar cen kir sug)) 0.01)
	         (setq ford kir kir zir zir ford ford -1)
	         (setq ford 1)
	      )
	      (WHILE (setq iq (1+ iq) w (< iq han))
	         (setq p2 (polar cen (+ kir (* alf iq ford)) sug))
	         (entmake (setq qwe (list (cons 0 "LINE") (cons 10 p1) (cons 11 p2) ret szi vti vtl) qwe (IF vva (append qwe (list vva)) qwe)))
				(entmake (list (cons 0 "POINT") (cons 10 p2)))
	         (setq p1 p2)
	      )
	      (entmake (setq qwe (list (cons 0 "LINE") (cons 10 p2) (cons 11 vpnt) ret szi vti vtl) qwe (IF vva (append qwe (list vva)) qwe)))
      )
   )
   (princ)
)

