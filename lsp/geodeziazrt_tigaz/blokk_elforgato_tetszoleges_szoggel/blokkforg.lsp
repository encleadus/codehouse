(defun c:blokkforg ( )
	(setvar "CMDECHO" 0)
	(setq bnam (getstring "\nAdd meg a forgatand� blokk nev�t vagy [Kiv�laszt�s]: "))
	(if (or (= bnam "") (= bnam "k") (= bnam "K"))
		(progn
			(setq bnam "")
			(while (= bnam "")
				(setq ent1 (car (entsel "\nB�kj az elemre!")))
				(setq enttip (cdr (assoc 0 (entget ent1))))
				(if (= enttip "INSERT") (setq bnam (cdr (assoc 2 (entget ent1))))  (princ "\nNincs blokk kiv�lasztva!") )
			)
		)
	)
	(setq szogo (atof (getstring "\nAdd meg az elforgat�si sz�get: ")))
	(command "_zoom" "_e")
	(setq lof (ssget "x" (list (cons 0 "INSERT") (cons 2 bnam))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq t-angle (cdr (assoc 50 tlist)))
				;(setq pstart (cdr (assoc 10 tlist)))
				;(command "_zoom" "_c" pstart "30")
				(setq szog (+ t-angle (/ (* pi szogo) 180)))
				(setq tlist (subst (cons 50 szog)(assoc 50 tlist) tlist))
				(entmod tlist)
				(setq j (+ j 1))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)

		)
		(princ "\nNincs ilyen nev� blokk a rajzban!")
	)
	(princ "\n")
	(setvar "CMDECHO" 1)
)
(princ "\nBlokkokat elforgatja megadott sz�ggel! \nParancs: blokkforg")
 


	