(defun c:csore()
  ; A rutin az ITR altal keszitett szelvenyezes zaszlok hosszat roviditi meg
  (setvar "cmdecho" 0)
  (setq javdb 0)
  (setq sset (ssget "x" '(( 0 . "circle"))))
  (if sset (command "_erase" sset ""))
  
  (command "_layer" "_set" "GAZ-Gfk-TEREPMAG-FELIRAT" "")
  (command "vt�pus" "b" "f" "")
  (setq javdb 0)
  (command "_color" "bylayer")
          
   (setq sset (ssget "x" '((0 . "line")(-4 . "<or")
                                             ( 8 . "GAZ-Gfk-TEREPMAG-FELIRAT")
                                             ( 8 . "GAZ-Gfn-TEREPMAG-FELIRAT")
                                             ( 8 . "GAZ-Gfnk-TEREPMAG-FELIRAT")
                                             ( 8 . "GAZ-Gf-TEREPMAG-FELIRAT")
                                             (-4 . "or>"))))
   (setq i 0)
   (if sset
    (progn
     (setq n2 (sslength sset))
     (repeat (sslength sset)
      (princ (strcat (itoa i) "/" (itoa n2) "\r"))  
      (setq ssn (ssname sset i))
      (setq lst (entget ssn))
;      (print lst)
      (setq p1a (getval 10 lst))
      (setq p2a (getval 11 lst))
      (setq ii 0 tmin 1000000000)
      (repeat 2
      (if (= ii 0) (setq p0 p1a p01 p2a))
      (if (= ii 1) (setq p0 p2a p01 p1a))
      (command "_zoom" "_c" p0 10)
;      (print p0)
      (setq x0 (car p0))
      (setq y0 (cadr p0))
      (setq p1 (list (+ x0 0.5) (+ y0 0.5)))
      (setq p2 (list (- x0 0.5) (+ y0 0.5)))
      (setq p3 (list (- x0 0.5) (- y0 0.5)))
      (setq p4 (list (+ x0 0.5) (- y0 0.5)))
      (setq svonal (ssget "_cp" (list p1 p2 p3 p4 p1) '((-4 . "<or")(0 . "lwpolyline")( 0 . "line")(-4 . "or>")
                                             (-4 . "<or")
                                             ( 8 . "GAZ-Gfk-GERINC")
                                             ( 8 . "GAZ-Gfn-GERINC")
                                             ( 8 . "GAZ-Gfnk-GERINC")
                                             ( 8 . "GAZ-Gf-GERINC")
                                             ( 8 . "GAZ-Gfk-LEAGAZAS")
                                             ( 8 . "GAZ-Gfn-LEAGAZAS")
                                             ( 8 . "GAZ-Gfnk-LEAGAZAS")
                                             ( 8 . "GAZ-Gf-LEAGAZAS")
                                             (-4 . "or>"))))
      
      (if svonal
        (progn
         (setq k 0 ssvuj (ssadd) idb 0 )
         (repeat (sslength svonal)
          (setq ssn3 (ssname svonal k))
          (setq vtx (vertex_list1 ssn3))
           (setq l 0)
           (repeat (length vtx)
            (setq takt (distance (nth l vtx) p0))
            (if (< takt tmin)
              (progn
                (setq tmin takt)
                (setq puj (nth l vtx))
              )
            )
            (setq l (1+ l))
           )
          (setq k (1+ k))
         );repeat
        );progn
       );if svonal

       (if (< tmin 2.0) 
         (progn
          (command "_line" puj p01 "")
          (command "_erase" ssn "")
          (setq javdb (1+ javdb))
         )
       )
        



      (if ( and (> tmin 2) svonal)
        (progn
         (setq k 0  idb 0 tmin 1000000000)
         (repeat (sslength svonal)
          (setq ssn3 (ssname svonal k))
          (setq vtx (vertex_list1 ssn3))
           (setq l 0)
           (repeat (1- (length vtx))


   (setq k_p (nth l vtx)  
         v_p (nth (1+ l) vtx)    
                  alap_vektor (vec1 k_p v_p)                   ; egysegnyi hosszu
                  normalis (nor alap_vektor nil)
                  ellen (nor normalis nil)
                  norellen (nor ellen nil)
       )
               (setq p1a (VEC+ (vec*k normalis 1) p0))
               (setq p2a (VEC+ (vec*k norellen 1) p0))
               (setq alap_ellen (vec1 v_p k_p))    
                (setq pm (inters p1a p2a k_p v_p))
                (if pm
                 (progn
                  (setq takt (distance pm p0))
                    (if (< takt tmin)
                     (progn
                      (setq tmin takt)
                       (setq puj pm)
                     )
                    )
                 )
                )
           (setq l (1+ l))
          )
           (setq k (1+ k))
          ) 
      (if (< tmin 2.0) 
         (progn
          (command "_line" puj p01 "")
          (command "_erase" ssn "")
          (setq javdb (1+ javdb))
         )
       )
         )
         ) ;if tmin > 2










       (setq ii (1+ ii))
       );repeat
      (setq i (1+ i))
     );repeat i
      );progn
     );if sset
   (print (strcat "Jav�tva: " (itoa javdb)))
);defun


 
