(defun c:linevag()
 (command "_zoom" "_e")
 (command "cmdecho" 1)
 (command "_expert" 1)
 (command "_osnap" "_none")
 (setq ssetpoly (ssget "x" '(( 8 . "gazsav")(0 . "lwpolyline"))))
 (command "_layer" "_make" "gazsav_tart" "_off" "gazsav_tart" "")
 (command "_copy" ssetpoly "" (list 0 0) (list 0 0))
 (setq ssetpoly_tart (ssget "x" '(( 8 . "gazsav_tart")(0 . "lwpolyline"))))
 (if (not ssetpoly_tart)
  (progn
   (command "_chprop" ssetpoly "" "_la" "gazsav_tart" "")
   (setq ssetpoly (ssget "x" '(( 8 . "gazsav")(0 . "lwpolyline"))))
  )
  (progn
   (command "_erase" ssetpoly "") 
   (setq ssetpoly (ssget "x" '(( 8 . "gazsav")(0 . "lwpolyline"))))
  )
 )
 (setq k 0)
 (repeat (sslength ssetpoly )
 (setq ssn (ssname ssetpoly k))
 (setq lst (entget ssn))
 (setq reteg (getval 8 lst))
 '(command "_layer" "KI" reteg "")
 (setq vtx1 (vertex_list1 ssn))
 (setq i 0 )

  (setq xmin 999999 ymin xmin xmax -99999 ymax xmax)
    (setq j 0)
    (repeat (length vtx)
      (setq pakt (nth j vtx))
      (setq xakt (nth 0 pakt))        
      (setq yakt (nth 1 pakt))
      (minmax)
     (setq j (1+ j))
    )
    (command "_zoom" "_w" (list (- xmin 3) (- ymin 3)) (list (+ xmax 3) (+ ymax +3)))



 (command "_layer" "_off" "foldreszlet" "")
  
  (setq sset1 (ssget "_cp"  vtx1 '((-4 . "<or")(0 . "line")(0 . "lwpolyline")(-4 . "or>")
                                    (-4 . "<not")(-4 . "<or")(8 . "gaz-gf-gerinc")
                                     (8 . "gaz-gfk-gerinc")
                                     (8 . "gaz-gfnk-gerinc")
                                     (8 . "gaz-gfn-gerinc")
                                     (8 . "foldreszlet")
                                     (8 . "gazsav")(-4 . "or>")
                                   (-4 . "not>")            )))
; (print sset1)
;  (grread)
  (setq j 0)
   (if sset1
   (progn
  ; (print (sslength sset1))
 
   (repeat (sslength sset1)
    (setq ssn1 (ssname sset1 j))
     (setq lst1 (entget ssn1))
    (setq tipus (getval 0 lst1))
    (if (= tipus "LINE")
     (progn 
      (setq p10 (getval 10 lst1))
      (setq p11 (getval 11 lst1))
     )
     (progn
      (setq vtxline (vertex_list1 ssn1))
      (setq p10 (nth 0 vtxline))
      (setq p11 (nth 1 vtxline))
     )
    )
   

      (command "_zoom" "_w" p10 p11)
    (setq bent1 (ecov vtx1 p10))
   (if (= (- bent1 (* (/ bent1 2) 2)) 1) 
     (progn
     )
     (progn
      (command "_trim" ssn "" p10 "")
     )
    )
     (setq bent1 (ecov vtx1 p11))
   (if (= (- bent1 (* (/ bent1 2) 2)) 1) 
     (progn
     )
     (progn
      (command "_trim" ssn "" p11 "")
     )
    )


    
    (setq j (1+ j))
   )
   ));if sset1

 (if nil
 (progn
 (setq xmin 999999 ymin xmin xmax -99999 ymax xmax)
    (setq j 0)
    (repeat (length vtx1)
      (setq pakt (nth j vtx1))
      (setq xakt (nth 0 pakt))        
      (setq yakt (nth 1 pakt))
      (minmax)
     (setq j (1+ j))
    )
    (command "_zoom" "_w" (list (- xmin 10) (- ymin 10)) (list (+ xmax 10) (+ ymax +10)))


 (setq sset (ssget "_cp" vtx1 '((-4 . "<or")(0 . "line")(0 . "lwpolyline")(-4 . "or>")
                                    (-4 . "<not")(-4 . "<or")(8 . "gaz-gf-gerinc")
                                     (8 . "gaz-gfk-gerinc")
                                     (8 . "gaz-gfnk-gerinc")
                                     (8 . "gaz-gfn-gerinc")
                                     (8 . "foldreszlet")
                                     (8 . "gazsav")(-4 . "or>")
                                   (-4 . "not>")            )))
 (setq i 0)
 (if sset
 (progn
 (repeat (sslength sset)
  (setq ssn (ssname sset i))
  (setq lst1 (entget ssn))
  (setq p10 (getval 10 lst))
  (setq p11 (getval 11 lst))

   (setq tipus (getval 0 lst1))
    (if (= tipus "LINE")
     (progn 
      (setq p10 (getval 10 lst1))
      (setq p11 (getval 11 lst1))
     )
     (progn
      (setq vtxline1 (vertex_list1 ssn))
      (setq p10 (nth 0 vtxline1))
      (setq p11 (nth 1 vtxline1))
     )
    )
   



  (setq pk (list (/ (+ (nth 0 p10) (nth 0 p11)) 2 )  (/ (+ (nth 1 p10) (nth 1 p11)) 2 ) ))
   (setq bent1 (ecov vtx1 pk))
   (if (= (- bent1 (* (/ bent1 2) 2)) 1) 
     (progn
       (command "_color" 1)
       (command "_circle" pk 1.0)
     )
     (progn
       (command "_erase" ssn "")
       (command "_color" 2)
       (command "_circle" pk 1.0)
     )
    )

  (setq i (1+ i))
 )
 ))
 ))
  (setq k (1+ k))
 )
; (setq sset (ssget "_cp" vtx1 ))
; (if sset 
;  (progn
;   (if (not (findfile "kivagat.dwg" ))
;    (command "_wblock" "kivagat" ""  (list 0 0) sset   "")
;    (command "_wblock" "kivagat" "_y" ""  (list 0 0) sset   "")
;   )
;  )
 ;)
 (command "_layer" "_on" "foldreszlet" "")
 (command "_erase" ssetpoly "")
 (setq sset (ssget "x" '(( 8 . "gazsav_tart"))))
 (command "_chprop" sset "" "_la" "gazsav" "")
)
(defun ecov ( vtx p0)
  (setq ii 0)
  (setq ptest (list 0 (nth 1 p0)))
  (setq metnum 0)
  (repeat (1- (length vtx))
   (setq p1a (nth ii vtx))
   (setq p2a (nth (1+ ii) vtx))
   (setq pm (inters p1a p2a p0 ptest))
   (if pm 
    (progn
     (setq metnum (1+ metnum))
     (if (< (distance pm p0) 0.001)  (setq metnum -100))
    );progn
   );if
 
   (setq ii (1+ ii))
  );repeat
   metnum
);defun

