(defun c:addst()
(setq kozt_jell (list "k�zter�let" "akna" "als�sor" "�rok" "�rt�r" "�tj�r�" "bej�r�" "domb" "d�l�"
"�p�let" "fasor" "fels�sor" "f�t�r" "hat�r" "hat�rsor" "hat�r�t" "h�d" "j�rand�" "j�r�s" "kapu"
"kert" "kish�d" "kol�nia" "k�rny�k" "k�r�nd" "k�rt�r" "k�r�t"  "k�rzet" "k�z" "k�z�ps�sor" "lak�telep"
"legel�" "lej�r�"  "lejt�" "l�pcs�"  "liget" "major" "m�ly�t" "nagyh�d" "negyed" "oldal" "orom"
"park" "pincesor" "puszta" "rakpart" "s�t�ny" "sik�tor" "sor" "sug�r�t" "szektor" "sz�l�" "tanya" "telep"
"telep�l�s" "t�r" "tere" "tet�" "udvar" "�jsor" "�jtelep" "�t" "utca" "�tf�l" "�tja" "�d�l�telep" 
"v�rkert" "v�ros" "v�rosr�sz" "villasor" "v�lgy"  "p�lyaudvar" "kik�t�" "alulj�r�" "cs�rda" "part"
"b�stya" "b�sty�ja"  "�sv�ny" "d�l��t" "erd�sor" "�d�l�part" "vas�t�llom�s" "sziget" "zug"
"rep�l�t�r" "�tel�gaz�s" "j�tsz�t�r" "d�l�sor" "fordul�" "erd�" "parkja" "utc�ja" "hegy" "malom" "�llom�s"
"l�nch�d" "s�ta�t" "f��t" "h�df�" "fasora" "el�tt" "bek�t�" "s�tat�r" "orsz�g�t" "fel�lj�r�" "csap�s"
"v�lgyh�d" "nagy�t"  "rakod�" "k�r�tja" "g�dre" "szurdok" "ligete"  "k�ze" "sora" "lak�park" "sz�le" "bokor" "vezet�k" ))
(ade_oddeletetab "utca") 
(setq lst_def
'(("Tablename" . "Utca") ("TableDesc" . "Utcanevek adatt�bl�ja") 
("Columns" (("ColName" . "UTCA_NEV") ("ColDesc" . "Utca neve") ("ColType" 
. "Character") ("DefaultVal" . "")) (("ColName" . "UTCA_JELLEG") ("ColDesc" . 
"K�zter�let jellege") ("ColType" . "Character") ("DefaultVal" . "")) 
(("ColName" . "VALT_ID") ("ColDesc" . "V�ltoz�s k�dja") ("ColType" . 
"Integer") ("DefaultVal" . 0)) (("ColName" . "OLD_STR_DICT_ID") ("ColDesc" . "R�gi utca azonos�t�ja")
 ("ColType" . "Character") ("DefaultVal" . "")) (("ColName" . 
"OLD_SETTLEM_NAME") ("ColDesc" . "R�gi telep�l�s neve") ("ColType" . "Character") 
("DefaultVal" . "")) (("ColName" . "OLD_STREET_NAME_ID") ("ColDesc" . "R�gi utca nev azonos�t�ja") 
("ColType" . "Character") ("DefaultVal" . "")) (("ColName" . "OLD_STREET_TYPE_ID") 
("ColDesc" . "R�gi jelleg azonos�t�ja") ("ColType" . "Character") ("DefaultVal" . "")) 
(("ColName" . "STR_DICT_ID") ("ColDesc" . "V�ltozott utca azonos�t�ja") ("ColType" . 
"Character") ("DefaultVal" . "")) (("ColName" . "SETTLEM_NAME") ("ColDesc" . "V�ltozott telep�l�s neve")
 ("ColType" . "Character") ("DefaultVal" . "")) (("ColName" . "STREET_NAME_ID") 
("ColDesc" . "V�ltozott utca n�v azonos�t�ja") ("ColType" . "Character") ("DefaultVal" . "")) 
(("ColName" . "STREET_TYPE_ID") ("ColDesc" . "V�ltozott jelleg azonos�t�ja") ("ColType" . "Character") 
("DefaultVal" . "")) (("ColName" . "VALT_USER") ("ColDesc" . "V�ltoztat� konzorciumi tag")
 ("ColType" . "Integer") ("DefaultVal" . 0)) (("ColName" . 
"KZG_NAME") ("ColDesc" . "GRASS telep�l�s neve") ("ColType" . "Character") 
("DefaultVal" . "")) (("ColName" . "UTCA_ID") ("ColDesc" . "GRASS utca azonos�t�ja") 
("ColType" . "Character") ("DefaultVal" . "")) (("ColName" . "Megj") 
("ColDesc" . "Megjegyz�s") ("ColType" . "Character") ("DefaultVal" . "")) 
)))
(ade_oddefinetab lst_def) 
        (setq sset_c (ssget "x" '((0 . "circle"))))
        (if sset_c (command "_erase" sset_c ""))

       (setq nutca 0 hibauzenet 0)
       (setq telnev (getstring "Add meg a telep�l�snevet"))
       (setq sset (ssget "x" '(( 8 . "b48_utcan�v")(0 . "text"))))
       (setq i 0)
       (repeat (sslength sset)
       (setq ssn_new (ssname sset i))
       (setq lst (entget ssn_new))
       (setq p0 (getval 10 lst))
       (setq nev (getval 1 lst))
        (setq j 1 vege 0)
        (repeat (strlen nev)
         (if (= (substr nev j 1) "(")
          (setq vege (1- j))
         ) 
         (setq j (1+ j))
        )
       (if (> vege 0)
       (progn
 
       (setq nev (substr nev 1 vege))

  
       (setq lst (subst (cons 1 nev) (assoc 1 lst) lst))
                  (entmod lst)
                  (entupd ssn_new)
       )
       )
       (if (= (substr nev 1 4) "HRSZ")
       (progn
       (setq nutca (1+ nutca))
       (setq new (ade_odnewrecord "utca"))
       (ade_odattachrecord ssn_new new)
        (ade_odsetfield ssn_new "utca" "UTCA_NEV" 0 "Ismeretlen") 
        (ade_odsetfield ssn_new "utca" "UTCA_JELLEG" 0 "k�zter�let") 
        (ade_odsetfield ssn_new "utca" "VALT_ID" 0 2) 
        (ade_odsetfield ssn_new "utca" "STR_DICT_ID" 0 (strcat "UJ_" (itoa nutca))) 
        (ade_odsetfield ssn_new "utca" "SETTLEM_NAME" 0 telnev)  
        (ade_odsetfield ssn_new "utca" "STREET_NAME_ID" 0 0) 
        (ade_odsetfield ssn_new "utca" "STREET_TYPE_ID" 0 0)
        (ade_odsetfield ssn_new "utca" "VALT_USER" 0 0)
        (ade_odsetfield ssn_new "utca" "Megj" 0 nev)
        );progn
        (progn
; K�zter�let jelleg vizsg�lat
        (setq k 0 jonev 0)
        (repeat (length kozt_jell)
        (setq aktjelleg (nth k kozt_jell))
         (setq l (strlen aktjelleg))
          (if (< l (strlen nev))
            (progn
             (setq n 1)
             (repeat (1+ (- (strlen nev) l))
              (if (= (substr nev n l) aktjelleg) (setq jonev 1 ))
   ;           (print (strcat (substr nev n l) " " (itoa hibauzenet)))
              (setq n (1+ n))
             )
            )
          )
         
         (setq k (1+ k))

        )
         (if ( = jonev  0) 
          (progn
          (command "_circle" p0 1.0)
          (setq hibauzenet 1)
          )
        )
       )
       )
       (setq i (1+ i))
       )  

(c:utca_space)

(if (= hibauzenet 1) (Alert "Hib�s utcanevek tal�lhat�k az �llom�nyban"))



)
