(defun c:polyvag()
 (command "_style" "HELYRAJZI_SZ�M__JOGER�S_" "" 0.0 2.0 0.0 "n" "n" "n" )
 (command "cmdecho" 0)
 (command "_expert" 1)
 (command "_osnap" "_none")
 (command "_layer" "_make" "munka" "_make" "foldreszlet_uj" "_make" "hrsz_uj" "_make" "foldreszlet_vagott" "")
 (setq ssetpoly (ssget "x" '((0 . "lwpolyline")(8 . "foldreszlet") )))
 (setq k 0)
 (repeat (sslength ssetpoly)
 (setq ssn (ssname ssetpoly k))
 (setq hrsz (ade_odgetfield ssn "hrsz" "hrsz" 0))

 (setq lst (entget ssn))
 (setq reteg (getval 8 lst))
 (setq vtx1 (vertex_list1 ssn))
      (setq xmin 999999 ymin xmin xmax -99999 ymax xmax)
        (setq j 0)
        (repeat (length vtx1)
         (setq pxx (nth j vtx1))
         (setq xakt (nth 0 pxx))        
         (setq yakt (nth 1 pxx))        
         (minmax)
         (setq j (1+ j))
        )
        (setq xmax (+ 10.0 xmax))
        (setq ymax (+ 10.0 ymax))
        (setq xmin (- xmin 10.0 ))
        (setq ymin (- ymin 10.0 ))
       (command "_zoom" "_w" (list xmin ymin) (list xmax ymax))

  (setq sset1 (ssget "_cp"  vtx1 '((0 . "lwpolyline")(8 . "gazsav") )))
  (command "_layer" "_set" "munka" "")
  (setq j 0)
   (if sset1
   (progn

   (command "_copy" sset1 ssn "" (list 0 0) (list 0 0))
   (command "_chprop" sset1 ssn "" "_la" "munka" "")
   (command "_region" sset1 "")
   (setq sset1 (ssget "x" '(( 8 . "munka")(0 . "region"))))
   (if sset1
    (if (> (sslength sset1) 1) 
     (command "_union" sset1 "")
    )
   )
   (command "_region" ssn "")
   (setq sset1 (ssget "x" '(( 8 . "munka")(0 . "region"))))
   (if sset1
    (command "_intersect" sset1 ssn "")
   )

  (setq korbe T)
  (while korbe
  (setq sset2 (ssget "x" '(( 0 . "region")(8 . "munka"))))
     (setq j 0)
     (repeat (sslength sset2)
      (setq ssn (ssname sset2 j))
      (command "_explode" ssn)
      (print j)
      (setq j (1+ j))
     )
     (while (setq sset2 (ssget "x" '((8 . "munka")(0 . "line"))))
      (if sset2
       (progn
        (command "_pedit" (ssname sset2 0) "_y" "_j" sset2 "" "")
        (setq korbe nil)
       )
      )
     )
   )
      (setq sset2 (ssget "x" '(( 0 . "lwpolyline")(8 . "munka"))))
      (command "_chprop" sset2 "" "_la" "foldreszlet_vagott" "")
      (setq j 0)
      (repeat (sslength sset2)
       (setq ssn1 (ssname sset2 j))
       (setq vtx1 (vertex_list1 ssn1))
        (setq ii 0 xcent 0.0 ycent 0.0 nn (length vtx1))
        (repeat nn
         (setq p0 (nth ii vtx1))
         (setq xakt (nth 0 p0))
         (setq yakt (nth 1 p0))
         (setq xcent (+ xcent (/ xakt nn)))
         (setq ycent (+ ycent (/ yakt nn)))
         (setq ii (1+ ii))
        )
    (setq bent1 (ecov vtx1 (list xcent ycent)))
     (if (/= (- bent1 (* (/ bent1 2) 2)) 1) 
      (progn
       (command "_circle" (list xcent ycent) 1.0)
      )
     )
        (command "_layer" "_set" "hrsz_uj" "")
        (command "_text" "s" "HELYRAJZI_SZ�M__JOGER�S_" (list xcent ycent) 8.0 0.0 hrsz)
 
    

       (setq j (1+ j))
      )
     
   ));if sset1
 

 (setq k (1+ k))
 )
 

)
(defun ecov ( vtx p0)
  (setq ii 0)
  (setq ptest (list 0 (nth 1 p0)))
  (setq metnum 0)
  (repeat (1- (length vtx))
   (setq p1 (nth ii vtx))
   (setq p2 (nth (1+ ii) vtx))
   (setq pm (inters p1 p2 p0 ptest))
   (if pm 
    (progn
     (setq metnum (1+ metnum))
     (if (< (distance pm p0) 0.001)  (setq metnum -100))
    );progn
   );if
 
   (setq ii (1+ ii))
  );repeat
   metnum
);defun

