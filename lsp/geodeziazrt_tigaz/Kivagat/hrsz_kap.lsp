(defun c:hrsz_kap()
;HRSZ kapcsol�s a f�ldr�szletekhez
(setq ff (open "debug.fsa" "w" ))
(setq tabldefn 
    '(("tablename" . "hrsz")  
       ("tabledesc" . " ") 
       ("columns" 
            ; Define a field 
            (("colname" . "hrsz") 
             ("coldesc" . " ")  
             ("coltype" . "character") 
            ("defaultval" . " ")) 
 )))
(setq ismeretlen_hrsz 1)
;(command "_layer" "_make" "kozt_tomb" "") 
;(command "_osnap" "_none")
(command "_cmdecho" 0)
(command "raszter" "ki")
(ade_oddefinetab tabldefn ) 
 (setq sset (ssget "x" '((8 . "B22_HRSZ"))))
 (setq j 0)
 (repeat (sslength sset)
 (print j)
 (setq ssn1 (ssname sset j))
 (setq lst1 (entget ssn1))
 (setq nev (getval 1 lst1))
  (if (not nev)
   (progn
   (setq nev (strcat "hiba" (itoa ismeretlen_hrsz)))
   (setq ismeretlen_hrsz (1+ ismeretlen_hrsz))
   )
  )
 (print nev ff)
 (setq p0 (getval 10 lst1))
 (command "_zoom" "_c" p0 500)
 (setq x0 (nth 0 p0) y0 (nth 1 p0))
 (setq p1 (list 0 y0))
 (setq p2 (list (/ x0 2.0) (+ y0 1.0)))

  (setq sset1 (ssget "_cp" (list p0 p1 p2 p0) '(( 0 . "lwpolyline")(8 . "foldreszlet"))))
 (if sset1
 (progn
; (print "elso")
 (setq i 0 megvan nil ssn_tolt nil)
  (repeat (sslength sset1)
    (setq ssn (ssname sset1 i))
 ;   (print ssn ff)
    (setq lst (entget ssn))
    (setq tipus (getval 0 lst))
    (if (= tipus "POLYLINE") (setq vtx (vertex_list ssn)))
    (if (= tipus "LWPOLYLINE") (setq vtx (vertex_list31 ssn)))
    (setq bent1 (ecov vtx p0))
    (if (= (- bent1 (* (/ bent1 2) 2)) 1) 
      (progn
        (if ssn_tolt
         (progn
          (print ssn_tolt ff)
          (print (nth 0 vtx) ff)
          (command "_area" "_o" ssn_tolt)
          (setq ter_old (getvar "area"))
          (command "_area" "_o" ssn)
          (setq ter_new (getvar "area"))
          (if (< ter_new  ter_old )
           (setq ssn_tolt ssn)
          )
         )
         (progn
          (setq ssn_tolt ssn)
          (print ssn_tolt ff)
         )
        )
      )
     )
      (setq i (1+ i))
  );
 ;        (setq hrsz  (ade_odgetfield ssn "hrsz" "hrsz" 0))

       (if ssn_tolt
        (progn
       (setq new (ade_odnewrecord "hrsz"))
       (ade_odattachrecord ssn_tolt new)
       (ade_odsetfield ssn_tolt "hrsz" "hrsz" 0 nev) 
       (print "beiras1: " ff)
       (print status ff)
       (command "_chprop" ssn1 "" "_c" 1 "")
       (setq megvan T)
       )
       (progn
        (command "_circle" p0 1.0)
       )
       )
 

 )
 )
 (if (or (not sset1)  (not megvan))
 (progn
  (setq sset1 (ssget "x"  '(( 0 . "lwpolyline")(8 . "foldreszlet"))))
 (if sset1
 (progn
; (print "Masodik")
 (setq i 0 megvan nil ssn_tolt nil)
  (repeat (sslength sset1)
    (setq ssn (ssname sset1 i))
 ;   (print ssn)
    (setq lst (entget ssn))
    (setq tipus (getval 0 lst))
    (if (= tipus "POLYLINE") (setq vtx (vertex_list ssn)))
    (if (= tipus "LWPOLYLINE") (setq vtx (vertex_list31 ssn)))

     (setq bent1 (ecov vtx p0))
    (if (= (- bent1 (* (/ bent1 2) 2)) 1) 
      (progn
        (if ssn_tolt
         (progn
          (print ssn_tolt ff)
          (print (nth 0 vtx) ff)
          (command "_area" "_o" ssn_tolt)
          (setq ter_old (getvar "area"))
          (command "_area" "_o" ssn)
          (setq ter_new (getvar "area"))
          (if (< ter_new ter_old) (setq ssn_tolt ssn))
         )
         (progn
          (setq ssn_tolt ssn)
          (print ssn_tolt ff)
         )
        )
      )
     )
      (setq i (1+ i))
  );
       (if ssn_tolt
        (progn
       (setq new (ade_odnewrecord "hrsz"))
       (ade_odattachrecord ssn_tolt new)
       (setq status (ade_odsetfield ssn_tolt "hrsz" "hrsz" 0 nev))
       (print "beiras2: " ff)
       (print status ff)
       (command "_chprop" ssn1 "" "_c" 2 "")
       (setq megvan T)
       )
       (progn
        (command "_circle" p0 1.0)
       )
       )
 )
 (progn
  (command "_circle" p0 1.0)
 )
 )
 )
 )

  (setq j (1+ j))
 )
(close ff)
)
(defun ecov ( vtx p0 / j)
;  (print "Belep")
  (setq ii 0 levon 0)
  (setq ptest (list 0 (nth 1 p0)))
  (setq metnum 0)
  (repeat (1- (length vtx))
   (setq p1 (nth ii vtx))
   (setq p2 (nth (1+ ii) vtx))
   (setq pm (inters p1 p2 p0 ptest))
 ;  (print "Metszespont")
;   (print pm)
   (if pm 
    (progn
     (setq j 0)
     (repeat (1- (length vtx))
      (setq pt (nth j vtx))
;      (print (distance pt pm))
      (if (< (distance pt pm) 0.0001) 
       (progn
        (setq levon (1+ levon))
;        (print "Egybeeses")
;        (print pt)
;        (print (strcat (rtos (distance pt pm) 3 2) " " (itoa levon)))
           
       )
      )
      (setq j (1+ j))
     )
     (setq metnum (1+ metnum))
     (if (< (distance pm p0) 0.001)  (setq metnum -100))
    );progn
   );if
 
   (setq ii (1+ ii))
  );repeat
;   (print (- metnum (/ levon 2)))
    metnum
);defun
(defun vertex_list31 ( en / ed vertex ret )
;
; input:	entity name
; output:	list of vertices if the entity was a polyline
;		nil otherwise
;
 (setq ed (entget en) vertex nil)

 (SETQ AKTENT (GETVAL 0 ED)) 
 (setq zarte (getval 70 ed))
; (PRINT AKTENT)
(if (= AKTENT "LWPOLYLINE")
   (progn
   (setq ii 0)
   (repeat (length ed)
   (setq akt (nth ii ed))
   (if (= (car akt) 10)
    (progn
     (setq xy (cdr akt))
     (setq vertex (cons xy vertex) )
    )
   )
   (setq ii (1+ ii))
  );repeat
 );progn
 );if
; (PRINT 'KESZPOLY)
 (if (= AKTENT "LINE")
 (PROGN
 ;(PRINT 'EZLINE)
 (setq vertex (cons (getval 10 ed) vertex))
 ;(PRINT 'KEZDOP)
 (setq vertex (cons (getval 11 ed) vertex))
 ;(PRINT VERTEX)
 ;(PRINT 'VEGP)
 ) ; PROGN
 ); line
; (PRINT 'LINEVEG)
  (if (or (= zarte 1) (= zarte 129))
    (progn
 ;    (print "\nZart poligon")
     (setq vertex (cons (last vertex) vertex))
    ) 
   )
 (if vertex (setq ret (reverse vertex)) )
)


(prin1)


