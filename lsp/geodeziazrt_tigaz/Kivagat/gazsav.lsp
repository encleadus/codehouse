(defun c:gazsav()

(setq itr_id (tpm_iterstart))
;(command "_osnap" "_none")
(command "_layer" "_m" "munka" "_make" "hiba" "")
;(setq ff (open "debug1.fsa" "w"))
(setq done nil)
(if (null itr_id)
    (prompt "\nERROR: Unable to start topology iterator.") 
    (while (not done) 
       (if (null (tpm_iternext itr_id)) 
          (setq done T) 
          (progn 
             (print (tpm_itername itr_id))
            (if (= (substr (tpm_itername itr_id) 1 12) "gaznyomvonal")
             (progn
              (setq tpm_id (tpm_acopen (tpm_itername itr_id)))
    ;          (print tpm_id)
              (if (tpm_acclose tpm_id)
               (print "Topologia lez�r�s sikeres")
               (print "Hiba a topologia lez�r�s�n�l")
              )
              (if (tpm_acunload (tpm_itername itr_id))
               (print "Topologia unload sikeres")
               (print "Hiba a topologia unloadban")
              )
              (tpm_mnterase (tpm_itername itr_id))
 ;             (print (strcat "T�r�lve:" (tpm_itername itr_id)))
             )
            )
            
          )  ; progn
       )  ; if
    )  ; while 
)  ; if 

(setq sset0 (ssget "x" '((-4 . "<or")(8 . "gaz-gf-gerinc")
                                     (8 . "gaz-gfk-gerinc")
                                     (8 . "gaz-gfnk-gerinc")
                                     (8 . "gaz-gfn-gerinc")
                         (-4 . "or>")(-4 . "<or")(0 . "lwpolyline")(0 . "line")(-4 . "or>"))))
(setq i 0)
(command "_LAYER" "_MAKE" "gazsav" "_set" "gazsav" "")
(command "_color" 1)
(setq sset (ssadd))
(repeat (sslength sset0)
(setq ssn (ssname sset0 i))
;(print ssn ff)
(setq lst (entget ssn))
(setq reteg (getval 8 lst))
(setq vtx (vertex_list1 ssn))
  (setq xmin 999999 ymin xmin xmax -99999 ymax xmax)
    (setq j 0)
    (repeat (length vtx)
      (setq pakt (nth j vtx))
      (setq xakt (nth 0 pakt))        
      (setq yakt (nth 1 pakt))
      (minmax)
     (setq j (1+ j))
    )
;    (command "_zoom" "_w" (list (- xmin 3) (- ymin 3)) (list (+ xmax 3) (+ ymax +3)))

(setq sset (ssadd))
(setq sset (ssadd ssn sset)) 
(setq nev (strcat "gaznyomvonal" (itoa i)))
(setq var_id (tpm_varalloc))
(setq status (tpm_mntbuild var_id nev " " 2 nil sset nil))
  (if status 
    (print " A vezet�k topologia l�trej�tt")
    (print " Hiba a topol�gia l�trehoz�s�n�l")
  )

(setq offset "20")


 
(setq tpm_id (tpm_acopen nev))
(if (not (tpm_anabuffer tpm_id offset var_id nil nil))
 (print "Hib�s poligon k�sz�t�s")
)
(setq i (1+ i))
)

)

  (defun minmax ()
    (if (< xakt xmin) (setq xmin xakt))
    (if (< yakt ymin) (setq ymin yakt))
    (if (> xakt xmax) (setq xmax xakt))
    (if (> yakt ymax) (setq ymax yakt))
   )

(defun pont_vonal_tav(sp0 ep0 p1)

    (setq tav 100000000)

    (setq szog_nor  (+ (angle sp0 ep0) (/ pi  2.0)))
    (setq p0sz (polar p1  szog_nor 1000000))
    (setq p1sz (polar p1  (+ szog_nor pi) 1000000))
     (setq pm (inters sp0 ep0 p0sz p1sz 1))

      (If pm
       (setq tav  (distance p1 pm))
      )
; (print (strcat "Bent: " (rtos tav 2 2)) ff)
  tav 
)
  (defun getpp(p0 / p1 p2 p3 p4 k)
   (setq x (nth 0 p0) y (nth 1 p0))
    (setq p01 (list (- x 0.3) (- y 0.3)))
   (setq p02 (list (- x 0.3) (+ y 0.3)))
   (setq p03 (list (+ x 0.3) (+ y 0.3)))
   (setq p04 (list (+ x 0.3) (- y 0.3)))
   (command "_zoom" "_c" p0 1.0)
   (setq sset31 (ssget "_cp" (list p01 p02 p03 p04 p01 ) '((-4 . "<or") ( 0 . "line") (0 . "lwpolyline")(-4 . "or>")(8 . "munka"))))
;    (setq sset3 (ssget "_f" (list p02 p04 ) '((-4 . "<or") ( 0 . "line") (0 . "lwpolyline")(-4 . "or>")(8 . "munka"))))
;   (setq ssn31 (ssname sset31 0))
 ;  (grread)
   (if sset31
   (progn
   (setq k 0)
   (repeat (sslength sset31)
;    (print k)
    (setq ssn31 (ssname sset31 k))
;    (print "break indul")
    (command "_break" ssn31 p0 p0)
 ;   (print "break k�sz")
     (setq k (1+ k))
   )
 
  (setq sset3 (ssget "x" '(( 8 . "munka"))))
 (setq j 0 )
  (repeat (sslength sset3)
   (setq ssn3 (ssname sset3 j))
   (setq vtx3 (vertex_list1 ssn3))
    (setq k 0 phossz 0.0)
    (repeat (1- (length vtx3))
     (setq tav (distance (nth k vtx3) (nth (1+ k) vtx3)))
     (setq phossz (+ tav phossz))
     (setq k (1+ k))
    )
   (if (< phossz 0.0001) 
    (progn
     (entdel ssn3)
   (setq sset31 (ssget "_cp" (list p01 p02 p03 p04 p01 ) '((-4 . "<or") ( 0 . "line") (0 . "lwpolyline")(-4 . "or>")(8 . "munka"))))

   (setq k 0)
   (repeat (sslength sset31)
 ;   (print k)
    (setq ssn31 (ssname sset31 k))
;     (print "potbreak indul")
    (command "_break" ssn31 p0 p0)
;      (print "potbreak k�sz")
     (setq k (1+ k))
   )

    )
   )
    
   (setq j (1+ j))
  )
  );progn
  (progn

   (command "_layer" "_set" "hiba" "")
   (command "_circle" p0 1.0)
   (command "_layer" "_set" "munka" "")
  )
  );
  )

