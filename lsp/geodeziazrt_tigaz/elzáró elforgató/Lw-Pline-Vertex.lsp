(defun Lw_pline_vertex ( Ent 
                    / EnL aa bb
                    )
;;; Egy vonal vagy vonallánc sarokpontjait adja vissza 
;;; Input: Rajzelem név
;;; Output: (list vertex vertex vertex)

  (if (not Ent)
    (progn
      (setq Ent (car (entsel "\nVálasszon vonalláncot: ")))
      (if Ent
        (progn
          (setq EnL (entget Ent))
          (if (not (or (= (cdr (assoc 0 EnL)) "LINE")
                       (= (cdr (assoc 0 EnL)) "POLYLINE")
                       (= (cdr (assoc 0 EnL)) "LWPOLYLINE")
                   )
              )
            (setq Ent Nil)
          )
        )
      )
    )
  )
  (if Ent
    (progn
      (setq EnL (entget Ent))
      (setq Closed (cdr (assoc 70 EnL)))
      (cond
        ((= (cdr (assoc 0 EnL)) "LINE")
          (setq bb (append bb (list (trans (cdr (assoc 10 EnL)) 0 1))
                              (list (trans (cdr (assoc 11 EnL)) 0 1))
                   )
          )
        )
        ((= (cdr (assoc 0 EnL)) "LWPOLYLINE")
          (setq ZZ (cdr (assoc 38 EnL)))
           (foreach aa EnL
            (if (= (car aa) 10)
              (if (= (length (cdr aa)) 2)
                (setq bb (append bb (list (append (reverse (cdr (reverse (trans (cdr aa) 0 1)))) (list ZZ)))))
                (setq bb (append bb (list (trans (cdr aa) 0 1))))
              )
            )
          )
          (if (= Closed 1)
            (setq bb (append bb (list (car bb))))
          )
        )
        ((= (cdr (assoc 0 EnL)) "POLYLINE")
          (while (/= (cdr (assoc 0 EnL)) "SEQEND")
            (if (= (cdr (assoc 0 EnL)) "VERTEX")
              (setq bb (append bb (list (trans (cdr (assoc 10 EnL)) 0 1))
;;;                                  (list (cdr (assoc 42 EnL)))
                       )
              )
            )
            (setq EnL (entget (entnext (cdr (assoc -1 EnL)))))
          )
           (if (= Closed 1)
            (setq bb (append bb (list (car bb))))
          )
        )
      )
    )
  )
  bb
)
