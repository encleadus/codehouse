;           
;  gaz-elz jelkulcs lerak�s le�gaz�s v�g�re (oda is rak, ahol m�r van �gy ott ketto lesz, elotte t�r�lni kell!)
;           
(defun c:elz()
(setvar "CMDECHO" 0)
  (setq	lof
	 (ssget
	   "x"
	   (list
	     (cons
	       8
	       "GAZ-Gf-LEAGAZAS,GAZ-Gfk-LEAGAZAS,GAZ-Gfn-LEAGAZAS,GAZ-Gfnk-LEAGAZAS"
	     )
	     (cons 0 "LINE,LWPOLYLINE")
	   )
	 )
  )
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq p10 nil)
	(setq p11 nil)
	(if
	  (and
	    (/= 1 (cdr (assoc 70 (entget (ssname lof j)))))
	    (= "LWPOLYLINE" (cdr (assoc 0 (entget (ssname lof j)))))
	  )
	   (progn
	     (setq entli (ssname lof j))
	     (make-vert-list entli)
	     (setq p10 (nth 0 vert-list))
	     (setq p11 (last vert-list))
	   )
	)
	(if (= "LINE" (cdr (assoc 0 (entget (ssname lof j)))))
	  (progn
	    (setq entli (ssname lof j))
	    (setq p10 (cdr (assoc 10 (entget (ssname lof j)))))
	    (setq p11 (cdr (assoc 11 (entget (ssname lof j)))))
	  )
	)
	(setq lay_leag (cdr (assoc 8 (entget (ssname lof j)))))
	(setq lay_trim (vl-string-trim "LEAGAZAS" lay_leag))
	(setq lay_trim (strcat "GAZ" lay_trim))
	(setq lay_gerinc (strcat lay_trim "GERINC"))
	(command "_layer" "_m" "gaz_tmp" "_c" "1" "" "")
	(command "_change" entli "" "_p" "_la" "gaz_tmp" "")
	(setq puffer 0.3)
	(setq p1 p10)
	(selvez)
	(if amer
	  (progn
	    (setq p1 p11)
	    (selvez)
	    (if	(= amer nil)
	      (progn
		(selins)
		(if (= amer nil)
		  (progn
		    (setq lay_jelk (strcat lay_trim "SZERELVENY"))
		    (command "_layer" "_s" lay_jelk "")
		    (setq p2 p10)
		    (setq p_angle (angle p2 p1))
		    (setq p_angle-meroleges (+ p_angle (/ pi 2)))
		    (setq szog (/ (* 180.0 p_angle-meroleges) pi))
		    (command "_insert" "gaz-elz" p1 "0.5" "0.5" szog)
		  )
		)
	      )
	    )
	  )
	  (progn
	    (selins)
	    (if	(= amer nil)
	      (progn
		(setq lay_jelk (strcat lay_trim "SZERELVENY"))
		(command "_layer" "_s" lay_jelk "")
		(setq p2 p11)
		(setq p_angle (angle p2 p1))
		(setq p_angle-meroleges (+ p_angle (/ pi 2)))
		(setq szog (/ (* 180.0 p_angle-meroleges) pi))
		(command "_insert" "gaz-elz" p1 "0.5" "0.5" szog)
	      )
	    )
	  )
	) ;if
	(command "_change" entli "" "_p" "_la" lay_leag "")
	(setq j (+ j 1))
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
      ) ;while
    ) ;progn
  ) ;if
  (setvar "CMDECHO" 1)
) ;defun

(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list

(defun selvez ()
  (setq sp11 (polar p1 (/ pi 4) puffer))
  (setq sp13 (polar p1 (* 5 (/ pi 4)) puffer))
  (command "_zoom" "_c" p1 "30")
  (setq	amer (ssget "_C"
		    sp11
		    sp13
		    (list (cons -4 "<OR")
			  (cons 8 lay_gerinc)
			  (cons 8 lay_leag)
			  (cons -4 "OR>")
		    )
	     )
  )
)

(defun selins ()
  (setq sp11 (polar p1 (/ pi 4) puffer))
  (setq sp13 (polar p1 (* 5 (/ pi 4)) puffer))
  (command "_zoom" "_c" p1 "30")
  (setq	amer
	 (ssget
	   "_C"
	   sp11
	   sp13
	   (list
	     (cons 0 "INSERT")
	     (cons
	       2
	       "gaz-eant,gaz-eanpg,gaz-eanto,gaz-eanszv,gaz-eanbv,gaz-eang,gaz-eanrg,gaz-eanrgv,gaz-elz,gaz-sapka,gaz-dugo,gaz-kIBS"
	     )
	   )
	 )
  )
)

(defun selinsg ()
  (setq sp11 (polar p1 (/ pi 4) puffer))
  (setq sp13 (polar p1 (* 5 (/ pi 4)) puffer))
  (command "_zoom" "_c" p1 "30")
  (setq	amer
	 (ssget
	   "_C"
	   sp11
	   sp13
	   (list
	     (cons 0 "INSERT")
	   )
	 )
  )
)