(defun c:para ( ) 
  (alert
     " Parancsok:\n
    fel	- Terepmmagass�gk�nt felvett felmen� magass�gok keres�se �s jav�t�sa az �sszes nyom�sfokozaton
	  Amit nem tud �rtelmezni oda hibak�rt tesz
    kk	- hibak�r keres�, v�gigl�pked az �llom�nyban tal�lhat� k�r�k�n, a m�r vizsg�ltat t�rli
    para	- ez az ablak
    
"
)
)


(defun c:fel ()
   (setq puffer 1)
   (setvar "CMDECHO" 0)
   (command "_osnap" "_non") 
   (command "_layer" "_m" "_terepmag_hiba" "_c" "_white" "" "")
   (command "_layer" "_s" "_terepmag_hiba" "")
   (command "_color" "_bylayer" )
   (command "_zoom" "_e")
   (setq torol (ssget "x"
		     '((0 . "CIRCLE") (8 . "_terepmag_hiba"))
	      )
  )
  (if torol (command "erase" torol ""))
   ; (setq entx (car (entsel "\n Nyomj a !")))
   (setq lof nil)
   (setq lof (ssget "x" '((-4 . "<AND")(0 . "TEXT")(-4 . "<OR")(8 . "GAZ-Gfk-TEREPMAG-FELIRAT")(8 . "GAZ-Gf-TEREPMAG-FELIRAT")(8 . "GAZ-Gfn-TEREPMAG-FELIRAT")(8 . "GAZ-Gfnk-TEREPMAG-FELIRAT")(-4 . "OR>")(-4 . "AND>"))))

   (if lof
    (progn
     (setq hlof (sslength lof))
      (setq j 0)
      (command "_justifytext" "_a" lof "" "_left")
      (while (< j hlof)
         
        (setq entx (ssname lof j))
        (setq tlist (entget entx))

        (setq pstart (cdr (assoc 10 tlist)))
	(setq t-angle (cdr (assoc 50 tlist)))
	(setq layer (cdr (assoc 8 tlist)))
         
        (command "_zoom" "_c" pstart "30")
        (setq sp10 (polar pstart (+ t-angle (* 6 (/ pi 4))) 0.05))
        (setq sp11 (polar sp10 t-angle puffer))
        (setq sp13 (polar pstart (+ t-angle (* 6 (/ pi 4))) puffer))
        (setq sp12 (polar pstart (+ t-angle (* 7 (/ pi 4))) (* 1.5 puffer)))
;(command "_color" "_red" )        
;(command "_point" sp10)
;(command "_color" "_green" )
;        (command "_point" sp11)
;(command "_color" "_blue" )
;        (command "_point" sp12)
;(command "_color" "_cyan" )
  ;      (command "_point" sp13)
        (setq len (- (strlen layer) 17))
        (setq lay2 (strcat (substr layer 1 len) "-FELMAG-FELIRAT"))
        ;(getstring layer)
        (setq vo nil)
        (setq vo (ssget "_F" (list sp10 sp13 sp12 sp11) (list (cons 0 "TEXT") (cons 8 layer))))
        (setq vb nil)
        (setq vb (ssget "_F" (list sp10 sp13 sp12 sp11) (list (cons 0 "LINE,LWPOLYLINE,MLINE") (cons 8 layer))))
        
	(if (and vo (not vb)) (setq tlist (subst (cons 8 lay2)(assoc 8 tlist) tlist)) (if (and vo vb) (command "circle" pstart 5) (if (and (not vo) (not vb)) (command "circle" pstart 5)) ) )
	
        
	(entmod tlist)
        (setq j (+ j 1))
        (princ (strcat "\n " (itoa j) "/" (itoa hlof)))
        
)
)
)
(setvar "CMDECHO" 1)
)


(defun c:kk ()
  (setq zoomk 30)
 ;(command "_zoom" "e")
  (setq circle-list (ssget "x" '((0 . "circle"))))
  (if circle-list
    (progn
      (setq h-circle (sslength circle-list))
      (setq circlex (ssname circle-list 0))
      (setq pt1 (cdr (assoc 10 (entget circlex))))
      (command "_zoom" "c" pt1 zoomk)
      (print (strcat "meg: " (itoa h-circle) "db van!"))
      (getstring "\nTorlom!")
      (command "erase" (ssname circle-list 0) "")
      (ssdel circlex circle-list)
      (setq h-circle (sslength circle-list))
    )
    (alert "Nincs benne t�bb hibak�r!")
  )
) ;defun kk
(c:para)