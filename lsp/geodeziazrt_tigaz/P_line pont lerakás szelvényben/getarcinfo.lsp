(defun getArcInfo (segment / a p1 bulge p2 c c|2 gamma midp p phi r r2 s theta)
  ;; assign variables to values in argument
  (mapcar 'set '(p1 bulge p2) segment)
  ;; find included angle
  ;; remember that bulge is negative if drawn clockwise
  (setq theta (* 4.0 (atan (abs bulge))))
  ;; output included angle
  (princ (strcat "\n Included angle: "
                 (rtos theta)
                 " rad ("
                 (angtos theta 0)
                 " degrees)"
         )
  )
  ;; find height of the arc
  (setq c (distance p1 p2)
        s (* (/ c 2.0) (abs bulge))
  )
  ;; output height of arc
  (princ (strcat "\n Height of arc:  " (rtos s)))
  ;; output chord length
  (princ (strcat "\n Chord length:   " (rtos c)))
  ;; If this function is used without making sure that the segment
  ;; is not simply a line segment (bulge = 0.0), it will produce
  ;; a division-by-zero error in the following. Therefore we want
  ;; to be sure that it doesn't process line segments.
  (cond ((not (equal bulge 0.0 1E-6))
         ;; find radius of arc
         ;; first find half the chord length
         (setq c|2 (/ c 2.0)
               ;; find radius with Pythagoras (used as output)
               r   (/ (+ (expt c|2 2.0) (expt s 2.0)) (* s 2.0))
               ;; find radius with trigonometry
               r2  (/ c|2 (sin (/ theta 2.0)))
         )
         (princ (strcat "\n Radius of arc:  " (rtos r)))
         ;; find center point of arc with angle arithmetic
         ;; (used as output)
         (setq gamma (/ (- pi theta) 2.0)
               phi   (if (>= bulge 0)
                       (+ (angle p1 p2) gamma)
                       (- (angle p1 p2) gamma)
                     )
               p     (polar p1 phi r)
         )
         ;; find center point of arc with Pythagoras
         (setq a    (sqrt (- (expt r 2.0) (expt c|2 2.0)))
               midp (polar p1 (angle p1 p2) c|2)
               p2   (if (>= bulge 0)
                      (polar midp (+ (angle p1 p2) (/ pi 2.0)) a)
                      (polar midp (- (angle p1 p2) (/ pi 2.0)) a)
                    )
         )
         ;; output coordinates of center point
         (princ (strcat "\n Center of arc:  "
                        (rtos (car p))
                        ","
                        (rtos (cadr p))
                )
         )
        )
        (T (princ "\n Segment has no arc info"))
  )
  (princ)
)
(defun c:indul ()
(setq a (list (list 6 7) 0.1 (list 8 5)))
(getArcInfo a)


)
 
