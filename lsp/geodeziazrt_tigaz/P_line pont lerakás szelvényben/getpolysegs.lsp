(defun C:gs (/ ent entl p1 pt bulge seg ptlst)
  (setvar "ERRNO" 0)
  ;; repeat request for polyline until user either picks
  ;; a polyline or exits without picking
  (while (and (not ent) (/= (getvar "ERRNO") 52))
    (if (and (setq ent (car (entsel "\nSelect polyline: ")))
             (/= (cdr (assoc 0 (setq entl (entget ent)))) "LWPOLYLINE")
        )
      (setq ent nil)
    )
  )
  (cond (ent
         ;; save start point if polyline is closed
         (if (= (logand (cdr (assoc 70 entl)) 1) 1)
           (setq p1 (cdr (assoc 10 entl)))
         )
         ;; run thru entity list to collect list of segments
         (while (setq entl (member (assoc 10 entl) entl))
           ;; if segment then add to list
           (if (and pt bulge)
             (setq seg (list pt bulge))
           )
           ;; save next point and bulge
           (setq pt    (cdr (assoc 10 entl))
                 bulge (cdr (assoc 42 entl))
           )
           ;; if segment is build then add last point to segment
           ;; and add segment to list
           (if seg
             (setq seg (append seg (list pt))
                   ptlst (cons seg ptlst))
           )
           ;; reduce list and clear temporary segment
           (setq entl  (cdr entl)
                 seg   nil
           )
         )
        )
  )
  ;; if polyline is closed then add closing segment to list
  (if p1 (setq ptlst (cons (list pt bulge p1) ptlst)))
  ;; reverse and return list of segments
  (reverse ptlst)
)
