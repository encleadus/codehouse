(defun c:para ( ) 
  (alert
     " Parancsok:\n
    forga - Text vonalra forgatása
    forgb - Text vonalra forgatása (fordított)
    para - ez az ablak
"
)
)

(defun c:forga ()
    (setvar "CMDECHO" 0)

    (setq ent1 (car (entsel "\n Nyomj a Text-re!")))
    (setq ent2 (car (entsel "\n Nyomj a Vonalra!")))
    (setq tlist (entget ent1))
    (setq tlist2 (entget ent2))
    

    (setq t-angle (cdr (assoc 50 tlist)))

    (setq pstart (cdr (assoc 10 tlist2)))
    (setq pend (cdr (assoc 11 tlist2)))
    (setq szog   (angle pstart pend))
    (setq tlist (subst (cons 50 szog)(assoc 50 tlist) tlist)); 
    (entmod tlist)
    (setvar "CMDECHO" 1)

)

(defun c:forgb ()
    (setvar "CMDECHO" 0)
    
    (setq ent1 (car (entsel "\n Nyomj a Text-re!")))
    (setq ent2 (car (entsel "\n Nyomj a Vonalra!")))
    (setq tlist (entget ent1))
    (setq tlist2 (entget ent2))
    

    (setq t-angle (cdr (assoc 50 tlist)))

    (setq pstart (cdr (assoc 10 tlist2)))
    (setq pend (cdr (assoc 11 tlist2)))
    (setq szog   (angle pend pstart))
    

    (setq tlist (subst (cons 50 szog)(assoc 50 tlist) tlist)); 
    (entmod tlist)
    (setvar "CMDECHO" 1)

)
(c:para)
