(defun c:d()
; 1. a k�t �rintett oldal felez�pontjain �tmen� egyenesek metsz�spontja
; 2. a k�t egyenes k�z�s pontj�nak m�sik v�gpontjait �sszek�t� egyenes felez�pontja
; 3. a sz�gfelez� �s a k�r metsz�spontja
(command "_osnap" "_none")
 (print "V�lassz k�t oldalt")
(setq sset (ssget))
 (if (> (sslength sset) 2) 
   (progn
     (alert "T�bb oldalt v�lasztott�l")
     (break)
   )
 )
(setq ssn1 (ssname sset 0))
(setq lst1 (entget ssn1))
(setq p001 (getval 10 lst1))
(setq p002 (getval 11 lst1))
(setq ssn2 (ssname sset 1))
(setq lst2 (entget ssn2))
(setq p003 (getval 10 lst2))
(setq p004 (getval 11 lst2))
(setq t1 (distance p001 p003))
(setq t2 (distance p001 p004))
(setq t3 (distance p002 p003))
(setq t4 (distance p002 p004))
(if (= t1 0.0)
  (progn
   (setq pelozo p002 pkovet p004 p0 p001)
  )
)
(if (= t2 0.0)
  (progn
   (setq pelozo p002 pkovet p003 p0 p001)
  )
)
(if (= t3 0.0)
  (progn
   (setq pelozo p001 pkovet p004 p0 p002)
  )
)
(if (= t4 0.0)
  (progn
   (setq pelozo p001 pkovet p003 p0 p002)
  )
)

 
   (setq k_p p0   
        v_p pelozo            
                  alap_vektor (vec1 k_p v_p)                   ; egysegnyi hosszu
                  normalis (nor alap_vektor nil)
                  ellen (nor normalis nil)
                  norellen (nor ellen nil)
                  egys_vektor (vec*k  alap_vektor 5000)     ; egysegnyi ugras

      )
       (setq pkozx (+ (/ (nth 0 k_p) 2.0) (/ (nth 0 v_p) 2.0)) )
       (setq pkozy (+ (/ (nth 1 k_p) 2.0) (/ (nth 1 v_p) 2.0)) )
       (setq pk (list pkozx pkozy 0.0))
               (setq p1a (VEC+ (vec*k normalis 5000) pk))
               (setq p2a (VEC+ (vec*k norellen 5000) pk))
  (setq k_p p0   
        v_p pkovet            
                  alap_vektor (vec1 k_p v_p)                   ; egysegnyi hosszu
                  normalis (nor alap_vektor nil)
                  ellen (nor normalis nil)
                  norellen (nor ellen nil)
                  egys_vektor (vec*k  alap_vektor 5000)     ; egysegnyi ugras

      )
       (setq pkozx (+ (/ (nth 0 k_p) 2.0) (/ (nth 0 v_p) 2.0)) )
       (setq pkozy (+ (/ (nth 1 k_p) 2.0) (/ (nth 1 v_p) 2.0)) )
      (setq pk (list pkozx pkozy 0.0))
               (setq p1b (VEC+ (vec*k normalis 5000) pk))
               (setq p2b (VEC+ (vec*k norellen 5000) pk))
;(command "_line" p1a p2a "")
;(command "_line" p1b p2b "")
               (setq pm (inters p1a p2a p1b p2b nil))  ; sz�gfelez� egyik pontja a m�sik a p0
  ;             (setq szog1 (angle pm p0))
  ;             (setq p0uj (polar p0 szog1 500))
  ;             (setq szog2 (angle p0 pm))
  ;             (setq pmuj (polar pm szog2 500))
  ;             (setq pm pmuj p0 p0uj)
       (setq pelozo_x (nth 0 pelozo))
       (setq pelozo_y (nth 1 pelozo))
       (setq pkovet_x (nth 0 pkovet))
       (setq pkovet_y (nth 1 pkovet))
       (setq pkorx (+ (/ pelozo_x 2.0) (/ pkovet_x 2.0)) )
       (setq pkory (+ (/ pelozo_y 2.0) (/ pkovet_y 2.0)) )
       (setq r (/ (distance pelozo pkovet) 2.0))
;       (command "_circle" (list pkorx pkory) r)
;       (setq x1 (- pelozo_x pkorx))
;       (setq y1 (- pelozo_y pkory))
        (setq x1 0.0 y1 0.0)
       (setq x2 (- (nth 0 p0) pkorx))
       (setq y2 (- (nth 1 p0) pkory))
       (setq dx (- x2 x1))
       (setq dy (- y2 y1))
       (setq dr  (+ (* dx dx) (* dy dy)))
       (setq nagyd (- (* x1 y2) (* x2 y1)))
       (if (< dy 0.0)
        (setq dxuj (* dx -1.0))
        (setq dxuj  dx)
        
       )
       (setq tag1  (sqrt (- (* (* r r) dr) (* nagyd nagyd)) ))
       (setq xm1 (/ (+ (* nagyd dy) (* dxuj tag1)) dr))
       (setq xm2 (/ (- (* nagyd dy) (* dxuj tag1)) dr))
       (setq ym1 (/ (+( - 0  (* nagyd dx)) (* (abs dy) tag1) ) dr))
       (setq ym2 (/ (- ( - 0  (* nagyd dx)) (* (abs dy) tag1) ) dr))
       (setq puj1 (list (+ xm1 pkorx) (+ ym1 pkory) 0.0))
       (setq puj2 (list (+ xm2 pkorx) (+ ym2 pkory) 0.0))
       (if (< (distance puj1 p0) (distance puj2 p0))
         (progn
         (print "puj1")
         (setq puj puj1)
          )
          (progn
         (setq puj puj2)
          (print "puj2")
          )
       )
 ;      (command "_line" puj1 puj2 "")



(setq szin (getval 62 lst1))
(setq reteg (getval 8 lst1))
(if (not szin) (command "_color" "bylayer"))
(command "_layer" "_set" reteg "")
   (command "_line" pelozo puj pkovet "")
   (command "_erase" ssn1 ssn2 "")



)

