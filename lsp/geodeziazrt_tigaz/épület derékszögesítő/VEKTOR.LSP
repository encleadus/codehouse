;|
様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様
  3D VEKTOR SZAMITASI RUTINOK
  Copyright 1994, TiSoft Co. Ltd.

  Az �sszes rutin szabadvektorokkal dolgozik, ezt a sz�m�t�sokn�l
  figyelembe kell venni.
  Egy vektort az (x y z) lista jellemez hasonl�an mint egy pont coordin�t�ja.
  A rutinok nem v�geznek param�ter ellen�rz�st.

. (vec p1 p2)   vektor
. (vecabs v)    abszol�t �rt�k
. (vec1 p1 p2)  egys�gvektor
. (vec*k v k)   vektor szorz�sa skal�rissal
. (vec/k v k)   vektor oszt�sa skal�rissal
. (vec+ v1 v2)  �sszead�s
. (vec- v1 v2)  kivon�s
. (vec* v1 v2)  skal�ris szorz�s
. (vecx v1 v2)  vektori�lis szorz�s
  (abc v1 v2 v3) vegyes szorzat a*(b X c)
. (nor v1 v2)   norm�l vektor
. (trn v m)     transzform�l�s transzform�l� m�trixxal
様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様
  MATRIX SZAMITASI RUTINOK
  Copyright 1994, TiSoft Co. Ltd.

  Az �sszes m�trix sz�m�t�si rutin n�gyzetes m�trixokkal dolgozik.
  Ha valamelyik operandus nem n�gyzetes akkor, az (mtrx_ext n) rutinnal
  n dimenzi�j� n�gyzetes m�trixx� tehet�. Az operandusoknak azonos
  dimenzi�j�aknak kell lenni�k.

  (mtrx_ext n)   m�trix kiterjeszt�se n x n elem�re
  (det m)        determin�ns
  (mtrx1 n)      n x n elem� egys�gm�trix
  (mtrx*k m k)   m�trix szorz�sa skal�rissal
  (mtrx/k m k)   m�trix oszt�sa skal�rissal
  (mtrx+ m1 m2)  �sszead�s
  (mtrx- m1 m2)  kivon�s
. (mtrx* m1 m2)  szorz�s
様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様
  3D VEKTOR SZAMITASI RUTINOK
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (vec p1 p2)   vektor  �s
  (vec- v1 v2)  kivon�s
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun vec (p1 p2)
    (list (- (car p2) (car p1))
          (- (cadr p2) (cadr p1))
          (- (caddr p2) (caddr p1))
    )
)
(defun vec2 (p1 p2)
    (list (- (car p2) (car p1))
          (- (cadr p2) (cadr p1))
    )
)


;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (vecabs v)       abszol�t �rt�k
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun vecabs (v)
    (sqrt (+ (* (car v) (car v)) (* (cadr v) (cadr v)) (* (caddr v) (caddr v))))
)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (vec*k v k)   vektor szorz�sa skal�rral
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun vec*k (v k)
    (mapcar '(lambda (a) (* a k)) v)
)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (vec/k v k)   vektor oszt�sa skal�rral
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun vec/k (v k)
    (mapcar '(lambda (a) (/ a k)) v)
)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (vec1 p1 p2)  egys�gvektor
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun vec1 (p1 p2 / v)
    (vec/k (setq v (vec p1 p2)) (VECABS v))
)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (vec+ v1 v2)  �sszead�s
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun vec+ (v1 v2)
    (list (+ (car v2) (car v1))
          (+ (cadr v2) (cadr v1))
          (+ (caddr v2) (caddr v1))
    )
)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (vec- v1 v2)  kivon�s
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun vec- (v1 v2)
    (list (- (car v1) (car v2))
          (- (cadr v1) (cadr v2))
          (- (caddr v1) (caddr v2))
    )
)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (vec* v1 v2)  skal�ris szorz�s
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun vec* (v1 v2)
;    (list (* (car v2) (car v1))
;          (* (cadr v2) (cadr v1))
;          (* (caddr v2) (caddr v1))
;    )
    (setq szorzat (+ (* (car v2) (car v1))
          (* (cadr v2) (cadr v1))
          (* (caddr v2) (caddr v1)))
    )

)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (vecx v1 v2)  vektori�lis szorzat
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun vecx (v1 v2 / x1 y1 z1 x2 y2 z2)
    (setq x1 (car v1)   x2 (car v2)
          y1 (cadr v1)  y2 (cadr v2)
          z1 (caddr v1) z2 (caddr v2)
    )
    (list (- (* y1 z2) (* z1 y2))
          (- (* z1 x2) (* x1 z2))
          (- (* x1 y2) (* y1 x2))
    )
)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (nor v1 v2)   norm�l vektor    3D  / v2           2D (v2=nil)
                               n|  /              n|
                                |/________ v1      |________ v1
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun nor (v1 v2 / n)
    (if (null v2) (setq v2 '(0 0 1)))
    (vec/k (setq n (vecx v1 v2)) (VECABS n))
)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (trn v m s)   transzform�l�s transzform�l� m�trixxal
                nentsel  eset�n m=(mapcar 'append m '(0 0 0 1))
                nentselp eset�n m=(mapcar 'list m)
�ltal�nos

             �Mxx Mxy Mxz Mxp�                X' = X�Mxx + Y�Myx + Z�Mzx + P�Mex
 �X Y Z P� � �Myx Myy Myz Myp� = �Z'Y'Z'P'�   Y' = X�Mxy + Y�Myy + Z�Mzy + P�Mey
             �Mzx Mzy Mzz Mzp�                Z' = X�Mxz + Y�Myz + Z�Mzz + P�Mez
             �Mex Mey Mez Mep�                P' = X�Mxp + Y�Myp + Z�Mzp + P�Mep
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
ha a transzf. nem perspektivikus

             �Mxx Mxy Mxz 0�                  X' = X�Mxx + Y�Myx + Z�Mzx + Mex
 �X Y Z 1� � �Myx Myy Myz 0� = �Z'Y'Z'1�      Y' = X�Mxy + Y�Myy + Z�Mzy + Mey
             �Mzx Mzy Mzz 0�                  Z' = X�Mxz + Y�Myz + Z�Mzz + Mez
             �Mex Mey Mez 1�                  1  =   0   +   0   +   0   + 1

           eltol�s E vektorral                    sk�l�z�s S vektorral
             �1  0  0  0�                               �Sx 0  0  0�
 �X Y Z 1� � �0  1  0  0� = �Z'Y'Z'1�       �X Y Z 1� � �0  Sy 0  0� = �Z'Y'Z'1�
             �0  0  1  0�                               �0  0  Sz 0�
             �Ex Ey Ez 1�                               �0  0  0  1�

  elforgat�s � � � sz�ggekkel, ahol a sz�g az elforgat�si tengely egy po-
  zit�v pontj�b�l az �ramutat� j�r�s�val megegyez� ir�nyban �rtend�.
                    � = X tengely kor�li elforgat�s sz�ge
                    � = Y tengely k�r�li elforgat�s sz�ge
                    � = Z tengely k�r�li elforgat�s sz�ge

                 �   1       0       0      0�
     �X Y Z 1� � �   0     cos(�) -sin(�)   0� = �X'Y'Z'1�
                 �   0     sin(�)  cos(�)   0�
                 �   0       0       0      1�

                 � cos(�)    0     sin(�)   0�
     �X Y Z 1� � �   0       1       0      0� = �X'Y'Z'1�
                 �-sin(�)    0     cos(�)   0�
                 �   0       0       0      1�

                 � cos(�) -sin(�)    0      0�
     �X Y Z 1� � � sin(�)  cos(�)    0      0� = �X'Y'Z'1�
                 �   0       0       1      0�
                 �   0       0       0      1�
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun trn (v m / x y z p)
    (setq x (car v) y (cadr v) z (caddr v) p 1)
    (setq v
        (mapcar
           '(lambda (Mx My Mz Mp)
                (+ (* x Mx) (* y My) (* z Mz) (* p Mp))
            )
            m
        )
    )
    (if (= 1 (setq p (cadddr v)))       ;ha p<>1 akkor perspektivikus
        'v
        (vec/k v p)                     ;perspektivikus lek�pz�sn�l normaliz�lni
    )
)
;|
様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様様
  MATRIX SZAMITASI RUTINOK

  M�trix �br�zol�si m�d:

  pl.      �M11 M12 M13�
           �M21 M22 M23� � ((M11 M12 M13) (M21 M22 M23) (M31 M32 M33))
           �M31 M32 M33�
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (mtrx_dim d)  m�trix dimenzion�l�sa d dimenzi�j�ra
  /ha az eredetin�l kisebb dimenzi�t adunk meg, akkor adatok vesznek el./
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;

;(defun mtrx_ext (d)
;)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (det m)        determin�ns
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
;(defun det (m)
;)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (mtrx1 n)      n x n elem� egys�gm�trix
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
;(defun mtrx1 (n)
;)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (mtrx*k m k)   m�trix szorz�sa skal�rissal
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
;(defun mtrx*k (m k)
;)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (mtrx/k m k)   m�trix oszt�sa skal�rissal
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
;(defun mtrx/k (m k)
;)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (mtrx+ m1 m2)  �sszead�s
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
;(defun mtrx+ (m1 m2)
;)

;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (mtrx- m1 m2)  kivon�s
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
;(defun mtrx- (m1 m2)
;)
|;
;|陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳
  (mtrx* m1 m2)  szorz�s    /dimenzi� f�ggetlen/
陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳陳|;
(defun mtrx* (m1 m2)
    (setq m2 (mapcar 'list m2))
    (mapcar
       '(lambda (v1)
            (mapcar
               '(lambda (v2)
                   (apply '+ '(mapcar '* 'v1 'v2))
                )
                m2
            )
3        )
        m1
    )
)


(princ "A vektorszamitasok betoltve")
(princ)
