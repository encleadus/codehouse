(defun c:ind (/ lwp sz gps tav hany)
    (setq lwp (car (entsel "Valassz polyline - t")))
    (print)
    (setq sz (getpoint "Add meg a pontot "))
    (command "_point" sz)
    (print (sajat lwp sz ))
    (princ)
) ;_ defun

(defun sajat (LWP_ID POINT / gps)
    (setq gps (getPolySegs1 LWP_ID))
    (setq tav(tavolsag gps ))
    (setq hany (hanyadik gps POINT))
    (setq ret (szelveny tav hany gps POINT))
)

(defun Elem (lst pnt / e p1 bulge p2 x0	y0 x1 y1 x2 y2 A B C szaml nev fpx fpy ft ptf d theta c|2 s r gamma phi p p0p delt ret)
;;;epszilon
    (setq e 0.1)
    (setq x0 (car pnt))
    (setq y0 (cadr pnt))
    (setq p1 (car lst))
    (setq x1 (car p1))
    (setq y1 (cadr p1))
    (setq bulge (cadr lst))
    (setq p2 (caddr lst))
    (setq x2 (car p2))
    (setq y2 (cadr p2))
    (if	(equal bulge 0.0 1E-6)
;;;	egyenes
	(progn
	    (setq A (/ (- y2 y1) (sqrt (+ (expt (- y2 y1) 2) (expt (- x2 x1) 2)))   ) ) 
	    (setq B (/ (- x1 x2) (sqrt (+ (expt (- y2 y1) 2) (expt (- x2 x1) 2)))   ) ) 
	    (setq C (- 0 (+ (* a x1) (* b y1))))
	    (setq szaml (+ (* a x0) (* b y0) c))
	    (setq nev (sqrt (+ (* a a) (* b b))))
	    (setq d (/ (abs szaml) nev))
;;;	    (print d)
	    (setq fpx (/ (+ x1 x2) 2))
	    (setq fpy (/ (+ y1 y2) 2))
	    (setq ft (/ (sqrt (+ (* (- x2 x1) (- x2 x1) ) (* (- y2 y1) (- y2 y1)))) 2))
;;;	    (print ft)
	    (setq ptf (sqrt (+ (* (- fpx x0) (- fpx x0) ) (* (- fpy y0) (- fpy y0)))) )
;;;	    (print ptf)
	    (if (and (< d e) (< (- ptf ft) e))
		(setq ret 1)
		(setq ret 0)
	     )
	) ;_ progn
;;;	iv
	(progn
	    (setq theta (* 4.0 (atan (abs bulge))))
	    (setq c|2 (/ (distance p1 p2) 2.0))
	    (setq s (* c|2 (abs bulge)))
	    (setq r (/ (+ (expt c|2 2.0) (expt s 2.0)) (* s 2.0)))
	    (setq gamma	(/ (- pi theta) 2.0)
		  phi	(if (>= bulge 0)
			    (+ (angle p1 p2) gamma)
			    (- (angle p1 p2) gamma)
			) ;_ if
		  p	(polar p1 phi r)
		  p0p (sqrt (+ (* (- (car p) x0) (- (car p) x0) ) (* (- (cadr p) y0) (- (cadr p) y0))))
		  delt	(/ e r)
	    ) ;_ setq
            (if (and (< p0p (+ r e)) (> p0p (- r e)))
		(progn
		(if (> bulge 0)
		    (progn
			(print "bulge >0")
			(if (< (angle p p2) (angle p p1) )
			    (progn
			
;;;				sigma2<sigma1
				(print (angle p pnt))
				(print (angle p p1))
				(print (angle p p2))
				(print delt)
				(if (or (<= (angle p pnt)  (angle p p2) )
					(>= (angle p pnt)  (angle p p1) )
				    )
				    (progn
				    (setq ret 1)
				    )
				    (setq ret 0)
				    )
				)
			    (progn
;;;				sigma2>sigma1
				(if (and (<= (angle p pnt)  (angle p p2) )
					 (>= (angle p pnt)  (angle p p1) )
				    )
				    (setq ret 1)
				    (setq ret 0)
				    )
				)
			     )
          		)
		    (progn
;;;			sigma1>sigma2
			(if (> (angle p p1) (angle p p2))
			    (progn
				(if (and (>= (angle p pnt)  (angle p p2) )
					 (<= (angle p pnt)  (angle p p1) )
				    )
				    (setq ret 1)
				    (setq ret 0)
				 )
			    )
			    (progn
				(if (or (<= (angle p pnt)  (angle p p1) )
					(>= (angle p pnt)  (angle p p2) )
				    )
				    (setq ret 1)
				    (setq ret 0)
				    )
				)
			 )
			)
		    )
		)
		(progn
		  (setq ret 0)
		)
	    )
	)
    ) ;_ if
    (setq ret ret)
) ;_ defun



(defun szelveny (tav_sz hany_sz gps_sz sz_sz / tavol dist bulge alfa theta c|2 s r gamma phi p alfa maradek ret)
    (setq tavol (nth hany_sz tav_sz))
    (setq bulge (cadr(nth hany_sz gps_sz)))
    (setq p1 (car(nth hany_sz gps_sz)))
    (setq p2 (caddr(nth hany_sz gps_sz)))
    (if (equal bulge 0.0 1E-6)
	(progn
;;;        egyenes
	    (setq dist (distance sz_sz (caddr(nth hany_sz gps_sz))))
	    (setq ret (- tavol dist)) 
	)
	(progn
;;;	    iv
	    (setq theta (* 4.0 (atan (abs bulge))))
	    (setq c|2 (/ (distance p1 p2) 2.0))
	    (setq s (* c|2 (abs bulge)))
	    (setq r (/ (+ (expt c|2 2.0) (expt s 2.0)) (* s 2.0)))
	    (setq gamma	(/ (- pi theta) 2.0)
		  phi	(if (>= bulge 0)
			    (+ (angle p1 p2) gamma)
			    (- (angle p1 p2) gamma)
			) ;_ if
		  p	(polar p1 phi r)
		 
	    ) ;_ setq
	    (if (> bulge 0)
		(progn
		    (if (> (angle p p1) (angle p p2))
			(progn
			    (if (< (angle p sz_sz) (angle p p2))
				(progn
				    (setq alfa (- (angle p p2) (angle p sz_sz)))
				    )
				(progn
				    (setq alfa (- (* 2 pi) (- (angle p sz_sz) (angle p p2))))
				)
			    )
			)
			(progn
			    (setq alfa (- (angle p p2) (angle p sz_sz )))
			    
			)
		    )
		)
		(progn
		    (if (< (angle p p1) (angle p p2))
			(progn
			    (if (< (angle p sz_sz) (angle p p1))
				(progn
				    (setq alfa (+ (angle p sz_sz) (- (* 2 pi) (angle p p2))))
				)
				(progn
				    (setq alfa (- (angle p sz_sz) (angle p p2)))
				)
			     )
			    )
			(progn
			    (setq alfa (- (angle p sz_sz) (angle p p2)))
		        )
		    )
		
		)
	    )
            (setq maradek (* r alfa))
	    (setq ret (- tavol maradek))
	)
  )
    (setq ret ret)
 )




(defun hanyadik (gps_p sz_p / i iedik elemee ret)
    (setq i 0)
    (setq ret -1)
    (repeat (length gps_p)
	(setq iedik (nth i gps_p ))
	(setq elemee (Elem iedik sz_p))
	(if (= elemee 1)
	    (progn
                (setq ret  i )
	    )
	)
     (setq i (+ i 1))
     )
    (if (= ret -1)
	(progn
	    (alert "Nincs rajta")
	    (quit)
	    )
	)
    (setq ret ret)
)

(defun getPolySegs1 (ent / entl p1 pt bulge seg ptlst)
    (setq entl (entget ent))
    (cond (ent
	   (if (= (logand (cdr (assoc 70 entl)) 1) 1)
	       (setq p1 (cdr (assoc 10 entl)))
	   ) ;_ if
	   (while (setq entl (member (assoc 10 entl) entl))
	       (if (and pt bulge)
		   (setq seg (list pt bulge))
	       ) ;_ if
	       (setq pt	   (cdr (assoc 10 entl))
		     bulge (cdr (assoc 42 entl))
	       ) ;_ setq
	       (if seg
		   (setq seg   (append seg (list pt))
			 ptlst (cons seg ptlst)
		   ) ;_ setq
	       ) ;_ if
	       (setq entl (cdr entl)
		     seg  nil
	       ) ;_ setq
	   ) ;_ while
	  )
    ) ;_ cond
    (if	p1
	(setq ptlst (cons (list pt bulge p1) ptlst))
    ) ;_ if
    (reverse ptlst)
) ;_ defun

(defun tavolsag	(lista / mun tav i bulge p1 p2 theta c|2 c s ker tavlist r)
(setq tavlist nil)
    (setq i 0)
    (setq mun 0)
    (setq tav 0)
    (repeat (length lista)
	(setq bulge (cadr (nth i lista)))
	(setq p1 (car (nth i lista)))
	(setq p2 (caddr (nth i lista)))
	(if (equal bulge 0.0 1E-6)
;;;	    egyenes
	    (progn
		(setq mun (distance p1 p2))
	    ) ;_ progn
;;;         iv
	    (progn
		(setq theta (* 4.0 (atan (abs bulge))))
		(setq c (distance p1 p2))
		(setq c|2 (/ c 2.0))
		(setq s (* (/ c 2.0) (abs bulge)))
		(setq r (/ (+ (expt c|2 2.0) (expt s 2.0)) (* s 2.0)))
		(setq mun (* r theta))
	    ) ;_ progn
	) ;_ if
	(setq tav (+ mun tav))
	(setq tavlist (cons tav tavlist))
	(setq i (+ i 1))
    ) ;_ repeat
    (setq tavlist (reverse tavlist))
) ;_ defun