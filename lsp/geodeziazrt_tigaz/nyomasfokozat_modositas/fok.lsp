(defun c:fok ()
  (command "_cmdecho" "0")
  (command "_view" "_save" "temp")
  (command "_osnap" "_non")
  (command "_dimzin" "0")
  (setq lof nil)
  (setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
  (setq sel-ent (nth 0 sel-ent))
  (make-vert-list sel-ent)
  (setq list1 vert-list)
  ;(princ list1)
  (setq	lof (ssget "_CP"
		   list1
		   (list (cons 0 "LWPOLYLINE,LINE,TEXT,INSERT"))
	    )
  )




(if lof
    (progn
      (initget 1 "1 2 3 4")
      (setq kw (getkword "\nC�l nyom�sfokozat? (1 - Gf, 2 - Gfk, 3 - Gfn, vagy 4 - Gfnk) "))
      (if (= kw "1") (setq fk "GAZ-Gf"))
      (if (= kw "2") (setq fk "GAZ-Gfk"))
      (if (= kw "3") (setq fk "GAZ-Gfn"))
      (if (= kw "4") (setq fk "GAZ-Gfnk"))
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq entx (ssname lof j))
	(setq tlist (entget entx))
	(setq layer (cdr (assoc 8 tlist)))
	(setq poz nil)
	(if (= (substr layer 7 1) "-") (setq poz 7))
	(if (= (substr layer 8 1) "-") (setq poz 8))
	(if (= (substr layer 9 1) "-") (setq poz 9))
	(if poz
		(progn
		(setq lay1 (strcat fk (substr layer poz) ))
		(command "_layer" "_m" lay1 "" "")
		(setq tlist (subst (cons 8 lay1)(assoc 8 tlist) tlist))
                (entmod tlist)
    		(entupd entx)

		) ;progn
	) ;if
        (setq j (+ j 1))	
      ) ; while

) ;progn
) ;if

  (command "_view" "_restore" "temp")
  (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
  (command "_cmdecho" "1")
) ;defun

(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list