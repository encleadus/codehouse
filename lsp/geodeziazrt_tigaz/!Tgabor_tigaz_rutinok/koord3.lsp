(defun c:mp1 ()
 (mertpont 1)
)
(defun c:mp2 ()
 (mertpont 2)
)

(defun mertpont (po)
  (setvar "CMDECHO" 0)
  (setq vhossz 12)
  (setq puffer 0.1)
  (setq kisbetumag 0.9)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "_ucs" "")
  (command "_osnap" "_end")
  (command "_dimzin" "0")
  (setq sp1 (getpoint "\n B�kj a m�rt pontra a vezet�ken !"))
  (setq sp11 (polar sp1 (/ pi 4) puffer))
  (setq sp12 (polar sp1 (* 5 (/ pi 4)) puffer))

  (setq sset (ssget "_C" sp11 sp12 '((-4 . "<or") ( 8 . "GAZ-Gfnk-GERINC")
                                      ( 8 . "GAZ-Gfk-GERINC")
                                      ( 8 . "GAZ-Gf-GERINC")
                                      ( 8 . "GAZ-Gfn-GERINC")
				      
                         (-4 . "or>") (0 . "line"))))
(if (not sset)
 (setq sset (ssget "_C" sp11 sp12 '((-4 . "<or") ( 8 . "GAZ-Gfnk-LEAGAZAS")
                                      ( 8 . "GAZ-Gfk-LEAGAZAS")
                                      ( 8 . "GAZ-Gf-LEAGAZAS")
                                      ( 8 . "GAZ-Gfn-LEAGAZAS")
				      
                         (-4 . "or>") (0 . "line"))))
)
  (if sset (progn
	;(if  (< 1 (sslength sset))
	;	(setq tlist (entget (car (entsel "\nB�kj a vezet�kre, amivel p�rhuzamos legyen a meg�r�s!"))))
        (setq tlist (entget (ssname sset 0)))
	(setq lay (cdr (assoc 8 tlist)))
	(setq p1 (cdr (assoc 10 tlist)))
	(setq p2 (cdr (assoc 11 tlist)))
       	(if (= "-" (substr lay 7 1)) (setq lay (substr lay 1 6)) )
	(if (= "-" (substr lay 8 1)) (setq lay (substr lay 1 7)) )
	(if (= "-" (substr lay 9 1)) (setq lay (substr lay 1 8)) )	
        

	(setq lay (strcat lay "-MERES-G"))

  	(command "_layer" "_s" lay "")
	  
	  (command "_osnap" "_non")
	
	  (setq szog (/ (* 180.0 (angle p1 p2)) pi))
	  (if (< 90 szog 270) 
		(setq szog (- szog 180)) 
	  )

          
	  (setq sp-angle (angle p1 p2))
;	  (setq pont (getstring "\nPontsz�m: "))
 ;         (setq mag nil)
;	  (setq mag (getreal "\nMagass�g: "))
  ;        (if mag 
 ;           (setq mag (rtos mag 2 2))
;	    (setq mag "")
;          )

	  ;(command "_insert" "mert_pont" sp1 "1" "1" "0" pont mag)
	         
  	 
	  (setq txt1 (rtos (car sp1) 2 po))
	  (setq txt2 (rtos (cadr sp1) 2 po)) 
	  (command "_insert" "gaz-koord" sp1 "1" "1" szog txt1 txt2) 
	  (setq entuj (entlast))
	  (princ "\n Koordin�ta meg�r�s helye!")
          (command "_move" entuj "" sp1 pause)
          (entupd entuj)
	  (setq tl2 (entget entuj))
	  (setq pt1 (cdr (assoc 10 tl2)))
	  (setq szog (* szog (/ pi 180)))
	  (setq pt2 (polar pt1 szog vhossz))
	  (if (< (distance pt1 sp1) (distance pt2 sp1))
		(command "_line" pt1 sp1 "")
		(command "_line" pt2 sp1 "")
	  )
	  (setq vonal (entlast))
	  (command "_change" vonal "" "_p" "_ltScale" "100" "")

	  
	 
	 	 	
 )
 (princ "\n Nem tal�ltam vezet�ket adott pontban!"))
  (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
 (command "_cmdecho" "1")
)

