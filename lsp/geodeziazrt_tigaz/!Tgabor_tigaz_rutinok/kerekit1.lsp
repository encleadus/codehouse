(defun c:kerekit1 ()
(setvar "cmdecho" 0)
(setq rep "")
(setq enttip "" pontpoz nil ps nil txt nil pe nil thossz nil teleje nil tvege nil tszam nil ntxt nil tlist nil)
(command "_layer" "_m" "_hiba" "_c" "1" "" "")
(command "_dimzin" "0")
(setq tizedes nil)
(while (= rep "")
	(setq pontpoz nil ps nil txt nil pe nil thossz nil teleje nil tvege nil tszam nil ntxt nil)
	(while (not tizedes) 
	(setq tizedes (atoi (getstring "\H�ny tizedesre kerek�tsen?: ")))
	(if (not tizedes) (princ "\nEg�sz sz�mot kell megadni!"))
	)
	(while (/= enttip "TEXT")
		(setq entx (car (entsel "\nB�kj az elemre!")))
		(setq tlist (entget entx))
		(setq enttip (cdr (assoc 0 tlist)))
	)
	(setq txt (cdr (assoc 1 tlist)))
	;(setq pstart (cdr (assoc 10 tlist)))
	;(command "_zoom" "_c" pstart "30")
	(setq pontpoz (vl-string-position (ascii ".") txt) )
	(if pontpoz
		(progn
			(setq ps pontpoz)
			(setq pontpoz (1+ pontpoz))
			(setq pe (1+ pontpoz))
			(while (and (< 0 ps) (member (substr txt ps 1) '("1" "2" "3" "4" "5" "6" "7" "8" "9" "0")))
				(setq ps (1- ps))
			)
			(while (member (substr txt pe 1) '("1" "2" "3" "4" "5" "6" "7" "8" "9" "0"))
				(setq pe (1+ pe))
			)
			(setq thossz (- pe (1+ ps)))
			(setq teleje (substr txt 1 ps))
			(setq tvege (substr txt pe))
			(setq tszam (rtos (atof (substr txt (1+ ps) thossz)) 2 tizedes))
			(setq ntxt (strcat teleje tszam tvege))
			(princ (strcat "\n" teleje "\n" tvege "\n" tszam))
			(setq tlist (subst (cons 1 ntxt)(assoc 1 tlist) tlist))
			(entmod tlist)
			(entupd entx)
		)
	)
	(setq entx (car (entsel "\nB�kj a k�vetkez� elemre! Vagy kil�p�s [L]")))
	(setq tlist (entget entx))
	(setq enttip (cdr (assoc 0 tlist)))
	(if (/= enttip "TEXT") (setq rep "k"))
	)
)