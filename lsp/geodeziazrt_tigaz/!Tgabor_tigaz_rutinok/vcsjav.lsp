(defun vocsok (sp1 sp2 layer)
   ;(command "ucs" "")
  (command "_layer" "_s" layer "")
  
  (setq sp-angle (angle sp1 sp2))
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  
  (setq sp3 (polar sp2 sp-angle-meroleges 0.5))
  (setq sp4 (polar sp1 sp-angle-meroleges 0.5))
  (setq sp2 (polar sp2 sp-angle-meroleges -0.5))
  (setq sp1 (polar sp1 sp-angle-meroleges -0.5))
  
  (command "_plINE" sp1 sp2 sp3 sp4 "_c")
 
 
) ;defun


(defun c:vcsjav ()
  (command "_cmdecho" "0")
  (command "_view" "_save" "temp")
  (command "_osnap" "_non")
  (command "_dimzin" "0")
  (setq lay_name "GAZ-Gfk-LEAGAZAS")
  (setq lay_name2 "foldreszlet")
  (setq lof nil)
  (command "_peditaccept" "1")
  (setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
  (setq sel-ent (nth 0 sel-ent))
  (make-vert-list sel-ent)
  (setq list1 vert-list)
  ;(princ list1)
  (setq ss (ssget "_CP" list1 '((-4 . "<AND")(-4 . "<OR")(0 . "LWPOLYLINE")(0 . "LINE")(-4 . "OR>")(-4 . "<OR")(8 . "GAZ-Gfk-VEDOCSO")(8 . "GAZ-Gf-VEDOCSO")(8 . "GAZ-Gfn-VEDOCSO")(8 . "GAZ-Gfnk-VEDOCSO")(-4 . "OR>")(-4 . "AND>"))))
  (command "_pedit" "_m" ss ""  "_j" "0.00" "")
  (setq lof (ssget "_CP" list1 '((-4 . "<AND")(0 . "LWPOLYLINE")(-4 . "<OR")(8 . "GAZ-Gfk-VEDOCSO")(8 . "GAZ-Gf-VEDOCSO")(8 . "GAZ-Gfn-VEDOCSO")(8 . "GAZ-Gfnk-VEDOCSO")(-4 . "OR>")(-4 . "AND>"))))
  (setq puffer 0.1)
  (setq atmero nil)
  (setq vamero nil)
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq entx (ssname lof j))
	(make-vert-list entx)
	(setq oldal (length vert-list))
	(if (= oldal 4)
	(progn
		
		(setq tlist (entget entx))
		(setq layer (cdr (assoc 8 tlist)))
		(setq len (- (strlen layer) 8))
		(setq lay1 (strcat (substr layer 1 len) "-ANY-ATM-FELIRAT"))
		(setq lay2 (strcat (substr layer 1 len) "-ANY-ATM-FELIRAT_VCS"))
		(setq lay3 (strcat (substr layer 1 len) "-GERINC"))
		(setq lay4 (strcat (substr layer 1 len) "-LEAGAZAS"))
		(setq layz (strcat lay3 "," lay4))

		(setq laos (ssget "_CP" vert-list
			    (list (cons 0 "LINE") (cons 8 layz)
			    )
		   	  )
	 		 )
		
		(setq tlisty (entget (ssname laos 0)))
		(setq pt1 (cdr (assoc 10 tlisty)))
		(setq pt2 (cdr (assoc 11 tlisty)))
		(setq i 0)
		(setq p1 nil)
		(setq p2 nil)
		(while (and (< i oldal) (not p2))
		  (setq vertp1 (nth i vert-list))
		  (if (> (+ i 1) (- oldal 1))
		    (setq vertp2 (nth 0 vert-list))
		    (setq vertp2 (nth (+ i 1) vert-list))
		  )
		  
		  (if p1 (setq p2 (inters vertp1 vertp2 pt1 pt2))	
		  	 (setq p1 (inters vertp1 vertp2 pt1 pt2))
		  )
		  (setq i (1+ i))
		  	  
		);while i
		(command "_layer" "_m" "vcs_regi" "_c" "1" "" "")
		(command "_change" entx "" "_p" "_la" "vcs_regi" "")

		(setq tav (atof (rtos (distance p1 p2) 2 1)))
		(setq sp-angle (angle p1 p2))
		(setq ujp (polar p1 sp-angle tav))
		(vocsok p1 ujp layer)
	);prog
	);if
	(setq j (1+ j))
      ) ;while j
    ) ;progn
  ) ;if
  (command "_layer" "_s" "0" "")
  (command "_view" "_restore" "temp")
  (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
  (command "_.purge" "_LA" "haz_tmp" "_N")
  (command "_cmdecho" "1")
) ;defun

(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list