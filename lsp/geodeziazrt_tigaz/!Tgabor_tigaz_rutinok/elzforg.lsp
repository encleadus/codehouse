(defun c:elzforg ( )
	(setvar "CMDECHO" 0)
	
	(command "_view" "_save" "temp")
 	(command "_osnap" "_none")

	(command "_zoom" "_e")
	(setq puffer 0.1)
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 2 "gaz-elz"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(command "_zoom" "_c" pstart 30)
				(setq sp1 (polar pstart (* 2 (/ pi 4)) puffer))
	  			(setq sp2 (polar pstart (* 4 (/ pi 4)) puffer))
			
				(setq l (ssget "_C" sp1 sp2 (list (cons 0 "LINE") (cons 8 "GAZ-Gf-LEAGAZAS,GAZ-Gfk-LEAGAZAS,GAZ-Gfnk-LEAGAZAS,GAZ-Gfn-LEAGAZAS"))))
				(setq v (entget (ssname l 0)))
				(setq p1 (cdr (assoc 10 v)))
				(setq p2 (cdr (assoc 11 v)))
				(setq szog (+ (angle p1 p2) (/ pi 2)))
				;(setq szog (+ t-angle (/ (* pi szogo) 180)))
				(setq tlist (subst (cons 50 szog)(assoc 50 tlist) tlist))
				(entmod tlist)
				(entupd entx)
				(setq j (+ j 1))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)

		)
		
	)
	(princ "\n")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

	

)
(princ "\nH�zi elz�r�kat beforgatja vezet�kre mer�legesre! \nParancs: elzforg")
 


	