(defun c:hsz ()
	(command "_cmdecho" "0")
	(command "_osnap" "_non")
	(command "_view" "_save" "temp")
	(setq lay_name "epulet_polygon")
	(setq lay_name2 "foldreszlet")
	(setq lof nil)
	(setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
	(setq sel-ent (nth 0 sel-ent))
	(make-vert-list sel-ent)
	(setq list1 vert-list)
	(command "_.erase" sel-ent "")
        (command "_-mapclean" "hsz.dpf")
	;(princ list1)
        (command "_-maptopocreate" "epulet" "" "_P" "_N" "_Y" "_N" "_Y" "ujepulet" "_Y" "_Y" "0" "" "_Y" "0" "" "_Y" "_N" "_N" "_N" "_N")
	(command "_maptopoadmin" "_load" "foldreszlet" "_Y")
        (command "_layer" "_m" "epulet_polygon" "_c" "1" "" "")
	(command "_layer" "_m" "foldreszlet" "_c" "1" "" "")
        (command "_mapclpline" "epulet" "epulet_polygon" "_N" "_N" "_N")
	(command "_mapclpline" "foldreszlet" "foldreszlet" "_N" "_N" "_N")
	(setq lof (ssget "_CP"
		   list1
		   (list (cons 8 lay_name2) (cons 0 "LWPOLYLINE"))
	          )
  	)
	(setq puffer 0.2)
	(if lof
	(progn
      		(setq hlof (sslength lof))
		(setq j 0)
		(while (< j hlof)
			(setq entx (ssname lof j))
			(setq vert-list nil)
			(make-vert-list entx)
			(setq list2 nil)
			(setq list2 vert-list)
			(setq oldal (length list2))
			(command "_zoom" "_o" "_si" entx)
			(command "_redraw")
			(setq lhf nil)
			(setq lhf (ssget "_CP"
				   list2
				   (list (cons 8 "C26_H�ZSZ�M") (cons 0 "TEXT"))
			          )
		  	)
			(setq lef nil)
			(setq lef (ssget "_CP"
		   		   list2
		   		   (list (cons 8 lay_name) (cons 0 "LWPOLYLINE"))
			          )
  			)
			(setq lkf nil)
			(setq lkf (ssget "_CP"
		   				   list2
						   (list (cons 8 "B22_K�ZTER_HRSZ") (cons 0 "TEXT"))
	    					  )
		 			)
			(if (and lhf lef (not lkf)) ; #### csak akkor megy tov�bb ha a telken van �p�let �s h�zsz�m is
			(progn
				(command "_layer" "_m" "foldreszlet_tmp" "_c" "1" "" "")
				(command "_change" entx "" "_p" "_la" "foldreszlet_tmp" "")
				;(command "_change" entx "" "_p" "_c" "blue" "")
				(setq i 0)
				(setq frontl nil)
				(while (< i oldal)
					(setq vertp1 (nth i list2))
			 		(if (> (+ i 1) (- oldal 1))
			    			(setq vertp2 (nth 0 list2))
			    			(setq vertp2 (nth (+ i 1) list2))
			 		)
			 		(setq sp-angle (angle vertp1 vertp2))
					(setq szog (/ (* 180 sp-angle) PI))
			 		(setq tav (distance vertp1 vertp2))
			 		(setq felezop (polar vertp1 sp-angle (/ tav 2)))
			 		(setq sp11 (polar felezop (/ pi 4) puffer))
			 		(setq sp14 (polar felezop (* 5 (/ pi 4)) puffer))

	 		 		
					(command "_zoom" "_c" felezop "70")
					(command "_redraw")
 			 		;(command "_rectang" sp11 sp14 "")
					(setq amer nil)
					(setq amer (ssget "_C"
			    			sp11 
						sp14
			    			(list (cons 8 lay_name2) (cons 0 "LWPOLYLINE"))
		 	    			)
	 		 		)
					;(if (< 1 (sslength amer)) (command "_circle" felezop "5"))
					(if amer
					(progn
					(setq enty nil)
					(setq enty (ssname amer 0))
					;(command "_change" enty "" "_p" "_c" "blue" "")
					(setq vert-list nil)
					(make-vert-list enty)
					(setq list3 nil)
					(setq list3 vert-list)
					(command "_zoom" "_o" "_si" enty)	
					(command "_redraw")	
					(setq lkf nil)
					(setq lkf (ssget "_CP"
		   				   list3
						   (list (cons 8 "B22_K�ZTER_HRSZ") (cons 0 "TEXT"))
	    					  )
		 			)
					;(princ list3)
					;(command "_l" list3 "")
			 		(if lkf
			   		(progn
						;(alert "die")
						(setq lpf nil)
						(command "_zoom" "_o" "_si" entx)
			        		(setq lpf (ssget "_CP"
		  			 		   list2
		 					   (list (cons 8 "0") (cons 0 "POINT"))
	   						  )
  						)
						(setq entpont (ssname lpf 0))
						(setq plist (entget entpont))
						(setq pstart (cdr (assoc 10 plist)))
						;(command "_osnap" "_node,_perp")
						;(command "_line" pstart felezop "")
						
						(setq frontl (cons felezop frontl))
						;(command "_osnap" "_non")				
						;(setq i oldal)
			   		);progn
			 		); if lkf
					)); if amer
					(setq i (+ i 1))
					
				) ;while
				(setq h 0)
				(setq fronth (length frontl))
				(setq flist nil)
				(while (< h fronth)
					(command "_osnap" "_node,_perp")
					(command "_line" pstart (nth h frontl) "")
					(command "_osnap" "_non")
					(setq e1 (entlast))
					(setq el1 (entget e1))
					(setq p0 (cdr (assoc 10 el1)))
					(setq p1 (cdr (assoc 11 el1)))
					(setq szog (angle p0 p1))
					(setq tav (+ (distance p0 p1) 2))
					(setq p2 (polar p0 szog tav))
					(command "_.erase" e1 "")
					;(command "_line" pstart p2 "")
					(setq flist (cons pstart (cons p2 flist)))

					;(command "_change" fr "" "_p" "_c" "red" "")
					(setq h (+ h 1))
				);while
				(command "_osnap" "_non")
				(setq akf (ssget "_F"
		  			   flist
		 			   (list (cons 8 "C01_LAK��P�LET") (cons 0 "LINE"))
	   				  )
  				)
				(if (and akf (= (sslength akf) 1))
					(progn
						(afole (ssname lhf 0) (ssname akf 0) pstart)
					)
					(progn
						(command "_zoom" "_o" "_si" entx)
						(command "_layer" "_off" "ujepulet,epulet_polygon,foldreszlet,foldreszlet_tmp,C07_KER�T�S_VAS" "Y" "")
						(command "_change" lhf "" "_p" "_c" "red" "")
						(setq faslop (car (entsel "\n V�laszd ki a frontVonalat!")))
						(command "_change" lhf "" "_p" "_c" "bylayer" "")
						(command "_layer" "_on" "ujepulet,epulet_polygon,foldreszlet,foldreszlet_tmp,C07_KER�T�S_VAS" "")
						(afole (ssname lhf 0) faslop pstart)
					)
				)
				
			) ;progn
	 		) ;if
		(setq j (+ j 1))
		(command "change" entx "" "p" "la" "foldreszlet" "")
		(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
      		) ;while
	) ;progn
	) ;if
  	(command "_maptopoadmin" "_D" "epulet" "_N" "_Y")
	(command "_maptopoadmin" "_unload" "foldreszlet")
 	(setq fos (ssget "_x" '((-4 . "<OR")
			      (8 . "ujepulet")
			      (8 . "epulet_polygon")
			      (8 . "haz_tmp")
			      (8 . "foldreszlet")
			      (8 . "foldreszlet_tmp")
			      (8 . "0")
			      (-4 . "OR>")
			     )))   
	(if fos (command "_erase" fos ""))
	(command "_.purge" "_LA" "foldreszlet,ujepulet,epulet_polygon,haz_tmp,foldreszlet_tmp" "_N") 
	(command "_view" "_restore" "temp")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

) ;defun

 ;--------------------------
 ;name      : make-vert-list
 ;--------------------------
(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list

(defun afole (ent1 ent2 ps)
    (setq buffer 1)
    ;(setq ent1 (car (entsel "\n B�kj a Text-re!")))
    ;(setq ent2 (car (entsel "\n B�kj a Vonalra!")))
    (setq tlist (entget ent1))
    (setq tlist2 (entget ent2))
    

    (setq t-angle (cdr (assoc 50 tlist)))

    (setq pstart (cdr (assoc 10 tlist2)))
    (setq pend (cdr (assoc 11 tlist2)))
    (setq midpt (list (/ (+ (car pstart) (car pend)) 2) (/ (+ (cadr pstart) (cadr pend)) 2)))
    
    (setq szog (angle pstart pend))
    
    (setq szog (/ (* 180.0 szog) pi))
    ;(alert (rtos szog 2 2)) 
    (if (< 90 szog 270) (setq szog (+ 180 szog)))
    (setq szog1 (* (+ szog 120) (/ pi 180)))
    (setq szog2 (* (+ szog 240) (/ pi 180)))
    (setq szog (* szog (/ pi 180)))
    (setq sp331 (polar midpt szog1 buffer))
    (setq sp332 (polar midpt szog2 2)) 
    (setq tlist (subst (cons 50 szog)(assoc 50 tlist) tlist))
    ;(command "_circle" sp331 "5")
    ;(command "_circle" sp332 "5")
    (if (< (distance sp331 ps) (distance sp332 ps))
    (setq tlist (subst (cons 10 sp331)(assoc 10 tlist) tlist))
    (setq tlist (subst (cons 10 sp332)(assoc 10 tlist) tlist))
    )
    (entmod tlist)
    (entupd ent1)
    
)

