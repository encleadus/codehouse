

(defun c:lanyag ()
  (command "_cmdecho" "0")
  (command "_view" "_save" "temp")
  (command "_osnap" "_non")
  (command "_dimzin" "0")
  (setq lay_name "GAZ-Gfk-LEAGAZAS")
  (setq lay_name2 "foldreszlet")
  (setq lof nil)
  (command "_peditaccept" "1")
  ;(setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
  ;(setq sel-ent (nth 0 sel-ent))
  ;(make-vert-list sel-ent)
  ;(setq list1 vert-list)
  ;(princ list1)
  (setq ss (ssget "x" '((-4 . "<AND")(-4 . "<OR")(0 . "LWPOLYLINE")(0 . "LINE")(-4 . "OR>")(-4 . "<OR")(8 . "GAZ-Gfk-LEAGAZAS")(8 . "GAZ-Gf-LEAGAZAS")(8 . "GAZ-Gfn-LEAGAZAS")(8 . "GAZ-Gfnk-LEAGAZAS")(-4 . "OR>")(-4 . "AND>"))))
  (command "_pedit" "_m" ss ""  "_j" "0.00" "")
  (setq lof (ssget "x" '((-4 . "<AND")(0 . "LWPOLYLINE")(-4 . "<OR")(8 . "GAZ-Gfk-LEAGAZAS")(8 . "GAZ-Gf-LEAGAZAS")(8 . "GAZ-Gfn-LEAGAZAS")(8 . "GAZ-Gfnk-LEAGAZAS")(-4 . "OR>")(-4 . "AND>"))))
  (setq puffer 0.1)
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq entx (ssname lof j))
	(make-vert-list entx)
	(setq oldal (length vert-list))
	(setq tlist (entget entx))
	(setq layer (cdr (assoc 8 tlist)))
	(setq len (- (strlen layer) 9))
	(setq lay1 (strcat (substr layer 1 len) "-ANY-ATM-FELIRAT"))

	(command "_layer" "_m" "haz_tmp" "_c" "1" "" "")
	(command "_change" entx "" "_p" "_la" "haz_tmp" "")
	(setq obj (vlax-ename->vla-object entx))
	(setq hoss (/ (- (vla-get-length obj) 2) 2))
        ;(princ hoss)
	(setq i 0)
	(setq van nil)
	(while (< i oldal)
	  (setq vertp1 (nth i vert-list))
	  (if (> (+ i 1) (- oldal 1))
	    (setq vertp2 (nth 0 vert-list))
	    (setq vertp2 (nth (+ i 1) vert-list))
	  )
	  (setq sp-angle (angle vertp1 vertp2))
	  (setq szog (/ (* 180 sp-angle) PI))
	  (setq tav (distance vertp1 vertp2))
	  (setq felezop (polar vertp1 sp-angle (/ tav 2)))
	  (setq sp11 (polar vertp1 (/ pi 4) puffer))
	  (setq sp12 (polar vertp2 (/ pi 4) puffer))
	  (setq sp14 (polar vertp1 (* 5 (/ pi 4)) puffer))	
	  (setq sp13 (polar vertp2 (* 5 (/ pi 4)) puffer))

	  (command "_zoom" "_c" felezop "70")
 	  ;(command "_line" sp11 sp12 sp13 sp14 "")

	  (setq	amer (ssget "_CP" (list
			    sp11
		            sp12
			    sp13		
			    sp14
			    )
			    (list (cons 0 "LINE") (cons 8 lay1)
			    )
		     )
	  )

	  (if (/= amer nil)
 	    (progn
		(setq van 1)
		(setq i oldal)
	    ) ;progn
	  ) ;if
	  (setq i (+ i 1))
	) ;while
	(setq j (+ j 1))
	(command "_change" entx "" "_p" "_la" layer "")
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
	(if (not van)
		(progn
			(command "_layer" "_m" "_hiba" "")
		      	(command "_circle" felezop "3")

		)
	)
      ) ;while
    ) ;progn
  ) ;if
  (command "_view" "_restore" "temp")
  (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
  (command "_.purge" "_LA" "haz_tmp" "_N")
  (command "_cmdecho" "1")
) ;defun

(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list