gg
gg; c:TM - Text Match
;-------------------------------------------------------------------------------
(defun c:TM (/ Cnt# EntList1@ EntName1^ EntList2@ EntName2^ Match$ SS&)
  (princ "\nText Match")
  (princ "\nSelect text or mtext to match")
  (if (setq EntName1^ (car (entsel)))
    (setq EntList1@ (entget EntName1^))
  );if
  (if (or (= (cdr (assoc 0 EntList1@)) "TEXT")(= (cdr (assoc 0 EntList1@)) "MTEXT"))
    (progn
      (setq Match$ (cdr (assoc 1 EntList1@)))
      (princ (strcat "\nSelect text or mtext to replace with " Match$))
      (if (setq SS& (ssget '((-4 . "<OR")(0 . "MTEXT")(0 . "TEXT")(-4 . "OR>"))))
        (progn
          (command "UNDO" "BEGIN")
          (setq Cnt# 0)
          (repeat (sslength SS&)
            (setq EntName2^ (ssname SS& Cnt#)
                  EntList2@ (entget EntName2^)
            );setq
            (setq EntList2@ (subst (cons 1 Match$) (assoc 1 EntList2@) EntList2@))
            (entmod EntList2@)
            (setq Cnt# (1+ Cnt#))
          );repeat
          (command "UNDO" "END")
        );progn
        (princ "\nNo text or mtext selected.")
      );if
    );progn
    (princ "\nNo text or mtext selected.")
  );if
  (princ)
);defun c:TM
;-------------------------------------------------------------------------------
 ;                               
 ; TIG�Z g�zvezet�k digitaliz�l�s
 ; ------------------------------


(defun c:par ()
  (alert
    "TIG�Z parancsok\n
  b15 - f�ldr�szlet hat�r r�teg
  c1 - lak��p�let r�teg
  c3 - gazd. �p�let r�teg
  b22 - HRSZ
  c26 - h�zsz�m
  b48 - utcan�v

  gg               - gerinc vezet�k r�teg
  gl                - le�gaz� vezet�k r�teg
  szg             - gerincvezet�k szelv�ny r�teg
  szl              - le�gaz�s szelv�ny r�teg
  gmer           - gerincvezet�k m�ret
  gmerv          - gerincvezet�k m�ret vonallal
  kpeb, kpej   - anyag, �tm�r� felirat (bal, jobb)
  kpe             - anyag, �tm�r� felirat (�ltal�nos)
  magb, magj - terep, cs�tet� magass�g (bal, jobb)
  bn               - bet�m�ret be�ll�t�s 0.6 v. 0.9
  vcs              - v�d�cs� rajzol�s
  idom, szerelveny, akna, iranyjelzo, keszulek,
        egyeb    - �tv�lt a megfelel� r�tegre
  mero           - mer�leges jel lerak�sa
  amero          - automata mer�leges jel lerak�
  kk             - k�r keres�
  sapka          - gaz-sapka jelkulcs lerak�s gerincvezet�k v�g�re
  elz            - gaz-elz jelkulcs lerak�s le�gaz�s v�g�re
  
  par   - parancsok le�r�sa, ez az ablak
  il, iu - k�p bet�lt�se/elrejt�se
  hna  - k�pfeliratoz�s
  kihrsz - hrsz-ek ki�r�sa f�jlba
  "
  )
)


 ;(c:par)



 ;----v�ltoz�k be�ll�t�sa----

 ;0.9 vagy 0.6
(setq kisbetumag 0.9)

(setq vhossz (* kisbetumag 4.35))


(defun c:bn ()
  (princ kisbetumag)
  (setq kisbetumag (getreal "\n Add meg a bet�m�retet 0.9 v. 0.6 !"))
  (setq vhossz (* kisbetumag 4.35))
)


(setq kpebetumag 1)




 ;----alapt�rk�p----


 ;�tv�lt az B15_FRL_HAT�R layerre
(defun C:b15 ()
  (command "layer" "s" "B15_FRL_HAT�R" "")
)

 ;�tv�lt az B13_T�MBHAT�R layerre
(defun C:b13 ()
  (command "layer" "s" "B13_T�MBHAT�R" "")
)

 ;�tv�lt az C01_LAK��P�LET layerre
(defun C:c1 ()
  (command "layer" "s" "C01_LAK��P�LET" "")
)

 ;�tv�lt az C03_GAZD_�P�LET layerre
(defun C:c3 ()
  (command "layer" "s" "C03_GAZD_�P�LET" "")
)

 ;Fotoszam_lerako
(defun C:fot ()
  (command "layer" "s" "7_FELIRAT" "")
  (setq txtmag 1.5)
  (setq betutip "Arial")
  (c:ti)
)

 ;B22_HRSZ layerre /-es HRSZ-t fel�r
(defun C:b22p ()
  (command "layer" "s" "B22_HRSZ" "")
  (setq txtmag 1)
  (setq betutip "STANDARD")
  (c:dp)
)


 ;C26_H�ZSZ�M layerre h�zsz�mot fel�r
(defun C:c26 ()
  (command "layer" "s" "7_FELIRAT" "")
  (setq txtmag 1.5)
  (setq betutip "ARIAL_�LL�")
  (c:ti2)
)

 ;B48_UTCAN�V layerre utcan�v felir�s
(defun C:b48 ()
  (command "layer" "s" "B48_UTCAN�V" "")
  (command "text" "s" "ARIAL_D�LT" pause "2.5" pause)
)

 ;D01_BURKOLAT layerre
(defun C:bu ()
  (command "_layer" "_s" "D01_BURKOLAT" "")
)

 ;D06_�ROK layerre
(defun C:arok ()
  (command "_layer" "_s" "D06_�ROK" "")
)
 ;D05_J�RDA layerre
(defun C:ja ()
  (command "_layer" "_s" "D05_J�RDA" "")


)

(defun Inc (Szam)
  (setq indulo (getstring "\nIndul� sz�m:"))
  (setq p1 (getpoint "\nText insert�l�si pontja:"))
  (setq szog (getangle p1 "\nAdd meg a sz�get!"))
  (setvar "CMDECHO" 0)
  (setq szog (/ (* szog 180) pi))
  (command "_text" "_s" betutip p1 szog indulo)
  (setq ent (entlast))
   (setq tlist (entget ent))
   (setq txh 1.5)
   (setq tlist (subst (cons 40 txh)(assoc 40 tlist) tlist))
   (entmod tlist)
    (entupd ent)
  (setq indulo (itoa (+ (atoi indulo) Szam)))
  (repeat 100
    (setq p1 (getpoint "\nText insert�l�si pontja:"))
    (princ indulo)
    (while (> 4 (strlen indulo))
	(setq indulo (strcat "0" indulo))
    )    
   (command "_text" "_s" betutip p1 szog indulo)
   (setq ent (entlast))
   (setq tlist (entget ent))
   (setq txh 1.5)
   (setq tlist (subst (cons 40 txh)(assoc 40 tlist) tlist))
   (entmod tlist)
    (entupd ent)
 

    (setq indulo (itoa (+ (atoi indulo) Szam)))
    
  )
  (setvar "CMDECHO" 0)
)

 ;TI:n�veli a beirt sz�mokat
(defun c:ti ()
  (Inc 1)
)

 ;TI2:kettes�vel n�veli a beirt sz�mokat
(defun c:ti2 ()
  (Inc 2)
)

 ;DP :egyenk�nt n�veli a be�rt sz�mokat a "/" jel utan
(defun c:dp ()
  (setq perelott (getstring "\nSz�m a / el�tt:"))
  (setq indulo (getstring "\nIndul� sz�m a / ut�n:"))
  (setq p1 (getpoint "\nText insertalasi pontja:"))
  (setq szog (getangle p1 "\nAdd meg a sz�get!"))
  (setq szog (/ (* szog 180) pi))
  (setq teljestext (strcat perelott "/" indulo))
  (command "text" "s" betutip p1 txtmag szog teljestext)
  (repeat 100
    (setq induloszam (atoi indulo))
    (setq induloszam (+ induloszam 1))
    (setq indulo (itoa induloszam))
    (setq p1 (getpoint "\nText insertalasi pontja:"))
    (setq teljestext (strcat perelott "/" indulo))
    (princ teljestext)
    (command "text" "s" betutip p1 txtmag szog teljestext)
  )
)


(defun c:cpf ()
  (command "select" pause)
  (command "copy" "p" "" "0,0" "0,0")
  (command "change" "p" "" "p" "la" "B15_FRL_HAT�R" "")
)

(defun c:cpe ()
  (command "select" pause)
  (command "copy" "p" "" "0,0" "0,0")
  (command "change" "p" "" "p" "la" "C01_LAK��P�LET" "")
)

(defun c:cpg ()
  (command "select" pause)
  (command "change" "p" "" "p" "la" "C03_GAZD_�P�LET" "")
)

(defun c:cpn ()
  (command "select" pause)
  (command "change" "p" "" "p" "la" "B48_UTCAN�V" "")
)

(defun c:fk ()
  (setvar "CMDECHO" 0)
  (command
    "layer"		 "off"		      "*"
    "Y"			 "on"
    "B15_FRL_HAT�R,B22_HRSZ,tiff,keret"	      ""
   )
  (setvar "CMDECHO" 1)
)

(defun c:ek ()
  (setvar "CMDECHO" 0)
  (command
    "layer"
    "off"
    "*"
    "Y"
    "on"
    "C01_LAK��P�LET,C03_GAZD_�P�LET,C06_K�Z�P�LET,C26_H�ZSZ�M,keret"
    ""
   )
  (setvar "CMDECHO" 1)
)

(defun c:itr ()
  (setvar "CMDECHO" 0)
  (command
    "layer" "off" "*" "Y" "on" "*itr*" "")
  (setvar "CMDECHO" 1)
)

(defun c:lll ()
  (setvar "CMDECHO" 0)
  (command
    "layer" "off" "*" "Y" "on" "l*" "" "off" "L01_FHMA_JK_FEL" "")
  (setvar "CMDECHO" 1)
)




(defun c:ik ()
  (setvar "CMDECHO" 0)
  (setvar "ORTHOMODE" 0)
  (command "ucs" "")
  (command "layer" "s" "B47_KAPCS_JEL" "")
  (while t
    (setq ent1 (car (entsel "\nBokj az elemre!")))
    (setq pstart (cdr (assoc 10 (entget ent1)))
	  psx	 (nth 0 pstart)
	  psy	 (nth 1 pstart)
	  pend	 (cdr (assoc 11 (entget ent1)))
	  pex	 (nth 0 pend)
	  pey	 (nth 1 pend)
	  ipx	 (/ (+ psx pex) 2)
	  ipy	 (/ (+ psy pey) 2)
	  ip	 (list ipx ipy)
    )
    (setq sp-angle (angle pstart pend))
    (setq szog (/ (* 180.0 sp-angle) pi))
    (command "-insert" "JKDATB04" ip "2" "2" szog)
    (setq ent1 nil)
  ) ;while
)


 ;------k�zm�------

 ;�tv�lt az GAZ-Gfk-IDOM layerre
(defun C:idom ()
  (command "layer" "s" "GAZ-Gfk-IDOM" "")
)

 ;�tv�lt az GAZ-Gfk-KESZULEK layerre
(defun C:keszulek ()
  (command "layer" "s" "GAZ-Gfk-KESZULEK" "")
)

 ;�tv�lt az GAZ-Gfk-AKNA layerre
(defun C:akna ()
  (command "layer" "s" "GAZ-Gfk-AKNA" "")
)

 ;�tv�lt az GAZ-Gfk-SZERELVENY layerre
(defun C:szerelveny ()
  (command "layer" "s" "GAZ-Gfk-SZERELVENY" "")
)

 ;�tv�lt az GAZ-Gfk-IRANYJELZO layerre
(defun C:iranyjelzo ()
  (command "layer" "s" "GAZ-Gfk-IRANYJELZO" "")
)

 ;�tv�lt az GAZ-EGYEBSZIMB layerre
(defun C:egyeb ()
  (command "layer" "s" "GAZ-EGYEBSZIMB" "")
)

 ;�tv�lt az GAZ-Gfk-GERINC layerre
(defun C:gg ()
  (command "layer" "s" "GAZ-Gfk-GERINC" "")
)

 ;�tv�lt az GAZ-Gfk-LEAGAZAS layerre
(defun C:gl ()
  (command "layer" "s" "GAZ-Gfk-LEAGAZAS" "")
)

 ;�tv�lt az GAZ-Gfk-SZELVENY-G layerre
(defun C:szg ()
  (command "layer" "s" "GAZ-Gfk-SZELVENY-G" "")
)

 ;�tv�lt az GAZ-Gfk-SZELVENY-L layerre
(defun C:szl ()
  (command "layer" "s" "GAZ-Gfk-SZELVENY-L" "")
)

 ;�tv�lt az GAZ-Gfk-VEDOCSO layerre
(defun C:gv ()
  (command "layer" "s" "GAZ-Gfk-VEDOCSO" "")
)

 ;�tv�lt az GAZ-Gfk-SZERELVENY layerre
(defun C:gs ()
  (command "layer" "s" "GAZ-Gfk-SZERELVENY" "")
)


 ;--------------------------------
 ;block insert
 ;--------------------------------

(defun jelkelh (ll ss)
  (setvar "cmdecho" 0)
  (command "_ucs" "_w")
  (setq olayer (getvar "CLAYER"))
 ;	(command "_osnap" "_node")
  (setq ip (getpoint "\n Inzert pont"))
  (command "_layer" "_s" ll "" "_insert" ss ip "" "" "")
 ;	(command "_osnap" "_non")
  (setvar "CLAYER" olayer)
  (terpri)
)

(defun jelkelhf	(ll ss)
  (setvar "cmdecho" 0)
  (setq olayer (getvar "CLAYER"))
 ;	(command "_osnap" "_end")
  (setq ip (getpoint "\n Inzert pont"))
  (command "_layer" "_s" ll "" "_insert" ss ip "" "" pause)
 ;	(command "_osnap" "_non")
  (setvar "CLAYER" olayer)
  (terpri)
)

 ;------- Jelkulcsok

 ;----bizonytalans�g jel elhelyez�se
 ;csak line-ra b�kve m�k�dik!
(defun c:bjel ()
  (setvar "CMDECHO" 0)
  (setvar "ORTHOMODE" 0)
  (command "_osnap" "_non")
  (command "ucs" "")
  (command "layer" "s" "GAZ-EGYEBSZIMB" "")
  (while t
    (setq ent1 (car (entsel "\nBokj az elemre!")))
    (setq pstart (cdr (assoc 10 (entget ent1)))
	  psx	 (nth 0 pstart)
	  psy	 (nth 1 pstart)
	  pend	 (cdr (assoc 11 (entget ent1)))
	  pex	 (nth 0 pend)
	  pey	 (nth 1 pend)
	  ipx	 (/ (+ psx pex) 2)
	  ipy	 (/ (+ psy pey) 2)
	  ip	 (list ipx ipy)
    )
    (setq sp-angle (angle pstart pend))
    (setq szog (/ (* 180.0 sp-angle) pi))
    (command "-insert" "gaz-bjel" ip "0.5" "0.5" szog)
    (setq ent1 nil)
  ) ;while
)


(defun c:jn ()
  (jelkelhf "GAZ-Gfk-MERES-G" "jobbranyil")
)

(defun c:bn ()
  (jelkelhf "GAZ-Gfk-MERES-G" "balranyil")
)


 ;----gerincvez m�ret
(defun c:gmerg ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gf-MERES-G" "")
  (command "-osnap" "nea,end")
  (setq sp1 (getpoint "\n Add meg a m�retvonal egyik v�g�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg a m�retvonal m�sik v�g�t !"))
  (command "osnap" "non")
  (setq szog (/ (* 180.0 (angle sp1 sp2)) pi))
  (setq sptxt (getpoint "\n Add meg a meg�r�s besz�r�si pontj�t !"))
  (setq txt (getstring "\n Add meg a sz�mot !"))
  (command "text" "s" "ARIAL_D�LT" sptxt kisbetumag szog txt "")
 (command "osnap" "nea,ext,end,ins")
) ;defun

 ;----gerincvez m�ret
(defun c:gmer ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-MERES-G" "")
  (command "-osnap" "nea,end")
  (setq sp1 (getpoint "\n Add meg a m�retvonal egyik v�g�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg a m�retvonal m�sik v�g�t !"))
  (command "osnap" "non")
  (setq szog (/ (* 180.0 (angle sp1 sp2)) pi))
  (setq sptxt (getpoint "\n Add meg a meg�r�s besz�r�si pontj�t !"))
  (setq txt (getstring "\n Add meg a sz�mot !"))
  (command "text" "s" "ARIAL_D�LT" sptxt kisbetumag szog txt "")
 (command "osnap" "nea,ext,end,ins")
) ;defun

 ;----gerincvez m�ret m�retvonallal
(defun c:gmerv ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-MERES-G" "")
  (command "-osnap" "nea,end")
  (setq sp1 (getpoint "\n Add meg a m�retvonal egyik v�g�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg a m�retvonal m�sik v�g�t !"))
  (command "osnap" "non")
  (setq szog (/ (* 180.0 (angle sp1 sp2)) pi))
  (command "line" sp1 sp2 "")
  (setq sptxt (getpoint "\n Add meg a meg�r�s besz�r�si pontj�t !"))
  (setq txt (getstring "\n Add meg a sz�mot !"))
  (command "text" "s" "ARIAL_D�LT" sptxt kisbetumag szog txt "")
(command "osnap" "nea,ext,end,ins")
) ;defun

 ;----mer�leges jelkulcs
(defun c:mero ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-MERES-G" "")
  (command "-osnap" "nea,end")
  (setq sp1 (getpoint "\n Add meg mer�leges besz�r�si pontj�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg a mer�leges ir�nyt !"))
  (command "osnap" "non")
  (setq szog (/ (* 180.0 (angle sp1 sp2)) pi))
  (setq szog2 (+ szog 45.0))
  (setq sp3 (polar sp1 (* (/ szog2 180) pi) 0.25))
  (command "-insert" "mer�leges" sp3 "1" "1" szog "")
  (command "osnap" "nea,ext,end,ins")
) ;defun

 ;----anyag, �tm�r� felirat - balos
(defun c:kpeb ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-ANY-ATM-FELIRAT" "")
  (command "osnap" "nea")
  (setq sp1 (getpoint "\n Add meg a m�retvonal illeszked� v�g�t !"))
  (command "osnap" "non")
  (command "circle" sp1 "3")
  (setq sp2 (getpoint sp1 "\n Add meg a m�retvonal m�sik v�g�t !"))
  (command "line" sp2 sp1 "")

  (setq	torol (ssget "x"
		     '((0 . "CIRCLE") (8 . "GAZ-Gfk-ANY-ATM-FELIRAT"))
	      )
  )
  (command "erase" torol "")

  (setq sp-angle (angle sp2 sp1))
  (setq szog (/ (* 180.0 sp-angle) pi))
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  (setq sp3 (polar sp2 sp-angle-meroleges 0.2))
  (setq sp4 (polar sp2 sp-angle-meroleges -1.2))
  (setq txt (getstring "\n Add meg az �tm�r�t !"))
  (command "text" "s" "ARIAL_D�LT" sp3 kpebetumag szog txt "")
  (command "text" "s" "ARIAL_D�LT" sp4 kpebetumag szog "PE" "")

) ;defun

 ;----anyag, �tm�r� felirat - jobbos
(defun c:kpej ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-ANY-ATM-FELIRAT" "")
  (command "osnap" "nea")
  (setq sp1 (getpoint "\n Add meg a m�retvonal illeszked� v�g�t !"))
  (command "osnap" "non")
  (command "circle" sp1 "3")
  (setq sp2 (getpoint sp1 "\n Add meg a m�retvonal m�sik v�g�t !"))
  (command "line" sp1 sp2 "")

  (setq	torol (ssget "x"
		     '((0 . "CIRCLE") (8 . "GAZ-Gfk-ANY-ATM-FELIRAT"))
	      )
  )
  (command "erase" torol "")

  (setq sp-angle (angle sp1 sp2))
  (setq szog (/ (* 180.0 sp-angle) pi))

  (setq spv1 (polar sp2 (- sp-angle pi) 2.0))
  (setq spv2 (polar sp2 (- sp-angle pi) 2.2))

  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  (setq sp3 (polar spv1 sp-angle-meroleges 0.2))
  (setq sp4 (polar spv2 sp-angle-meroleges -1.2))
  (setq txt (getstring "\n Add meg az �tm�r�t !"))
  (command "text" "s" "ARIAL_D�LT" sp3 kpebetumag szog txt "")
  (command "text" "s" "ARIAL_D�LT" sp4 kpebetumag szog "PE" "")

) ;defun

 ;***
 ;----anyag, �tm�r� felirat - altalanos
(defun c:kpe ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-ANY-ATM-FELIRAT" "")
  (command "osnap" "nea")
  (setq sp1 (getpoint "\n Add meg a m�retvonal illeszked� v�g�t!"))
  (command "osnap" "non")
  (command "circle" sp1 "6")
  (setq sp2 (getpoint sp1 "\n Add meg a m�retvonal m�sik v�g�t!"))
  (command "line" sp1 sp2 "")

  (setq	torol (ssget "x"
		     '((0 . "CIRCLE") (8 . "GAZ-Gfk-ANY-ATM-FELIRAT"))
	      )
  )
  (command "erase" torol "")

  (setq
    flagmode (getstring "\n Add meg a z�szl� t�pus�t! <Bal>/<Jobb>")
  )
  (if (= flagmode "b")
    (progn
      (setq sp-angle (angle sp2 sp1))
      (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
      (setq sp3 (polar sp2 sp-angle-meroleges 0.2))
      (setq sp4 (polar sp2 sp-angle-meroleges -1.2))
    ) ;progn
  ) ; if flagmode = "b"
  (if (= flagmode "j")
    (progn
      (setq sp-angle (angle sp1 sp2))
      (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
      (setq spv1 (polar sp2 (- sp-angle pi) 2.0))
      (setq spv2 (polar sp2 (- sp-angle pi) 2.2))
      (setq sp3 (polar spv1 sp-angle-meroleges 0.2))
      (setq sp4 (polar spv2 sp-angle-meroleges -1.2))
    ) ;progn
  ) ; if flagmode = "j"

  (setq szog (/ (* 180.0 sp-angle) pi))
  (setq txt (getstring "\n Add meg az �tm�r�t !"))
  (command "text" "s" "ARIAL_D�LT" sp3 kpebetumag szog txt "")
  (command "text" "s" "ARIAL_D�LT" sp4 kpebetumag szog "PE" "")

) ;defun

 ;----terepmag., cs�tet�mag. felirat - balos
(defun c:magb ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-TEREPMAG-FELIRAT" "")
  (command "osnap" "end")
  (setq sp1 (getpoint "\n Add meg a m�retvonal illeszked� v�g�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg a t�r�spontot !"))
  (command "osnap" "non")
  (setq sp3 (getpoint sp2 "\n Add meg a m�retvonal v�g�t !"))
  (setq sp-angle (angle sp3 sp2))
  (setq szog (/ (* 180.0 sp-angle) pi))

  (setq spv (polar sp2 (- sp-angle pi) vhossz))
  (command "line" sp1 sp2 spv "")
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  (setq sp4 (polar spv sp-angle-meroleges 0.1))
  (setq sp5 (polar spv sp-angle-meroleges (* -1 (+ kisbetumag 0.1))))
  (setq txt1 (getstring "\n Add meg a terepmag. feliratot !"))
  (setq txt2 (getstring "\n Add meg a cs�tet� mag. feliratot !"))
 ;	(command "osnap" "non")
  (command "text" "s" "ARIAL_D�LT" sp4 kisbetumag szog txt1 "")
  (command "layer" "s" "GAZ-Gfk-CSOTETOMAG-FELIRAT" "")
  (command "text" "s" "ARIAL_D�LT" sp5 kisbetumag szog txt2 "")
)

(defun c:fmagb ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-TEREPMAG-FELIRAT" "")
  (command "osnap" "end")
  (setq sp1 (getpoint "\n Add meg a m�retvonal illeszked� v�g�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg a t�r�spontot !"))
  (command "osnap" "non")
  (setq sp3 (getpoint sp2 "\n Add meg a m�retvonal v�g�t !"))
  (setq sp-angle (angle sp3 sp2))
  (setq szog (/ (* 180.0 sp-angle) pi))

  (setq spv (polar sp2 (- sp-angle pi) vhossz))
  (command "line" sp1 sp2 spv "")
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  (setq sp4 (polar spv sp-angle-meroleges 0.1))
  (setq sp5 (polar spv sp-angle-meroleges (* -1 (+ kisbetumag 0.1))))
  (setq sp6 (polar spv sp-angle-meroleges (+ kisbetumag 0.2)))
 (setq txt0 (getstring "\n Add meg a felmag. feliratot !"))  
 (setq txt1 (getstring "\n Add meg a terepmag. feliratot !"))
 (setq txt2 (getstring "\n Add meg a cs�tet� mag. feliratot !"))
 ;	(command "osnap" "non")
  (command "text" "s" "ARIAL_D�LT" sp4 kisbetumag szog txt1 "")
  (command "layer" "s" "GAZ-Gfk-CSOTETOMAG-FELIRAT" "")
  (command "text" "s" "ARIAL_D�LT" sp5 kisbetumag szog txt2 "")
(command "layer" "s" "GAZ-Gfk-FELMAG-FELIRAT" "")
  (command "text" "s" "ARIAL_D�LT" sp6 kisbetumag szog txt0 "")
)

(defun c:magj ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-TEREPMAG-FELIRAT" "")
  (command "osnap" "end")
  (setq sp1 (getpoint "\n Add meg a m�retvonal illeszked� v�g�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg a t�r�spontot !"))
  (command "osnap" "non")
  (setq sp3 (getpoint sp2 "\n Add meg a m�retvonal v�g�t !"))

  (setq szog (/ (* 180.0 (angle sp2 sp3)) pi))
  (setq sp-angle (angle sp2 sp3))
  (setq spv (polar sp2 sp-angle vhossz))
  (command "line" sp1 sp2 spv "")
  (setq sp6 (polar sp2 sp-angle 0.5))
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  (setq sp4 (polar sp6 sp-angle-meroleges 0.1))
  (setq sp5 (polar sp6 sp-angle-meroleges (* -1 (+ kisbetumag 0.1))))
  (setq txt1 (getstring "\n Add meg a terepmag. feliratot !"))
  (setq txt2 (getstring "\n Add meg a cs�tet� mag. feliratot !"))
 ;	(command "osnap" "non")
  (command "text" "s" "ARIAL_D�LT" sp4 kisbetumag szog txt1 "")
  (command "layer" "s" "GAZ-Gfk-CSOTETOMAG-FELIRAT" "")
  (command "text" "s" "ARIAL_D�LT" sp5 kisbetumag szog txt2 "")
)
 ;----terepmag., cs�tet�mag. felirat - jobbos
(defun c:fmagj ()
  (setvar "CMDECHO" 0)
  (if (= ortho-change 0)
    (setvar "ORTHOMODE" 0)
    (setvar "ORTHOMODE" 0)
  ) ;if
  (command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-TEREPMAG-FELIRAT" "")
  (command "osnap" "end")
  (setq sp1 (getpoint "\n Add meg a m�retvonal illeszked� v�g�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg a t�r�spontot !"))
  (command "osnap" "non")
  (setq sp3 (getpoint sp2 "\n Add meg a m�retvonal v�g�t !"))

  (setq szog (/ (* 180.0 (angle sp2 sp3)) pi))
  (setq sp-angle (angle sp2 sp3))
  (setq spv (polar sp2 sp-angle vhossz))
  (command "line" sp1 sp2 spv "")
  (setq sp6 (polar sp2 sp-angle 0.5))
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  (setq sp4 (polar sp6 sp-angle-meroleges 0.1))
  (setq sp5 (polar sp6 sp-angle-meroleges (* -1 (+ kisbetumag 0.1))))
  (setq sp7 (polar sp6 sp-angle-meroleges (+ kisbetumag 0.2)))
  (setq txt0 (getstring "\n Add meg a felmag. feliratot !"))   
  (setq txt1 (getstring "\n Add meg a terepmag. feliratot !"))
  (setq txt2 (getstring "\n Add meg a cs�tet� mag. feliratot !"))
 ;	(command "osnap" "non")
  (command "text" "s" "ARIAL_D�LT" sp4 kisbetumag szog txt1 "")
  (command "layer" "s" "GAZ-Gfk-CSOTETOMAG-FELIRAT" "")
  (command "text" "s" "ARIAL_D�LT" sp5 kisbetumag szog txt2 "")
(command "layer" "s" "GAZ-Gfk-FELMAG-FELIRAT" "")
  (command "text" "s" "ARIAL_D�LT" sp7 kisbetumag szog txt0 "")
)

 ;----v�d�cs�
(defun c:vcsg ()
  (setvar "CMDECHO" 0)
  (setvar "ORTHOMODE" 0)
  ;(command "ucs" "")
  (command "layer" "s" "GAZ-Gf-VEDOCSO" "")
   (command "_osnap" "_end,_int,_nea,_ext")
  (setq sp1 (getpoint "\n Add meg az v�d�cs� egyik v�g�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg az v�d�cs� m�sik v�g�t !"))
  (setq sp-angle (angle sp1 sp2))
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  
  (setq sp3 (polar sp2 sp-angle-meroleges 0.5))
  (setq sp4 (polar sp1 sp-angle-meroleges 0.5))
  (setq sp2 (polar sp2 sp-angle-meroleges -0.5))
  (setq sp1 (polar sp1 sp-angle-meroleges -0.5))
  (command "_osnap" "_none")
  (command "plINE" sp1 sp2 sp3 sp4 "c")
  (command "_osnap" "_end,_int,_nea,_ext")
 
) ;defun

 ;----v�d�cs�
(defun c:vcs ()
  (setvar "CMDECHO" 0)
  (setvar "ORTHOMODE" 0)
  ;(command "ucs" "")
  (command "layer" "s" "GAZ-Gfk-VEDOCSO" "")
   (command "_osnap" "_end,_int,_nea,_ext")
  (setq sp1 (getpoint "\n Add meg az v�d�cs� egyik v�g�t !"))
  (setq sp2 (getpoint sp1 "\n Add meg az v�d�cs� m�sik v�g�t !"))
  (setq sp-angle (angle sp1 sp2))
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  
  (setq sp3 (polar sp2 sp-angle-meroleges 0.5))
  (setq sp4 (polar sp1 sp-angle-meroleges 0.5))
  (setq sp2 (polar sp2 sp-angle-meroleges -0.5))
  (setq sp1 (polar sp1 sp-angle-meroleges -0.5))
  (command "_osnap" "_none")
  (command "plINE" sp1 sp2 sp3 sp4 "c")
  (command "_osnap" "_end,_int,_nea,_ext")
 
) ;defun


 ;---- cs�tet�mag felirat eltol�s
(defun c:csm ()
  (setvar "CMDECHO" 0)
  (setvar "ORTHOMODE" 0)
  (command "ucs" "")
  (setq t-lay "GAZ-Gfk-CSOTETOMAG-FELIRAT")
  (while t
    (setq ent1 (car (entsel "\nB�kj a sz�mra!")))
    (setq tlist (entget ent1))
    (setq t-pt	  (cdr (assoc 10 tlist))
	  t-angle (cdr (assoc 50 tlist))
    )
   
   (setq sp1 (getpoint "\n Add meg a m�retvonal egyik v�g�t !"))
   (setq sp2 (getpoint sp1 "\n Add meg a m�retvonal m�sik v�g�t !"))
  
  (setq szog  (/ (* 180.0 (angle sp1 sp2)) pi))

    
    (setq tlist (subst (cons 50 szog) (assoc 50 tlist) tlist))
    ;(setq tlist (subst (cons 8 t-lay) (assoc 8 tlist) tlist))
    (entmod tlist)
    (entupd ent1)
    (setq ent1 nil)
  ) ;while
)


 ;---- terepmag, cs�tet�mag felirat eltol�s
(defun c:magfor ()
  (setvar "CMDECHO" 0)
  (setvar "ORTHOMODE" 0)
  (command "ucs" "")
  (setq puffer 5)
   (setq ent1 (car (entsel "\nB�kj a vonalra!")))
    (setq tlist (entget ent1))
    (setq pstart (cdr (assoc 10 tlist)))
    (setq pend (cdr (assoc 11 tlist)))
    (setq sp1 (getpoint "\n Add meg a m�retvonal egyik v�g�t !"))
    (setq sp2 (getpoint sp1 "\n Add meg a m�retvonal m�sik v�g�t !"))
  
    (setq szog   (angle sp1 sp2)) 
    (setq szog2   (angle pstart pend))

    (setq szog3 (/ (* (- szog szog2) 180) pi))
  
  (setq sp11 (polar pstart (/ pi 4) puffer))
  (setq sp13 (polar pstart (* 5 (/ pi 4)) puffer))
  ;(command "_zoom" "_c" p1 "30")
  (setq	lof (ssget "_C" sp11 sp13
		     '(
		      (-4 . "<AND")
		      (0 . "TEXT")
                      (-4 . "<OR")
		      (8 . "GAZ-Gfk-CSOTETOMAG-FELIRAT")
		      (8 . "GAZ-Gfk-TEREPMAG-FELIRAT")
		      (8 . "GAZ-Gfk-FELMAG-FELIRAT")
		      (-4 . "OR>")
                      (-4 . "AND>")
		     )
	     )
  )
    (setq hlof (sslength lof))
     (setq j 0)
     (while (< j hlof)
     (setq entx (ssname lof j))
     (setq j (+ j 1))
     (command "_rotate" entx "" pstart szog3)    
)
    (command "_rotate" ent1 "" pstart szog3)
 
(command "cmdecho" "1")
)


(defun c:csmo ()
  (setvar "CMDECHO" 0)
  (setvar "ORTHOMODE" 0)
  (command "ucs" "")
  (setq t-lay "GAZ-Gfk-CSOTETOMAG-FELIRAT")
  (while t
    (setq ent1 (car (entsel "\nB�kj a sz�mra!")))
    (setq tlist (entget ent1))
    (setq t-pt	  (cdr (assoc 10 tlist))
	  t-angle (cdr (assoc 50 tlist))
    )
   

    (setq t-pnew (polar t-pt t-angle -4))
 ;	(setq tlist (subst (cons 10 t-pnew) (assoc 10 tlist) tlist))
    (setq tlist (subst (cons 8 t-lay) (assoc 8 tlist) tlist))
    (entmod tlist)
    (entupd ent1)
    (setq ent1 nil)
  ) ;while
)


 ;------------------------------------------------------
 ;Block Change ind�t�ny�l
 ;
 ;-----------------------------------------------------
(defun c:bcn ()
  (command "cmddia" "0")
  (command "cmdecho" "0")
  (command "osnap" "nea")
  (command "zoom" "e" "")
  (setq bscale 1)
  (setq entli (ssget "X" '((2 . "ITR_SI1_FNT-209"))))
  (if entli
    (progn
      (setq jobb "jobbranyil")
      (setq bal "balranyil")
      (setq reteg "GAZ-Gfk-MERES-G")
      (setq hossz (sslength entli))
      (setq i 0)
      (while (< i hossz)
	(setq ent (ssname entli i))
	(setq insp (cdr (assoc 10 (entget ent))))
	(command "zoom" "c" insp zoomc)
	(command "layer" "s" reteg "")
	(initget "j b J B")
	(setq expl (getkword "\nJobbra vagy Balra? <j>:"))
	(if (null expl)
	  (setq expl "j")
	)
	(if (or (= expl "j") (= expl "J"))
	  (command "insert" jobb insp bscale bscale pause)
	  (command "insert" bal insp bscale bscale pause)
	)
	(setq i (+ i 1))
	(princ (strcat "\n " (itoa i) "/" (itoa hossz)))
	(terpri)
	(command "erase" ent "")
      ) ;while
      (command "erase" entli "")
    )
  )
  (command "cmddia" "1")
  (command "cmdecho" "1")
)




 ;------------------------------------------------------
 ;Block Change
 ;
 ;-----------------------------------------------------
(defun c:bc ()
  (command "cmddia" "0")
  (command "cmdecho" "0")
  (command "osnap" "none")
  (setq bscale 1)
  (setq	entli (ssget "X"
		     '(
		       (2 . "ITR_SI1_FNT-59")
		      )
	      )
  )
  (if entli
    (progn
      (setq mire "gaz-sapka")
      (setq hossz (sslength entli))
      (setq i 0)
      (while (< i hossz)
	(setq ent (ssname entli i))
	(setq ins-angle (cdr (assoc 50 (entget ent))))
	(setq rot (/ (* 180 ins-angle) PI))
	(setq insp (cdr (assoc 10 (entget ent))))
 ;    (setq reteg (cdr (assoc 8 (entget ent))))
	(setq reteg "GAZ-Gfk-IDOM")
	(command "zoom" "c" insp zoomc)
	(command "layer" "s" reteg "")
	(command "insert" mire insp bscale bscale pause)
	(setq i (+ i 1))
      ) ;while
      (command "erase" entli "")
    )
  )
  (command "cmddia" "1")
  (command "cmdecho" "1")
)



 ;-------------------------------------
 ;Image Load
 ;rabox es betolti
 ;-------------------------------------
(defun c:il ()
  (command "cmddia" "0")
  (setq ent-i (entsel "\nBokj a Reload-olando imagere!"))
  (setq ent-i (nth 0 ent-i))
  (setq i-name (cdr (assoc 1 (entget ent-i))))
  (command "-image" "r" i-name)
  (setq i-ent (ssget "X" '((0 . "IMAGE"))))
  (if i-ent
    (command "draworder" i-ent "" "b")
  )
  (command "cmddia" "1")
)

 ;-------------------------------------
 ;Image Unload
 ;rabox es kitolti
 ;-------------------------------------
(defun c:iu ()
  (command "cmddia" "0")
  (setq ent-i (entsel "\nBokj a Unload-olando imagere!"))
  (setq ent-i (nth 0 ent-i))
  (setq i-name (cdr (assoc 1 (entget ent-i))))
  (command "-image" "u" i-name)
  (command "cmddia" "1")
)

 ;-------------------------------------
 ;Halo Nev
 ;tiff nevet felirja automatic
 ;-------------------------------------
(defun c:hna ()
  (command "_cmddia" "0")
  (command "_layer" "_m" "halonev" "_c" "_yellow" "" "")
  (setq imagelist (ssget "x" '((0 . "image"))))
  (if imagelist
    (progn
      (setq him (sslength imagelist))
      (setq im 0)
      (while (< im him)

	(setq ent-i (ssname imagelist im))
	(setq lof340 (cdr (assoc 340 (entget ent-i))))

	(setq lof330 nil)
	(foreach xent (entget lof340)
	  (if (= 330 (car xent))
	    (setq lof330 (cons (cdr xent) lof330))
	  )
	)


	(if lof330
	  (progn
	    (setq hlof330 (length lof330))
	    (setq jj 0)
	    (setq list3 nil)
	    (setq list350 nil)
	    (setq noname T)
	    (while (AND (< jj hlof330) noname)
	      (setq ent (entget (nth jj lof330)))
	      (foreach ti ent
		(if ti
		  (progn
		    (if	(= (car ti) 3)
		      (setq list3 (cons (cdr ti) list3))
		    )
		    (if	(= (car ti) 350)
		      (setq list350 (cons (cdr ti) list350))
		    )
		  )
		)
	      )
	      (if list350
		(setq noname nil)
	      )
	      (setq jj (+ jj 1))
	    )
	    (if	list350
	      (progn
		(setq i 0)
		(setq h-list350 (length list350))
		(while (< i h-list350)
		  (if (eq lof340 (nth i list350))
		    (setq i-name (nth i list3))
		  )
		  (setq i (+ i 1))
		)
 (setq insp (getpoint "\nBesz.pont"))
		(setq insp (cdr (assoc 10 (entget ent-i))))
		(command "_text"
			 insp
			 "20"
			 "0"
			 (strcase i-name T)
		)
	      )
	    )
	  )
	)
	(setq im (+ im 1))
      )
    )
    (alert "no images found")
  )
  (command "_cmddia" "1")
)



(defun c:kihrsz	()

  (setq lof nil)
  (setq lof (ssget "x" (list (cons 0 "TEXT") (cons 8 "B22_HRSZ"))))
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)

      (setq fajl (getfiled "Pontok ment�se..." "d:/" "txt" 1))
      (setq outf (open fajl "w"))
      (close outf)
      (setq ext (vl-filename-extension fajl))

      (if (/= fajl nil)
	(progn
	  (while (< j hlof)
	    (setq entx (ssname lof j))
	    (setq txtval (cdr (assoc 1 (entget entx))))

	    (setq sor (strcat txtval))

	    (setq outf (open fajl "a"))
	    (write-line sor outf)
	    (close outf)
	    (setq j (+ j 1))
	    (princ (strcat "\n " (itoa j) "/" (itoa hlof)))
	    (terpri)
	  )
	)
      )
    )
  )
)



 ;--------- auto kapcsol�jel
 ;El�felt�telek: h�zakat tartalmaz� z�rt poligonos r�teg, f�ldr�szlet hat�rokat tartalmaz� vonalas r�teg
 ;El�k�szit�s: 1. �p�letek poligoniz�l�sa (CAD -> ArcView)
 ;             2. �p�letek elv�g�sa a telekhat�rokkal (ArcView)
 ;             3. Area < 0.5 m2 poligonok t�rl�se
 ;             4. ArcView -> CAD
 ;Ismert hiba: ha t�bb �p�let van 1 telken bel�l, akkor azon a telken nem ker�l fel egyetlen kapcsol�jel sem.
 ;Fejleszthet�s�g: leghosszabb oldalra tegye a kapcsol�jelet

(defun c:kapcs ()
  (command "cmdecho" "0")
  (command "osnap" "non")
  (setq lay_name "ujepulet_closed")
  (setq lay_name2 "foldreszlet")
  (setq lof nil)
  (setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
  (setq sel-ent (nth 0 sel-ent))
  (make-vert-list sel-ent)
  (setq list1 vert-list)
  (princ list1)
  (setq	lof (ssget "CP"
		   list1
		   (list (cons 8 lay_name) (cons 0 "LWPOLYLINE"))
	    )
  )

  (setq puffer 0.2)
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq entx (ssname lof j))
	(make-vert-list entx)
	(setq oldal (length vert-list))
	(command "_layer" "_m" "haz_tmp" "_c" "1" "" "")
	(command "_change" entx "" "_p" "_la" "haz_tmp" "")
	(setq i 0)
	(while (< i oldal)
	  (setq vertp1 (nth i vert-list))
	  (if (> (+ i 1) (- oldal 1))
	    (setq vertp2 (nth 0 vert-list))
	    (setq vertp2 (nth (+ i 1) vert-list))
	  )
	  (setq sp-angle (angle vertp1 vertp2))
	  (setq szog (/ (* 180 sp-angle) PI))
	  (setq tav (distance vertp1 vertp2))
	  (setq felezop (polar vertp1 sp-angle (/ tav 2)))
	  (setq sp11 (polar felezop (/ pi 4) puffer))
	  (setq sp14 (polar felezop (* 5 (/ pi 4)) puffer))

	  (command "zoom" "c" felezop "70")
 ;	  (command "rectang" sp11 sp14 "")

	  (setq	amer (ssget "_C"
			    sp11
			    sp14
			    '((-4 . "<OR")
			      (-4 . "<AND")
			      (0 . "LINE")
			      (8 . "foldreszlet")
			      (-4 . "AND>")
			      (-4 . "<AND")
			      (0 . "LWPOLYLINE")
			      (8 . "ujepulet_closed")
			      (-4 . "AND>")
			      (-4 . "OR>")
			     )
		     )
	  )

	  (if (= amer nil)
 ; ha nem tal�l akkor kapcsjel
	    (progn
	      (command "_layer" "_m" "B47_KAPCS_JEL" "")
	      (command "_insert" "jkdatb04" felezop "" "" szog)
	      (setq i oldal)
	    ) ;progn
	  ) ;if
	  (setq i (+ i 1))
	) ;while
	(setq j (+ j 1))
	(command "change" entx "" "p" "la" "ujepulet_closed" "")
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
      ) ;while
    ) ;progn
  ) ;if
  (command "cmdecho" "1")
) ;defun





 ;////////////////////////////////////
 ; feliratok auto eltol�sa adott m�rettel mer�legesen, a megadott r�tegben
(defun c:tmv ()
  (setq
    lista (ssget "x" '((8 . "GAZ-Gfk-TEREPMAG-FELIRAT") (0 . "Text")))
  )
  (setq db (sslength lista))
 ; (princ db)
  (setq im 0)
  (while (< im db)
    (setq ent-i (ssname lista im))
    (setq szog (cdr (assoc 50 (entget ent-i))))
    (setq sp (cdr (assoc 10 (entget ent-i))))
    (setq szog-meroleges (+ szog (/ pi 2)))
    (setq ujsp (polar sp szog-meroleges -0.8))
    (command "move" ent-i "" sp ujsp)
    (setq im (+ im 1))
  )
)



 ;////////////////////////////////////
 ;K�rbeker�tett r�sz elk�l�n�t�se
(defun c:sep ()
  (setq sel-ent (entsel "\valassz elemet"))
  (setq sel-ent (nth 0 sel-ent))
  (make-vert-list sel-ent)
  (setq list1 vert-list)
  (princ list1)
  (setq ents (ssget "CP" list1))
  (princ ents)
  (command "change"   ents	 ""	    "_prop"    "layer"
	   "ok"	      "color"	 "bylayer"  "ltype"    "bylayer"
	   ""
	  )

  (setq db (sslength ents))
  (princ db)
  (setq im 0)

  (while (< im db)
    (princ im)
    (setq ent-i (ssname ents im))
    (setq sp (cdr (assoc 10 (entget ent-i))))
    (command "circle" sp "5")
    (setq im (+ im 1))
  )
) ;c:sep





 ;////////////////////////////////////
 ;szelv�nyt �s sz�k�zt tartalmaz� magass�gmeg�r�sok tiszt�t�sa
(defun c:tch ()

  (setq sel-ent (car (entsel "\valassz elemet")))
  (setq reteg (cdr (assoc 8 (entget sel-ent))))
  (command "_zoom" "_e")
  (setq lista (ssget "x" (list (cons 8 reteg) (cons 0 "Text"))))
  (setq db (sslength lista))
  (setq im 0)
  (while (< im db)
    (princ (strcat "\n " (rtos im) "/" (rtos db)))
    (setq ent-i (ssname lista im))
    (setq szoveg (cdr (assoc 1 (entget ent-i))))
    (setq elist (entget ent-i))
    (if	(/= (vl-string-search "+" szoveg) nil)
      (progn
	(setq txt (vl-string-left-trim "0123456789+.," szoveg))
	(setq txt (vl-string-left-trim " " txt))
	(setq txt2 (subst (cons 1 txt) (assoc 1 elist) elist))
	(entmod txt2)
	(entupd ent-i)
      )
    ) ;if 
    (setq im (+ im 1))
  )
)


 ; automata mer�leges felrak� (ahova nem rak oda k�r ker�l)
(defun c:amero ()
  (setvar "CMDECHO" 0)
  (setvar "ORTHOMODE" 0)
  (command "_ucs" "")
  (command "_osnap" "_non")
  (command "_zoom" "_e")
  (setq puffer 0.5)
  (setq	lof (ssget "x"
		   (list (cons 8 "GAZ-Gfk-MERES-G,GAZ-Gfnk-MERES-G")
			 (cons 0 "LINE")
		   )
	    )
  )
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq entx (ssname lof j))
	(setq lay (cdr (assoc 8 (entget entx))))
	(setq teng_start (cdr (assoc 10 (entget entx))))
	(setq teng_end (cdr (assoc 11 (entget entx))))
	(setq p1 teng_start)
	(sel)
	(if amer
	  (progn
	    (command "_layer" "_s" lay "")
	    (setq p2 teng_end)
	    (setq szog (/ (* 180.0 (angle p2 p1)) pi))
	    (setq szog2 (+ szog 45.0))
	    (setq spmero (polar p2 (* (/ szog2 180) pi) 0.25))
	    (command "_insert" "mer�leges" spmero "1" "1" szog)
	  )
	  (progn
	    (setq p1 teng_end)
	    (sel)
	    (if	amer
	      (progn
		(command "_layer" "_s" lay "")
		(setq p2 teng_start)
		(setq szog (/ (* 180.0 (angle p2 p1)) pi))
		(setq szog2 (+ szog 45.0))
		(setq spmero (polar p2 (* (/ szog2 180) pi) 0.25))
		(command "_insert" "mer�leges" spmero "1" "1" szog)
	      )
	      (progn
		(command "_layer" "_m" "meroleges_hiba" "")
		(command "_circle" p1 "1")
	      )
	    ) ;if
	  ) ;progn
	) ;if

	(setq lay_tmp (strcat "tmp_" lay))
	(command "_layer" "_m" lay_tmp "_c" "1" "" "")
	(command "_change" entx "" "_p" "_la" lay_tmp "")
	(setq j (+ j 1))
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
      ) ;while
    ) ;progn
  ) ;if
  (setvar "CMDECHO" 1)
)

(defun sel ()
  (setq sp11 (polar p1 (/ pi 4) puffer))
  (setq sp13 (polar p1 (* 5 (/ pi 4)) puffer))
  (command "_zoom" "_c" p1 "30")
  (setq	amer (ssget "_C"
		    sp11
		    sp13
		    '((-4 . "<OR")
		      (8 . "GAZ-Gfk-GERINC")
		      (8 . "GAZ-Gfnk-GERINC")
		      (-4 . "OR>")
		     )
	     )
  )

)


 ;--------------------------------------------------------------------------
 ;name      : kor keres selekcio
 ;action    : 
 ;components: 
 ;--------------------------------------------------------------------------
(defun c:kks ()
  (setq circle-list (ssget '((0 . "circle"))))
)
 ;--------------------------------------------------------------------------
 ;name      : kor keres
 ;action    : aki kor megkeresi
 ;components: 
 ;--------------------------------------------------------------------------
(defun c:un ()
  (setq zoomk 50)
  (command "_zoom" "e")
  (setq lof (ssget "x" '((8 . "B48_UTCAN�V_"))))
  (setq hlof (sslength lof))
  (while (/= hlof 0)
    (progn
      (setq circlex (ssname lof 0))
      (setq pt1 (cdr (assoc 10 (entget circlex))))
      (command "_zoom" "c" pt1 zoomk)
      (print (strcat "meg: " (itoa hlof) "db van!"))
      (setvar "CMDECHO" 0)
      ;(getstring "\nTorlom!")
      
      ;(command "_change" (ssname circle-list 0) "" "_prop" "_layer" "B48_UTCAN�V" "_color" "_bylayer" "_ltype" "_bylayer"  "")
      (command "layer" "s" "B48_UTCAN�V" "")
      (setq rad (cdr (assoc 50 (entget (ssname circle-list 0)))))
      (setq szog (/ (* 180.0 rad) pi))
 
      (setq hrsz (car(entsel "\nBokj a hrsz-re!")))
      (setq enlist(entget hrsz))
      (setq txt1 (cdr (assoc 1 (entget (ssname circle-list 0)))))
      (setq txt2 (cdr (assoc 1 enlist)))
      (command "text" "s" "ARIAL_D�LT" pause "1.5" szog (strcat txt1 " (" txt2 ")")) 
      (command "erase" (ssname lof 0) "")
;(command "_text" "ARIAL_D�LT" pause " 1.5 " (cdr(assoc 50 (ssname circle-list 0)))  "")
      (ssdel circlex lof)
      (setq hlof (sslength lof))
    )
  )
    (alert "Nincs benne tobb utcanev!")
  
) ;defun un

(defun c:kk ()
  (setq zoomk 30)
 ;(command "_zoom" "e")
  (setq circle-list (ssget "x" '((0 . "circle"))))
  (setq h-circle (sslength circle-list))
  (if (/= h-circle 0)
    (progn
      (setq circlex (ssname circle-list 0))
      (setq pt1 (cdr (assoc 10 (entget circlex))))
      (command "_zoom" "c" pt1 zoomk)
      (print (strcat "meg: " (itoa h-circle) "db van!"))
      (getstring "\nTorlom!")
      (command "erase" (ssname circle-list 0) "")
      (ssdel circlex circle-list)
      (setq h-circle (sslength circle-list))
    )
    (alert "Nincs benne tobb kor!")
  )
) ;defun kk


;           
;  gaz-elz jelkulcs lerak�s le�gaz�s v�g�re
;           
(defun c:elz()
(setvar "CMDECHO" 0)
  (setq	lof
	 (ssget
	   "x"
	   (list
	     (cons
	       8
	       "GAZ-Gf-LEAGAZAS,GAZ-Gfk-LEAGAZAS,GAZ-Gfn-LEAGAZAS,GAZ-Gfnk-LEAGAZAS"
	     )
	     (cons 0 "LINE,LWPOLYLINE")
	   )
	 )
  )
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq p10 nil)
	(setq p11 nil)
	(if
	  (and
	    (/= 1 (cdr (assoc 70 (entget (ssname lof j)))))
	    (= "LWPOLYLINE" (cdr (assoc 0 (entget (ssname lof j)))))
	  )
	   (progn
	     (setq entli (ssname lof j))
	     (make-vert-list entli)
	     (setq p10 (nth 0 vert-list))
	     (setq p11 (last vert-list))
	   )
	)
	(if (= "LINE" (cdr (assoc 0 (entget (ssname lof j)))))
	  (progn
	    (setq entli (ssname lof j))
	    (setq p10 (cdr (assoc 10 (entget (ssname lof j)))))
	    (setq p11 (cdr (assoc 11 (entget (ssname lof j)))))
	  )
	)
	(setq lay_leag (cdr (assoc 8 (entget (ssname lof j)))))
	(setq lay_trim (vl-string-trim "LEAGAZAS" lay_leag))
	(setq lay_trim (strcat "GAZ" lay_trim))
	(setq lay_gerinc (strcat lay_trim "GERINC"))
	(command "_layer" "_m" "gaz_tmp" "_c" "1" "" "")
	(command "_change" entli "" "_p" "_la" "gaz_tmp" "")
	(setq puffer 0.3)
	(setq p1 p10)
	(selvez)
	(if amer
	  (progn
	    (setq p1 p11)
	    (selvez)
	    (if	(= amer nil)
	      (progn
		(selins)
		(if (= amer nil)
		  (progn
		    (setq lay_jelk (strcat lay_trim "SZERELVENY"))
		    (command "_layer" "_s" lay_jelk "")
		    (setq p2 p10)
		    (setq p_angle (angle p2 p1))
		    (setq p_angle-meroleges (+ p_angle (/ pi 2)))
		    (setq szog (/ (* 180.0 p_angle-meroleges) pi))
		    (command "_insert" "gaz-elz" p1 "0.5" "0.5" szog)
		  )
		)
	      )
	    )
	  )
	  (progn
	    (selins)
	    (if	(= amer nil)
	      (progn
		(setq lay_jelk (strcat lay_trim "SZERELVENY"))
		(command "_layer" "_s" lay_jelk "")
		(setq p2 p11)
		(setq p_angle (angle p2 p1))
		(setq p_angle-meroleges (+ p_angle (/ pi 2)))
		(setq szog (/ (* 180.0 p_angle-meroleges) pi))
		(command "_insert" "gaz-elz" p1 "0.5" "0.5" szog)
	      )
	    )
	  )
	) ;if
	(command "_change" entli "" "_p" "_la" lay_leag "")
	(setq j (+ j 1))
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
      ) ;while
    ) ;progn
  ) ;if
  (setvar "CMDECHO" 1)
) ;defun







;           
;  gaz-sapka jelkulcs lerak�s gerincvezet�k v�g�re
;           
(defun c:sapka()
(setvar "CMDECHO" 0)
  (setq	lof
	 (ssget
	   "x"
	   (list
	     (cons
	       8
	       "GAZ-Gf-GERINC,GAZ-Gfk-GERINC,GAZ-Gfn-GERINC,GAZ-Gfnk-GERINC"
	     )
	     (cons 0 "LINE,LWPOLYLINE")
	   )
	 )
  )
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq p10 nil)
	(setq p11 nil)
	(if
	  (and
	    (/= 1 (cdr (assoc 70 (entget (ssname lof j)))))
	    (= "LWPOLYLINE" (cdr (assoc 0 (entget (ssname lof j)))))
	  )
	   (progn
	     (setq entli (ssname lof j))
	     (make-vert-list entli)
	     (setq p10 (nth 0 vert-list))
	     (setq p11 (last vert-list))
	   )
	)
	(if (= "LINE" (cdr (assoc 0 (entget (ssname lof j)))))
	  (progn
	    (setq entli (ssname lof j))
	    (setq p10 (cdr (assoc 10 (entget (ssname lof j)))))
	    (setq p11 (cdr (assoc 11 (entget (ssname lof j)))))
	  )
	)
	(setq lay_leag (cdr (assoc 8 (entget (ssname lof j)))))
	(setq lay_trim (vl-string-trim "GERINC" lay_leag))
	(setq lay_trim (strcat "G" lay_trim))
	(setq lay_gerinc (strcat lay_trim "GERINC"))
	(command "_layer" "_m" "gaz_tmp" "_c" "1" "" "")
	(command "_change" entli "" "_p" "_la" "gaz_tmp" "")
	(setq puffer 0.3)
	(setq p1 p10)
	(selvez)
	(if amer
	  (progn
	    (setq p1 p11)
	    (selvez)
	    (if	(= amer nil)
	      (progn
		(selinsg)
		(if (= amer nil)
		  (progn
		    (setq lay_jelk (strcat lay_trim "IDOM"))
		    (command "_layer" "_s" lay_jelk "")
		    (setq p2 p10)
		    (setq p_angle (angle p2 p1))
		    ;(setq p_angle-meroleges (+ p_angle (/ pi 2)))
		    (setq szog (/ (* 180.0 p_angle) pi))
		    (command "_insert" "gaz-sapka" p1 "1" "1" szog)
		  )
		)
	      )
	    )
	  )
	  (progn
	    (selins)
	    (if	(= amer nil)
	      (progn
		(setq lay_jelk (strcat lay_trim "IDOM"))
		(command "_layer" "_s" lay_jelk "")
		(setq p2 p11)
		(setq p_angle (angle p2 p1))
		;(setq p_angle-meroleges (+ p_angle (/ pi 2)))
		(setq szog (/ (* 180.0 p_angle) pi))
		(command "_insert" "gaz-sapka" p1 "1" "1" szog)
	      )
	    )
	  )
	) ;if
	(command "_change" entli "" "_p" "_la" lay_leag "")
	(setq j (+ j 1))
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
      ) ;while
    ) ;progn
  ) ;if
  (setvar "CMDECHO" 1)
) ;defun


(defun selvez ()
  (setq sp11 (polar p1 (/ pi 4) puffer))
  (setq sp13 (polar p1 (* 5 (/ pi 4)) puffer))
  (command "_zoom" "_c" p1 "30")
  (setq	amer (ssget "_C"
		    sp11
		    sp13
		    (list (cons -4 "<OR")
			  (cons 8 lay_gerinc)
			  (cons 8 lay_leag)
			  (cons -4 "OR>")
		    )
	     )
  )
)


(defun selins ()
  (setq sp11 (polar p1 (/ pi 4) puffer))
  (setq sp13 (polar p1 (* 5 (/ pi 4)) puffer))
  (command "_zoom" "_c" p1 "30")
  (setq	amer
	 (ssget
	   "_C"
	   sp11
	   sp13
	   (list
	     (cons 0 "INSERT")
	     (cons
	       2
	       "gaz-eant,gaz-eanpg,gaz-eanto,gaz-eanszv,gaz-eanbv,gaz-eang,gaz-eanrg,gaz-eanrgv,gaz-elz,gaz-sapka,gaz-dugo,gaz-kIBS"
	     )
	   )
	 )
  )
)

(defun selinsg ()
  (setq sp11 (polar p1 (/ pi 4) puffer))
  (setq sp13 (polar p1 (* 5 (/ pi 4)) puffer))
  (command "_zoom" "_c" p1 "30")
  (setq	amer
	 (ssget
	   "_C"
	   sp11
	   sp13
	   (list
	     (cons 0 "INSERT")
	   )
	 )
  )
)





 ;------------------------
 ;     seg�drutinok       
 ;------------------------


 ;--------------------------
 ;name      : make-vert-list
 ;--------------------------
(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list