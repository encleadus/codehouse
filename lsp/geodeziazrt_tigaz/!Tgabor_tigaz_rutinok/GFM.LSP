(defun lineford (lwp /  Lista pk pv)
    (setq Lista (entget lwp))
    (setq pk (cdr (assoc 10 Lista)))
    (setq pv (cdr (assoc 11 Lista)))
    (setq Lista (subst (cons 11 pk) (assoc 11 Lista) Lista))
    (setq Lista (subst (cons 10 pv) (assoc 10 Lista) Lista))
    (entmod Lista)
    (entupd lwp)
)
(defun rob (/ kv n vnev i)
    (setq kv (ssget "x"
		    '(
		      (-4 . "<or")
		      (8 . "gaz-gfk-gerinc")
		      (8 . "gaz-gf-gerinc")
		      (8 . "gaz-gfnk-gerinc")
		      (8 . "gaz-gfn-gerinc")
		      (-4 . "or>")
		      (-4 . "<or")
		      (0 . "lwpolyline")
		      (-4 . "or>")
		     )
	     ) ;_ ssget
    ) ;_ setq
    (if	(/= kv nil)
	(progn
	    (setq n (sslength kv))
	    (setq i 0)
	    (repeat n
		(setq vnev (ssname kv i))
		(command "_explode" vnev)
		(setq i (+ i 1))
	    ) ;_ repeat
	) ;_ progn
    ) ;_ if
) ;_ defun

(defun  c:GFM (/ omode)
    (SETVAR "CMDECHO" 0)
    (setq omode (getvar "osmode"))
    (command "_osnap" "_none")
    (rob)
    (torzs)
    (command "_setvar" "osmode" omode)
    (SETVAR "CMDECHO" 1)
    (print "Kesz")
    (princ)
)
(defun torzs (/ kv n i vnev)
    (setq kv (ssget "x"
		    '(
		      (-4 . "<or")
		      (8 . "gaz-gfk-gerinc")
		      (8 . "gaz-gf-gerinc")
		      (8 . "gaz-gfnk-gerinc")
		      (8 . "gaz-gfn-gerinc")
		      (-4 . "or>")
		      (-4 . "<or")
		      (0 . "line")
		      (-4 . "or>")
		     )
	     ) ;_ ssget
    ) ;_ setq
    (if	(/= kv nil)
	(progn
	    (setq n (sslength kv))
	    (setq i 0)
	    (repeat n
;;;		Az osszes vonal
		(setq vnev (ssname kv i))
		(vizsgalat vnev)		
		(setq i (+ i 1))
	    ) ;_ repeat
	) ;_ progn
    ) ;_ if
)
(defun vizsgalat (vi_vnev / Lista pk pv alfa)
    (setq Lista (entget vi_vnev))
    (setq pk (cdr (assoc 10 Lista)))
    (setq pv (cdr (assoc 11 Lista)))
    (setq alfa (rad2deg (angle pk pv)))
    (if (and (> alfa 90) (< alfa 270))
	(progn
	    (lineford vi_vnev)
	)
     )
)
(defun rad2deg (alfa / ret)
    (setq ret (* alfa (/ 180.0 pi)))
)