Kartograf�l�st seg�t� alkalmaz�s, anyagmeg�r�s �s magass�g meg�r�s feliratok helyezhet�ek �t vele.

Haszn�lat:

za paranccsal indul, addig fut am�g ESC-el meg nem szak�tj�k a felirat valamelyik elem�re kell kattintani.

A z�szl�knak line tipus�nak kell lennie, polyline nem j�. 
Magass�gmeg�r�sokn�l ha t�bb cs�tet� magass�g van egy z�szl�n, akkor nem haszn�lhat�.