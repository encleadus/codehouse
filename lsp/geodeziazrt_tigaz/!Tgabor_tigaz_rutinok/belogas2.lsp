(defun c:bl()
 (command "cmdecho" "0")
 (setq puffer 0.1)
 ;(setq belogas 0.5)
 (setq belogas (getreal "\nAdd meg a bel�g�s �rt�k�t: "))
 (setq keres (getreal "\nAdd meg milyen t�vols�gig keressen a le�gaz�s v�g�t�l ha nem �ri el a f�ldr�szlethat�rt a le�gaz�s: "))
 (setq pifel (/ pi 2)) 
  (command "_view" "_save" "temp")
 (command "_osnap" "_none")
 (command "_layer" "_make" "_belogas" "_set" "_belogas" "")

 (setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
 (setq sel-ent (nth 0 sel-ent))
 (make-vert-list sel-ent)
 (command "_zoom" "_o" "_si" sel-ent)
 (Command "_redraw")

 (setq list1 vert-list)
 (setq lof (ssget "_CP" list1 '((-4 . "<AND") (0 . "INSERT") (2 . "gaz-elz") (-4 . "AND>") )))
 (if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(command "_zoom" "_c" pstart 30)
				(Command "_redraw")
				(setq sp1 (polar pstart (/ pi 4) puffer))
	  			(setq sp2 (polar pstart (* 5 (/ pi 4)) puffer))

	 			(setq laf (ssget "_C" sp1 sp2 '( (-4 . "<OR")
				( 8 . "GAZ-Gfk-LEAGAZAS")
				( 8 . "GAZ-Gfnk-LEAGAZAS")
				( 8 . "GAZ-Gfn-LEAGAZAS")
				( 8 . "GAZ-Gf-LEAGAZAS")
				(-4 . "OR>")
				 )))
				(if laf 
				(progn
					(setq entl (ssname laf 0))
					(setq tlistl (entget entl))
					(setq p1 (cdr (assoc 10 tlistl)))
					(setq p2 (cdr (assoc 11 tlistl)))
					(if (< (distance p1 pstart) (distance p2 pstart))
							(progn
							(setq ang (angle p2 p1))
							(setq put1 (polar p1 ang keres))
							(setq put2 p2)
							)
							(progn
							(setq ang (angle p1 p2))
							(setq put1 (polar p2 ang keres))
							(setq put2 p1)
							))
					;(command "_circle" put1 5)
					;(command "_circle" put2 5)
					
					;(command "_line" pk1 pk2 pk3 pk4 pk1 "")
					
					 
					(setq van nil)
					

					;(setq put1 (polar p1 (+ ang pi) 0.1))
					;(setq put2 (polar p2 ang 0.1))
					(setq lcf (ssget "_F" (list put1 put2) '( (-4 . "<OR")
					(8 . "B15_FRL_HAT�R")
                                        (8 . "C42_FAL_KER�T�S")
					(-4 . "OR>")
				 	)))
					(setq lcu (ssget "_F" (list put1 put2) '( (-4 . "<OR")
					(8 . "C01_LAK��P�LET")
                                        (8 . "C03_GAZD_�P�LET")
					(8 . "C06_K�Z�P�LET")
					(8 . "C09_TEMPLOM")
					(8 . "C20_REND.LEN_�P")
					(-4 . "OR>")
				 	)))
					(if (and lcf (not van) (not lcu)) 
					(progn
						(setq entfr (ssname lcf 0))
						;(if (> (sslength lcf) 1) (command "_circle" pstart 5))
						(setq tlistfr (entget entfr))
						(setq pf1 (cdr (assoc 10 tlistfr)))
						(setq pf2 (cdr (assoc 11 tlistfr)))
						(setq ip (inters put1 put2 pf1 pf2 nil))
					  	;(command "_line" put1 put2 "")
					  	;(command "_line" pf1 pf2 "")
						(setq puj (polar ip ang belogas))
						(if (< (distance p1 pstart) (distance p2 pstart))
							(progn
							(setq tlistl (subst (cons 10 puj) (assoc 10 tlistl) tlistl))
                  					(entmod tlistl)
                  					(entupd entl)
							)
							(progn
							(setq tlistl (subst (cons 11 puj) (assoc 11 tlistl) tlistl))
                  					(entmod tlistl)
                  					(entupd entl)
							))
					
						(setq tlist (subst (cons 10 puj) (assoc 10 tlist) tlist))
                  				(entmod tlist)
                  				(entupd entx)
					));if lcf
				));if laf

				
				(setq j (+ j 1))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))

			)
		)
 )
 (command "_view" "_restore" "temp")	
 (command "_regen")
 (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
 (command "cmdecho" "1")
)

 ;--------------------------
 ;name      : make-vert-list
 ;--------------------------
(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list
