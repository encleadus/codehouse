mert_pont blokk lerak�, koordin�ta z�szl�val.

Az �llom�nynak tartalmaznia kell a meres-g r�teget, �s a mert_pont, gaz-koord blokkokat. 

A sablon k�nyvt�rban tal�lhat� sablon f�jl tartalmazza a gaz-koord blokkot DWG checker hib�t jelez a blokkra, nincs �ll�sfoglal�s, hogy haszn�lhat� e
ha sz�ks�ges, "burst" paranccsal felrobbanthat�, megtartva az adatokat.

A vezet�k line t�pus� kell legyen. Csak megl�v� gerinc vezet�ken helyezhet� el.

mp1 - m�rt pont besz�r�sa, koordin�ta dm pontoss�ggal
mp2 - m�rt pont besz�r�sa, koordin�ta cm pontoss�ggal


1.	- meg kell adni a pont hely�t
2.	- pontsz�mot be kell �rni, ha van
3.	- pont magass�g�t be kell �rni, ha van
4.	- a koordin�ta meg�r�s hely�t meg kell adni