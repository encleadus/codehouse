;;;---------------------------------------------------------------------------;
;;;
;;;    CREATE.LSP
;;;
;;;    (C) Copyright 1998 by Autodesk, Inc.
;;;
;;;    Permission to use, copy, modify, and distribute this software
;;;    for any purpose and without fee is hereby granted, provided
;;;    that the above copyright notice appears in all copies and
;;;    that both that copyright notice and the limited warranty and
;;;    restricted rights notice below appear in all supporting
;;;    documentation.
;;;
;;;    AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
;;;    AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
;;;    MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC.
;;;    DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
;;;    UNINTERRUPTED OR ERROR FREE.
;;;
;;;    Use, duplication, or disclosure by the U.S. Government is subject to
;;;    restrictions set forth in FAR 52.227-19 (Commercial Computer
;;;    Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
;;;    (Rights in Technical Data and Computer Software), as applicable.
;;;
;;;    Written by: Milton Hagler
;;;    July 1996
;;;
;;;---------------------------------------------------------------------------;
;;;    DESCRIPTION
;;;
;;;    This routine demonstrates how to create a topology 
;;;    using the Topology API.
;;;   
;;;---------------------------------------------------------------------------;

;;;******************************************************************
;;; Function: C:CREATE
;;;
;;; Create a node, network or polygon topology. Allocate variable
;;; memory space for the topology, input the different objects 
;;; based on the topology's type, then build it. The topology 
;;; created is left loaded, but not open.
;;;
;;;
(defun C:UJTOPO ( 
  /
  var_id                  ; variable memory ID
  name                    ; topology name
  desc                    ; topology description
  typ                     ; topology type
  filter                  ; object type filter
  ss_nod                  ; node objects
  ss_lnk                  ; link objects
  ss_ctr                  ; centroid objects
  indx                    ; index
  result
  done
  error
  )
  
  (setq error *error*)
  ;;
  ;; Define error handler
  ;;
  (defun *error* (msg)
    (alert msg)
	 (if tpm@opened
      (tpm_acclose tpm@tpm_id)
    )
    (if tpm@loaded
      (tpm_acunload tpm@name)
    )
    (setq *error* error)
    (exit)
  )
  (setq done nil)
  (while (not done)
    ;;
    ;; Create a menu of current topologies and input selection
    ;;
    (setq name (INPUT_TPM_TABLE))
    
    ;;
    ;; Delete topology
    ;;
    (if name 
      (tpm_mnterase name)
      (setq done T)  
    ) 
   )
  ;;
  ;; Allocate variable memory for parameters of topology
  ;;
	(setq var_id (tpm_varalloc))

   ;;
   ;; Input parameters for the topology
   ;;
	(initget "Gaznyomvonal Foldreszlet")
	(setq typ (getkword "\nAdd meg a topol�gia t�pus�t (Gaznyomvonal/Foldreszlet) <Exit>: "))
	(if (null typ)
		(prompt "\nNem v�lasztott�l topol�gi�t.")
		(progn
			(setq typ (strcase typ))
			(setq name "gaznyomvonal")
			(setq desc "")
		)
	)	
   

  (if (and typ name)
    (progn
      ;;
      ;; Input objects for the topology (nodes, links, centroids)
      ;; dependent on the topology type.
      ;;
		(cond 
			((= typ "GAZNYOMVONAL")
				(setq ss_nod (ssadd))
				(setq filter (list 	(cons -4 "<AND")
								(cons -4 "<OR") 
												(cons 0 "LINE")
												(cons 0 "POLYLINE")
												(cons 0 "LWPOLYLINE")
								(cons -4 "OR>")
								(cons -4 "<OR") 
												(cons 8 "GAZ-Gf-GERINC")
												(cons 8 "GAZ-Gf-LEAGAZAS")
												(cons 8 "GAZ-Gfk-GERINC")
												(cons 8 "GAZ-Gfk-LEAGAZAS")
												(cons 8 "GAZ-Gfn-GERINC")
												(cons 8 "GAZ-Gfn-LEAGAZAS")
												(cons 8 "GAZ-Gfnk-GERINC")
												(cons 8 "GAZ-Gfnk-LEAGAZAS")
								(cons -4 "OR>")
								(cons -4 "AND>")
                         )
				)
            
				(setq ss_lnk (ssget "_X" filter))
         
				(setq ss_ctr null)
			)
			((= typ "FOLDRESZLET")
				(setq name "foldreszlet")
				(setq ss_nod (ssadd))
				(setq filter (list 	(cons -4 "<AND")
								(cons -4 "<OR") 
												(cons 0 "LINE")
												(cons 0 "POLYLINE")
												(cons 0 "LWPOLYLINE")
												(cons 0 "ARC")
								(cons -4 "OR>")
								(cons -4 "<OR") 
												(cons 8 "B01_�LLAMHAT�R")
												(cons 8 "B03_MEGYEHAT�R")
												(cons 8 "B05_TELEP�L�SHAT")
												(cons 8 "B07_FEKV�SHAT�R_BT")
												(cons 8 "B09_FEKV�SHAT�R_ZK")
												(cons 8 "B11_KER�LETHAT�R")
												(cons 8 "B13_T�MBHAT�R")
												(cons 8 "B15_FRL_HAT�R")
												(cons 8 "B16_FRL_HAT�R_H")
												(cons 8 "B17_FRL_HAT�R_�")
												(cons 8 "B18_FRL_HAT�R_NL")
												(cons 8 "B20_TERV_HAT�R")
												(cons 8 "B21_TERV_HAT�R_NL")
												(cons 8 "B24_EL�_FRL_HAT")
												(cons 8 "X01_FRL_LEZ�R�S")
								(cons -4 "OR>")
								(cons -4 "AND>")
                         )
				)
            
				(setq ss_lnk (ssget "_X" filter))
				(setq filter (list 	(cons -4 "<AND")
								(cons -4 "<OR") 
												(cons 0 "TEXT")
												(cons 0 "POINT")
								(cons -4 "OR>")
								(cons -4 "<OR") 
												(cons 8 "B22_HRSZ")
												(cons 8 "B22_K�ZTER_HRSZ")
												(cons 8 "B23_T�RED�K_HRSZ")
												(cons 8 "B25_EL�_HRSZ")
								(cons -4 "OR>")
								(cons -4 "AND>")
                         )
				)
         
				(setq ss_ctr (ssget "_X" filter))
			)
		)
      

      ;;
      ;; Build the topology based on the topology type.
      ;;
	  
	  (tpm_varset var_id "CREATE_NODE" 0)
	  (tpm_varset var_id "CREATE_VIEW" 0)
	  (tpm_varset var_id "CREATE_MARKERS" 1)
      (cond
        ((and (= typ "NODE") ss_nod)
          (setq result (tpm_mntbuild var_id name desc 1 ss_nod))
        )
        ((and (= typ "GAZNYOMVONAL") ss_lnk)
          (setq result (tpm_mntbuild var_id name desc 2 ss_nod ss_lnk))
        )
        ((and (= typ "FOLDRESZLET") ss_lnk)
          (tpm_varset var_id "STOP_AT_MISSING_CNTR" 1)
		  (setq result (tpm_mntbuild var_id name desc 3 ss_nod ss_lnk ss_ctr))
		  
        )
      )
      (if (null result)
        (progn
          (prompt "\nHiba: Topol�gia nem j�tt l�tre.")
          (setq indx -1)
          (repeat (ade_errqty)
            (setq indx (1+ indx))
            (prompt (strcat "\n   ADE ERROR " (itoa indx) ": "))
            (prompt (ade_errmsg indx))
          )
        )
        (prompt "\nTopol�gia k�sz.")
      )
    )
  );if

  ;;
 
  
  (setq *error* error)                               ;restore error handler
  
  (prompt "\nK�sz.")
  (princ)

)



;;;*****************************************************************
;;; Function: INPUT_TPM_TABLE
;;;
;;; Input from the user the topology to use.
;;;
(defun INPUT_TPM_TABLE (
  /
  item
  tpmlist           ;topology list
  tpmname           ;topology name
  indx              ;index
  done
  choice
  count
  dsplist           ;display list
  )
  
  (setq tpmlist (CONSTRUCT_TPMLIST))
  
  (if (= (length tpmlist) 1)
    (progn
      (setq tpmname (caar tpmlist))
      (prompt (strcat "\nTopology " tpmname " set."))
    )
    (progn
      (textscr)
      (while (not done)
        (setq indx 0)
        (setq count 0)
        (setq dxplist nil)

       
        (foreach item tpmlist
          (setq indx (1+ indx))
          (if (or (= 1 (cadr item))                 ;node topo
				  (= 2 (cadr item))                 ;network topo 
                  (= 3 (cadr item))                 ;polygon topo
              )
            (progn
              (setq dsplist (cons (car item) dsplist))
              (setq count (1+ count))
              
            )
          )
        );foreach
         
        (if dsplist
			(progn
            (setq tpmname (nth 0 (reverse dsplist)))
            (prompt (strcat "\nTopology " tpmname " set."))
		))
            (setq done t)
          
        
      );while
    )
  )
  
  tpmname
  
);INPUT_TPM_TABLE

;;;*****************************************************************
;;; Function: CONSTRUCT_TPMLIST
;;;
;;; Interate through the list of topologies. Make a list of
;;; topology names, with topo type and description.
;;;
(defun CONSTRUCT_TPMLIST (
  /
  done
  tpmlist
  lst
  itr_id
  name
  )
  
  (prompt "\nTall�z�s a topol�gi�k k�z�tt.")
  
  (setq itr_id (tpm_iterstart))
  (if (null itr_id)
    (prompt "\nHIBA: Nem lehet elkezdeni a tall�z�st.")
    (while (not done)
      (if (null (tpm_iternext itr_id))
        (setq done T)
        (progn
          (setq name (tpm_itername itr_id))
          ;;
          ;; Capture only current topologies
          ;;
          (if (tpm_acexist name nil nil)    
            (progn
              (setq lst (list name (tpm_itertype itr_id)
                                   (tpm_iterdesc itr_id)
                        )
              )
              (setq tpmlist (cons lst tpmlist))
            )
          )
        )
      )
    )      
  );if
  
  (if (null tpmlist)
    (prompt "\nNincs l�tez� topol�gia.")
  )
  
  (reverse tpmlist)

)
(setvar "CMDECHO" 0)
(prompt "\nUJTOPO: Minden topol�gia t�rl�se.")
(princ)
(setvar "CMDECHO" 1)

