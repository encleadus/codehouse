(defun c:vhosz ()
  (command "_cmdecho" "0")
  (command "_view" "_save" "temp")
  (command "_osnap" "_non")
  (command "_dimzin" "0")
  (setq lay_name "GAZ-Gfk-VEDOCSO")
  (setq lay_name2 "foldreszlet")
  (setq lof nil)
  
  ;(setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
  ;(setq sel-ent (nth 0 sel-ent))
  ;(make-vert-list sel-ent)
  ;(setq list1 vert-list)
  ;(princ list1)
  (setq lof (ssget "x" '((-4 . "<AND")(0 . "LWPOLYLINE")(-4 . "<OR")(8 . "GAZ-Gfk-VEDOCSO")(8 . "GAZ-Gf-VEDOCSO")(8 . "GAZ-Gfn-VEDOCSO")(8 . "GAZ-Gfnk-VEDOCSO")(-4 . "OR>")(-4 . "AND>"))))


  (setq puffer 0.1)
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq entx (ssname lof j))
	(make-vert-list entx)
	(setq oldal (length vert-list))
	(setq tlist (entget entx))
	(setq layer (cdr (assoc 8 tlist)))
	(setq len (- (strlen layer) 8))
	(setq lay1 (strcat (substr layer 1 len) "-ANY-ATM-FELIRAT"))

	(command "_layer" "_m" "haz_tmp" "_c" "1" "" "")
	(command "_change" entx "" "_p" "_la" "haz_tmp" "")
	(setq obj (vlax-ename->vla-object entx))
	(setq hoss (/ (- (vla-get-length obj) 2) 2))
        ;(princ hoss)
	(setq i 0)
	(while (< i oldal)
	  (setq vertp1 (nth i vert-list))
	  (if (> (+ i 1) (- oldal 1))
	    (setq vertp2 (nth 0 vert-list))
	    (setq vertp2 (nth (+ i 1) vert-list))
	  )
	  (setq sp-angle (angle vertp1 vertp2))
	  (setq szog (/ (* 180 sp-angle) PI))
	  (setq tav (distance vertp1 vertp2))
	  (setq felezop (polar vertp1 sp-angle (/ tav 2)))
	  (setq sp11 (polar vertp1 (/ pi 4) puffer))
	  (setq sp12 (polar vertp2 (/ pi 4) puffer))
	  (setq sp14 (polar vertp1 (* 5 (/ pi 4)) puffer))	
	  (setq sp13 (polar vertp2 (* 5 (/ pi 4)) puffer))

	  (command "_zoom" "_c" felezop "70")
 	  ;(command "_line" sp11 sp12 sp13 sp14 "")

	  (setq	amer (ssget "_CP" (list
			    sp11
		            sp12
			    sp13		
			    sp14
			    )
			    (list (cons 0 "LINE") (cons 8 lay1)
			    )
		     )
	  )

	  (if (/= amer nil)
 
	    (progn
		(setq enty (ssname amer 0))
		(setq tlisty (entget enty))	      
		(setq pstart (cdr (assoc 10 tlisty)))
		(setq pend (cdr (assoc 11 tlisty)))
		(command "_change" enty "" "_p" "_la" "haz_tmp" "")
		(setq spo1 (polar pstart (/ pi 4) puffer))
 		(setq spo2 (polar pstart (* 5 (/ pi 4)) puffer))
		(setq spo11 (polar pend (/ pi 4) puffer))
 		(setq spo12 (polar pend (* 5 (/ pi 4)) puffer))
		(setq vv (ssget "_C" spo1 spo2 (list (cons 0 "LINE") (cons 8 lay1))))
		(if (= vv nil) (setq vv (ssget "_C" spo11 spo12  (list (cons 0 "LINE") (cons 8 lay1)))))
 		(if (/= vv nil)
			(progn
				(command "_change" enty "" "_p" "_la" lay1 "")
				(setq enty (ssname vv 0))
				(setq tlisty (entget enty))	      
				(setq pstart (cdr (assoc 10 tlisty)))
				(setq pend (cdr (assoc 11 tlisty)))
			)
			(command "_change" enty "" "_p" "_la" lay1 "")
		)
                
		(setq midpt (list (/ (+ (car pstart) (car pend)) 2) (/ (+ (cadr pstart) (cadr pend)) 2)))
		(setq szog (angle pstart pend))
		(setq szog2 szog)
		(setq szog (/ (* 180.0 szog) pi))
		(if (< 90 szog 270) (setq szog (+ 180 szog)))
		(setq szog (* szog (/ pi 180)))
		(setq sp31 (polar midpt (+ szog (* 7.5 (/ pi 4))) (- (distance midpt pend) 0.2)))
		(setq sp32 (polar midpt (+ szog (* 4.5 (/ pi 4))) (- (distance midpt pend) 0.2)))
		(setq sp33 (polar midpt (+ szog (* 6 (/ pi 4))) 0.9))
		;(command "_circle" sp33 "5")
		(setq anyag (ssget "_F" (list sp33 sp31) (list (cons 0 "TEXT") (cons 8 lay1))))
		(if (= anyag nil) (setq anyag (ssget "_F" (list sp33 sp32) (list (cons 0 "TEXT") (cons 8 lay1)))))
		(if anyag
		(progn
			(setq entz (ssname anyag 0))
			(command "_layer" "_m" "_hiba" "")
			(setq tlistz (entget entz))	      
			(setq txt (cdr (assoc 1 tlistz)))
			(setq pins (cdr (assoc 10 tlistz)))
			(if (= (substr txt (strlen txt)) ")")
				(progn
					(setq sp nil)
					(if (= (substr txt (- (strlen txt) 4) 1) "(") (setq sp 3))
					(if (= (substr txt (- (strlen txt) 5) 1) "(") (setq sp 4))
					;(setq hossz (rtos hoss 2 1) )
	
					(if sp
						(progn
							(setq txth (substr txt (- (strlen txt) sp) sp ))
							(setq txth (atof txth))
							(if (<= 0.01 (abs (- hoss txth))) (command "_circle" midpt "3"))
							;(alert hossz)
							;(alert txth)
						);progn
					);if
					
				)
			)
 		)
		(progn
			(command "_layer" "_m" "_hiba" "")
		      	(command "_circle" midpt "3")
		      	
		))
		(setq i oldal)
	    ) ;progn
	  ) ;if
	  (setq i (+ i 1))
	) ;while
	(setq j (+ j 1))
	(command "_change" entx "" "_p" "_la" layer "")
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
      ) ;while
    ) ;progn
  ) ;if
  (command "_view" "_restore" "temp")
  (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
  (command "_cmdecho" "1")
) ;defun

(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list