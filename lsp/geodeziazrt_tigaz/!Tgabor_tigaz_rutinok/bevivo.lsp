; Koordin�ta list�b�l "mert_pont" blokkokat rak fel a "GAZ-GFK-MERES-G" f�li�ra.
; Adatok CSV form�tumban:
; P;Y.y;X.x;Z.z
; A "P" pontsz�m �s a "Z.z" magass�g a blokk attrib�tum�ba ker�l.
(defun c:bevivo ()
	(setvar "CMDECHO" 0)
	(command "._undo" "_begin")
	(command "_osnap" "_non")
	(command "_dimzin" "0")
	(command "_view" "_save" "temp")
	(setq plast nil)
	
	(setq fname (getfiled "V�laszd ki a CSV f�jlt!" "." "csv" 8))
	(if fname (setq infile (open fname "r"))
			  (setq infile nil))
	(if infile
		(progn
			 ;(command "_insert" "vedoov.dxf" "0,0"  "1" "1" "0")
			 (setq line (read-line infile))
			 (setq i 0)
			 (vl-cmdf "_layer" "_s" "GAZ-GFK-MERES-G" "")
			 (while line
				
				(setq sp (vl-string-position (ascii ";") line))
				(setq ep 0)
				(setq lista (list (substr line 1 sp)))
				(while sp
					(setq ep sp)
					(setq sp (vl-string-position (ascii ";") line (1+ ep)))
					(if sp
						(setq lista (cons (substr line (+ ep 2) (- sp ep 1)) lista))
						(setq lista (cons (substr line (+ ep 2)) lista))
					)
				)
				(setq lista (reverse lista))
				(setq pstart (strcat (nth 1 lista) "," (nth 2 lista)))
				;(command "_point" pstart)
				(setq pont (nth 0 lista))
				(setq mag (nth 3 lista))
				(command "_insert" "mert_pont" pstart "0.5" "0.5" "0" pont mag) 
						
					
				
				(setq line (read-line infile))
				(setq i (1+ i))
				(princ (strcat "\n " (itoa i)))
			 )
		)
	)
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_view" "_restore" "temp")
	(command "_regen")
	(command "._undo" "_end")
	(setvar "CMDECHO" 1)
)








