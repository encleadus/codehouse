(defun kpe (sp1 sp2 txt1 txt2 layer)
 
  (command "_layer" "_s" layer "")
  (setq sp-angle (angle sp1 sp2))
  (setq stl (* (/ (+ (max (strlen txt1) (strlen txt2)) 1) 2) 0.9))
  (if (< 2 stl) (setq sp2 (polar sp2 sp-angle stl)))
  (command "_line" sp1 sp2 "")
  (setq stl (* (/ (+ (max (strlen txt1) (strlen txt2)) 1) 2) 0.9))
  (setq szogi (/ (* 180 (angle sp1 sp2)) PI))
  (if (<= 90  szogi 270) 
	(progn
	(setq sp-angle (angle sp2 sp1))
	(setq spv (polar sp2 sp-angle stl)) 
	)
	(progn
	(setq sp-angle (angle sp1 sp2))
	(setq spv (polar sp2 (- sp-angle pi) stl))
	)
  )
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  
  (setq sp3 (polar spv sp-angle-meroleges 0.01))
  (setq sp4 (polar spv sp-angle-meroleges -0.2))
 

  (setq szog (/ (* 180.0 sp-angle) pi))
  
  (command "_text" "_s" "ARIAL_D�LT" "_J" "_BC" sp3 "1" szog txt1 "")
  (command "_text" "_s" "ARIAL_D�LT" "_J" "_TC" sp4 "1" szog txt2 "")

) ;defun


(defun c:vmegiro ()
  (command "_cmdecho" "0")
  (command "_view" "_save" "temp")
  (command "_osnap" "_non")
  (command "_dimzin" "0")
  (setq lay_name "GAZ-Gfk-LEAGAZAS")
  (setq lay_name2 "foldreszlet")
  (setq lof nil)
  (command "_peditaccept" "1")
  (setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
  (setq sel-ent (nth 0 sel-ent))
  (make-vert-list sel-ent)
  (setq list1 vert-list)
  ;(princ list1)
  (setq ss (ssget "_CP" list1 '((-4 . "<AND")(-4 . "<OR")(0 . "LWPOLYLINE")(0 . "LINE")(-4 . "OR>")(-4 . "<OR")(8 . "GAZ-Gfk-VEDOCSO")(8 . "GAZ-Gf-VEDOCSO")(8 . "GAZ-Gfn-VEDOCSO")(8 . "GAZ-Gfnk-VEDOCSO")(-4 . "OR>")(-4 . "AND>"))))
  (command "_pedit" "_m" ss ""  "_j" "0.00" "")
  (setq lof (ssget "_CP" list1 '((-4 . "<AND")(0 . "LWPOLYLINE")(-4 . "<OR")(8 . "GAZ-Gfk-VEDOCSO")(8 . "GAZ-Gf-VEDOCSO")(8 . "GAZ-Gfn-VEDOCSO")(8 . "GAZ-Gfnk-VEDOCSO")(-4 . "OR>")(-4 . "AND>"))))
  (setq puffer 0.1)
  (setq atmero nil)
  (setq vamero nil)
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq entx (ssname lof j))
	(make-vert-list entx)
	(setq oldal (length vert-list))
	(setq tlist (entget entx))
	(setq layer (cdr (assoc 8 tlist)))
	(setq len (- (strlen layer) 8))
	(setq lay1 (strcat (substr layer 1 len) "-ANY-ATM-FELIRAT"))
	(setq lay2 (strcat (substr layer 1 len) "-ANY-ATM-FELIRAT_VCS"))
	(setq lay3 (strcat (substr layer 1 len) "-GERINC"))
	(setq lay4 (strcat (substr layer 1 len) "-LEAGAZAS"))
	(setq layz (strcat lay3 "," lay4))


	(command "_layer" "_m" "haz_tmp" "_c" "1" "" "")
	(command "_change" entx "" "_p" "_la" "haz_tmp" "")
	(setq obj (vlax-ename->vla-object entx))
	(setq hoss (/ (- (vla-get-length obj) 2) 2))
        ;(princ hoss)
	(setq i 0)
	(setq van nil)
	(while (< i oldal)
	  (setq vertp1 (nth i vert-list))
	  (if (> (+ i 1) (- oldal 1))
	    (setq vertp2 (nth 0 vert-list))
	    (setq vertp2 (nth (+ i 1) vert-list))
	  )
	  (setq sp-angle (angle vertp1 vertp2))
	  (setq szog (/ (* 180 sp-angle) PI))
	  (setq tav (distance vertp1 vertp2))
	  (setq vertp11 (polar vertp1 sp-angle -0.01))
	  (setq vertp21 (polar vertp2 sp-angle 0.01))
	  (setq felezop (polar vertp1 sp-angle (/ tav 2)))
	  (setq sp11 (polar vertp11 (/ pi 4) puffer))
	  (setq sp12 (polar vertp21 (/ pi 4) puffer))
	  (setq sp14 (polar vertp11 (* 5 (/ pi 4)) puffer))	
	  (setq sp13 (polar vertp21 (* 5 (/ pi 4)) puffer))

	  (command "_zoom" "_c" felezop "70")
 	  ;(command "_line" sp11 sp12 sp13 sp14 "")

	  (setq	amer (ssget "_CP" (list
			    sp11
		            sp12
			    sp13		
			    sp14
			    )
			    (list (cons 0 "LINE") (cons 8 lay1)
			    )
		     )
	  )

	  (if (/= amer nil)
 	    (progn
		(setq amerh (sslength amer))
		(setq x 0)
		(while (< x amerh)
			(setq entz (ssname amer x))
			(setq tlistz (entget entz))
			(setq pt1 (cdr (assoc 10 tlistz)))
			(setq pt2 (cdr (assoc 11 tlistz)))
			(if (or (< (abs (- (angle vertp1 pt1) (angle vertp1 vertp2))) 0.01) (< (abs (- (angle vertp1 pt2) (angle vertp1 vertp2))) 0.01))
				(progn
					(setq van 1)
					(setq i oldal)
					(setq x amerh)
				))
			(setq x (+ x 1))
		);while
	    ) ;progn
	  ) ;if
	  (setq i (+ i 1))
	) ;while

	(setq j (+ j 1))
	(command "_change" entx "" "_p" "_la" layer "")
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
	(if (not van)
		(progn
			(command "_layer" "_m" "_hiba" "")
		      	(command "_circle" felezop "3")
			(setq i 0)
			(while (and (ssget "_F" (list vertp1 vertp2) (list (cons 0 "LINE,LWPOLYLINE") (cons 8 "*GERINC,*LEAGAZAS") )) (< i oldal))
	  			(setq vertp1 (nth i vert-list))
	  			(if (> (+ i 1) (- oldal 1))
	    				(setq vertp2 (nth 0 vert-list))
	   				(setq vertp2 (nth (+ i 1) vert-list))
	 			)
				(setq i (+ i 1))
			);while
		
		(setq sp-angle (angle vertp1 vertp2))
	  	(setq szog (/ (* 180 sp-angle) PI))
	  	(setq tav (distance vertp1 vertp2))
		(setq felezop (polar vertp1 sp-angle (/ tav 2)))
		(setq sp-angle (+ sp-angle (/ pi 2)))
		(setq ujpont1a (polar felezop sp-angle -4))
		
		(setq ujpont2a (polar felezop sp-angle 4))
		(setq haiti nil)
		(setq haiti (ssget "_F" (list
			    ujpont2a
		            ujpont1a
			    )
			    (list (cons 0 "LWPOLYLINE,LINE") (cons 8 lay3)
			    )
		   	  )
	 		 )
		(setq laos nil)
		(setq laos (ssget "_F" (list
			    felezop
		            ujpont1a
			    )
			    (list (cons 0 "LWPOLYLINE,LINE") (cons 8 layz)
			    )
		   	  )
	 		 )
		(if haiti
			(progn
			(if (not vamero) 
				(progn
				(setq vamero (getstring "\n Add meg az �tm�r�t gerincre !"))
				(setq vanyag (getstring "\n Add meg az anyagot !"))
				)
			)
			(if (not laos) (kpe felezop ujpont1a vamero vanyag lay1) (kpe felezop ujpont2a vamero vanyag lay1))
			)
			(progn
			(if (not atmero) 
				(progn
				(setq atmero (getstring "\n Add meg az �tm�r�t le�gaz�sra !"))
				(setq anyag (getstring "\n Add meg az anyagot !"))
				)
			)
			(if (not laos) (kpe felezop ujpont1a atmero anyag lay1) (kpe felezop ujpont2a atmero anyag lay1))
			)
		);if
		);progn
		
	)
      ) ;while
    ) ;progn
  ) ;if
  (command "_layer" "_s" "0" "")
  (command "_view" "_restore" "temp")
  (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
  (command "_.purge" "_LA" "haz_tmp" "_N")
  (command "_cmdecho" "1")
) ;defun

(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list