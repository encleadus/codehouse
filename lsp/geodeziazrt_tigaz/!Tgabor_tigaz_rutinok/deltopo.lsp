;;;---------------------------------------------------------------------------;
;;;
;;;    CREATE.LSP
;;;
;;;    (C) Copyright 1998 by Autodesk, Inc.
;;;
;;;    Permission to use, copy, modify, and distribute this software
;;;    for any purpose and without fee is hereby granted, provided
;;;    that the above copyright notice appears in all copies and
;;;    that both that copyright notice and the limited warranty and
;;;    restricted rights notice below appear in all supporting
;;;    documentation.
;;;
;;;    AUTODESK PROVIDES THIS PROGRAM "AS IS" AND WITH ALL FAULTS.
;;;    AUTODESK SPECIFICALLY DISCLAIMS ANY IMPLIED WARRANTY OF
;;;    MERCHANTABILITY OR FITNESS FOR A PARTICULAR USE.  AUTODESK, INC.
;;;    DOES NOT WARRANT THAT THE OPERATION OF THE PROGRAM WILL BE
;;;    UNINTERRUPTED OR ERROR FREE.
;;;
;;;    Use, duplication, or disclosure by the U.S. Government is subject to
;;;    restrictions set forth in FAR 52.227-19 (Commercial Computer
;;;    Software - Restricted Rights) and DFAR 252.227-7013(c)(1)(ii)
;;;    (Rights in Technical Data and Computer Software), as applicable.
;;;
;;;    Written by: Milton Hagler
;;;    July 1996
;;;
;;;---------------------------------------------------------------------------;
;;;    DESCRIPTION
;;;
;;;    This routine demonstrates how to create a topology 
;;;    using the Topology API.
;;;   
;;;---------------------------------------------------------------------------;

;;;******************************************************************
;;; Function: C:CREATE
;;;
;;; Create a node, network or polygon topology. Allocate variable
;;; memory space for the topology, input the different objects 
;;; based on the topology's type, then build it. The topology 
;;; created is left loaded, but not open.
;;;
;;;
(defun C:DELTOPO ( 
  /
  var_id                  ; variable memory ID
  name                    ; topology name
  desc                    ; topology description
  typ                     ; topology type
  filter                  ; object type filter
  ss_nod                  ; node objects
  ss_lnk                  ; link objects
  ss_ctr                  ; centroid objects
  indx                    ; index
  result
  done
  error
  )
  
  (setq error *error*)
  ;;
  ;; Define error handler
  ;;
  (defun *error* (msg)
    (alert msg)
	 (if tpm@opened
      (tpm_acclose tpm@tpm_id)
    )
    (if tpm@loaded
      (tpm_acunload tpm@name)
    )
    (setq *error* error)
    (exit)
  )
  (setq done nil)
  (while (not done)
    ;;
    ;; Create a menu of current topologies and input selection
    ;;
    (setq name (INPUT_TPM_TABLE))
    
    ;;
    ;; Delete topology
    ;;
    (if name 
      (tpm_mnterase name)
      (setq done T)  
    ) 
   )
    
  
  ;;
 
  
  (setq *error* error)                               ;restore error handler
  
  (prompt "\nK�sz.")
  (princ)

);C:DELTOPO

(prompt "\nDELTOPO: Minden topol�gia t�rl�se.")
(princ)

;;;*****************************************************************
;;; Function: INPUT_TPM_TABLE
;;;
;;; Input from the user the topology to use.
;;;
(defun INPUT_TPM_TABLE (
  /
  item
  tpmlist           ;topology list
  tpmname           ;topology name
  indx              ;index
  done
  choice
  count
  dsplist           ;display list
  )
  
  (setq tpmlist (CONSTRUCT_TPMLIST))
  
  (if (= (length tpmlist) 1)
    (progn
      (setq tpmname (caar tpmlist))
      (prompt (strcat "\nTopology " tpmname " set."))
    )
    (progn
      (textscr)
      (while (not done)
        (setq indx 0)
        (setq count 0)
        (setq dxplist nil)

       
        (foreach item tpmlist
          (setq indx (1+ indx))
          (if (or (= 1 (cadr item))                 ;node topo
				  (= 2 (cadr item))                 ;network topo 
                  (= 3 (cadr item))                 ;polygon topo
              )
            (progn
              (setq dsplist (cons (car item) dsplist))
              (setq count (1+ count))
              
            )
          )
        );foreach
         
        (if dsplist
			(progn
            (setq tpmname (nth 0 (reverse dsplist)))
            (prompt (strcat "\nTopology " tpmname " set."))
		))
            (setq done t)
          
        
      );while
    )
  )
  
  tpmname
  
);INPUT_TPM_TABLE

;;;*****************************************************************
;;; Function: CONSTRUCT_TPMLIST
;;;
;;; Interate through the list of topologies. Make a list of
;;; topology names, with topo type and description.
;;;
(defun CONSTRUCT_TPMLIST (
  /
  done
  tpmlist
  lst
  itr_id
  name
  )
  
  (prompt "\nTall�z�s a topol�gi�k k�z�tt.")
  
  (setq itr_id (tpm_iterstart))
  (if (null itr_id)
    (prompt "\nHIBA: Nem lehet elkezdeni a tall�z�st.")
    (while (not done)
      (if (null (tpm_iternext itr_id))
        (setq done T)
        (progn
          (setq name (tpm_itername itr_id))
          ;;
          ;; Capture only current topologies
          ;;
          (if (tpm_acexist name nil nil)    
            (progn
              (setq lst (list name (tpm_itertype itr_id)
                                   (tpm_iterdesc itr_id)
                        )
              )
              (setq tpmlist (cons lst tpmlist))
            )
          )
        )
      )
    )      
  );if
  
  (if (null tpmlist)
    (prompt "\nNincs l�tez� topol�gia.")
  )
  
  (reverse tpmlist)

);CONSTRUCT_TPMLIST
