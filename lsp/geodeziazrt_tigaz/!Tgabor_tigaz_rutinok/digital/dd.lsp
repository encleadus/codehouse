(defun c:dd ()
(setvar "CMDECHO" 0)
(command "._undo" "_begin")
;(command "_layer" "_m" "_hiba" "_c" "1" "" "")
(command "_osnap" "_non")
(command "_dimzin" "0")
(command "_view" "_save" "temp")
(command "_zoom" "_e")
(setq puffer 0.1)
(setq buffer 2.5)
(setq kisbetumag 0.9)

(setq lof (ssget "_X" (list (cons 0 "TEXT") (cons 8 "B22_HRSZ"))))
(if lof 
	(progn
		(setq lilo 0)
		(setq lista (list ""))
		(setq ssn (sslength lof))
		(repeat ssn
				(setq entit (ssname lof lilo))
				(setq tlist (entget entit))
				(setq lilo (1+ lilo))
				(setq txt (cdr (assoc 1 tlist)))
				(if (member txt lista)
					(command "_erase" entit "")
					(setq lista (cons txt lista))
				)
		)
				
	)
)
(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
(command "_view" "_restore" "temp")
(command "._undo" "_end")
(setvar "CMDECHO" 1)

)
(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list
