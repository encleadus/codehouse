(defun c:zaszlo ( )
	(setvar "CMDECHO" 0)
	(command "._undo" "_begin")
	(command "_view" "_save" "temp")
	(command "_osnap" "_non")
	(command "_dimzin" "0")
	(setq buffer 0.3)
	(setq lay (getstring "\nAdd meg a gerinc r�teg nev�t vagy [Kiv�laszt�s]: "))
	(if (or (= lay "") (= lay "k") (= lay "K"))
		(progn
			(setq lay "")
			(while (= lay "")
				(setq ent1 (car (entsel "\nB�kj az elemre!")))
				(if ent1
				(setq lay (cdr (assoc 8 (entget ent1))))
				)
			)
		)
	)

	(command "_zoom" "_e")
	(setq lof (ssget "x" (list (cons 0 "LINE") (cons 8 lay))))
	(if lof
		(progn
			(command "_layer" "_m" "zaszlo" "")
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(setq pend (cdr (assoc 11 tlist)))
				;(command "_zoom" "_c" pstart "30")
				(setq sp11 (polar pstart (/ pi 4) buffer))
				(setq sp12 (polar pend (/ pi 4) buffer))
				(setq sp14 (polar pstart (* 5 (/ pi 4)) buffer))	
				(setq sp13 (polar pend (* 5 (/ pi 4)) buffer))
				(command "_zoom" "_c" pstart "30")
				(setq lef (ssget "_C" sp11 sp14 (list (cons 0 "LINE") (cons 8 lay))))
				(command "_zoom" "_c" pend "30")
				(setq ref (ssget "_C" sp12 sp13 (list (cons 0 "LINE") (cons 8 lay))))
				(if (or (< (sslength lef) 2) (< (sslength ref) 2))
					(progn
					(setq tlist (subst (cons 8 "zaszlo")(assoc 8 tlist) tlist))
					(entmod tlist)
					)
				)
				(setq j (+ j 1))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)

		)
		(princ "\nNincs egyetlen elem sem a rajzban!")
	)
	(princ "\n")
	  (command "_view" "_restore" "temp")
	  (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	  ;(command "._purge" "_LA" "haz_tmp" "_N")
      (command "._undo" "_end")
      (setvar "CMDECHO" 1)

)
 


	