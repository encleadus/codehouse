(defun kpe (sp1 sp2 txt1 txt2 layer)
 
  (command "_layer" "_s" layer "")
  (command "_line" sp1 sp2 "")
  (setq stl (* (/ (+ (max (strlen txt1) (strlen txt2)) 1) 2) 0.9))
  (setq szogi (/ (* 180 (angle sp1 sp2)) PI))
  (if (<= 90  szogi 270) 
	(progn
	(setq sp-angle (angle sp2 sp1))
	(setq spv (polar sp2 sp-angle stl)) 
	)
	(progn
	(setq sp-angle (angle sp1 sp2))
	(setq spv (polar sp2 (- sp-angle pi) stl))
	)
  )
  (setq sp-angle-meroleges (+ sp-angle (/ pi 2)))
  
  (setq sp3 (polar spv sp-angle-meroleges 0.01))
  (setq sp4 (polar spv sp-angle-meroleges -0.2))
 

  (setq szog (/ (* 180.0 sp-angle) pi))
  
  (command "_text" "_s" "ARIAL_D�LT" "_J" "_BC" sp3 "1" szog txt1 "")
  (command "_text" "_s" "ARIAL_D�LT" "_J" "_TC" sp4 "1" szog txt2 "")

) ;defun

(defun c:gdigit ()
  (setvar "CMDECHO" 0)
  (command "._undo" "_begin")
  (command "_view" "_save" "temp")
  (command "_osnap" "_non")
  (command "_dimzin" "0")
  (setq lay_name "GAZ-Gfk-LEAGAZAS")
  (setq lay_name2 "foldreszlet")
  (setq lof nil)
  (command "_peditaccept" "1")
  (setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
  (setq sel-ent (nth 0 sel-ent))
  (make-vert-list sel-ent)
  (setq list1 vert-list)
  ;(princ list1)
  ;(setq atmero (getstring "\n Add meg az �tm�r�t !"))
  (setq anyag "PE")
  (setq ss (ssget "_CP" list1 (list (cons 0 "LINE,LWPOLYLINE") (cons 8 "*gerinc*"))))
  (setq ssn (sslength ss))
  (setq i 0)
  (repeat ssn
  (setq entx (ssname ss i))
  (setq tlist (entget entx))
  (setq layer (cdr (assoc 8 tlist)))
  (if (= i 0) (setq reteglista (list layer)) (if (not (member layer reteglista)) (setq reteglista (cons layer reteglista))))
  (setq i (1+ i))
  (princ (strcat "\n " (itoa i) "/" (itoa ssn)))
  )
  (setq i 0)
  (foreach reteg reteglista
	(setq ss (ssget "_CP" list1 (list (cons 0 "LINE,LWPOLYLINE") (cons 8 reteg))))
	(command "_pedit" "_m" ss ""  "_j" "0.00" "")
	(setq i (1+ i))
	(princ (strcat "\n " (itoa i) "/" (itoa (length reteglista))))
  )
  ; (setq lof (ssget "_CP" list1 (list (cons 0 "LWPOLYLINE") (cons 8 "*gerinc*"))))
  ; (repeat 5
  ; (if lof
		; (progn
			; (setq hlof (sslength lof))
			; (setq j 0)
			; (while (< j hlof)
				; (setq entx (ssname lof j))
				; (make-vert-list entx)
				; (setq tlist (entget entx))
				; (setq pstart (cdr (assoc 10 tlist)))
				; (command "_zoom" "_c" pstart 30)
				; (setq vertex (length vert-list))
				; (setq i 0)
				; (while (< (+ 2 i) vertex)
					; (setq vertp1 (nth i vert-list))
					; (setq vertp2 (nth (1+ i) vert-list))
					; (setq vertp3 (nth (+ 2 i) vert-list))
					; (if (< (* 0.3 pi) (abs (- (angle vertp1 vertp2) (angle vertp2 vertp3))))
						; (command "_.break" entx vertp2 "@")
					; )
					; (setq i (1+ i))
				; )
				; (setq j (1+ j))
				; (princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			; )
		; )
	; )
	; )

  (setq lof (ssget "_CP" list1 (list (cons 0 "LWPOLYLINE") (cons 8 "*gerinc*"))))
  (setq puffer 0.2)
  (setq buffer 2)
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
	  (command "_layer" "_m" "GAZ-Gfk-ANY-ATM-FELIRAT" "")
      (while (< j hlof)
	(setq entx (ssname lof j))
	(make-vert-list entx)
	(setq oldal (length vert-list))
	(setq tlist (entget entx))
	(setq layer (cdr (assoc 8 tlist)))
	(setq atmero (substr layer (+ 1 (vl-string-position (ascii " ") layer))))
	;(setq len (- (strlen layer) 9))
	(setq lay1 "GAZ-Gfk-ANY-ATM-FELIRAT")

	(command "_layer" "_m" "haz_tmp" "_c" "1" "" "")
	(command "_change" entx "" "_p" "_la" "haz_tmp" "")
	(setq obj (vlax-ename->vla-object entx))
	(setq hoss (/ (- (vla-get-length obj) 2) 2))
        ;(princ hoss)
	(setq i 0)
	(setq van nil)
	(setq tavszum 0)
	(while (< i (- oldal 1))
	  (setq vertp1 (nth i vert-list))
	  (if (> (+ i 1) (- oldal 1))
	    (setq vertp2 (nth 0 vert-list))
	    (setq vertp2 (nth (+ i 1) vert-list))
	  )
	  (setq sp-angle (angle vertp1 vertp2))
	  (setq szog (/ (* 180 sp-angle) PI))
	  (setq tav (distance vertp1 vertp2))
	  (setq tavszum (+ tav tavszum))
	  (setq felezop (polar vertp1 sp-angle (/ tav 2)))
	  (setq sp11 (polar vertp1 (/ pi 4) puffer))
	  (setq sp12 (polar vertp2 (/ pi 4) puffer))
	  (setq sp14 (polar vertp1 (* 5 (/ pi 4)) puffer))	
	  (setq sp13 (polar vertp2 (* 5 (/ pi 4)) puffer))

	  (command "_zoom" "_c" felezop "70")
 	  ;(command "_line" sp11 sp12 sp13 sp14 "")
		(if (< 150 tavszum)
		(progn
			(setq tavszum (- tavszum 150))
			(command "_layer" "_m" "_hiba" "")
		      	;(command "_circle" felezop "3")
			(setq sp-angle (+ sp-angle (/ pi 2)))
			(setq ujpont1a (polar felezop sp-angle -4))
			(setq ujpont1b (polar felezop sp-angle -2))
			(setq ujpont2a (polar felezop sp-angle 4))
			(setq sp11 (polar ujpont1a (/ pi 4) buffer))
	  		(setq sp12 (polar ujpont1b (/ pi 4) buffer))
	  		(setq sp14 (polar ujpont1a (* 5 (/ pi 4)) buffer))	
	 	    (setq sp13 (polar ujpont1b (* 5 (/ pi 4)) buffer))
			(setq laos nil)
			(setq laos (ssget "_CP" (list
			    sp11
		            sp12
			    sp13		
			    sp14
			    )
			    (list (cons 0 "TEXT,LINE")
			    )
		   	  )
	 		 )	
			(if (not laos) (kpe felezop ujpont1a atmero anyag lay1) (kpe felezop ujpont2a atmero anyag lay1))

		)
	)
	  

	  
	  (setq i (+ i 1))
	) ;while
	(setq j (+ j 1))
	(command "_change" entx "" "_p" "_la" layer "")
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
	(if (not van)
		(progn
			(command "_layer" "_m" "_hiba" "")
		      	;(command "_circle" felezop "3")
			(setq sp-angle (+ sp-angle (/ pi 2)))
			(setq ujpont1a (polar felezop sp-angle -4))
			(setq ujpont1b (polar felezop sp-angle -2))
			(setq ujpont2a (polar felezop sp-angle 4))
			(setq sp11 (polar ujpont1a (/ pi 4) buffer))
	  		(setq sp12 (polar ujpont1b (/ pi 4) buffer))
	  		(setq sp14 (polar ujpont1a (* 5 (/ pi 4)) buffer))	
	 	        (setq sp13 (polar ujpont1b (* 5 (/ pi 4)) buffer))
			(setq laos nil)
			(setq laos (ssget "_CP" (list
			    sp11
		            sp12
			    sp13		
			    sp14
			    )
			    (list (cons 0 "TEXT,LINE")
			    )
		   	  )
	 		 )	
			(if (not laos) (kpe felezop ujpont1a atmero anyag lay1) (kpe felezop ujpont2a atmero anyag lay1))

		)
	)
      ) ;while
    ) ;progn
  ) ;if
  (command "_view" "_restore" "temp")
  (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
  (command "_.purge" "_LA" "haz_tmp" "_N")
  (command "._undo" "_end")
(setvar "CMDECHO" 1)
) ;defun

(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list

