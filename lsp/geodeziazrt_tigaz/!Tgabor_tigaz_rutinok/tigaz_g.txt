	atm	- anyag �tm�r� feliratot mer�legesre forgatja a vezet�khez k�pest �s a z�szl� k�ls� oldal�ra rendezi
	mr	- magass�gi meg�r�st �jrarendezi, a gerinc vezet�kre forgatja
	mrk	- ugyanaz mint az el�z� de tetsz�legesen v�lasztott vonalra forgatja r�
	vcszar	- V�d�cs�/burokcs� lez�r�, amit nem tud lez�rni, oda k�rt tesz
	kk	- hibak�r keres�, v�gigl�pked az �llom�nyban tal�lhat� k�r�k�n, a m�r vizsg�ltat t�rli
	ff	- textet vonalra forgatja �s a k�z�ppontja f�l� mozgatja
	fa	- textet vonalra forgatja �s a k�z�ppontja al� mozgatja
	fg	- textet vonalra forgatja �s nem mozgatja el


A vonalaknak line-oknak kell lenni a m�k�d�shez, polyline-ra nem m�k�dik, az atm, mr, mrk parancsn�l a z�szl�vonalra kell mutatni amin a feliratok vannak.
A vcszar v�gigmegy az �sszes burokcs�v�n az �llom�nyban.
