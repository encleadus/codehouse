(defun c:kart ( / sstxt sskeret hsskeret entx vlentx nv serr skeret sz tlist pstart lay)
	(setvar "CMDECHO" 0)
	(vl-load-com)
	(command "_view" "_save" "temp")
 	(command "_osnap" "_none")
	(command "._undo" "_begin")
	(command "_zoom" "_e")
	(setq sstxt (ssget "_X" (list (cons 0 "TEXT"))))
	(if sstxt
		(progn
			(vl-cmdf "_layer" "_m" "_TXTKERET" "")
			(tcircle sstxt 0.1 "Rectangles")
			 
			
			(setq sskeret (ssget "_X" (list (cons 8 "_TXTKERET"))))
			(if sskeret
				(progn
					(setq hsskeret (sslength sskeret))
					(setq i 0)
					(while (< i hsskeret)
						(setq entx (ssname sskeret i))
						(vl-cmdf "_zoom" "_o" "_si" entx "_redraw")
						(make-vert-list entx)
						(setq skeret vert-list)
						(setq midpt (list (/ (+ (car (car skeret)) (car (caddr skeret))) 2) (/ (+ (cadr (car skeret)) (cadr (caddr skeret))) 2)))
						(vl-cmdf "_offset" "0.2" entx midpt "")
						(setq nv (entlast))
						(make-vert-list nv)
						(setq serr vert-list)
						(vla-delete (vlax-ename->vla-object nv))
						(setq vlentx (vlax-ename->vla-object entx))
						(vla-delete vlentx)
						(vl-cmdf "_layer" "_s" "0" "")
						;(command "_pline") (foreach n serr (command n)) (command "")
						(if (ssget "_CP" serr (list (cons 0 "LINE,LWPOLYLINE,POLYLINE")  (cons -4 "<not") (cons 8 "0") (cons -4 "not>")))
							(progn
								(vl-cmdf "_zoom" "_c" (car serr) "30" "_redraw")
								(setq sz (ssget "_WP" skeret (list (cons 0 "TEXT"))))
								(if sz
									(progn
										(setq tlist (entget (ssname sz 0)))
										(setq pstart (cdr (assoc 10 tlist)))
										(setq lay (cdr (assoc 8 tlist)))
										(if (or (vl-string-search "ANY-ATM" lay) (vl-string-search "MAG" lay)) 
											(zaza (ssname sz 0))
											(vl-cmdf "_move" (ssname sz 0) "" pstart pause)
										)
									)
								)
								
							)
						)
					(setq i (1+ i))
					(prompt (strcat "\n " (itoa i) "/" (itoa hsskeret)))

					)
				)
			)
			
			

		)
		(prompt "\nNincs sz�veg az �llom�nyban")
	)



	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(setvar "CMDECHO" 1)
)

(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list


(defun zaza (entx / enttip puffer buffer kisbetumag nagybetumag tlist tba entlay pstart poz fok tereplay csotelay anylay gerlay 
					vcslay  fellay tip txt sp-angle ker enty tbb txt2 entz tlistz zp1 zp2 sp11 sp12 sp21 sp22 sela selb entu i tlistu h1 
					h2 vhossz midpt pv ap bp vonal szog sza szb tl2 pt1 pt2 porig csoent entfel listfel feltxt terent felent entcso listcso csotxt cp fp)
(setq
 enttip ""
 puffer 0.2
 buffer 2.5
 kisbetumag 0.9
 nagybetumag 1.0
 pifel (/ pi 2)
)

(vl-cmdf "_dimzin" "0")

	
	
	
	(vl-cmdf "_osnap" "_non")
	
	(setq tlist (entget entx))
  	(setq tba (textbox tlist))
  	;(command "_erase" entx "")
	(setq enttip (cdr (assoc 0 tlist)))
	(setq entlay (cdr (assoc 8 tlist)))
	(setq pstart (cdr (assoc 10 tlist)))
	;(command "_zoom" "_c" pstart "30")
	(setq poz nil)
	(if (= (substr entlay 7 1) "-") (setq poz 7))
	(if (= (substr entlay 8 1) "-") (setq poz 8))
	(if (= (substr entlay 9 1) "-") (setq poz 9))
	(setq fok (substr entlay 1 poz))
	(setq tereplay (strcat fok "TEREPMAG-FELIRAT"))
	(setq csotelay (strcat fok "CSOTETOMAG-FELIRAT"))
	(setq fellay (strcat fok "FELMAG-FELIRAT"))
	(setq anylay (strcat fok "ANY-ATM-FELIRAT"))
	(setq gerlay (strcat fok "GERINC"))
	(setq lelay (strcat fok "LEAGAZAS"))
	(setq vcslay (strcat fok "VEDOCSO"))
  	(setq tip nil)
	(if (or (= entlay tereplay) (= entlay csotelay) (= entlay fellay)) (setq tip "mag"))
  	(if (= entlay anylay) (setq tip "any"))
  	(if (= tip "any")
		(progn
			(if (= enttip "TEXT")
				(progn
					(setq txt (cdr (assoc 1 tlist)))
				  	(setq sp-angle (cdr (assoc 50 tlist)))
					(if (/= (atoi txt) 0) (setq ker -0.9) (setq ker 1.8))
					(setq p1 (polar pstart (+ sp-angle pifel) ker))
				  	(setq p2 (polar p1 sp-angle (* (strlen txt) 0.7)))
					;(command "_circle" p1 1)
				  	;(command "_circle" p2 1)
					(setq enty (ssname (ssget "_F" (list p1 p2) (list (cons 0 "TEXT") (cons 8 anylay) )) 0))
				  	(setq tbb (textbox (entget enty)))
				  	(setq txt2 (cdr (assoc 1 (entget enty))))
				  	(vl-cmdf "_erase" entx "")
				  	(vl-cmdf "_erase" enty "")
				  	(if (= ker -0.9) (setq txta txt txtb txt2) (setq txta txt2 txtb txt))
				  	;(command "_line" pstart p1 "")
				  	(setq entz (ssname (ssget "_F" (list pstart p2) (list (cons 0 "LINE") (cons 8 anylay) )) 0))
				  	(setq tlistz (entget entz))
				  	(vl-cmdf "_erase" entz "")
				  	(setq zp1 (cdr (assoc 10 tlistz)))
				  	(setq zp2 (cdr (assoc 11 tlistz)))
				  	(setq sp11 (polar zp1 (/ pi 4) puffer))
					(setq sp12 (polar zp1 (* 5 (/ pi 4)) puffer))
				  	(setq sp21 (polar zp2 (/ pi 4) puffer))
					(setq sp22 (polar zp2 (* 5 (/ pi 4)) puffer))
					(setq sela nil)
					(setq selb nil)
				  	(setq entu nil)
					(setq i 0)
					(setq porig nil)
					(while (and (not sela) (< i 5))
						(setq i (1+ i))
						(setq sela (ssget "_C" sp11 sp12 (list (cons 0 "LINE,LWPOLYLINE") (cons 8 (strcat gerlay "," lelay "," vcslay)) )))
						(if (not sela) (setq sela (ssget "_C" sp21 sp22 (list (cons 0 "LINE,LWPOLYLINE") (cons 8 (strcat gerlay "," lelay "," vcslay)) )))
								(setq porig zp1)
						)
						(if (not sela) (setq selb (ssget "_C" sp11 sp12 (list (cons 0 "LINE") (cons 8 anylay) )))
								(if (not porig) (setq porig zp2))
						)
						(if (and (not sela) (not selb)) (setq selb (ssget "_C" sp21 sp22 (list (cons 0 "LINE") (cons 8 anylay) ))))
						(if (not sela)
							(progn
								(setq entu (ssname selb 0))
							  	(setq tlistu (entget entu))
							  	(vl-cmdf "_erase" entu "")
								(setq zp1 (cdr (assoc 10 tlistu)))
				  				(setq zp2 (cdr (assoc 11 tlistu)))
							  	(setq sp11 (polar zp1 (/ pi 4) puffer))
								(setq sp12 (polar zp1 (* 5 (/ pi 4)) puffer))
							  	(setq sp21 (polar zp2 (/ pi 4) puffer))
								(setq sp22 (polar zp2 (* 5 (/ pi 4)) puffer))
							)
						)
					)
				  	;porig,entx,enty,entz,entu,txta,txtb,sp-angle
				  	(setq h1 (distance (car tba) (cadr tba)) )
				  	(setq h2 (distance (car tbb) (cadr tbb)) )
				  	(setq h1 (sqrt (- (* h1 h1) (* 0.9 0.9))))
				  	(setq h2 (sqrt (- (* h2 h2) (* 0.9 0.9))))
				  	(setq vhossz (max h1 h2))
				  	(setq midpt (polar porig sp-angle (/ vhossz 2)))
				  	(setq pv (polar porig sp-angle vhossz))
				  	(setq ap (polar midpt (+ sp-angle pifel) 0.01))
				  	(setq bp (polar midpt (+ sp-angle pifel) -0.3))
				  	(vl-cmdf "_layer" "_s" anylay "")
				  	(vl-cmdf "_line" porig pv "")
				  	(setq vonal (entlast))
				  	(setq szog (/ (* 180.0 sp-angle) pi))
				  	(vl-cmdf "_text" "_s" "ARIAL_D�LT" "_j" "_bc" ap nagybetumag szog txta "")
				  	(setq sza (entlast))
				  	(vl-cmdf "_text" "_s" "ARIAL_D�LT" "_j" "_tc" bp nagybetumag szog txtb "")
					(setq szb (entlast))
				  	;(command "_erase" entx "")
				  	;(command "_erase" enty "")
				  	;(command "_erase" entz "")
				  	;(command "_erase" entu "")
				  	
				  	(vl-cmdf "_move" vonal sza szb "" porig pause)
				  	(entupd vonal)
	  				(setq tl2 (entget vonal))
	  				(setq pt1 (cdr (assoc 10 tl2)))
				  	(setq pt2 (cdr (assoc 11 tl2)))
				  	(if (< (distance pt1 porig) (distance pt2 porig))
						(vl-cmdf "_line" pt1 porig "")
						(vl-cmdf "_line" pt2 porig "")
	  				)

				  
				  	
						

				)
			)
		)
	);if tip
   	(if (= tip "mag")
		(progn
			(if (= enttip "TEXT")
				(progn
					(setq txt (cdr (assoc 1 tlist)))
				  	(setq sp-angle (cdr (assoc 50 tlist)))
					(setq q1 (cond
						   ((= entlay tereplay) 1.7)
						   ((= entlay csotelay) 2.9)
						   ((= entlay fellay) 0.7)
				  		  
						)
					)
				  	(setq q2 (cond
						   ((= entlay tereplay) -1.1)
						   ((= entlay csotelay) 0.1)
						   ((= entlay fellay) -2.1
						    )
				  		  
						)
					)

					(setq p1 (polar pstart (+ sp-angle pifel) q1))
			  		(setq pt (polar pstart (+ sp-angle pifel) q2))
				  	(setq p2 (polar pt sp-angle (* (strlen txt) 0.6)))
					;(vl-cmdf "_line" p1 p2 "")
				  	(setq a2 (angle p1 p2))
				  	(setq fpt2 (polar p1 a2 1.2)) 
					(setq felmag (ssget "_F" (list p1 fpt2) (list (cons 0 "TEXT") (cons 8 fellay) )))
					(setq termag (ssget "_F" (list fpt2 p2) (list (cons 0 "TEXT") (cons 8 tereplay) )))
					(setq csomag (ssget "_F" (list fpt2 p2) (list (cons 0 "TEXT") (cons 8 csotelay) )))
					(setq entz (ssname (ssget "_F" (list p1 p2) (list (cons 0 "LINE") (cons 8 tereplay) )) 0))
;;;				  	(setq tbb (textbox (entget enty)))



					(setq tlistz (entget entz))

				  	(setq zp1 (cdr (assoc 10 tlistz)))
				  	(setq zp2 (cdr (assoc 11 tlistz)))
				  	(setq sp11 (polar zp1 (/ pi 4) puffer))
					(setq sp12 (polar zp1 (* 5 (/ pi 4)) puffer))
				  	(setq sp21 (polar zp2 (/ pi 4) puffer))
					(setq sp22 (polar zp2 (* 5 (/ pi 4)) puffer))
					(setq sela nil)
					(setq selb nil)
				  	(setq entu nil)
					(setq i 0)
					(setq porig nil)
					
					(while (and (not sela) (< i 5))
						(setq i (1+ i))
						(setq sela (ssget "_C" sp11 sp12 (list (cons 0 "LINE,LWPOLYLINE") (cons 8 (strcat gerlay "," lelay "," vcslay)) )))
						(if (not sela) (setq sela (ssget "_C" sp21 sp22 (list (cons 0 "LINE,LWPOLYLINE") (cons 8 (strcat gerlay "," lelay "," vcslay)) )))
								(setq porig zp1)
						)
						(if (not sela) (setq selb (ssget "_C" sp11 sp12 (list (cons 0 "LINE") (cons 8 tereplay) )))
								(if (not porig) (setq porig zp2))
						)
						(if (and (not sela) (not selb)) (setq selb (ssget "_C" sp21 sp22 (list (cons 0 "LINE") (cons 8 tereplay) ))))
						(if (not sela)
							(progn
								(setq entu (ssname selb 0))
							  	(setq tlistu (entget entu))
							  	(vl-cmdf "_erase" entu "")
								(setq zp1 (cdr (assoc 10 tlistu)))
				  				(setq zp2 (cdr (assoc 11 tlistu)))
							  	(setq sp11 (polar zp1 (/ pi 4) puffer))
								(setq sp12 (polar zp1 (* 5 (/ pi 4)) puffer))
							  	(setq sp21 (polar zp2 (/ pi 4) puffer))
								(setq sp22 (polar zp2 (* 5 (/ pi 4)) puffer))
							)
						)
					)
				  	(vl-cmdf "_erase" entz "")
;;;				  	;porig,entx,enty,entz,entu,txta,txtb,sp-angle
					(setq midpt (polar porig sp-angle 2))
					(setq szog (/ (* 180.0 sp-angle) pi))
				  	(setq pv (polar porig sp-angle 4))
				  	(setq tp (polar midpt (+ sp-angle pifel) -0.1))
				  	(setq cp (polar midpt (+ sp-angle pifel) -1.3))
					(setq fp (polar midpt (+ sp-angle pifel) 0.9))
					(vl-cmdf "_layer" "_s" tereplay "")
				  	(vl-cmdf "_line" porig pv "")
				  	(setq vonal (entlast))
				  	(setq ss (ssadd))
				 	(ssadd vonal ss) 
					(if felmag
					  	(progn
						  (setq entfel (ssname felmag 0))
						  (setq listfel (entget entfel))
						  (setq feltxt (cdr (assoc 1 listfel)))
						  (vl-cmdf "_erase" entfel "")
						  (vl-cmdf "_layer" "_s" fellay "")
						  (vl-cmdf "_text" "_s" "ARIAL_D�LT" "_j" "_bc" fp kisbetumag szog feltxt "")
						  (setq felent (entlast))
						  (ssadd felent ss) 
						)
					  	
					)
					(if termag
					  	(progn
						  (setq entter (ssname termag 0))
						  (setq listter (entget entter))
						  (setq tertxt (cdr (assoc 1 listter)))
						  (vl-cmdf "_erase" entter "")
						  (vl-cmdf "_layer" "_s" tereplay "")
						  (vl-cmdf "_text" "_s" "ARIAL_D�LT" "_j" "_bc" tp kisbetumag szog tertxt "")
						  (setq terent (entlast))
						  (ssadd terent ss) 
						)
					  	
					)
					(if csomag
					  	(progn
						  (setq entcso (ssname csomag 0))
						  (setq listcso (entget entcso))
						  (setq csotxt (cdr (assoc 1 listcso)))
						  (vl-cmdf "_erase" entcso "")
						  (vl-cmdf "_layer" "_s" csotelay "")
						  (vl-cmdf "_text" "_s" "ARIAL_D�LT" "_j" "_bc" cp kisbetumag szog csotxt "")
						  (setq csoent (entlast))
						  (ssadd csoent ss) 
						)
					  	
					)

				  	(vl-cmdf "_move" ss "" porig pause)
				  	(entupd vonal)
					(vl-cmdf "_layer" "_s" tereplay "")
	  				(setq tl2 (entget vonal))
	  				(setq pt1 (cdr (assoc 10 tl2)))
				  	(setq pt2 (cdr (assoc 11 tl2)))
				  	(if (< (distance pt1 porig) (distance pt2 porig))
						(vl-cmdf "_line" pt1 porig "")
						(vl-cmdf "_line" pt2 porig "")
	  				)

				  
				  	
					

				)
			)
		)
	);if tip
	
	
)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;TCIRCLE - Text-CIRCle puts a circle around selected text entities
;
(defun tcircle ( ss offset ent / j rad slot)
 



 
     (if (not #bns_tcircle_offset)
         (setq #bns_tcircle_offset 0.35)
     );if
     (initget 6)
     
     (if (or (not offset)
             (equal offset "")
         );or
         (setq offset #bns_tcircle_offset)
         (setq #bns_tcircle_offset offset)
     );if
 
 
     (if (not #bns_tcircle_ent)
         (setq #bns_tcircle_ent "Circles")
     );if
     
     (if (or (not ent)
             (equal ent "")
         );or
         (setq ent #bns_tcircle_ent)
         (setq #bns_tcircle_ent ent)
     );if
 
     (if (not #bns_tcircle_rad)
         (setq #bns_tcircle_rad "Variable")
     );if
     
     (if (or (not rad)
             (equal rad "")
         );or
         (setq rad #bns_tcircle_rad)
         (setq #bns_tcircle_rad rad)
     );if
     (if (and (equal rad "Constant")
              (or (equal ent "Slots")
                  (equal ent "Rectangles")
              );or
         );and
         (progn
          (if (not #bns_tcircle_slot)
              (setq #bns_tcircle_slot "Both")
          );if
          (initget "Width Height Both")
          (setq slot (getkword
                      (acet-str-format
                              "\nMaintain constant %1 [Width/Height/Both] <%2>: "
                              (strcase (substr ent 1 (- (strlen ent) 1)) T)
                              #bns_tcircle_slot
                      )
                     )
          );setq
          (if (or (not slot)
                  (equal slot "")
              );or
              (setq slot #bns_tcircle_slot)
              (setq #bns_tcircle_slot slot)
          );if
         );progn else get rectang or slot size info
     );if
 
     (setq j (bns_tcircle ss rad ent slot offset))
     
     (princ (strcat "\n" (itoa j) " " ent " created."))
    
    
);defun c:tcircle
  
(defun bns_tcircle ( ss rad ent slot offset /
                      a b d na e1 n j lst ll lr ul ur addw addh
                      p0 p1 p2 p3 w h slot down left right up
                   )
  (setq j 0)
  (if (equal "Variable" rad)
    (progn
      (princ (strcat "\nCreating " ent "..."))
      (setq n 0);setq
      (repeat (sslength ss)
        (setq na (ssname ss n)
              e1 (entget na)
               a (acet-geom-textbox e1 offset)
               b (acet-geom-midpoint (car a) (caddr a))
        );setq
        (if (equal "Circles" ent)
            (entmake  (list '(0 . "CIRCLE")
                       (cons 10 (trans b 1 (cdr (assoc 210 e1))))
                       (cons 40 (distance (car a) b))
                       (assoc 210 e1)
                      );list
            );entmake
            (entmake
             (list  '(0 . "LWPOLYLINE")
                    '(100 . "AcDbEntity")
                    '(100 . "AcDbPolyline")
                    '(90 . 4)
                    (cons 38 (last (cdr (assoc 10 e1))))
                    '(70 . 1)
                    (cons 10 (trans (nth 0 a) 1 (cdr (assoc 210 e1))))
                    (if (equal ent "Rectangles")
                       '(42 . 0.0)
                       '(42 . -1.0)
                    );if
                    (cons 10 (trans (nth 3 a) 1 (cdr (assoc 210 e1))))
                    '(42 . 0.0)
                    (cons 10 (trans (nth 2 a) 1 (cdr (assoc 210 e1))))
                    (if (equal ent "Rectangles")
                        '(42 . 0.0)
                        '(42 . -1.0)
                    );if
                    (cons 10 (trans (nth 1 a) 1 (cdr (assoc 210 e1))))
                    '(42 . 0.0)
                    (assoc 210 e1)
             )
            );entmake
        );if
        (setq j (+ j 1));setq
        (setq n (+ n 1));setq
      );repeat
      (princ "Done.")
    );progn then VARIABLE
    (progn
      (if (equal "Circles" ent)
        (progn
          (princ "\nDetermining best radius...")
          (setq n 0
                d -99999.0
          );setq
          (repeat (sslength ss)
            (setq na (ssname ss n)
                  e1 (entget na)
                   a (acet-geom-textbox e1 offset)
                   b (acet-geom-midpoint (car a) (caddr a))
            );setq
            (setq lst (append lst
                              (list (list (trans b 1 (cdr (assoc 210 e1)))
                                          (assoc 210 e1)
                              )     )
                      );append
            );setq
            (if (> (distance (car a) b) d)
                (setq d (distance (car a) b));setq then
            );if
            (setq n (+ n 1));setq
          );repeat find the max radius and get a list of center points
          (princ "Done.")
 
          (princ (strcat "\nCreating " ent "..."))
          (setq n 0);setq
          (repeat (length lst)
            (setq a (nth n lst));setq
            (entmake (list '(0 . "CIRCLE")
                           (cons 10 (car a))
                           (cons 40 d)
                           (cadr a)
                     );list
            );entmake
            (setq j (+ j 1));setq
            (setq n (+ n 1));setq
          );repeat
        ) ;progn
        (progn
          (princ "\nDetermining best size...")
          (setq n 0
                w -99999.0
                h -99999.0
          );setq
          (repeat (sslength ss)
            (setq na (ssname ss n)
                  e1 (entget na)
                   a (acet-geom-textbox e1 offset)
            );setq
            (if (> (distance (car a) (cadr a)) w)
              (setq w (distance (car a) (cadr a)));setq then
            );if
            (if (> (distance (nth 0 a) (nth 3 a)) h)
              (setq h (distance (nth 0 a) (nth 3 a)));setq then
            );if
            (setq n (+ n 1));setq
          );repeat find the max radius and get a list of center points
          (princ "Done.")
 
          (princ (strcat "\nCreating " ent "..."))
          (setq n 0);setq
          (repeat (sslength ss)
            (setq na (ssname ss n)
                  e1 (entget na)
                   a (acet-geom-textbox e1 offset)
            );setq
            (setq ll (nth 0 a)
                  lr (nth 1 a)
                  ur (nth 2 a)
                  ul (nth 3 a)
            )
            (setq left  (angle lr ll)
                  right (angle ll lr)
                  up    (angle ll ul)
                  down  (angle ul ll)
            )
            (if (or (equal "Width" slot)
                    (equal "Both" slot))
              (setq addw (/ (- w (distance ll lr)) 2))
              (setq addw 0)
            ) ;if
            (if (or (equal "Height" slot)
                    (equal "Both" slot))
              (setq addh (/ (- h (distance ll ul)) 2))
              (setq addh 0)
            ) ;if
 
            (setq p0 (polar (polar ll left addw) down addh)
                  p1 (polar (polar lr right addw) down addh)
                  p2 (polar (polar ur right addw) up addh)
                  p3 (polar (polar ul left addw) up addh)
            )
            (entmake
              (list  '(0 . "LWPOLYLINE")
                     '(100 . "AcDbEntity")
                     '(100 . "AcDbPolyline")
                     '(90 . 4)
                     (cons 38 (last (cdr (assoc 10 e1))))
                     '(70 . 1)
                     (cons 10 (trans p0 1 (cdr (assoc 210 e1))))
                     (if (equal ent "Rectangles")
                         '(42 . 0.0)
                         '(42 . -1.0)
                     );if
                     (cons 10 (trans p3 1 (cdr (assoc 210 e1))))
                     '(42 . 0.0)
                     (cons 10 (trans p2 1 (cdr (assoc 210 e1))))
                     (if (equal ent "Rectangles")
                         '(42 . 0.0)
                         '(42 . -1.0)
                     );if
                     (cons 10 (trans p1 1 (cdr (assoc 210 e1))))
                     '(42 . 0.0)
                     (assoc 210 e1)
              )
            )
            (setq j (+ j 1));setq
            (setq n (+ n 1));setq
          );repeat
        ) ;progn
      ) ;if
      (princ "Done.")
    );progn else CONSTANT
  );if
 
  j
);defun bns_tcircle

(setvar "CMDECHO" 0)
(prompt "\nKart")
(setvar "CMDECHO" 1)