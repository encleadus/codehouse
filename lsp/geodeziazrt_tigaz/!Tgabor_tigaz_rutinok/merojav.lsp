(defun c:merojav()
 (command "cmdecho" "0")
 (command "_osnap" "_non")
 (command "_view" "_save" "temp")
 (command "_color" 1)
 ;(command "_layer" "_make" "dummy" "_set" "dummy" "")
 (command "_dimzin" "0")


 (setq sset
	 (ssget
	   "x"
	   (list
	     (cons
	       8
	       "GAZ-Gf-MERES-G,GAZ-Gfk-MERES-G,GAZ-Gfn-MERES-G,GAZ-Gfnk-MERES-G"
	     )
	     (cons 0 "INSERT")
	     (cons 2 "meröleges")
	   )
	 )
  )

 (setq i 0)
 (repeat (sslength sset)
  (setq ssn (ssname sset i))
  (setq lst (entget ssn))
  (setq szog (getval 50 lst))
  (setq pstart (getval 10 lst))
  (setq nszog (+ szog (/ pi 4)))
  (setq puj (polar pstart nszog 0.2))
  (setq lst (subst (cons 10 puj)(assoc 10 lst) lst))
  (entmod lst)
  (entupd ssn)
  
 
  (setq i (1+ i))
 )
 
 (command "_view" "_restore" "temp")	
 (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
 (command "cmdecho" "1")
)


(defun GETVAL (grp ele)                 ;"dxf value" of any ent...
  (cond ((= (type ele) 'ENAME)          ;ENAME
          (cdr (assoc grp (entget ele))))
        ((not ele) nil)                 ;empty value
        ((not (listp ele)) nil)         ;invalid ele
        ((= (type (car ele)) 'ENAME)    ;entsel-list
          (cdr (assoc grp (entget (car ele)))))
        (T (cdr (assoc grp ele)))))     ;entget-list
