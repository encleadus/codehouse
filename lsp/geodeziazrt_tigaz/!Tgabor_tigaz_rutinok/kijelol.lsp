(defun c:kijelol()
 (command "_cmdecho" "0")
 
  (command "_view" "_save" "temp")
 (command "_osnap" "_none")

 (setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
 (setq sel-ent (nth 0 sel-ent))
 (make-vert-list sel-ent)
 (command "_zoom" "_o" "_si" sel-ent)
 (Command "_redraw")

 (setq list1 vert-list)
 (setq sset (ssget "_CP" list1 ))
 (command "_copyclip" "_m" "_a" sset "" )
 
 (setq sset nil vert-list nil sel-ent nil)
 (command "_view" "_restore" "temp")	
 (command "_regen")
 (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
 (command "_cmdecho" "1")
)

 ;--------------------------
 ;name      : make-vert-list
 ;--------------------------
(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list
