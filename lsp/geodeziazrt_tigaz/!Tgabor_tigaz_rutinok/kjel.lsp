;C01_�P�LET_�P�TM�NY,C01_LAK��P�LET,C02_LAK��P�LET_NL,C03_GAZD_�P�LET,C04_GAZD_�P�LET_NL,C06_K�Z�P�LET,C07_K�Z�P�LET_NL,C09_TEMPLOM,
;C10_TEMPLOM_NL,C12_�ZEMI_�P�LET,C13_�ZEMI_�P�LET_NL,C15_�D�L��P�LET,C16_�D�L��P�LET_NL,C17_VEGYES_�P,C18_VEGYES_�P_NL,C20_REND.LEN_�P,
;C21_REND.LEN_�P_NL,C23_TORONY_�P,C24_TORONY_�P_NL
(defun c:kjel ()
  (command "_cmdecho" "0")
  (command "_view" "_save" "temp")
  (command "_osnap" "_non")
  (command "_dimzin" "0")
  (command "_layer" "_m" "ujepulet" "_c" "1" "" "")
  (setq sel-ent (entsel "\nV�lassz hat�rol� vonalat"))
  (setq	exr (ssget "_x"	    '((-4 . "<AND")
			      (0 . "LWPOLYLINE")
			      (-4 . "<OR")
			      (8 . "C01_�P�LET_�P�TM�NY")
			      (8 . "C01_LAK��P�LET")
			      (8 . "C02_LAK��P�LET_NL")
			      (8 . "C03_GAZD_�P�LET")
                              (8 . "C04_GAZD_�P�LET_NL")
			      (8 . "C06_K�Z�P�LET")
			      (8 . "C07_K�Z�P�LET_NL")
			      (8 . "C09_TEMPLOM")
			      (8 . "C10_TEMPLOM_NL")
			      (8 . "C12_�ZEMI_�P�LET")
   			      (8 . "C13_�ZEMI_�P�LET_NL")
			      (8 . "C15_�D�L��P�LET")
			      (8 . "C16_�D�L��P�LET_NL")
			      (8 . "C17_VEGYES_�P")
			      (8 . "C18_VEGYES_�P_NL")
			      (8 . "C20_REND.LEN_�P")
			      (8 . "C21_REND.LEN_�P_NL")
			      (8 . "C23_TORONY_�P")
			      (8 . "C24_TORONY_�P_NL")
			      (-4 . "OR>")
			      (-4 . "AND>")
			     )
		     
	  )
	)
  (if exr (command "_explode" exr))
  (command "_-mapclean" "kjel.dpf")
  (command "_-maptopocreate" "epulet" "" "_P" "_N" "_Y" "_N" "_Y" "ujepulet" "_Y" "_Y" "0" "" "_Y" "0" "" "_Y" "_N" "_N" "_N" "_N")
  (command "_layer" "_m" "epulet_polygon" "_c" "1" "" "")
  (command "_mapclpline" "epulet" "epulet_polygon" "_N" "_N" "_N")
  (setq lay_name "epulet_polygon")
  (setq lay_name2 "B05_TELEP�L�SHAT,B07_FEKV�SHAT�R_BT,B15_FRL_HAT�R,B24_EL�_FRL_HAT,B21_TERV_HAT�R_NL")
  (setq lof nil)
  
  (setq sel-ent (nth 0 sel-ent))
  (make-vert-list sel-ent)
  (setq list1 vert-list)
  (princ list1)
  (setq	lof (ssget "_CP"
		   list1
		   (list (cons 8 lay_name) (cons 0 "LWPOLYLINE"))
	    )
  )

  (setq puffer 1.2)
  (if lof
    (progn
      (setq hlof (sslength lof))
      (setq j 0)
      (while (< j hlof)
	(setq entx (ssname lof j))
	(make-vert-list entx)
	(setq oldal (length vert-list))
	(command "_layer" "_m" "haz_tmp" "_c" "1" "" "")
	(command "_change" entx "" "_p" "_la" "haz_tmp" "")
	(setq i 0)
	(setq puffer 1.2)
	(while (< i oldal)
	  (setq vertp1 (nth i vert-list))
	  (if (> (+ i 1) (- oldal 1))
	    (setq vertp2 (nth 0 vert-list))
	    (setq vertp2 (nth (+ i 1) vert-list))
	  )
	  (setq sp-angle (angle vertp1 vertp2))
	  (setq szog (/ (* 180 sp-angle) PI))
	  (setq tav (distance vertp1 vertp2))
	  (setq felezop (polar vertp1 sp-angle (/ tav 2)))
	  (if (< (/ tav 2) puffer) (setq puffer (/ tav 2)))
	  (setq sp11 (polar felezop (/ pi 4) puffer))
	  (setq sp14 (polar felezop (* 5 (/ pi 4)) puffer))

	  (command "_zoom" "_c" felezop "70")
 	 ; (command "rectang" sp11 sp14 "")

	  (setq	amer (ssget "_C"
			    sp11
			    sp14
			    '((-4 . "<OR")
			      (8 . "B05_TELEP�L�SHAT")
			      (8 . "B07_FEKV�SHAT�R_BT")
			      (8 . "B15_FRL_HAT�R")
			      (8 . "B24_EL�_FRL_HAT")
			      (8 . "B21_TERV_HAT�R_NL")
			      (8 . "C31_�P�LET_TART")
			      (8 . "foldreszlet")
			      (8 . "epulet_polygon")
			      (-4 . "OR>")
			     )
		     )
	  )


	  (if (= amer nil)
 ; ha nem tal�l akkor kapcsjel
	    (progn
	      (command "_layer" "_m" "B47_KAPCS_JEL" "")
	      (command "_insert" "jkdatb04" felezop "0.5" "0.5" szog)
	      (setq i oldal)
	    ) ;progn
	  ) ;if
	  (setq i (+ i 1))
	) ;while
	(setq j (+ j 1))
	(command "_change" entx "" "_p" "_la" "epulet_polygon" "")
	(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
      ) ;while
    ) ;progn
  ) ;if
  (command "_maptopoadmin" "_D" "epulet" "_N" "_Y")
  (setq	fos (ssget "_x" '((-4 . "<OR")
			      (8 . "ujepulet")
			      (8 . "epulet_polygon")
			      (8 . "haz_tmp")
			      (8 . "0")
			      (-4 . "OR>")
			     )))
 
		     
	
  (if fos (command "_erase" fos ""))
  (command "_.purge" "_LA" "ujepulet,epulet_polygon,haz_tmp" "_N") 
  (command "_view" "_restore" "temp")
  (command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
  (command "_cmdecho" "1")
) ;defun






(defun c:kk ()
  (setq zoomk 30)
 ;(command "_zoom" "e")
  (setq circle-list (ssget "x" '((0 . "circle"))))
  (setq h-circle (sslength circle-list))
  (if (/= h-circle 0)
    (progn
      (setq circlex (ssname circle-list 0))
      (setq pt1 (cdr (assoc 10 (entget circlex))))
      (command "_zoom" "c" pt1 zoomk)
      (print (strcat "meg: " (itoa h-circle) "db van!"))
      (getstring "\nTorlom!")
      (command "erase" (ssname circle-list 0) "")
      (ssdel circlex circle-list)
      (setq h-circle (sslength circle-list))
    )
    (alert "Nincs benne tobb kor!")
  )
) ;defun kk


 ;--------------------------
 ;name      : make-vert-list
 ;--------------------------
(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list