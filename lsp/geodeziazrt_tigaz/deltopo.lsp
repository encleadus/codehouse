(defun c:deltopo()
(setq itr_id (tpm_iterstart))
(if (null itr_id)
    (prompt "\nERROR: Unable to start topology iterator.") 
    (while (not done) 
       (if (null (tpm_iternext itr_id)) 
          (setq done T) 
          (progn 
             (print (tpm_itername itr_id))
            (tpm_mnterase (tpm_itername itr_id))
            
          )  ; progn
       )  ; if
    )  ; while 
)  ; if 
    (setq sset (ssget "x"  '((-4 . "<or")
                                       ( 8 . "gaz-gfk-leagazas") 
                                       ( 8 . "gaz-gf-leagazas")
                                       ( 8 . "gaz-gfnk-leagazas")
                                       ( 8 . "gaz-gfn-leagazas")
                                       ( 8 . "gaz-gfk-gerinc") 
                                       ( 8 . "gaz-gf-gerinc")
                                       ( 8 . "gaz-gfnk-gerinc")
                                       ( 8 . "gaz-gfn-gerinc")
                                   (-4 . "or>")
                             (-4 . "<or")(0 . "lwpolyline")(0 . "line") (-4 . "or>"))))
(setq var_id (tpm_varalloc))
(setq status (tpm_mntbuild var_id "gaznyomvonal" " " 2 nil sset nil))
  (if status 
    (print " A g�znyomvonal topologia l�trej�tt")
    (print " Hiba a topol�gia l�trehoz�s�n�l")
  )

)
