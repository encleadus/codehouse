(defun c:behuze ()
  ; A rutin az ITR altal keszitett szelvenyezes zaszlok hosszat roviditi meg
  (setvar "cmdecho" 0)
  (setq sset (ssget "x" '(( 0 . "circle"))))
  (if sset (command "_erase" sset ""))
  
  (command "_layer" "_set" "GAZ-Gfk-TEREPMAG-FELIRAT" "")
  (command "vt�pus" "b" "f" "")
  (setq javdb 0)
  (command "_color" "bylayer")
 ;  (setq sset (ssget "x"  '((-4 . "<or")
 ;                                      ( 8 . "gaz-gfk-terepmag-felirat") 
 ;                                      ( 8 . "gaz-gf-terepmag-felirat")
 ;                                      ( 8 . "gaz-gfnk-terepmag-felirat")
 ;                                      ( 8 . "gaz-gfn-terepmag-felirat")
 ;                                  (-4 . "or>") (0 . "text"))))
           
   (setq sset (ssget))   
 ;  (setq sset (ssget "x" '((0 . "line")( 8 . "GAZ-Gfk-TEREPMAG-FELIRAT"))))
   (setq i 0)
   (if sset
    (progn
     (setq n2 (sslength sset))
     (repeat (sslength sset)
      (princ (strcat (itoa i) "/" (itoa n2) "\r"))  
      (setq ssn (ssname sset i))
      (setq lst (entget ssn))
      (setq ptest1 (getval 10 lst))
      (setq ptest2 (getval 11 lst))
      (setq ttest (distance ptest1 ptest2))
      (if (> ttest 1) 
      (progn
;      (setq p0 (getval 10 lst))
      (setq p0 (textveg ptest1 ptest2))
;      (command "_circle" p0 1.0)
      (command "_zoom" "_c" p0 10)
      (setq x0 (car p0))
      (setq y0 (cadr p0))
      (setq p1 (list (+ x0 2.0) (+ y0 2.0)))
      (setq p2 (list (- x0 2.0) (+ y0 2.0)))
      (setq p3 (list (- x0 2.0) (- y0 2.0)))
      (setq p4 (list (+ x0 2.0) (- y0 2.0)))
      (setq svonal (ssget "_cp" (list p1 p2 p3 p4 p1)))
      (setq sset_text (ssadd))
;      (textscr)
       (if svonal
        (progn
         (setq k 0 ssvuj (ssadd) idb 0)
         (setq mehet T)
         (repeat (sslength svonal)
           (setq ssn3 (ssname svonal k))
;         (print (strcat (itoa k) (getval 0 (entget ssn3)) (getval 8 (entget ssn3))))
          (setq vreteg (getval 8 (entget ssn3)))
          (if (and (= (getval 0 (entget ssn3)) "LWPOLYLINE") 
           (or
                  (= vreteg "GAZ-Gfk-TEREPMAG-FELIRAT") 
                  (= vreteg "GAZ-Gfnk-TEREPMAG-FELIRAT") 
                  (= vreteg "GAZ-Gf-TEREPMAG-FELIRAT") 
                  (= vreteg "GAZ-Gfn-TEREPMAG-FELIRAT") 
            ))
           (progn
            (setq mehet nil)
           )
          )
          (if (and (= (getval 0 (entget ssn3)) "LINE") 
           (or
                  (= vreteg "GAZ-Gfk-TEREPMAG-FELIRAT") 
                  (= vreteg "GAZ-Gfnk-TEREPMAG-FELIRAT") 
                  (= vreteg "GAZ-Gf-TEREPMAG-FELIRAT") 
                  (= vreteg "GAZ-Gfn-TEREPMAG-FELIRAT") 
            ))
           (progn
            (setq idb (1+ idb))
;            (print (entget ssn3))
            (setq ssvuj (ssadd ssn3 ssvuj))
           );progn
          );if
          (setq txtreteg (getval 8 (entget ssn3)))
          (if (and (= (getval 0 (entget ssn3)) "TEXT") 
            ( or
                  (=  txtreteg "GAZ-Gfk-TEREPMAG-FELIRAT") 
                  (=  txtreteg "GAZ-Gfk-CSOTETOMAG-FELIRAT") 
                  (=  txtreteg "GAZ-Gfnk-TEREPMAG-FELIRAT") 
                  (=  txtreteg "GAZ-Gfnk-CSOTETOMAG-FELIRAT") 
                  (=  txtreteg "GAZ-Gf-TEREPMAG-FELIRAT") 
                  (=  txtreteg "GAZ-Gf-CSOTETOMAG-FELIRAT") 
                  (=  txtreteg "GAZ-Gfn-TEREPMAG-FELIRAT") 
                  (=  txtreteg "GAZ-Gfn-CSOTETOMAG-FELIRAT") 
             ))
            (progn
             (setq sset_text (ssadd ssn3 sset_text))
            )
           )


          (setq k (1+ k))
         );repeat
        );progn
       );
       (setq svonal ssvuj)
;      (command pause)
      (if (< (sslength svonal) 2)
       (progn
;        (command "zoom" "c" p0 10.0)
;        (alert "Nincs a szoveg kozeleben ket segedvonal")
;        (command "zoom" "p")
       );progn
       (progn
 
         (IF (> (sslength svonal) 2)
          (progn
; Ha 2-nel tobb vetitovonalat talaltunk azt valasztjuk amelyiknek egy pontja a
; legkozelebb van szoveg kezdopontjahoz
           (setq l 0 tmin 1000.0)
            (repeat (sslength svonal)
             (setq ss1 (ssname svonal l))
             (setq lst1 (entget ss1))
             (setq p01 (getval 10 lst1))
             (setq p02 (getval 11 lst1))
             (if (< (distance p01 p0) tmin) (setq tmin (distance p01 p0)))
             (if (< (distance p02 p0) tmin) (setq tmin (distance p02 p0)))
             (setq l (1+ l))
            );repeat
            (setq ssvuj (ssadd) l 0)
            (repeat (sslength svonal)
             (setq ss1 (ssname svonal l))
             (setq lst1 (entget ss1))
             (setq p01 (getval 10 lst1))
             (setq p02 (getval 11 lst1))
              (if (or (> (+ tmin 0.01) (distance p01 p0))
                      (> (+ tmin 0.01) (distance p02 p0)))
                   (setq ssvuj (ssadd ss1 ssvuj))
              )
             (setq l (1+ l))
            );repeat
            (setq svonal ssvuj)
          );progn
          );if
           (setq ssn1 (ssname svonal 0))
           (setq ssn2 (ssname svonal 1))
           (setq lst1 (entget ssn1))
           (setq lst2 (entget ssn2))
           (setq p11 (getval 10 lst1))
           (setq p12 (getval 11 lst1))
           (setq p21 (getval 10 lst2))
           (setq p22 (getval 11 lst2))
           (setq t1 (distance p11 p12))
           (setq t2 (distance p21 p22))
           (if (> t1 t2)
            (setq ssnper ssn2)
            (setq ssnper ssn1)
           )

        (command "_pedit" ssn1 "_y" "_j" ssn2 "" "")
        (setq vtx (vertex_list1 (entlast)))
        (setq pk (nth 1 vtx))
        (setq p0vtx (nth 0 vtx))
        (setq p1vtx (nth 2 vtx))
        (setq t1vtx (distance pk p0vtx))
        (setq t2vtx (distance pk p1vtx))
        (if (> t1vtx t2vtx)
         (setq pveg p0vtx provid p1vtx trovid t2vtx rovir (angle pk p1vtx) hosszir (angle pk p0vtx) thossz t1vtx) 
         (setq pveg p1vtx provid p0vtx trovid t1vtx rovir (angle pk p0vtx) hosszir (angle pk p1vtx) thossz t2vtx)
        )
        
;        (print vtx)
;        (print "p0")
;        (print p0)
;        (print "txtveg")
;        (print txtveg)
        (setq puj (polar pk hosszir (/ thossz 2.0)))
        (setq puj1 (polar puj rovir  trovid))
        (if mehet
        (progn
         (command "_erase" (entlast) "")
         (command "_zoom" "_c" puj 50)
         (setq x0 (car puj))
         (setq y0 (cadr puj))
         (setq p1 (list (+ x0 5.0) (+ y0 5.0)))
         (setq p2 (list (- x0 5.0) (+ y0 5.0)))
         (setq p3 (list (- x0 5.0) (- y0 5.0)))
         (setq p4 (list (+ x0 5.0) (- y0 5.0)))
         (setq sset_chk (ssget "_cp" (list p1 p2 p3 p4 p1) '((-4 . "<or")
                                       ( 8 . "gaz-gfk-terepmag-felirat") 
                                       ( 8 . "gaz-gf-terepmag-felirat")
                                       ( 8 . "gaz-gfnk-terepmag-felirat")
                                       ( 8 . "gaz-gfn-terepmag-felirat")
                                   (-4 . "or>") (0 . "text"))))
         (if sset_chk
          (progn
          (setq mehet1 T)
          (while mehet1
           (setq lst (grread 1))
           (command "_redraw")
           (setq kod (nth 0 lst))
           (setq pakt (nth 1 lst))
           (setq pakt1 (polar pakt rovir  trovid))
           (grdraw  pveg pakt 1)
           (grdraw pakt pakt1 1)

            (if (= kod 3)
             (progn
              (setq mehet1 nil)
              (setq puj pakt)
             )
            )

          )





 ;          (setq puj (getpoint "Add meg a sz�veg �j hely�t"))
 ;          (command "_circle" puj 2.0)
           (setq puj1 (polar puj rovir  trovid))
          )
         )
         (if (> (sslength sset_text) 0)
          (command "_move" sset_text "" pk puj)
          (command "_circle" puj 1.0)
         )
         (command "_line" pveg puj puj1 "")
         (setq javdb (1+ javdb))
        )
         (progn

             (command "_chprop" ssvuj "" "_c" 1 "")
         )
        )
       );progn
      );if
      );progn
      );if vonalhossz > 30
      (setq i (1+ i))
     );repeat
    );progn
   );if sset
   (print (strcat "Jav�tva: " (itoa javdb)))
);defun


    (defun textveg (ptest1 ptest2)
      (setq icik 1)
      (setq pout nil)
      (repeat 2
      (if (= icik 1) (setq p00 ptest1))
      (if (= icik 2) (setq p00 ptest2))
      (setq x0 (car p00))
      (setq y0 (cadr p00))
      (setq p1 (list (+ x0 0.5) (+ y0 0.5)))
      (setq p2 (list (- x0 0.5) (+ y0 0.5)))
      (setq p3 (list (- x0 0.5) (- y0 0.5)))
      (setq p4 (list (+ x0 0.5) (- y0 0.5)))
      (command "_zoom" "_c" p00 10)
      (setq svonal1 (ssget "_cp" (list p1 p2 p3 p4 p1)))
        (if svonal1
        (progn
          (setq k 0  idb 0)
         (repeat (sslength svonal1)
           (setq ssn3 (ssname svonal1 k))
          (setq vreteg (getval 8 (entget ssn3)))
          (if (and (= (getval 0 (entget ssn3)) "LINE") 
           (or
                  (= vreteg "GAZ-Gfk-TEREPMAG-FELIRAT") 
                  (= vreteg "GAZ-Gfnk-TEREPMAG-FELIRAT") 
                  (= vreteg "GAZ-Gf-TEREPMAG-FELIRAT") 
                  (= vreteg "GAZ-Gfn-TEREPMAG-FELIRAT") 
            ))
           (progn
            (setq idb (1+ idb))
           );progn
          );if
 
          (setq k (1+ k))
         );repeat
        (if (= idb 2) (setq pout p00))
        );progn
       );
      (setq icik (1+ icik))
      )
     pout
   )
