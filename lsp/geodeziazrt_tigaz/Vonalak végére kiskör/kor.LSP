(defun c:kor()

  (setq omode (getvar "osmode"))
  (command "_osnap" "_none")

  (setq kv (ssget "x" '((0 . "LINE") ( 8 . "kor") )))
  (command "_layer" "�" "0" "")
  (setq n (sslength kv))
  (setq i 0)

  (repeat n
    (setq vnev (ssname kv i))
    (setq Lista (entget vnev))
    (setq pk (cdr (assoc 10 Lista)))
    (setq pv (cdr (assoc 11 Lista)))
    (command "_circle" pv 3 )
    (command "_circle" pk 3 )
    (setq i (+ i 1))
  )

  (command "_mapclean" "c:\\Program Files\\Autodesk Map 3D 2007\\Torles.dpf")
  (command "_setvar" "osmode" omode)  
)