      (defun c:y()
        ;Pontot sz�r be a poligonba , �s �tveszi az attrib�tum t�bl�t
       (setq omode (getvar "osmode"))
       (command "_osnap" "_none")
       (while (not (setq lst (entsel "V�lassz input objektumot "))))
       (setq p0 (nth 1 lst))
       (setq ssn (nth 0 lst))
       (setq lst (entget ssn))
       (setq reteg (getval 8 lst))
       (command "_break" ssn p0 p0)
       (setq p0x (nth 0 p0))
       (setq p0y (nth 1 p0))
       (setq p1 (list (- p0x 0.1) (- p0y 0.1)))
       (setq p2 (list (+ p0x 0.1) (- p0y 0.1)))
       (setq p3 (list (+ p0x 0.1) (+ p0y 0.1)))
       (setq p4 (list (- p0x 0.1) (+ p0y 0.1)))
       (setq sset1 (ssget "_cp" (list p1 p2 p3 p4 p1)))
       
       (if (/= (sslength sset1) 2) 
         (progn
          (alert "A kijel�lt pont k�rnyezet�ben t�l sok objektum van")
         )
         (progn 
          (setq ssn1 (ssname sset1 0))
          (setq ssn2 (ssname sset1 1))    
           (setq attr1  (ade_odgetfield ssn1 "t_hal_elek" "T_VCSP_ID" 0))
           (setq attr2  (ade_odgetfield ssn2 "t_hal_elek" "T_VCSP_ID" 0))

            (if attr1
            (progn
              (command "_pedit" ssn1 "_j" ssn2 "" "")
            )
            (progn
              (command "_pedit" ssn2 "_j" ssn1 "" "")
            )
           );if old_table_lst1
         );progn
        );if
       (command "_setvar" "osmode" omode)
       )

