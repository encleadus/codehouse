; ***********************************************************************
; Egy blokk �sszes el�fordul�s�t lecser�li
; input adatok :
;                oldblk - az eredeti blokkn�v
;                newblk - az �j blook neve
;                nattr  - a blokkok attributumainak sz�ma
;************************************************************************
(defun bcsere (oldblk  newblk nattr )

; az �sszes r�gi blokkot tartalmaz� selection set elk�sz�t�se
(setq sset (ssget "x" (list (cons 2  oldblk) )))

; nincs parancs vissza�r�s
 (SETVAR "CMDECHO" 0)
  (setq i 0 ATR1 " ")

; az �j aktiv layer be�ll�t�sa
  (if sset
  (repeat (sslength sset)
   (setq ssn (ssname sset i))
   (setq ssnold ssn)
   (setq lst (entget ssn))
   (setq newlay (getval 8 lst))
   (COMMAND "_LAYER" "_set" newlay "")

 ; a blokk leszur�si pontja
   (setq p0 (getval 10 lst))

; az x �s y ir�nyu m�retar�nyt�nyez�
   (setq mx (getval 41 lst))
   (setq my (getval 42 lst))
;    (setq mx 10 my 10)


; a blokk sz�ge
   (setq szog (getval 50 lst))
   (setq szog (* (/ szog pi) 180.0) )

; az attributumok sz�m�t�l f�gg�en az attributumok �rt�keinek
; kikeres�se a r�gi blokkokb�l

  (if ( >= nattr 1)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr1 (getval 1 lst))
   );progn
  );
  (if ( >= nattr 2)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr2 (getval 1 lst))
   );progn
  )
  (if ( >= nattr 3)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr3 (getval 1 lst))
   );
  );if
  (if ( >= nattr 4)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr4 (getval 1 lst))
   )
  )
  (if ( >= nattr 5)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr5 (getval 1 lst))
   );
  );if
  (if ( >= nattr 6)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr6 (getval 1 lst))
   );
  );if
  (if ( >= nattr 7)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr7 (getval 1 lst))
   );
  );if
  (if ( >= nattr 8)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr8 (getval 1 lst))
   );
  );if
  (if ( >= nattr 9)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr9 (getval 1 lst))
   );
  );if
  (if ( >= nattr 10)
   (progn
    (setq ssn (entnext ssn))
    (setq lst (entget ssn))
    (setq atr10 (getval 1 lst))
   );
  );if
   (setq atr1 "??")
   (setq my 0.5 mx 0.5)
   (if (= nattr 0) (command "_insert" newblk p0 MX MY SZOG))
   (if (= nattr 1) (command "_insert" newblk p0 MX MY SZOG ATR1 ))
   (if (= nattr 2) (command "_insert" newblk p0 MX MY SZOG ATR1 ATR2 ))
   (if (= nattr 3) (command "_insert" newblk p0 MX MY SZOG ATR1 ATR2 ATR3 ))
   (if (= nattr 4) (command "_insert" newblk p0 MX MY SZOG ATR1 ATR2 ATR3 ATR4 ))
   (if (= nattr 5) (command "_insert" newblk p0 MX MY SZOG ATR1 ATR2 ATR3 ATR4 ATR5))
   (if (= nattr 6) (command "_insert" newblk p0 MX MY SZOG ATR1 ATR2 ATR3 ATR4 ATR5 ATR6))
   (if (= nattr 7) (command "_insert" newblk p0 MX MY SZOG ATR1 ATR2 ATR3 ATR4 ATR5 ATR6 ATR7))
   (if (= nattr 8) (command "_insert" newblk p0 MX MY SZOG ATR1 ATR2 ATR3 ATR4 ATR5 ATR6 ATR7 ATR8))
   (if (= nattr 9) (command "_insert" newblk p0 MX MY SZOG ATR1 ATR2 ATR3 ATR4 ATR5 ATR6 ATR7 ATR8 ATR9))
   (if (= nattr 10) (command "_insert" newblk p0 MX MY SZOG ATR1 ATR2 ATR3 ATR4 ATR5 ATR6 ATR7 ATR8 ATR9 ATR10))

; A r�gi blokk t�rl�se
   (entdel ssnold)
  (setq i (1+ i))
  );repeat
 );if sset
);defun
