(defun c:deldvet ()
  ; A rutin az ITR altal keszitett szelvenyezes zaszlok hosszat roviditi meg
  (setvar "cmdecho" 0)
  (setq javdb 0)
  (setq sset (ssget "x" '(( 0 . "circle"))))
  (if sset (command "_erase" sset ""))
  
  (command "_layer" "_set" "GAZ-Gfk-TEREPMAG-FELIRAT" "")
  (command "vt�pus" "b" "f" "")
  (setq javdb 0)
  (command "_color" "bylayer")
          
   (setq sset (ssget "x" '((0 . "lwpolyline")(-4 . "<or")
                                             ( 8 . "GAZ-Gfk-TEREPMAG-FELIRAT")
                                             ( 8 . "GAZ-Gfn-TEREPMAG-FELIRAT")
                                             ( 8 . "GAZ-Gfnk-TEREPMAG-FELIRAT")
                                             ( 8 . "GAZ-Gf-TEREPMAG-FELIRAT")
                                             (-4 . "or>"))))
   (setq i 0)
   (if sset
    (progn
     (setq n2 (sslength sset))
     (repeat (sslength sset)
      (princ (strcat (itoa i) "/" (itoa n2) "\r"))  
      (setq ssn (ssname sset i))
      (setq lst (entget ssn))
      (setq vtx (vertex_list1 ssn))
      (setq p0 (nth 1 vtx))
      (command "_zoom" "_c" p0 10)
      (setq x0 (car p0))
      (setq y0 (cadr p0))
      (setq p1 (list (+ x0 0.5) (+ y0 0.5)))
      (setq p2 (list (- x0 0.5) (+ y0 0.5)))
      (setq p3 (list (- x0 0.5) (- y0 0.5)))
      (setq p4 (list (+ x0 0.5) (- y0 0.5)))
      (setq svonal (ssget "_cp" (list p1 p2 p3 p4 p1) ' (( 0 . "line")(-4 . "<or")
                                             ( 8 . "GAZ-Gfk-TEREPMAG-FELIRAT")
                                             ( 8 . "GAZ-Gfn-TEREPMAG-FELIRAT")
                                             ( 8 . "GAZ-Gfnk-TEREPMAG-FELIRAT")
                                             ( 8 . "GAZ-Gf-TEREPMAG-FELIRAT")
                                             (-4 . "or>"))))
      (if svonal
        (progn
         (setq k 0 ssvuj (ssadd) idb 0)
         (repeat (sslength svonal)
          (setq ssn3 (ssname svonal k))
          (command "_erase"  ssn3 "")
          (setq javdb (1+ javdb))
          (setq k (1+ k))
         );repeat
        );progn
       );if svonal

      (setq i (1+ i))
     );repeat i
      );progn
     );if sset
   (print (strcat "Jav�tva: " (itoa javdb)))
);defun


 
