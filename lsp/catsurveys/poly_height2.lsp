(defun c:ptlab (/ Text _PromptWithDefault _Cross Pt pos)
  ;; Lee Mac  ~  30.03.10

  (defun Text (pt hgt str)
    (entmakex (list (cons 0 "TEXT") (cons 10  pt)
                    (cons 40 hgt)   (cons 7 (getvar 'TEXTSTYLE))
                    (cons 1  str))))

  (defun _PromptWithDefault (f arg d)
    (cond ((apply f arg)) (d)))

  (defun _Cross (p h / l)
    (setq l (sqrt (* 0.5 h h)))
    (mapcar
      (function
        (lambda (p1 p2)
          (entmakex (list (cons 0 "LINE") (cons 10 p1) (cons 11 p2)))))

      (list (polar p (/      pi  4.) l)
            (polar p (/ (* 3 pi) 4.) l))

      (list (polar p (/ (* 5 pi) 4.) l)
            (polar p (/ (* 7 pi) 4.) l))))
  
  (setq *Coord* (cond (*Coord*) ("Y"))
        *tHgt*  (cond (*tHgt* ) ((getvar 'TEXTSIZE)))
        *thOff* (cond (*thOff*) (1.0))
        *tvOff* (cond (*tvOff*) (1.0))
        *cSze*  (cond (*cSze* ) ((getvar 'TEXTSIZE))))

  (setq pos '(("X" . 0) ("Y" . 1) ("Z" . 2)))

  (initget "X Y Z")
  (mapcar (function set) '(*Coord* *tHgt* *thOff* *tvOff* *cSze*)
          (mapcar (function _PromptWithDefault) '(getkword getdist getdist getdist getdist )
                  (list (list (strcat "\nSpecify Coord to Label [X/Y/Z] <" *Coord* "> : "))
                        (list (strcat "\nSpecify Text Height <" (rtos *tHgt*) "> : "))
                        (list (strcat "\nSpecify Horizontal Text Offset <" (rtos *thOff*) "> : "))
                        (list (strcat "\nSpecify Vertical Text Offset <" (rtos *tvOff*) "> : "))
                        (list (strcat "\nSpecify Cross Height <" (rtos *cSze*) "> : ")))
                  (list *Coord* *tHgt* *thOff* *tvOff* *cSze*)))

  (while (setq pt (getpoint "\nPick Point to Label <Exit> : "))
    (_Cross (setq pt (trans pt 1 0)) *cSze*)

    (Text (polar  (polar pt 0. *thOff*) (/ pi 2.) *tvOff*) *tHgt*
          (rtos (nth (cdr (assoc *Coord* pos)) pt))))

  (princ))