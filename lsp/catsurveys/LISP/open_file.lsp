(setq filename "c:/Users/tkosa/Desktop/Teszt/TEST.dwg")
(setq vgao (vlax-get-acad-object))
(setq vgad (vla-get-activedocument vgao))
(setq vgd (vla-get-documents vgao))
(if
  (= 0 (getvar "SDI"))
  (vla-activate (vla-open vgd filename))                               ; if sdi = 0
  (vla-sendcommand vgad (strcat "(command \"_open\")\n" filename "\n")); if sdi = 1
  )