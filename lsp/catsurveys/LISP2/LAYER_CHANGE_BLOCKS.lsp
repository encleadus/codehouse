(defun c:ae()
 (command "cmdecho" "0")
 (setq sp1 (getpoint "\n Add meg az �teresz kezd�pontj�t !"))
 (setq sp2 (getpoint sp1 "\n Add meg a v�gpontot !"))
 (command "_layer" "_set" "D54_�TERESZ" "")
 (command "_line" sp1 sp2 "")
 (setq szog (angle sp1 sp2))
 (setq szog (+ szog (/ pi 2)))
 (setq szog2 (+ szog pi))
 (setq szog (/ (* szog 180) pi))
 (setq szog2 (/ (* szog2 180) pi))

 
 (command "_layer" "_set" "D55_�TERESZ_JK" "")
 (command "_insert" "JKDATD07" sp1 "0.5" "0.5" szog)
 (command "_insert" "JKDATD07" sp2 "0.5" "0.5" szog2)

)

 ;CSL-RECORDS ELECTRIC EHV layerre
(defun C:ehv ()
  (command "_layer" "_s" "CSL-RECORDS ELECTRIC EHV" "")
)

 ;D06_�ROK layerre
(defun C:arok ()
  (command "_layer" "_s" "D06_�ROK" "")
)
 ;D05_J�RDA layerre
(defun C:ja ()
  (command "_layer" "_s" "D05_J�RDA" "")


)

 ;C26_H�ZSZ�M layerre h�zsz�mot fel�r
(defun C:c26 ()
  (command "_layer" "_s" "C26_H�ZSZ�M" "")
  (setq txtmag 1)
  (setq betutip "ARIAL_�LL�")
  (c:ti2)
)


(defun Inc (Szam)
  (setq indulo (getstring "\nIndul� sz�m:"))
  (setq p1 (getpoint "\nText insert�l�si pontja:"))
  (setq szog (getangle p1 "\nAdd meg a sz�get!"))
  (setvar "CMDECHO" 0)
  (setq szog (/ (* szog 180) pi))
  (command "_text" "_s" betutip p1 txtmag szog indulo)
  (setq indulo (itoa (+ (atoi indulo) Szam)))
  (repeat 100
    (setq p1 (getpoint "\nText insert�l�si pontja:"))
    (setq szog (getangle p1 "\nAdd meg a sz�get!"))
    (setq szog (/ (* szog 180) pi))
    ;(princ indulo)
    (command "_text" "_s" betutip p1 txtmag szog indulo)
    (setq indulo (itoa (+ (atoi indulo) Szam)))
  )
  (setvar "CMDECHO" 0)
)


 ;TI:n�veli a beirt sz�mokat
(defun c:ti ()
  (Inc 1)
)

 ;TI2:kettes�vel n�veli a beirt sz�mokat
(defun c:ti2 ()
  (Inc 2)
)


(defun c:fgy()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "FAOSZLOP_GY�M" pause "0.5" "0.5" pause)
)

(defun c:bo()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "BETONOSZLOP" pause "0.5" "0.5" pause)
)

(defun c:bd()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "BETONOSZLOP_2" pause "0.5" "0.5" pause)
)

(defun c:fa()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "FA" pause "0.5" "0.5" "0")
)

(defun c:fgy2()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "FAOSZLOP_2GY�M" pause "0.5" "0.5" pause)
)

(defun c:fo()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "FAOSZLOP" pause "0.5" "0.5" pause)
)

(defun c:ak()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "AKNAFEDLAP_K�R" pause "0.5" "0.5" pause)
)

(defun c:an()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "AKNAFEDLAP_N�GYZET" pause "0.5" "0.5" pause)
)

(defun c:alap()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "JKDATA29" pause "0.5" "0.5" pause)
)

(defun c:vny()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "V�ZNYEL�" pause "0.5" "0.5" pause)
)

(defun c:vt()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "VASTRAVERZ" pause "0.5" "0.5" pause)
)

(defun c:tff()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "T�ZCSAP_F�LD_FELETT" pause "0.5" "0.5" pause)
)

(defun c:tfa()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "T�ZCSAP_F�LD_ALATT" pause "0.5" "0.5" pause)
)

(defun c:trafo()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "TRANSZFORM�TOR" pause "0.5" "0.5" pause)
)

(defun c:nyk()
 (command "_layer" "_set" "E04_K�ZM�_ALAP_JK" "")
 (command "_insert" "NYOM�SK�T" pause "0.5" "0.5" pause)
)

(defun c:kj ()
  (setvar "CMDECHO" 0)
  (setvar "ORTHOMODE" 0)
  (command "_ucs" "")
  (command "_layer" "_s" "B47_KAPCS_JEL" "")
  (while t
    (setq ent1 (car (entsel "\nBokj az elemre!")))
    (setq pstart (cdr (assoc 10 (entget ent1)))
	  psx	 (nth 0 pstart)
	  psy	 (nth 1 pstart)
	  pend	 (cdr (assoc 11 (entget ent1)))
	  pex	 (nth 0 pend)
	  pey	 (nth 1 pend)
	  ipx	 (/ (+ psx pex) 2)
	  ipy	 (/ (+ psy pey) 2)
	  ip	 (list ipx ipy)
    )
    (setq sp-angle (angle pstart pend))
    (setq szog (/ (* 180.0 sp-angle) pi))
    (command "_insert" "JKDATB04" ip "0.5" "0.5" szog)
    (setq ent1 nil)
  ) ;while
  (setvar "CMDECHO" 1)
  (setvar "ORTHOMODE" 1)
)