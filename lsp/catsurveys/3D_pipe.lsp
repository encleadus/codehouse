;;;===================================================================
;;; CURVE2TUBE -Gilles Chanteau- (gile)
;;; Extrudes a "donut region" along a curve(s) object(s).
;;;===================================================================
(defun c:curve2tube	(/	   AcDoc     Space     ext_rad
			 int_rad   ss	     obj       start
			 ext_circ  int_circ  ext_reg   int_reg
			 norm
			)

  (vl-load-com)

  (defun vlen (v)
    (sqrt (vxv v v))
  )

  (defun vxv (v1 v2)
    (apply '+ (mapcar '* v1 v2))
  )

  (defun vunit (v / l)
    (if	(/= 0 (setq l (vlen v)))
      (mapcar '(lambda (x) (/ x l)) v)
    )
  )

  (setq AcDoc (vla-get-ActiveDocument (vlax-get-acad-object)))
  (setq	Space
	 (if (= 1 (getvar "CVPORT"))
	   (vla-get-PaperSpace AcDoc)
	   (vla-get-ModelSpace AcDoc)
	 )
  )
  (if
    (and
      (setq ext_rad (getdist "\nExternal radius: "))
      (setq int_rad (getdist "\nInternal radius: "))
      (setq ss
	     (ssget
	       '((-4 . "<OR")
		 (0 . "ARC,CIRCLE,ELLIPSE,LINE,LWPOLYLINE")
		 (-4 . "<AND")
		 (0 . "POLYLINE")
		 (-4 . "<NOT")
		 (-4 . "&")
		 (70 . 112)
		 (-4 . "NOT>")
		 (-4 . "AND>")
		 (-4 . "<AND")
		 (0 . "SPLINE")
		 (-4 . "&")
		 (70 . 8)
		 (-4 . "AND>")
		 (-4 . "OR>")
		)
	     )
      )
    )
     (progn
       (vla-StartUndoMark AcDoc)
       (foreach	path (vl-remove-if 'listp (mapcar 'cadr (ssnamex ss)))
	 (setq obj (vlax-ename->vla-object path))
	 (setq start (vlax-curve-getPointAtParam
		       obj
		       (vlax-curve-getStartParam obj)
		     )
	 )
	 (setq ext_circ (vla-addCircle Space (vlax-3d-Point start) ext_rad))
	 (setq int_circ (vla-addCircle Space (vlax-3d-Point start) int_rad))
	 (setq norm (vunit (vlax-curve-getFirstDeriv
			     obj
			     (vlax-curve-getStartParam obj)
			   )
		    )
	 )
	 (vla-put-Normal ext_circ (vlax-3d-point norm))
	 (vla-put-Normal int_circ (vlax-3d-point norm))
	 (setq ext_reg (car (vlax-invoke Space 'addRegion (list ext_circ))))
	 (setq int_reg (car (vlax-invoke Space 'addRegion (list int_circ))))
	 (vla-Boolean ext_reg acSubtraction int_reg)
	 (vla-addExtrudedSolidAlongPath Space ext_reg obj)
	 (mapcar 'vla-delete (list obj ext_circ int_circ ext_reg))
       )
       (vla-EndUndoMark AcDoc)
     )
  )
  (princ)
)
;;;=======================================================================================