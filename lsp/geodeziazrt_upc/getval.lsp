(defun GETVAL (grp ele)                 ;"dxf value" of any ent...
  (cond ((= (type ele) 'ENAME)          ;ENAME
          (cdr (assoc grp (entget ele))))
        ((not ele) nil)                 ;empty value
        ((not (listp ele)) nil)         ;invalid ele
        ((= (type (car ele)) 'ENAME)    ;entsel-list
          (cdr (assoc grp (entget (car ele)))))
        (T (cdr (assoc grp ele)))))     ;entget-list

