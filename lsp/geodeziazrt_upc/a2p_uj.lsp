(defun c:go()
 (setq sset (ssget "x" '(( 0 . "arc"))))
 
   (setq j 0)
    (repeat (sslength sset)
    (setq ssn (ssname sset j))
    (segs ssn)
    (setq j (1+ j))
    )
)
(defun Segs (ent /  EPAR I INC PTS UFLAG)
  (vl-load-com) ; Lee Mac  ~  29.01.10

  (setq *doc  (cond (*doc) ((vla-get-ActiveDocument (vlax-get-acad-object))))        
        *segs (cond (*segs) (10)))
  
  (while
  
  
    (progn
 ;     (setq ent (car (entsel)))
   ;     (setq ent (ssname sset j))

      (cond (  (eq 'ENAME (type ent))

               (if (vl-catch-all-error-p
                     (setq ePar
                       (vl-catch-all-apply
                         (function vlax-curve-getEndParam) (list ent))))

                 (princ "\n ** Invalid Object **")
                 (progn
                   (initget 6)
          ;         (setq *segs (cond ((getint (strcat "\nSpecify Number of Segments <"
          ;                                            (itoa *segs) "> : "))) (*segs)))
                   (setq *segs 10)
                   (vla-StartUndoMark *doc)

                   (setq inc (/ (vlax-curve-getDistatParam ent ePar) (float *segs)) i -1)

                   (repeat (1+ *segs)
                     (setq pts (cons (vlax-curve-getPointatDist ent
                                       (* (setq i (1+ i)) inc)) pts)))

                   (entmake (append (list (cons 0   "LWPOLYLINE")
                                          (cons 100 "AcDbEntity")
                                          (cons 100 "AcDbPolyline")
                                          (cons 90 (length pts))
                                          (cons 70 0))
                                    (mapcar (function (lambda (a) (cons 10 a))) pts)))
                   (entdel ent)                   
                   (vla-EndUndoMark *doc)))))))
  (princ))
