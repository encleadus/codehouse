(defun c:uns ()
	(setvar "cmdecho" 0)
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(if (not (member "utcanev" (ade_odtablelist)))
		(progn 
			(setq lst_def
			'(("Tablename" . "utcanev") ("TableDesc" . "Utcanevek adattblja") 
			("Columns" (("ColName" . "nev") ("ColDesc" . "Utca neve") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq enty nil)
	(while (not enty)
	(setq enty (car (entsel "\nBkj az utcanevre! vagy ESC")))
	)
	(setq txt (cdr (assoc 1 (entget enty))))
	
	
	
	(princ "\n")
	(command "_view" "_save" "temp")
	(command "_zoom" "_e")
	(setq sset (ssget "_x" (list (cons 0 "TEXT") (cons 8 "Level 53,hazszam"))))
    (setq i 0)
	(repeat (sslength sset)
		(setq entx (ssname sset i))
		(if (member "utcanev" (ade_odgettables entx))
			(progn
				(if (= txt (ade_odgetfield entx "utcanev" "nev" 0))
				(if (= 1 (cdr (assoc 62 (entget entx))))
				(command "_chprop" entx "" "_c" 5 "")
				(command "_chprop" entx "" "_c" 1 "")
				)
				)
			)
		)
		
		(setq i (1+ i))
		(princ (strcat "\r " (work i (sslength sset)) "%"))
	)
	(command "_view" "_restore" "temp")	
		
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
	(command "._undo" "_end")
	
)


(defun work ( act maxi )
	(itoa (fix (/ (* 100 act) maxi)))
)