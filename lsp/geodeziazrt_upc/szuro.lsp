(defun work ( act maxi )
	(itoa (fix (/ (* 100 act) maxi)))
)

(defun c:trenchaknabekot ( )
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(setq puffer 1)
	(command "_zoom" "_e" "_layer" "_m" "kotes" "")
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 2 "EVKAST*"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(setq szog (cdr (assoc 50 tlist)))
				(setq np1 (polar pstart (+ szog (/ pi 2)) 0.5))
				(command "_circle" np1 5)
				
				(setq j (1+ j))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ (strcat "\nKsz \n" (itoa i)))
		)
		
	)
	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
)



(defun c:evsel ( )
	(setvar "CMDECHO" 0)
	
	(command "_view" "_save" "temp")
 	(command "_osnap" "_none")

	(command "_zoom" "_e" "_layer" "_m" "evtxt" "")
	(setq lof (ssget "_x" (list (cons 0 "TEXT") (cons 8 "Level 26"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				;(setq obj (vlax-ename->vla-object (entx)))
				(setq txt (cdr (assoc 1 tlist)))
				
				(if (= 10 (strlen txt)) 
				(progn
					(setq tlist (subst (cons 8 "evtxt")(assoc 8 tlist) tlist))
					(entmod tlist)
					(entupd entx)
				))
				(setq j (1+ j))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ "\nKsz")
		)
		
	)
	(princ "\n")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" 1)

	

)

(defun c:gvtxt ( )
	(setvar "CMDECHO" 0)
	
	(command "_view" "_save" "temp")
 	(command "_osnap" "_none")

	(command "_zoom" "_e" "_layer" "_m" "gvtxt" "")
	(setq lof (ssget "_x" (list (cons 0 "TEXT") (cons 8 "nemevfelirat"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				;(setq obj (vlax-ename->vla-object (entx)))
				(setq txt (cdr (assoc 1 tlist)))
				
				(if (and (not (vl-string-search "WC" (strcase txt))) (or (vl-string-search "GV" (strcase txt)) (> 10 (strlen txt) 7) (and (= 8 (strlen txt)) (= "-" (substr txt 3 1) (substr txt 6 1))))) 
				(progn
					(setq tlist (subst (cons 8 "gvtxt")(assoc 8 tlist) tlist))
					(entmod tlist)
					(entupd entx)
				))
				(setq j (1+ j))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ "\nKsz")
		)
		
	)
	(princ "\n")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

	

)

(defun c:evtxt ( )
	(setvar "CMDECHO" 0)
	
	(command "_view" "_save" "temp")
 	(command "_osnap" "_none")

	(command "_zoom" "_e" "_layer" "_m" "evtxt" "")
	(setq lof (ssget "_x" (list (cons 0 "TEXT"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				;(setq obj (vlax-ename->vla-object (entx)))
				(setq txt (cdr (assoc 1 tlist)))
				
				(if (or (vl-string-search "EV" (strcase txt)) (< 9 (strlen txt)) (and (= 5 (strlen txt)) (= "-" (substr txt 3 1))) )
				(progn
					(setq tlist (subst (cons 8 "evtxt")(assoc 8 tlist) tlist))
					(entmod tlist)
					(entupd entx)
				))
				(setq j (1+ j))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ "\nKsz")
		)
		
	)
	(princ "\n")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

	

)
(defun c:bestp ( )
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 8 "Level 20"))))
	(if lof
		(progn
			(setq puff 10)
			(while (< puff 30) 
			(setq hlof (sslength lof))
			(setq j 0)
			(setq i 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(command "_polygon" 20 pstart "_c" puff)
				(setq pol (entlast))
				(make-vert-list pol)
				(setq vertlist vert-list)
				(command "_zoom" "_o" "_si" pol "_redraw")
				(entdel pol)
				(setq sset (ssget "_CP" vertlist (list (cons 0 "TEXT") (cons 8 "Level 51"))))
				(if (or (not sset) (< 1 (sslength sset)))
					(progn
					
					(setq i (1+ i))
					)
					
					
				)
				
				
				(setq j (1+ j))
				;(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ (strcat "\n " (itoa i) "/" (itoa puff)))
			(setq puff (1+ puff))
			)
		)
		
	)
	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

	

)

(defun c:geta ()

	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")

	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 8 "*SYM*") )))
			(if lof
				(progn
					(setq hlof (sslength lof))
					(setq j 0)
					(setq plista (list (list (cons "szam" "koord"))))
					(while (< j hlof)
						(setq entx (ssname lof j))
						(setq tlist (entget entx))
						(setq pstart (cdr (assoc 10 tlist)))
						(if (member "erosito_nev" (ade_odgettables entx))
							(setq nrec (ade_odrecordqty entx "erosito_nev"))
							(setq nrec 0)
						)
						(setq i 0)
						(repeat nrec
							(setq txt (ade_odgetfield entx "erosito_nev" "az" i))
							(setq plista (append plista (list (cons txt pstart))))
							(setq i (1+ i))
						)
						(setq j (1+ j))
						(princ (strcat "\rSzmok legyjtse: " (work j hlof) "%"))
					)
				)
			)
			
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 8 "*KST*,*KAST*") )))
			(if lof
				(progn
					(setq hlof (sslength lof))
					(setq j 0)
					(while (< j hlof)
						(setq entx (ssname lof j))
						(setq tlist (entget entx))
						(if (member "erosito_nev" (ade_odgettables entx))
							(setq nrec (ade_odrecordqty entx "erosito_nev"))
							(setq nrec 0)
						)
						(setq i 0)
						(repeat nrec
							(setq txt (ade_odgetfield entx "erosito_nev" "az" i))
							(setq plista (vl-remove  (assoc txt plista) plista))
							(setq i (1+ i))
						)
						(setq j (1+ j))
						(princ (strcat "\rSzmok legyjtse: " (work j hlof) "%"))
					)
				)
			)
	(setq j 0)
	(repeat (length plista)
		(command "_circle" (cdr (nth j plista)) 5)
		(setq j (1+ j))
	)

	(command "._undo" "_end")
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

)



(defun c:a ()
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(if (member "erosito_nev" (ade_odtablelist))
		(progn
		)
		(progn 
			(setq lst_def
			'(("Tablename" . "erosito_nev") ("TableDesc" . "Erst adattblja") 
			("Columns" (("ColName" . "az") ("ColDesc" . "Erst/Hub szma") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq entx nil enty nil)
  	(while (not entx)
	(setq entx (car (entsel "\nBkj az aknra! vagy ESC")))
	)
	

	(while (not enty)
	(setq enty (car (entsel "\nBkj az elvire! vagy ESC")))
	)
	(setq nrecx (ade_odrecordqty enty "erosito_nev"))
	
	
	;(setq txt (cdr (assoc 1 (entget enty))))
	(setq nrec (ade_odrecordqty entx "erosito_nev"))
	(setq i 0)
	(repeat nrecx
		(setq new (ade_odnewrecord "erosito_nev"))
		(ade_odattachrecord entx new) 
		(setq txt (ade_odgetfield enty "erosito_nev" "az" i))
		(ade_odsetfield entx "erosito_nev" "az" (+ i nrec) txt)
		(setq i (1+ i))
	)
	(command "._undo" "_end")
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
	
)
(defun c:kv ()
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(if (member "sorszam" (ade_odtablelist))
		(progn
		)
		(progn 
			(setq lst_def
			'(("Tablename" . "sorszam") ("TableDesc" . "Erst adattblja") 
			("Columns" (("ColName" . "az") ("ColDesc" . "Erst/Hub szma") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq entx nil enty nil)
  	(while (not entx)
	(setq entx (car (entsel "\nBkj a blokkra! vagy ESC")))
	)
	

	(while (not enty)
	(setq enty (car (entsel "\nBkj a textre! vagy ESC")))
	)
	(setq txt (cdr (assoc 1 (entget enty))))
	(setq new (ade_odnewrecord "sorszam"))
	(ade_odattachrecord entx new) 
	(setq nrec (ade_odrecordqty entx "sorszam"))
	(ade_odsetfield entx "sorszam" "az" (- nrec 1) txt) 
	(command "._undo" "_end")
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
	
)

(defun c:hv ()
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(if (not (member "utcanev" (ade_odtablelist)))
		(progn 
			(setq lst_def
			'(("Tablename" . "utcanev") ("TableDesc" . "Utcanevek adattblja") 
			("Columns" (("ColName" . "nev") ("ColDesc" . "Utca neve") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq entx nil enty nil)
  	(princ "\nJelld ki amihez hozz kell rendelni a nevet!\n")
	(setq sset nil)
	(while (not sset)
	(setq sset (ssget))
	
	;(setq entx (car (entsel "\nBkj a valamire! vagy ESC")))
	)
	

	(while (not enty)
	(setq enty (car (entsel "\nBkj a textre! vagy ESC")))
	)
	(setq txt (cdr (assoc 1 (entget enty))))
	(setq i 0)
	(repeat (sslength sset)
		(setq entx (ssname sset i))
		(setq new (ade_odnewrecord "utcanev"))
		(ade_odattachrecord entx new) 
		(setq nrec (ade_odrecordqty entx "utcanev"))
		(ade_odsetfield entx "utcanev" "nev" (- nrec 1) txt)
		(command "_chprop" entx "" "_c" 5 "")
		(setq i (1+ i))
	)
		
	(command "._undo" "_end")
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
	
)



(defun c:s ()
	(setvar "cmdecho" 0)
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(if (not (member "utcanev" (ade_odtablelist)))
		(progn 
			(setq lst_def
			'(("Tablename" . "utcanev") ("TableDesc" . "Utcanevek adattblja") 
			("Columns" (("ColName" . "nev") ("ColDesc" . "Utca neve") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq enty nil)
	(while (not enty)
	(setq enty (car (entsel "\nBkj a hzra! vagy ESC")))
	)
	;(setq txt (cdr (assoc 1 (entget enty))))
	
	(make-vert-list enty)
	(setq sset (ssget "_cp" vert-list (list (cons 0 "TEXT") (cons 8 "Level 53,hazszam"))))
	(setq lis (list (cons "NEV" 0)))
	(setq i 0)
	(repeat (sslength sset)
		(setq entx (ssname sset i))
		(if (member "utcanev" (ade_odgettables entx))
			(progn
				(setq txt (ade_odgetfield entx "utcanev" "nev" 0))
				(if (not (assoc txt lis))
					(progn
						(setq lis (cons (cons txt (+ 6 (fix(* 254 (rnd))))) lis))
					)
				)
				(command "_chprop" entx "" "_c" (cdr (assoc txt lis)) "")
			)
		)
		
		(setq i (1+ i))
	)
	(setq i 0)
	(repeat (length lis)
		(setq txt (car (nth i lis)))
		(setq col (cdr (nth i lis)))
		(setq lof (ssget "_x" (list (cons 0 "TEXT") (cons 1 txt))))
		(if lof (command "_chprop" lof "" "_c" col ""))
		(setq i (1+ i))
	)
	
		
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
	(command "._undo" "_end")
	
)

(defun c:attwc ( )
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	(if (member "sorszam" (ade_odtablelist))
		(progn
		)
		(progn 
			(setq lst_def
			'(("Tablename" . "sorszam") ("TableDesc" . "Erst adattblja") 
			("Columns" (("ColName" . "az") ("ColDesc" . "Erst/Hub szma") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 8 "Level 20"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(setq i 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(command "_polygon" 20 pstart "_c" 19)
				(setq pol (entlast))
				(make-vert-list pol)
				(setq vertlist vert-list)
				(command "_zoom" "_o" "_si" pol "_redraw")
				(entdel pol)
				(setq sset (ssget "_CP" vertlist (list (cons 0 "TEXT") (cons 8 "Level 51"))))
				(if (or (not sset) (< 1 (sslength sset)))
					(progn
					(command "_circle" pstart 5)
					(setq i (1+ i))
					)
					(progn
						(setq txt (cdr (assoc 1 (entget (ssname sset 0)))))
						(setq new (ade_odnewrecord "sorszam"))
						(ade_odattachrecord entx new) 
						(setq nrec (ade_odrecordqty entx "sorszam"))
						(ade_odsetfield entx "sorszam" "az" (- nrec 1) txt) 
					)
					
				)
				
				
				(setq j (1+ j))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ (strcat "\nKsz \n" (itoa i)))
		)
		
	)
	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
)

(defun c:attdupe ( )
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	(if (member "sorszam" (ade_odtablelist))
		(progn
		)
		(progn 
			(setq lst_def
			'(("Tablename" . "sorszam") ("TableDesc" . "Erst adattblja") 
			("Columns" (("ColName" . "az") ("ColDesc" . "Erst/Hub szma") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq lof (ssget "_x" (list (cons 0 "INSERT"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(setq h 1)
			(setq lista (list (cons "NEV" 0)))
			(setq slista (list "NEV"))
			(setq clista (list (list 0 1 2)))
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(if (member "sorszam" (ade_odgettables entx))
					(setq nrec (ade_odrecordqty entx "sorszam"))
					(setq nrec 0)
				)
				(setq i 0)
				(repeat nrec
					(setq txt (ade_odgetfield entx "sorszam" "az" i))
					(setq elem (cons txt h))
					(if (member txt slista)
						(progn
							(setq no (cdr (assoc txt lista)))
							(setq plis (nth no clista))
							(setq clista (subst (append plis (list pstart)) plis clista))
							(setq col (1+ (fix(* 254 (rnd)))))
							(setq n 0)
							(repeat (length plis)
								(if (< (distance pstart (nth n plis)) 200)
									(progn
										(command "_circle" pstart 5)
										(command "_chprop" (entlast) "" "_c" col "")
										(command "_circle" (nth n plis) 5)
										(command "_chprop" (entlast) "" "_c" col "")
									)
								)
								(setq n (1+ n))
							)
						)
						(progn
							(setq lista (cons elem lista))
							(setq slista (cons txt slista))
							(setq clista (append clista (list (list pstart))))
							(setq h (1+ h))

						)
					)
					(setq i (1+ i))
				)
				
				
				(setq j (1+ j))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ (strcat "\nKsz \n"))
		)
		
	)
	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
)

(defun c:attgv ( )
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	(if (member "sorszam" (ade_odtablelist))
		(progn
		)
		(progn 
			(setq lst_def
			'(("Tablename" . "sorszam") ("TableDesc" . "Erst adattblja") 
			("Columns" (("ColName" . "az") ("ColDesc" . "Erst/Hub szma") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 8 "Level 25"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(setq i 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(command "_polygon" 20 pstart "_c" 29)
				(setq pol (entlast))
				(make-vert-list pol)
				(setq vertlist vert-list)
				(command "_zoom" "_o" "_si" pol "_redraw")
				(entdel pol)
				(setq sset (ssget "_CP" vertlist (list (cons 0 "TEXT") (cons 8 "Level 26"))))
				(if (or (not sset) (< 1 (sslength sset)))
					(progn
					(command "_circle" pstart 5)
					(setq i (1+ i))
					)
					(progn
						(setq txt (cdr (assoc 1 (entget (ssname sset 0)))))
						;(setq szog (cdr (assoc 50 tlist)))
						;(setq szog (/ (* 180.0 szog) pi))
						;(command "_insert" "GVSYMB_ATT" pstart "1" "1" szog txt)
						;(entdel entx)
						(setq new (ade_odnewrecord "sorszam"))
						(ade_odattachrecord entx new) 
						(setq nrec (ade_odrecordqty entx "sorszam"))
						(ade_odsetfield entx "sorszam" "az" (- nrec 1) txt) 
					)
					
				)
				
				
				(setq j (1+ j))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ (strcat "\nKsz \n" (itoa i)))
		)
		
	)
	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
)
(defun c:attev ( )
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e" "_layer" "_m" "Level_31" "")
	(if (member "sorszam" (ade_odtablelist))
		(progn
		)
		(progn 
			(setq lst_def
			'(("Tablename" . "sorszam") ("TableDesc" . "Erst adattblja") 
			("Columns" (("ColName" . "az") ("ColDesc" . "Erst/Hub szma") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 8 "Level 30"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(command "_polygon" 20 pstart "_c" 60)
				(setq pol (entlast))
				(make-vert-list pol)
				(setq vertlist vert-list)
				(command "_zoom" "_o" "_si" pol "_redraw")
				(entdel pol)
				(setq sset (ssget "_CP" vertlist (list (cons 0 "TEXT") (cons 8 "Level 31,Level_31"))))
				(if sset
					(progn
						(setq i 0)
						(setq dis 999 ent nil)
						(repeat (sslength sset)
							(setq entz (ssname sset i))
						  	;(command "_chprop" entz "" "_c" "5" "")
						  	;(command "_line" pstart ps "")
							(setq tlistz (entget entz))
							(setq ps (cdr (assoc 11 tlistz)))
							(if (< (distance ps pstart) dis)
								(progn
									(setq dis (distance ps pstart))
									(setq ent entz)
								)
							)
						  	;(command "_chprop" entz "" "_c" "92" "")
						  	
							(setq i (1+ i))
						)
						(setq txt (cdr (assoc 1 (entget ent))))
						(setq new (ade_odnewrecord "sorszam"))
						(ade_odattachrecord entx new) 
						(setq nrec (ade_odrecordqty entx "sorszam"))
						(ade_odsetfield entx "sorszam" "az" (- nrec 1) txt)
						(command "_chprop" ent "" "_LA" "Level_31" "")						
					)
					(progn
						(command "_circle" pstart 5)
					)
					
				)
				
				
				(setq j (1+ j))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ (strcat "\nKsz \n" (itoa i)))
		)
		
	)
	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
)

(defun ta (/ j lof entx hlof nrec new txt)
	
	(command "_zoom" "_e")
	(if (not (member "utcanev" (ade_odtablelist)))
		(progn 
			(setq lst_def
			'(("Tablename" . "utcanev") ("TableDesc" . "Utcanevek adattblja") 
			("Columns" (("ColName" . "nev") ("ColDesc" . "Utca neve") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq lof (ssget "_x" (list (cons 0 "LWPOLYLINE") (cons 8 "KZPVONAL") )))
	(if lof
		(progn
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(make-vert-list entx)
				(command "_zoom" "_o" "_si" entx "_redraw")
				(setq sset2 (ssget "_F" vert-list (list (cons 0 "TEXT") (cons 8 "Level 53"))))
				(if (and sset2 (> 1 (ade_odrecordqty entx "utcanev")))
					(progn
						
						(setq txt (cdr (assoc 1 (entget (ssname sset2 0)))))
						(setq new (ade_odnewrecord "utcanev"))
						(ade_odattachrecord entx new) 
						(setq nrec (ade_odrecordqty entx "utcanev"))
						(ade_odsetfield entx "utcanev" "nev" (- nrec 1) txt) 
										
									
					)
				)
				(setq j (1+ j))
				(princ (strcat "\rUtcanv tengelyhez rendelse: " (itoa j) "/" (itoa hlof)))
			)
				
		)
	)
	
	
	
	
)
(defun getconn ( p typ / j i sp1 sp2 hss1 ss1 ss2 lay col ssout )
	(if (= typ "GV") (setq lay "Level 22" col 71 blk "WCSYM*,AFTAK*,GVSYM*"))
	(if (= typ "EV") (setq lay "Level 27" col 120 blk "GVSYM*,AFTAK*"))
	(setq sp1 (polar p (* 1 (/ pi 4)) 4))
	(setq sp2 (polar p (* 5 (/ pi 4)) 4))
	(setq ss1 (ssget "_C" sp1 sp2 (list (cons 0 "LWPOLYLINE") (cons 8 lay) (cons 62 col))))
	(setq ssout (ssget "_C" sp1 sp2 (list (cons 0 "INSERT") (cons 2 blk) )))
	(if ssout
		(progn)
		(setq ssout (ssadd))
	)
	(if ss1
	(progn
		(setq j 0)
		(setq hss1 (sslength ss1))
		(while (< j hss1)
			(setq pl (ssname ss1 j))
			(make-vert-list pl)
			(if (> (distance p (last vert-list)) (distance p (car vert-list)))
				(setq puj (last vert-list))
				(setq puj (car vert-list))
			)
			(setq sp1 (polar puj (* 1 (/ pi 4)) 4))
			(setq sp2 (polar puj (* 5 (/ pi 4)) 4))
			;(command "_line" sp1 sp2 "")
			(setq ss2 (ssget "_C" sp1 sp2 (list (cons 0 "INSERT") (cons 2 blk) )))
			(if ss2 
				(progn
					(setq i 0)
					(repeat (sslength ss2)
						(setq ssout (ssadd (ssname ss2 i) ssout))
						(setq i (1+ i))
						
					)
				)
			)
			(setq j (1+ j))
		)
	)
	(setq ssout (ssget "_C" sp1 sp2 (list (cons 0 "INSERT") (cons 2 blk))))
	
	
	)
	(if (and ssout (< 0 (sslength ssout)))
	(setq a ssout)
	(setq a nil)
	)
)

(defun c:gvnevez (/ j lof entx hlof nrec new txt)
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	
	(princ "\n")
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 8 "GVSYMB*") )))
	(if lof
		(progn
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(if (member "sorszam" (ade_odgettables entx))
					 (setq nrec (ade_odrecordqty entx "sorszam"))
					 (setq nrec 0)
				 )
				 (setq i 0)
				 (repeat nrec
					(setq txt (ade_odgetfield entx "sorszam" "az" i))
					(if (or (< (strlen txt) 8) (vl-string-search "GV" (strcase txt)) (vl-string-search "-" txt))
						(progn
							(setq txt (vl-string-subst "" "GV" (strcase txt)))
							(setq txt (vl-string-subst "" "-" txt))
							(if (and (= 6 (strlen txt)) (= "0" (substr txt 1 1)) (member (substr txt 6 1) (list "1" "2" "3" "4" "5" "6" "7" "8" "9" "0")))
								(setq txt (substr txt 2 5))
							)
							(if (= 5 (strlen txt))
								(progn
									(setq ts 1)
									(setq ps (list pstart))
									(setq s 0)
									(setq f nil)
									(while (and ts (< s (length ps)))
										(getconn (nth s ps) "GV")
										(if a
											(progn
												(setq hujset (sslength a))
												(setq h 0)
												(while (and ts (< h hujset))
													(setq enty (ssname a h))
													(setq blk (cdr (assoc 2 (entget enty))))
													(if (or (vl-string-search "AFTAK" blk) (vl-string-search "GVSYM" blk))  (setq ps (append ps (list (cdr (assoc 10 (entget enty)))))))
													(if (vl-string-search "WCSYM" blk)
														(setq ts nil f 1)
													)
													(setq h (1+ h))
												)
											)
										)
										(setq s (1+ s))
									)
									(if f
										(progn
											(if (member "sorszam" (ade_odgettables enty))
											(setq nrecy (ade_odrecordqty enty "sorszam"))
											(setq nrecy 0)
											)
											(setq s 0)
											(setq ujtxt nil)
											(while (< s nrecy)
												(setq txty (ade_odgetfield enty "sorszam" "az" s))
												
												(if (vl-string-search (substr txt 1 3) txty)
														(setq s 999 ujtxt (strcat (substr txty 1 3) txt))
												)
												(setq s (1+ s))
											)
									
									
										)
										(setq ujtxt nil)
									)
								)
								(setq ujtxt nil)
							)
							(if ujtxt
								(progn
									(ade_odsetfield entx "sorszam" "az" i ujtxt)
									(command "_color" 2 "_circle" pstart 5)
								)
								(command "_color" 1 "_circle" pstart 5)
							)
							
						)
					)
					(setq i (1+ i))
				 )
				
				;(command "_chprop" a "" "_c" 1 "")
				;(command "_circle" pstart 5)
				(setq j (1+ j))
				(princ (strcat "\rNevez: " (itoa j) "/" (itoa hlof)))
			)
				
		)
	)
		(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

)

(defun c:evnevez (/ j lof entx hlof nrec new txt)
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	
	(princ "\n")
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 8 "EVSYMB*") )))
	(if lof
		(progn
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				(setq pstart (cdr (assoc 10 tlist)))
				(if (member "sorszam" (ade_odgettables entx))
					 (setq nrec (ade_odrecordqty entx "sorszam"))
					 (setq nrec 0)
				 )
				 (setq i 0)
				 (repeat nrec
					(setq txt (ade_odgetfield entx "sorszam" "az" i))
					(if (or (< (strlen txt) 10) (vl-string-search "EV" (strcase txt)) (vl-string-search "-" txt))
						(progn
							(setq txt (vl-string-subst "" "EV" (strcase txt)))
							(setq txt (vl-string-subst "" "-" txt))
							; (if (and (= 5 (strlen txt)) (not (member (substr txt 5 1) (list "1" "2" "3" "4" "5" "6" "7" "8" "9" "0"))))
								; (setq txt (substr txt 3 3))
								; (if (= 4 (strlen txt)) (setq txt (substr txt 2 2)))
							; )
							(if (or (= 4 (strlen txt)) (and (= 5 (strlen txt)) (not (member (substr txt 5 1) (list "1" "2" "3" "4" "5" "6" "7" "8" "9" "0")))))
								(progn
									(setq ts 1)
									(setq ps (list pstart))
									(setq s 0)
									(setq f nil)
									(while (and ts (< s (length ps)))
										(getconn (nth s ps) "EV")
										(if a
											(progn
												(setq hujset (sslength a))
												(setq h 0)
												(while (and ts (< h hujset))
													(setq enty (ssname a h))
													(setq blk (cdr (assoc 2 (entget enty))))
													(if (or (vl-string-search "AFTAK" blk) (vl-string-search "EVSYM" blk))  (setq ps (append ps (list (cdr (assoc 10 (entget enty)))))))
													(if (vl-string-search "GVSYM" blk)
														(setq ts nil f 1)
													)
													(setq h (1+ h))
												)
											)
										)
										(setq s (1+ s))
									)
									(if f
										(progn
											(if (member "sorszam" (ade_odgettables enty))
											(setq nrecy (ade_odrecordqty enty "sorszam"))
											(setq nrecy 0)
											)
											(setq s 0)
											(setq ujtxt nil)
											(while (< s nrecy)
												(setq txty (ade_odgetfield enty "sorszam" "az" s))
												
												(if (vl-string-search (substr txt 1 2) txty)
														(setq s 999 ujtxt (strcat (substr txty 1 6) txt))
												)
												(setq s (1+ s))
											)
									
									
										)
										(setq ujtxt nil)
									)
								)
								(setq ujtxt nil)
							)
							(if ujtxt
								(progn
									(ade_odsetfield entx "sorszam" "az" i ujtxt)
									(command "_color" 2 "_circle" pstart 5)
								)
								(command "_color" 1 "_circle" pstart 5)
							)
							
						)
					)
					(setq i (1+ i))
				 )
				
				;(command "_chprop" a "" "_c" 1 "")
				;(command "_circle" pstart 5)
				(setq j (1+ j))
				(princ (strcat "\rNevez: " (itoa j) "/" (itoa hlof)))
			)
				
		)
	)
		(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

)

(defun c:wckeres (/ j lof entx hlof nrec new txt)
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	
	(princ "\n")
	(setq lof (ssget "_x" (list (cons 0 "INSERT") (cons 8 "WCSYMB") )))
	(if lof
		(progn
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (entget (ssname lof j)))
				(setq pstart (cdr (assoc 10 entx)))
				(command "_circle" pstart 5)
				(setq j (1+ j))
				(princ (strcat "\rKeres: " (itoa j) "/" (itoa hlof)))
			)
				
		)
	)
		(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

)

(defun c:lf (/ j lof entx hlof nrec new txt)
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	(if (not (member "utcanev" (ade_odtablelist)))
		(progn 
			(setq lst_def
			'(("Tablename" . "utcanev") ("TableDesc" . "Utcanevek adattblja") 
			("Columns" (("ColName" . "nev") ("ColDesc" . "Utca neve") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq lof (ssget "_x" (list (cons 0 "LWPOLYLINE") (cons 8 "epulet") )))
	(if lof
		(progn
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(if (member "utcanev" (ade_odgettables entx))
					 (setq nrec (ade_odrecordqty entx "utcanev"))
					 (setq nrec 0)
				)
				(if (= 1 nrec )
					(progn
						(make-vert-list entx)
						(command "_zoom" "_o" "_si" entx "_redraw")
						(setq txt (ade_odgetfield entx "utcanev" "nev" 0))
						(setq sset (ssget "_cp" vert-list (list (cons 0 "TEXT") (cons 8 "Level 53,hazszam"))))
						(setq i 0)
						(if sset
						(repeat (sslength sset)
							(setq enty (ssname sset i))
							(if (not (member "utcanev" (ade_odgettables enty)))
								(progn
									(setq new (ade_odnewrecord "utcanev"))
									(ade_odattachrecord enty new) 
									(ade_odsetfield enty "utcanev" "nev" 0 txt)
									(command "_chprop" enty "" "_c" 5 "")
								)
								
								
							)
							(setq i (1+ i))
						))
					)
				)
				(setq j (1+ j))
				(princ (strcat "\rBanzaiii?! " (work j hlof) "%"))
			)
		)
		
		
	)
		
	
	
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
	(command "._undo" "_end")
	
	
)

(defun c:sargahaz (/ j lof entx hlof nrec new txt)
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	(if (not (member "utcanev" (ade_odtablelist)))
		(progn 
			(setq lst_def
			'(("Tablename" . "utcanev") ("TableDesc" . "Utcanevek adattblja") 
			("Columns" (("ColName" . "nev") ("ColDesc" . "Utca neve") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq lof (ssget "_x" (list (cons 0 "LWPOLYLINE") (cons 8 "epulet") )))
	(if lof
		(progn
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(if (member "utcanev" (ade_odgettables entx))
					 (setq nrec (ade_odrecordqty entx "utcanev"))
					 (setq nrec 0)
				)
				(if (< 1 nrec ) (command "_chprop" entx "" "_c" 2 ""))
				
				(setq j (1+ j))
				(princ (strcat "\rSrgazhz: " (itoa j) "/" (itoa hlof)))
			)
				
		)
	)
	
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
	
	
)
(defun c:nic (/ j lof entx hlof nrec new txt)
	
	(if (not (member "utcanev" (ade_odtablelist)))
		(progn 
			(setq lst_def
			'(("Tablename" . "utcanev") ("TableDesc" . "Utcanevek adattblja") 
			("Columns" (("ColName" . "nev") ("ColDesc" . "Utca neve") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq lof (ssget "_x" (list (cons 0 "LWPOLYLINE") (cons 8 "KZPVONAL") )))
	(if lof
		(progn
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(if (not (member "utcanev" (ade_odgettables entx)))
					(command "_chprop" entx "" "_c" 5 "")
				)
				(setq j (1+ j))
				(princ (strcat "\rUtcanv tengelyhez rendelse: " (itoa j) "/" (itoa hlof)))
			)
				
		)
	)
	
)

(defun c:duplanev (/ j lof entx hlof nrec new txt)
	
	(command "_zoom" "_e")
	(if (not (member "utcanev" (ade_odtablelist)))
		(progn 
			(setq lst_def
			'(("Tablename" . "utcanev") ("TableDesc" . "Utcanevek adattblja") 
			("Columns" (("ColName" . "nev") ("ColDesc" . "Utca neve") ("ColType" 
			. "Character") ("DefaultVal" . "")) )))
			(ade_oddefinetab lst_def) 
		)
	)
	(setq lof (ssget "_x" (list (cons 0 "LWPOLYLINE") (cons 8 "KZPVONAL") )))
	(if lof
		(progn
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(make-vert-list entx)
				(command "_zoom" "_o" "_si" entx "_redraw")
				(setq sset2 (ssget "_F" vert-list (list (cons 0 "TEXT") (cons 8 "Level 53"))))
				
				(if sset2
					(progn
						(setq txt (cdr (assoc 1 (entget (ssname sset2 0)))))
						(setq i 0)
						(repeat (- (sslength sset2) 1)
							(if (/= txt (cdr (assoc 1 (entget (ssname sset2 i)))))
								(command "_chprop" entx "" "_c" 4 "")
							)
							(setq i (1+ i))
						)
					)
				)
				(setq j (1+ j))
				(princ (strcat "\rKeresfos: " (itoa j) "/" (itoa hlof)))
			)
				
		)
	)
	
	
	
	
)


(defun c:hnev ( ) ; utcanev plethez rendelse tengelybl
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	(initget "igen nem")
	(setq typ (getkword "\nAdd meg a topolgia tpust (igen/nem) <Exit>: "))
	(setq typ (strcase typ))
	(setq szogpuff (getreal "szog (R): "))
	(setq puff (getreal "puffer (m): "))
		
			
	(if (= typ "IGEN")	(ta))
	
	(princ "\n")
	(command "_zoom" "_e")
	(setq lof (ssget "_x" (list (cons 0 "LWPOLYLINE") (cons 8 "epulet") (cons 62 1))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				
				(if (not (member "utcanev" (ade_odgettables entx)))
				(progn	
				
				
				(make-vert-list entx)
				(setq vertlist vert-list)
				(command "_zoom" "_o" "_si" entx "_redraw")
				
				(setq sset (ssget "_WP" vertlist (list (cons 0 "TEXT") (cons 8 "Level 53"))))
				(if sset
					(progn
						(setq hsset (sslength sset))
						(setq i 0)
						(while (< i hsset)
							(setq enty (ssname sset i))
							(setq tlist (entget enty))
							(setq pstart (cdr (assoc 10 tlist)))
							(setq szog (cdr (assoc 50 tlist)))
							(setq np1 (polar pstart (+ szog (/ pi 2)) puff))
							(setq np2 (polar pstart (- szog (/ pi 2)) puff))
							(command "_line" np1 np2 "")
							(setq v (entlast))
							(command "_zoom" "_o" "_si" v "_redraw")
							(entdel v)
							(setq s 0)
							(setq int1 (inters pstart np1 (nth 0 vertlist) (last vertlist)))
							(while (and (not int1)  (< (1+ s) (length vertlist)))
								(setq int1 (inters pstart np1 (nth s vertlist) (nth (+ s 1) vertlist)))
								(setq s (1+ s))
							)
							(setq s 0)
							(setq int2 (inters pstart np2 (nth 0 vertlist) (last vertlist)))
							
							(while (and (not int2) (< (1+ s) (length vertlist)))
								(setq int2 (inters pstart np2 (nth s vertlist) (nth (+ s 1) vertlist)))
								(setq s (1+ s))
							)
							(if int1
							(progn
							(setq sset2 (ssget "_F" (list np1 int1) (list (cons 0 "LWPOLYLINE") (cons 8 "KZPVONAL"))))
							(setq kv1 nil)
							(if sset2
								(progn
									(setq hsset2 (sslength sset2))
									(setq mindist 999)
									(setq k 0)
									
									(repeat hsset2
											(setq entz (ssname sset2 k))
											(make-vert-list entz)
											(setq s 0)
											(setq int nil)
											(while (and (not int) (< (1+ s) (length vert-list)))
												(setq int (inters int1 np1 (nth s vert-list) (nth (+ s 1) vert-list)))
												(if (and int (< szogpuff (abs (- (rem (angle int1 np1) (/ pi 2)) (rem (angle (nth s vert-list) (nth (+ s 1) vert-list)) (/ pi 2))))))
													(setq int nil)
												)
												(setq s (1+ s))
											)
											(if int
											(if (< (distance int int1) mindist)
												(progn
													(setq intkv1 int)
													(setq mindist (distance int int1))
													(setq kv1 entz)
													
												)
											)
											)
											(setq k (1+ k))
									)
								)
							)
							)
							(setq kv1 nil)
							)
							(if int2
							(progn
							(setq sset2 (ssget "_F" (list np2 int2) (list (cons 0 "LWPOLYLINE") (cons 8 "KZPVONAL"))))
							(setq kv2 nil)
							(if sset2
								(progn
									(setq hsset2 (sslength sset2))
									(setq mindist 999)
									(setq k 0)
									
									(repeat hsset2
											(setq entz (ssname sset2 k))
											(make-vert-list entz)
											(setq s 0)
											(setq int (inters int2 np2 (nth 0 vert-list) (last vert-list)))
											(while (and (not int) (< (1+ s) (length vert-list)))
												(setq int (inters int2 np2 (nth s vert-list) (nth (+ s 1) vert-list)))
												(if (and int (< szogpuff (abs (- (rem (angle int2 np2) (/ pi 2)) (rem (angle (nth s vert-list) (nth (+ s 1) vert-list)) (/ pi 2))))))
													(setq int nil)
												)
												(setq s (1+ s))
											)
											(if int
											(if (< (distance int int2) mindist)
												(progn
													(setq intkv2 int)
													(setq mindist (distance int int2))
													(setq kv2 entz)
												
												)
											)
											)
											(setq k (1+ k))
									)
								)
							)
							)
							(setq kv2 nil)
							)
							(setq vonal nil)
							(if (and kv2 kv1)
								(if (< (distance intkv2 int2) (distance intkv1 int1))
									(setq vonal kv2)
									(setq vonal kv1)
								)
								(if kv1 (setq vonal kv1)
										(setq vonal kv2)
								)
							)
							
							(if vonal
								(progn
								
									
									(command "_chprop" entx "" "_c" 5 "")
									;(command "_chprop" vonal "" "_c" 5 "")
									(setq lista (list "")) 
									(setq txt (ade_odgetfield vonal "utcanev" "nev" 0))
									(setq new (ade_odnewrecord "utcanev"))
									(ade_odattachrecord enty new) 
									(setq nrec (ade_odrecordqty enty "utcanev"))
									(ade_odsetfield enty "utcanev" "nev" (- nrec 1) txt)
									(command "_chprop" enty "" "_c" 5 "")
									(setq k 0)
									(if (member "utcanev" (ade_odgettables entx))
									(repeat (ade_odrecordqty entx "utcanev")
										(setq lista (cons (ade_odgetfield entx "utcanev" "nev" k) lista))
										(setq k (1+ k))
									)
									)
									(if (not (member txt lista))
										(progn
											(setq new (ade_odnewrecord "utcanev"))
											(ade_odattachrecord entx new) 
											(setq nrec (ade_odrecordqty entx "utcanev"))
											(ade_odsetfield entx "utcanev" "nev" (- nrec 1) txt)
											;(command "_chprop" entx "" "_c" 5 "")
										)
									)
							

								)
							)
							(setq i (1+ i))
							
						)
							
						

					)
					(command "_chprop" entx "" "_c" 3 "")
				)
				
				
				(setq j (1+ j))
				(princ (strcat "\rUtcanv plethez rendelse: " (itoa j) "/" (itoa hlof)))
				))
			)
			(princ (strcat "\nKsz \n" (itoa i)))
		)
		
	)
	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
)

(defun groupby2 (lst / grp)
        (while (< 1 (length lst))
            (Setq grp (cons (list
                                (car lst)
                                (cadr lst)
                            )
                            grp
                      )
            )
            (setq lst (cddr lst))
        )
        (vl-remove 'nil (reverse (cons lst grp)))
)
(defun clockwise-p (p1 p2 p3)
        (< (sin (- (angle p1 p3) (angle p1 p2))) -1e-14)
)
(defun c:kellhaz ( ) ;
	(setvar "CMDECHO" 0)
	(command "_view" "_save" "temp")
	(command "._undo" "_begin")
 	(command "_osnap" "_none")
	(setvar "attdia" 0)
	(command "_zoom" "_e")
	(princ "\n")
	(setq lof (ssget "_x" (list (cons 0 "LWPOLYLINE") (cons 8 "felesleges_epulet"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(command "_zoom" "_o" "_si" entx "_redraw")
				(setq pl (ssname lof j))
				(make-vert-list pl)
				(setq pts vert-list)
				
				
				;(setq lef (ssget "_cp" pts (list (cons 0 "LWPOLYLINE") (cons 62 97))))
				(setq lef (ssget "_cp" pts (list (cons 0 "LWPOLYLINE") (cons 8 "buffer"))))
				;(setq lif (ssget "_cp" pts (list (cons 0 "INSERT"))))
				(if (or lef lif) (command "_chprop" entx "" "_c" 1 ""))
				
				
				(setq j (1+ j))
				
				(princ (strcat "\rplet szrs: " (itoa j) "/" (itoa hlof)))
				
			)
		)
	)
				
	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")
)


(defun c:txtblokk ( )
	(setvar "CMDECHO" 0)
	
	(command "_view" "_save" "temp")
 	(command "_osnap" "_none")
	(command "._undo" "_begin")
	(setq lof (ssget "_x" (list (cons 0 "TEXT") (cons 1 "e"))))
	(if lof
		(progn
			 
			(setq hlof (sslength lof))
			(setq j 0)
			(while (< j hlof)
				(setq entx (ssname lof j))
				(setq tlist (entget entx))
				;(setq obj (vlax-ename->vla-object entx))
				(setq p (cdr (assoc 10 tlist)))
				(setq s (cdr (assoc 50 tlist)))
				(setq p (polar p (+ s (/ pi 2)) 3))
				(setq szog (/ (* 180.0 s) pi))
				(command "_-insert" "EVSYMB_4" p "1" "1" (+ 90 szog))
				(entdel entx)
				(setq j (1+ j))
				(princ (strcat "\n " (itoa j) "/" (itoa hlof)))
			)
			(princ "\nKsz")
		)
		
	)
	(princ "\n")
	(command "._undo" "_end")
	(command "_view" "_restore" "temp")	
	(command "_regen")
	(command "_osnap" "_end,_int,_nea,_ext,_ins,_nod")
	(command "_cmdecho" "1")

	

)
(defun make-vert-list (entli)
  (setq ent-pl (entget entli))
  (setq ent-name (cdr (assoc 0 ent-pl)))
  (if (= ent-name "LWPOLYLINE")
    (progn
      (setq v-list nil)
      (foreach vert ent-pl
	(if (= 10 (car vert))
	  (setq v-list (cons (cdr vert) v-list))
	)
      )
    )
  ) ;if progn
  (setq vert-list v-list)
) ;make-vert-list

(defun rnd (/ modulus multiplier increment random)
  (if (not seed)
    (setq seed (getvar "DATE"))
  )
  (setq modulus    65536
        multiplier 25173
        increment  13849
        seed  (rem (+ (* multiplier seed) increment) modulus)
        random     (/ seed modulus)
  )
)
