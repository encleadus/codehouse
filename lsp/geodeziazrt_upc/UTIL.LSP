;(C) L. Angyal 1993
;************
;*** UTIL ***
;************

;********************
;*** input/output ***
;********************

(defun read-word (input / c str return)
;
; reads words from a file
; [word: string of characters separated by space, tab or newline]
; *** only for ascii files !
;
; input:	file descriptor
; output:	string read
;
 (setq str "")
 (setq c (read-char input))
 (while (iswhite c)
  (setq c (read-char input))
 );while
 (if (= c nil)
     (setq return nil)
     (progn
      (while (and (not (iswhite c)) (not (= c nil)) )
       (setq str (strcat str (chr c)) )
       (setq c (read-char input))
      );while
      (setq return str)
     );progn
 );if
);defun read-word

(defun iswhite (c / ch return)
;
; input:	character
; output:	T if c= " ", "\t" or "\n"
;		nil otherwise
;
 (if (= c nil)
     (setq return nil)
     (progn
      (setq ch (chr c))
      (if (or (= ch " ") (= ch "\t") (= ch "\n"))
	  (setq return T)
	  (setq return nil)
      );if
     );progn
 );if
);defun iswhite

(defun prinsp (var file)
 (princ var file)
 (princ " " file)
)

(defun nlprinsp (var file)
 (princ "\n" file)
 (princ var file)
 (princ " " file)
)

(defun exist (fn / ret)
;
; input:	file descriptor
; output:	T if the file can be opened
;		nil otherwise
;
 (if (setq ret (open fn "r"))
  (progn
   (close ret)
   (setq ret T)
  );progn
  (setq ret nil)
 );if
);defun exist

;*************
;*** block ***
;*************

(defun c:bl () (blocklist))
(defun blocklist (/ first i MAXLINE bl)
;
; input:	none
; output:	list of blocks
;
 (textscr)
 (setq first T i 0 MAXLINE 20)
 (while (setq bl (tblnext "block" first))
  (if first (setq first nil) )
  (prompt (strcat "\n" (getval 2 bl)))
  (setq i (1+ i))
  (if (= (rem i MAXLINE) 0) (stop) );if
 );while
 (prompt (strcat "\n\n" (itoa i) " block[s] found") )
 (prin1)
);defun blocklist

(defun c:bi () (blins) )
(defun c:blins () (blins) )

(defun blins (/ first bl i MAXLINE MAXITEM)
;
; inserts blocks
;
; input:	none
; output:	insertion
;
;*** save & set AutoCAD parameters ***
 (setq olderr  *error*
       *error* std_err)
 (setq ocmd (getvar "cmdecho"))
 (setq oblip (getvar "blipmode"))
 (setvar "cmdecho" 0)
 (setvar "blipmode" 0)

;*** main program ***
 (textscr)
 (setq first T i 0 MAXLINE 20)
 (while (setq bl (tblnext "block" first))
  (if first (setq first nil) )
  (setq i (1+ i))
  (prompt (strcat "\n" (itoa i) ": " (getval 2 bl)))
  (if (= (rem i MAXLINE) 0)
      (getstring "\npress any key...")
  );if
 );while
 (if (= first T)
  (prompt "\nempty block list")
  (progn
   (setq MAXITEM i)
   (initget (+ 1 2 4))
   (while (> (setq i (getint "\nblock number: ")) MAXITEM)
    (initget (+ 1 2 4))
   );while
   (setq bl (getval 2 (tblnext "block" T)))
   (if (/= i 1)
    (repeat (1- i)
     (setq bl (getval 2 (tblnext "block")))
    );repeat
   );if
   (command "insert" bl)
  );progn
 );if

;*** restore AutoCAD parameters ***
 (setvar "cmdecho" ocmd)            ; restore saved mode
 (setvar "blipmode" oblip)
 (setq *error* olderr)		    ; restore old *error* handler
 (prin1)
);defun blins

(defun bblock (bln sset)
;
; makes a block from a selection set
; *** overwrites old blocks having the same name !
;
; input:	a block name and a selection set
; output:	none
;
 (if (not (bln_ok bln)) (setq bln (bln_prep bln)) )
 (if sset
  (if (tblsearch "block" bln)
   (command "block" bln "y" "0,0" sset "")
   (command "block" bln "0,0" sset "")
  );if
 );if
 (prin1)
);defun bblock

(defun bln_ok (bln / c i ok return)
;
; input:	block name
; output:	T if the block name is correct
;		nil otherwise
 (setq return nil)
 (if (= (type bln) 'STR)
  (if (<= (strlen bln) 31)
   (progn
    (setq i 0 ok 0)
    (repeat (strlen bln)
     (setq i (1+ i) c (substr bln i 1) )
     (if (or (= c "$")
	     (= c "-")
	     (= c "_")
	     (and (>= c "0") (<= c "9"))
	     (and (>= c "A") (<= c "Z"))
	     (and (>= c "a") (<= c "z"))
	 );or
	 (setq ok (1+ ok))
     );if
    );repeat
    (if (= i ok) (setq return bln) )
   );progn
  );if
 );if
);defun bln_ok

(defun bln_prep (bln / i c newbln)
;
; changes forbidden characters to "_"
; truncates the string if it was longer than 31
;
; input:	block name
; output:	block name
;
 (setq newbln "")
 (if (= (type bln) 'STR)
  (progn
   (setq i 0)
   (repeat (strlen bln)
    (setq i (1+ i) c (substr bln i 1) )
    (if (or (= c "$")
	    (= c "-")
	    (= c "_")
	    (and (>= c "0") (<= c "9"))
	    (and (>= c "A") (<= c "Z"))
	    (and (>= c "a") (<= c "z"))
	);or
	(setq newbln (strcat newbln c))
	(setq newbln (strcat newbln "_"))
    );if
   );repeat
  );progn
 );if
 (setq newbln (substr newbln 1 31))
);defun bln_prep

(defun tmp_block (/ i tmp ret)
;
; creates an temporary block
;
; input:	none
; output:	a unique block name
;
 (setq i 0)
 (setq tmp (strcat "_tmp_block_" (rtos (getvar "date") 2 0) "_" (itoa i)))
 (while (tblsearch "block" tmp)
  (setq i (1+ i))
  (setq tmp (strcat "_tmp_block_" (itoa i)))
 );while
 (if (> (strlen tmp) 31)
  (setq ret nil)
  (setq ret tmp)
 );if
);defun tmp_block

;***********************************
;*** entities and selection sets ***
;***********************************

(defun getval (n e) (cdr (assoc n e)))

(defun ssub (ss s / i ssn ret)
;
; input:	two selection sets [ss and s]
; output:	selection set created by subtracting s from ss [ss-s]
;
 (setq i 0)
 (if (and ss s)
  (progn
   (repeat (sslength s)
    (setq ssn (ssname s i))
    (if (ssmemb ssn ss)
     (ssdel ssn ss)
    );if
    (setq i (1+ i))
   );repeat
  );progn
 );if
 (setq ret ss)
);defun ssub

(defun selcw (p1 p2 / ssetc ssetw ret)
;
; input:	two points specifying a window
; output:	a selection set containing elements crossing
;		the borders of the window
;
 (setq ssetc (ssget "c" p1 p2)
       ssetw (ssget "w" p1 p2)
       ret   (ssub ssetc ssetw)
 );setq
);defun selcw

(defun viewset (sset / i en ed)
;
; input:	selection set
; output:	lists the elements of the set
;
 (prompt "\nselection set: ") (princ sset)
 (setq i 0)
 (repeat (sslength sset)
  (setq en (ssname sset i))
  (setq ed (entget en))
  (princ (strcat "\nentity " (itoa i) ":\n"))
  (princ ed)
  (stop)
  (setq i (1+ i))
 );repeat
);defun viewset

(defun c:ev ()
;
; input:	selection set
; output:	data of entities in the set
;
 (if (setq sset (ssget))
 (progn
  (textscr)
  (setq i 0)
  (repeat (sslength sset)
   (setq en (ssname sset i) ed (entget en))
   (pr "data" ed) (stop)
   (while (/= (getval 0 (entget (entnext en))) "SEQEND")
    (setq en (entnext en) ed (entget en))
    (pr "data" ed) (stop)
   );while
   (setq en (entnext en) ed (entget en))
   (pr "data" ed) (stop)
   (setq i (1+ i))
  );repeat
 );progn
 );if
 (prin1)
);defun c:ev

(defun vertex_list ( en / ed vertex ret )
;
; input:	entity name
; output:	list of vertices if the entity was a polyline
;		nil otherwise
;
 (setq ed (entget en) vertex nil)


 (SETQ AKTENT (GETVAL 0 ED)) 
 (setq zarte (getval 70 ed))
; (PRINT AKTENT)
(if (= AKTENT "POLYLINE")
  (progn
  (while (/= (getval 0 (entget (entnext en))) "SEQEND")
   (setq en (entnext en)
	 ed (entget en)
   );setq
   (setq vertex (cons (getval 10 ed) vertex) )
  );while
   (if (or (= zarte 1) (= zarte 129))
    (progn
 ;    (print "\nZart poligon")
     (setq vertex (cons (last vertex) vertex))
    ) 
   )
  );
 );if
; (PRINT 'KESZPOLY)
 (if (= AKTENT "LINE")
 (PROGN
 ;(PRINT 'EZLINE)
 (setq vertex (cons (getval 10 ed) vertex))
 ;(PRINT 'KEZDOP)
 (setq vertex (cons (getval 11 ed) vertex))
 ;(PRINT VERTEX)
 ;(PRINT 'VEGP)
 ) ; PROGN
 ); line
; (PRINT 'LINEVEG)
 (if vertex (setq ret (reverse vertex)) )
);defun vertex_list

;**************
;*** others ***
;**************

(defun std_err (s)
;
; standard error handler
; [cmdecho and blipmode = 0]
;
 (if (/= s "Function cancelled")    ; if an error (such as CTRL-C) occurs
     (princ (strcat "\nError: " s)) ; while this command is active...
 );if
 (setvar "cmdecho" ocmd)            ; restore saved mode
 (setvar "blipmode" oblip)
 (setq *error* olderr)		    ; restore old *error* handler
 (prin1)
);defun std_err

(defun showit (s v) (prompt (strcat "\n"  s "= "))(princ v))
(defun pr (st va) (prompt (strcat "\n" st "= "))  (princ va))

(defun stop () (getstring "\npress any key..."))

(defun _yesno (str / ret)
;
; input:	string
; output:	T if the answer is "y" or "yes"
;		nil if the answer is "n" or "no"
;		[not case sensitive]
;
 (initget 1 "Yes No")
 (setq ret (getkword str))
 (if (= ret "Yes")
     (setq ret T)
     (setq ret nil)
 );if
);defun yesno

(defun dosshell ()
 (command "shell" "")
 (prin1)
)

(defun neg (n) (setq n (- 0 n)))

(defun rectangle (p1 p2 / x1 y1 x2 y2)
 (setq x1 (car p1) y1 (cadr p1))
 (setq x2 (car p2) y2 (cadr p2))
 (command "pline" (list x1 y1) (list x2 y1) (list x2 y2) (list x1 y2) "c")
);defun rectangle

(defun vertex_list1 ( en / ed vertex ret amag)
;
; input:	entity name
; output:	list of vertices if the entity was a polyline
;		nil otherwise
;
 (setq ed (entget en) vertex nil)

 (SETQ AKTENT (GETVAL 0 ED)) 
; (PRINT AKTENT)
(if (= AKTENT "LWPOLYLINE")
   (progn
   (setq ii 0)
 (setq zarte (getval 70 ed))
 (setq amag (getval 38 ed))
   (repeat (length ed)
   (setq akt (nth ii ed))
   (if (= (car akt) 10)
    (progn
     (setq xy (list (nth 0 (cdr akt)) (nth 1 (cdr akt)) amag))
     (setq vertex (cons xy vertex) )
    )
   )
   (setq ii (1+ ii))
  );repeat
   (if (or (= zarte 1) (= zarte 129))
    (progn
 ;    (print "\nZart poligon")
     (setq vertex (cons (last vertex) vertex))
    ) 
 )
 );progn
 );if
; (PRINT 'KESZPOLY)
 (if (= AKTENT "LINE")
 (PROGN
 ;(PRINT 'EZLINE)
 (setq vertex (cons (getval 10 ed) vertex))
 ;(PRINT 'KEZDOP)
 (setq vertex (cons (getval 11 ed) vertex))
 ;(PRINT VERTEX)
 ;(PRINT 'VEGP)
 ) ; PROGN
 ); line
; (PRINT 'LINEVEG)
 (if vertex (setq ret (reverse vertex)) )
)

(prin1)
